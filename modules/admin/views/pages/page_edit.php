<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title                   = 'Страница';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-admin">

    <?php

    $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal', 'role' => 'form'],
        //'action' => '/admin/setting/'.$model->id,
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-sm-11\">{input}</div>\n<div class=\"col-sm-1\"></div><div class=\"col-sm-11\">{error}</div>",
            'labelOptions' => ['class' => 'col-sm-1 control-label'],
        ],
    ]);

    echo $form->field($model, 'title');
    echo $form->field($model, 'alias');
    echo $form->field($model, 'linkTitle');
    echo $form->field($model, 'text')->textArea(['rows' => '12']);
    echo $form->field($model, 'weight');

    echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']);

    ActiveForm::end();
    ?>


</div>