<?php

/**
 * @var $this \yii\web\View
 * @var $monitoring SnifferMonitoringParameters
 */

use app\models\SnifferMonitoringParameters;
use app\models\SnifferServers;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin();

$list = [];
$servers = SnifferServers::find()
	->orderBy([
		'parent' => SORT_ASC,
		'type' => SORT_ASC,
		'id' => SORT_ASC
	])
	->all();

/** @var SnifferServers $server */
foreach ($servers as $server) {
	$list[$server->id] = $server->group->name . ' - ' . $server->name . ' (' . long2ip($server->ip) . ')';
}

echo $form->field($monitoring, 'server_id')->dropDownList($list);
echo $form->field($monitoring, 'user_id')->label(false)->hiddenInput();


?>

<div class="row">
	<div class="col-md-4">
		<?= $form->field($monitoring, 'parameter')->dropDownList(SnifferMonitoringParameters::getParametersList()) ?>
	</div>
	<div class="col-md-4">
		<?= $form->field($monitoring, 'operator')->dropDownList(SnifferMonitoringParameters::getOperatorDescriptions()) ?>
	</div>
	<div class="col-md-4">
		<?= $form->field($monitoring, 'value') ?>
	</div>
</div>

<?php

$notifications = SnifferMonitoringParameters::getNotificationsList();

foreach ($notifications as $notification => $label) {
	echo $form->field($monitoring, $notification)->checkbox();
}

?>

<?php

echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']);

ActiveForm::end();