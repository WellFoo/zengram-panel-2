<?php

namespace app\controllers;

use alexandernst\devicedetect\DeviceDetect;
use app\components\RPCHelper;
use app\components\UHelper;
use app\models\Account;
use app\models\AccountActionLog;
use app\models\AccountProcessLog;
use app\models\AccountStartPlaceStats;
use app\models\ActionsLog;
use app\models\ContactForm;
use app\models\LoginForm;
use app\models\Mail;
use app\models\Options;
use app\models\Post;
use app\models\Prices;
use app\models\RegisterForm;
use app\models\ShpionBalance;
use app\models\SupportRequest;
use app\models\Users;
use app\components\Controller;
use app\components\Counters;
use app\forms\UserPasswordChangeForm;
use app\models\UsersLog;
use app\models\UserTokens;
use app\models\UsersInfo;
use app\models\Whitelist;
use core\logger\Logger;
use Freshdesk\Config\Connection;
use Freshdesk\Ticket;
use Yii;
use yii\db\Expression;
use yii\db\oci\QueryBuilder;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\ConflictHttpException;
use yii\web\Cookie;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

class SiteController extends Controller
{
	private $client = false;
	private $js_lang_file;

	public function init()
	{
		parent::init();

		$this->js_lang_file = Yii::getAlias('@app/messages/' . Yii::$app->language . '/js.php');
	}

	public function behaviors()
	{
		$js_lang_file = $this->js_lang_file;
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only'  => ['logout'],
				'rules' => [
					[
						'actions' => ['logout'],
						'allow'   => true,
						'roles'   => ['@'],
					],
				],
			],
			'verbs'  => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'logout' => ['post', 'get'],
				],
			],
			[
				'class'        => 'yii\filters\HttpCache',
				'only'         => ['language-js'],
				'lastModified' => function ($action, $params) use ($js_lang_file) {
					return filectime($this->js_lang_file);
				},
			]
		];
	}

	public function actionTestInterval() {
		$end = date('2016-10-18');
		$start = date('Y-m-d', (strtotime($end) - 86400));

		var_dump($start, $end);

		$result = AccountActionLog::getActionsTime(21797, $start, $end);

		var_dump($result);
	}

	public function actionTest()
	{
		$total_price = 0;
		$total2_users = 0;
		for ($left = 1; $left < 7; $left++) {
			unset($accounts, $groups, $users);

			$accounts = (new \yii\db\Query())
				->select([
					/* Инфо аккаунта */
					new Expression('distinct accounts.instagram_id'), 'accounts.account_avatar', 'accounts.login', 'accounts.user_id', 'account_stats.date',

					/* Текущие показатели */
					'accounts.account_media', 'accounts.account_follows', 'accounts.account_followers',

					/* Инфо пользователя */
					'users.mail', 'users.reg_ip',

					/* Стата */
					'account_stats.came', 'account_stats.gone',
				])
				->from('accounts')
				->join('LEFT JOIN', 'users', 'users.id = accounts.user_id')
				->join('LEFT JOIN', 'account_stats', "account_stats.account_id = accounts.id")
				->where(['accounts.monitoring_status' => 'work'])
				->andWhere("account_stats.date >= current_date - '".$left."days'::interval and account_stats.date <= current_date - '".($left-1) ."days'::interval")
				->andWhere('account_stats.came > 0 or account_stats.gone > 0')
				->andWhere('accounts.user_id != 54')
				->all();

			$groups = [];
			$users = [];

			foreach ($accounts as $account) {
				$account['reg_ip'] = long2ip($account['reg_ip']);
				$users[$account['user_id']][] = $account;
			}

			foreach ($users as $user_id => $accounts) {
				$groups[count($accounts)][$user_id] = $accounts;
			}

			$total_users = 0;
			foreach ($groups as $accounts_counts => $users) {
				$total_users += count($users);
				print count($users) . 'пользователей по ' . $accounts_counts . ' аккаунтов <br /><br />';
			}

			$price = (0.005 * $total_users);
			$total_price += $price;
			$total2_users += $total_users;

			print 'Всего было бы отправлено писем для всех рассылок за ' . $accounts[0]['date'] . ': <b>' . $total_users . '</b>. Цена: '. $price.'y.e. <hr />';
		}

		print 'Всего за неделю <b>' . $total2_users . '</b> писем. Цена: '. $total_price.'y.e. <hr />';
	}

	public function actions()
	{
		return [
			'error'   => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class'           => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function actionIndex()
	{
		$ref_key = Yii::$app->request->get('p', null);
		if (!is_null($ref_key)) {
			Yii::$app->response->cookies->add(new Cookie([
				'name' => 'ref_key',
				'value' => $ref_key
			]));
		}

		if (strpos(Yii::$app->request->url, '/site') !== false) {
			Yii::$app->response->redirect(['/'], 301);
		}

		if (Yii::$app->user->isGuest || NO_DB_CONNECTION) {
			if (Yii::$app->request->referrer
				&& strpos(Yii::$app->request->referrer, Yii::$app->params['host']) === false
				&& !Yii::$app->session->get('refferer', false)
			) {
				Yii::$app->session->set('refferer', Yii::$app->request->referrer);
			}
			$stataCounters = require('../config/ru/statisticForLanding.php');

			// Сохраняем данные об источнике перехода
			Counters::gaFirstSource();

			return $this->render(
				'home', [
//				'model' => $model,
				'stataCounters' => $stataCounters
			]);
		} else {
			$user_id = Yii::$app->user->id;
			if (!empty(Yii::$app->request->queryParams['preview'])) {
				if (Yii::$app->user->identity->isAdmin) {
					$user_id = (int)Yii::$app->request->queryParams['preview'];
				}
			}

			/** @var UsersInfo $modelInfo */
			$modelInfo = UsersInfo::findOne(['user_id' => Yii::$app->user->id]);
			if ($modelInfo === null) {
				if (Yii::$app->user->id) {
					$modelInfo = new UsersInfo([
						'user_id' => Yii::$app->user->id,
						'country' => Users::locationInfo('country'),
						'service' => LANGUAGE
					]);
					$modelInfo->save();
				}
			} else {
				if (!$modelInfo->country) {
					$modelInfo->country = Users::locationInfo('country');
					$modelInfo->save();
				}
			}

			if (!$modelInfo->refferer) {
				$this->setUserReferrer($modelInfo);
			}

			if (LANGUAGE === 'rf') {
				$token = UserTokens::getToken(Yii::$app->user->id);
				return $this->redirect('https://zengram.ru/?access_key=' . $token);
			}

			$accounts = Account::find()
				->with(['options', 'user'])
				->where(['user_id' => $user_id])
				->orderBy('id')
				->all();

			if (Yii::$app->user->id === $user_id) {
				$user = Yii::$app->user->identity;
			} else {
				$user = Users::findIdentity($user_id);
			}

			if (count($accounts) && $user->demo) {
				$user->demo = 0;
				$user->save(false, ['demo']);
			}

			$prices = Prices::find()->limit(3)->all();


			return $this->render('//dashboard/index', [
				'accounts' => $accounts,
				'user' => $user,
				'prices' => $prices
			]);
		}
	}

	/**
	 * @param UsersInfo $userInfo
	 */
	private function setUserReferrer(&$userInfo)
	{
		if (Yii::$app->session->get('refferer', false)) {
			$userInfo->refferer = Yii::$app->session->get('refferer', '');
			$userInfo->save();
			return;
		}

		if (Yii::$app->request->referrer && strpos(Yii::$app->request->referrer, Yii::$app->params['host']) === false) {
			$userInfo->refferer = Yii::$app->request->referrer;
			$userInfo->save();
			return;
		}
	}

	public function actionErrors()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;
		//actions deleted
		$actionLogs = ActionsLog::find()->where(['and', ['user_id' => Yii::$app->user->id, 'deleted' => 1, 'showed' => 0]]);
		if ($actionLogs->count()){
			$sum = $actionLogs->sum('cost');
			if ($sum){
				Yii::$app->db->createCommand()->update('actions_log', ['showed' => 1], ['and', ['user_id' => Yii::$app->user->id, 'deleted' => 1, 'showed' => 0]])->execute();
				return ['error' =>
				        Yii::t('app', 'One or more of your accounts has removed post with recommendation of zengram. Bonus balance of {cost} was subbed. We are recommending to <a href="{url}">make another submition</a> for balance renewal',
					        ['cost' => Account::getStaticTimerText($sum), 'url' => Url::to(['/actions'])])];
			}
		}
		return [];
	}

	/**
	 * @param $id
	 * @return bool|string
	 */
	public function actionStart($id)
	{
		if (empty($id)) {
			return 'error';
		}

		if ($id != 'all') {
			$accounts = [Account::findOne($id)];
		} else {
			$accounts = Account::find()->where(['user_id' => Yii::$app->user->id])->with('options')->all();
		}
		/* @var $accounts \app\models\Account[] */
		if ($accounts) {

			/** @var UsersInfo $modelInfo */
			$modelInfo = UsersInfo::findOne(['user_id' => Yii::$app->user->id]);
			if ($modelInfo === null) {
				$modelInfo = new UsersInfo(['user_id' => Yii::$app->user->id]);
			}
			$modelInfo->try_start = 1;
			$modelInfo->save();
			$action_log_rows = [];
			foreach ($accounts as $account) {
//	            if ($account->isFollowsLimitExceeded()) {
//		            continue;
//	            }
				$options = $account->options;
				if ($options->likes) {
					$action_log_rows[] = ['account_id' => $account->id, 'action' => 'likes', 'operation' => 'start'];
				}
				if ($options->comment && !$account->comment_status) {
					$action_log_rows[] = ['account_id' => $account->id, 'action' => 'comment', 'operation' => 'start'];
				}
				if ($options->follow && !$account->follow_status) {
					$action_log_rows[] = ['account_id' => $account->id, 'action' => 'follow', 'operation' => 'start'];
				}
				if ($options->unfollow) {
					$action_log_rows[] = ['account_id' => $account->id, 'action' => 'unfollow', 'operation' => 'start'];
				}
				Yii::$app->db->createCommand()->batchInsert(
					AccountActionLog::tableName(),
					['account_id', 'action', 'operation'],
					$action_log_rows
				)->execute();

				//$result = $account->accountStart();
				// Надеюсь метод возвращает то, что нужно
				$result = RPCHelper::startAccount($account, 'user');

				AccountProcessLog::log([
					'worker_ip' => ip2long($account->server),
					'instagram_id' => $account->instagram_id,
					'user_id' => $account->user_id,
					'code' => Logger::Z011_ACCOUNT_START_BY_USER,
				]);

				if ($account->whitelist_added == Whitelist::WHITELIST_DELAY || $account->whitelist_added == Whitelist::WHITELIST_ERROR) {
					$account->whitelist_added = Whitelist::WHITELIST_EMPTY;
					$account->save();
				}

				$rows = [];
				if ($account->options->isSearchByRegions()) {
					$regions = $account->getRegionsID();
					foreach ($regions as $region) {
						$rows[] =[
							'account_id'   => $account->id,
							'user_id'      => $account->user_id,
							'instagram_id' => $account->instagram_id,
							'date'         => date('Y-m-d'),
							'type'         => 'region',
							'place_id'     => $region
						];
					}
					unset($regions);
				} else {
					$places = $account->places;
					foreach ($places as $place) {
						$rows[] = [
							'account_id'   => $account->id,
							'user_id'      => $account->user_id,
							'instagram_id' => $account->instagram_id,
							'date'         => date('Y-m-d'),
							'type'         => 'place',
							'type_id'     => $place->id
						];
					}
					unset($places);
				}
				if (!empty($rows)) {
					/** @var \Yii::$app->db BatchIgnoreConnection */
					\Yii::$app->db
						->createCommand()
						->batchInsertIgnore(AccountStartPlaceStats::tableName(), [
							'account_id',
							'user_id',
							'instagram_id',
							'date',
							'type',
							'type_id'
						], $rows)
						
						->execute();
				}
				if ($id != 'all' && is_string($result)) {

					return $result;
				}
			}
		}
		return 'ok';
	}

	public function actionUnsubscribe()
	{
		if (Yii::$app->user->isGuest) {
			return $this->redirect('/');
		}

		$user_id = Yii::$app->user->id;
		$user = \app\models\Users::findOne($user_id);
		
		if (Yii::$app->request->isPost) {
			$unsubscribe = (Yii::$app->request->post('stats') ? true : false);

			if ($unsubscribe) {
				$user->subscribe_stats = false;
			} else {
				$user->subscribe_stats = true;

				if ((Yii::$app->request->post('every_week'))) {
					$user->delivery_stats_type = 2;
				} else {
					$user->delivery_stats_type = 1;
				}
			}
			$user->save();
		}
		
		return $this->render('unsubscribe', compact('user'));
	}

	public function actionBalance()
	{
		$user_id = Yii::$app->user->id;
		if (!empty(Yii::$app->request->queryParams['preview'])) {
			if (Yii::$app->user->identity->isAdmin) {
				$user_id = Yii::$app->request->queryParams['preview'];
			}
		}
		$userBalance = Users::getUserBalance($user_id);

		$balance['d'] = '00';
		$balance['H'] = '00';
		$balance['i'] = '00';

		if ($userBalance > 0) {
			if ($userBalance > 86400) {
				$balance['d'] = sprintf("%02d", floor($userBalance / 86400));
			}

			if ($userBalance > 3600) {
				$countBalance = $userBalance - (floor($userBalance / 86400) * 86400);
				$balance['H'] = sprintf("%02d", floor($countBalance / 3600));
			}

			$balance['i'] = date("i", $userBalance);
		}
		return json_encode($balance);
	}

	public function actionStop($id)
	{
		if (empty($id)) {
			return 'error';
		}

		if ($id != 'all') {
			$accounts = [$account = Account::find()->where('id = ' . $id)->one()];
		} else {
			$accounts = Account::find()->where(['user_id' => Yii::$app->user->id])->all();
		}
		$result = true;
		if ($accounts) {
			$action_log_rows = [];
			/* @var $accounts \app\models\Account[] */
			foreach ($accounts as $account) {

				$options = $account->options;
				if ($options->likes) {
					$action_log_rows[] = ['account_id' => $account->id, 'action' => 'likes', 'operation' => 'stop'];
				}
				if ($options->comment && !$account->comment_status) {
					$action_log_rows[] = ['account_id' => $account->id, 'action' => 'comment', 'operation' => 'stop'];
				}
				if ($options->follow && !$account->follow_status) {
					$action_log_rows[] = ['account_id' => $account->id, 'action' => 'follow', 'operation' => 'stop'];
				}
				if ($options->unfollow) {
					$action_log_rows[] = ['account_id' => $account->id, 'action' => 'unfollow', 'operation' => 'stop'];
				}
				//$result = $account->accountStop() && $result;
				$result = RPCHelper::stopAccount($account, 'user') && $result;

				AccountProcessLog::log([
					'worker_ip' => ip2long($account->server),
					'instagram_id' => $account->instagram_id,
					'user_id' => $account->user_id,
					'code' => Logger::Z012_ACCOUNT_STOP_BY_USER,
				]);
			}

			Yii::$app->db->createCommand()->batchInsert(
				AccountActionLog::tableName(),
				['account_id', 'action', 'operation'],
				$action_log_rows
			)->execute();
		}
		if ($result) {
			return 'ok';
		}
		return '';
	}

	public function actionReset($id)
	{
		if (empty($id)) {
			return 'error';
		}

		/* @var $account \app\models\Account */
		$account = Account::findOne($id);
		if (is_null($account)) {
			throw new NotFoundHttpException();
		}

		$message = 'Сброшен аккаунт ' . $account->login
			. '.Лайки: ' . $account->likes . '.Комменты: ' . $account->comments . '.Подписки: ' . $account->follow . '.Отписки: ' .
			$account->unfollow;

		$logEntry = new UsersLog(['user_id' => Yii::$app->user->id, 'text' => $message]);
		$logEntry->save();

		AccountProcessLog::log([
			'worker_ip' => ip2long($account->server),
			'instagram_id' => $account->instagram_id,
			'user_id' => $account->user_id,
			'code' => Logger::Z010_ACCOUNT_STAT_RESET,
			'description' => $message,
		]);

		$account->likes = 0;
		$account->comments = 0;
		$account->follow = 0;
		$account->unfollow = 0;
		if ($account->save()) {
			return 'ok';
		}

		return 'error';
	}

	public function actionTimer($id)
	{
		if (empty($id)) {
			return 'error';
		}

		$time = (int) $_POST['timer'];

		/* @var $account \app\models\Account */
		$account = Account::find()->where('id = ' . $id)->one();
		$account->timer = $time;
		if ($account->save()) {
			return 'ok';
		}
		return 'error';
	}

	public function actionSpeed($id)
	{
		if (empty($id)) {
			return 'id error';
		}

		if (!isset($_POST['speed'])) {
			return 'speed value error';
		}

		/* @var $account \app\models\Account */
		$options = Options::findByAccountId($id);
		//$options->scenario = 'speedUpdate';
		$options->speed = (int)$_POST['speed'];
		$options->skipRelations = true;
		if ($options->save()) {
			return 'ok';
		} else {
			return 'error';
		}
	}

//	public function actionOptions($id)
//	{
//		Yii::$app->response->format = Response::FORMAT_JSON;
//
//		/** @var Account $account */
//		$account = Account::findOne(['id' => $id]);
//		if ($account === null) {
//			throw new BadRequestHttpException();
//		}
//		$option = Yii::$app->request->post('option', '');
//		$allowed_options = [
//			'likes', 'follow', 'comment', 'unfollow', 'likeFollowers',
//			'likeMutuals', 'mutual', 'autounfollow'
//		];
//		if (!in_array($option, $allowed_options)) {
//			throw new BadRequestHttpException();
//		}
//
//		$value = intval(Yii::$app->request->post('value', -1));
//		if ($value < 0) {
//			throw new BadRequestHttpException();
//		}
//
//		$options = Options::findByAccountId($id, !Yii::$app->user->identity->isAdmin);
//		/** @var AccountActionLog[] $action_log_rows */
//		$action_log_rows = [];
//		if ($options->$option != $value) {
//			if (($option == 'follow' && !$account->follow_status) || ($option == 'comment' && !$account->comment_status) || $option == 'likes') {
//				$action_log_rows[] = ['account_id' => $id, 'action' => $option, 'operation' => $value ? 'start' : 'stop'];
//			}
//		}
//
//		$options->$option = $value;
//		if ($option == 'unfollow') {
//			if ($value) {
//				// Если включена опция параллельных действий
//				if (Yii::$app->user->identity->unfollow_mode_multiactions) {
//					if ($options->follow) {
//						$action_log_rows[] = ['account_id' => $id, 'action' => 'follow', 'operation' => 'stop'];
//					}
//					//$options->setPrevOptions();
//					$options->follow = 0;
//				} else {
//					if ($options->likes) {
//						$action_log_rows[] = ['account_id' => $id, 'action' => 'likes', 'operation' => 'stop'];
//					}
//					if ($options->follow) {
//						$action_log_rows[] = ['account_id' => $id, 'action' => 'follow', 'operation' => 'stop'];
//					}
//					if ($options->comment) {
//						$action_log_rows[] = ['account_id' => $id, 'action' => 'comment', 'operation' => 'stop'];
//					}
//					$options->setPrevOptions();
//					$options->likes = 0;
//					$options->follow = 0;
//					$options->comment = 0;
//				}
//			} else {
//				if (!Yii::$app->user->identity->unfollow_mode_multiactions){
//					$options->unfollow = 0;
//				}
//				if ($options->prev_mask & Options::OPT_LIKES) {
//					$options->likes = 1;
//					$action_log_rows[] =['account_id' => $id, 'action' => 'likes', 'operation' => 'start'];
//				}
//				if ($options->prev_mask & Options::OPT_FOLLOW) {
//					$options->follow = 1;
//					if (!$account->follow_status) {
//						$action_log_rows[] = ['account_id' => $id, 'action' => 'follow', 'operation' => 'start'];
//					}
//				}
//				if ($options->prev_mask & Options::OPT_COMMENT) {
//					$options->comment = 1;
//					if (!$account->comment_status) {
//						$action_log_rows[] = ['account_id' => $id, 'action' => 'comment', 'operation' => 'start'];
//					}
//				}
//				$options->prev_mask = Options::OPT_UNFOLLOW;
//			}
//		} elseif (in_array($option, ['likes', 'follow', 'comment'])) {
//			if (!Yii::$app->user->identity->unfollow_mode_multiactions){
//				if ($options->unfollow){
//					$action_log_rows[] = ['account_id' => $id, 'action' => 'unfollow', 'operation' => 'stop'];
//				}
//				$options->unfollow = 0;
//			}
//		}
//
//		if (!($options->likes || $options->comment || $options->follow)) {
//			$options->unfollow = 1;
//		} elseif ($options->unfollow === 1) {
//			$options->unfollow = 0;
//		}
//
//		if ($account->is_paused && $account->getStatus()) {
//			$account->is_paused = 0;
//			$account->pause_until = 0;
//			$account->save();
//			RPCHelper::stopAccount($account, 'changeOptions');
//			RPCHelper::startAccount($account, 'changeOptions');
//		}
//
//		if (!$options->save()) {
//			return [
//				'status' => 'error'
//			];
//		}
//
//		if ($account->getStatus() && count($action_log_rows)) {
//			Yii::$app->db->createCommand()->batchInsert(
//				AccountActionLog::tableName(),
//				['account_id', 'action', 'operation'],
//				$action_log_rows
//			)->execute();
//		}
//		$account->use_trusted = 0;
//		$account->save();
//
//		$logEntry = new UsersLog(['user_id' => Yii::$app->user->id, 'text' => 'Изменена настройка ' . $option . ' => '.($value ? 'вкл' : 'выкл').' аккаунта ' .
//			$account->login]);
//		$logEntry->save();
//
//		if (Yii::$app->user->identity->unfollow_mode_multiactions){
//			return [
//				'status' => 'ok',
//				'data'   => [
//					'likes'         => [
//						'checked' => !!$options->likes
//					],
//					'follow'        => [
//						'checked' => !!$options->follow
//					],
//					'comment'       => [
//						'checked' => !!$options->comment
//					],
//					'unfollow'      => [
//						'checked' => !!$options->unfollow
//					],
//					'likeFollowers' => [
//						'checked'  => !!$options->likeFollowers,
//					],
//					'likeMutuals'   => [
//						'checked'  => !!$options->likeMutuals,
//					],
//					'mutual'        => [
//						'checked' => !!$options->mutual
//					],
//					'autounfollow'  => [
//						'checked' => !!$options->autounfollow
//					],
//				]
//			];
//		}
//
//		return [
//			'status' => 'ok',
//			'data'   => [
//				'likes'         => [
//					'checked' => !!$options->likes
//				],
//				'follow'        => [
//					'checked' => !!$options->follow
//				],
//				'comment'       => [
//					'checked' => !!$options->comment
//				],
//				'unfollow'      => [
//					'checked' => !!$options->unfollow
//				],
//				'likeFollowers' => [
//					'checked'  => !!$options->likeFollowers,
//					'disabled' => !!$options->unfollow
//				],
//				'likeMutuals'   => [
//					'checked'  => !!$options->likeMutuals,
//					'disabled' => !!$options->unfollow
//				],
//				'mutual'        => [
//					'checked' => !!$options->mutual
//				],
//				'autounfollow'  => [
//					'checked' => !!$options->autounfollow
//				],
//			]
//		];
//	}

	public function actionOptions($id)
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		if ($id === Account::DEMO_ID) {
			return [];
		}

		/** @var Account $account */
		$account = Account::findOne(['id' => $id]);
		if ($account === null) {
			throw new BadRequestHttpException();
		}
		$option = Yii::$app->request->post('option', '');
		$allowed_options = [
			'likes', 'follow', 'comment', 'unfollow', 'likeFollowers',
			'likeMutuals', 'mutual', 'autounfollow', 'use_geotags'
		];
		if (!in_array($option, $allowed_options)) {
			throw new BadRequestHttpException();
		}

		$value = intval(Yii::$app->request->post('value', -1));
		if ($value < 0) {
			throw new BadRequestHttpException();
		}

		$options = Options::findByAccountId($id, !Yii::$app->user->identity->isAdmin);
		/** @var AccountActionLog[] $action_log_rows */
		$action_log_rows = [];
		if ($options->$option != $value){
			if (($option == 'follow' && !$account->follow_status) || ($option == 'comment' && !$account->comment_status) || $option == 'likes') {
				$action_log_rows[] = ['account_id' => $id, 'action' => $option, 'operation' => $value ? 'start' : 'stop'];
			}
		}
		$options->$option = $value;
		if ($option == 'unfollow') {
			if ($value) {
				if ($options->likes){
					$action_log_rows[] = ['account_id' => $id, 'action' => 'likes', 'operation' => 'stop'];
				}
				if ($options->follow){
					$action_log_rows[] = ['account_id' => $id, 'action' => 'follow', 'operation' => 'stop'];
				}
				if ($options->comment){
					$action_log_rows[] = ['account_id' => $id, 'action' => 'comment', 'operation' => 'stop'];
				}
				$options->setPrevOptions();
				$options->likes = 0;
				$options->follow = 0;
				$options->comment = 0;
			} else {
				$options->unfollow = 0;
				if ($options->prev_mask & Options::OPT_LIKES) {
					$options->likes = 1;
					$action_log_rows[] =['account_id' => $id, 'action' => 'likes', 'operation' => 'start'];
				}
				if ($options->prev_mask & Options::OPT_FOLLOW) {
					$options->follow = 1;
					if (!$account->follow_status) {
						$action_log_rows[] = ['account_id' => $id, 'action' => 'follow', 'operation' => 'start'];
					}
				}
				if ($options->prev_mask & Options::OPT_COMMENT) {
					$options->comment = 1;
					if (!$account->comment_status) {
						$action_log_rows[] = ['account_id' => $id, 'action' => 'comment', 'operation' => 'start'];
					}
				}
				$options->prev_mask = Options::OPT_UNFOLLOW;
			}
		} elseif (in_array($option, ['likes', 'follow', 'comment'])) {
			if ($options->unfollow){
				$action_log_rows[] = ['account_id' => $id, 'action' => 'unfollow', 'operation' => 'stop'];
			}
			$options->unfollow = 0;
		}

		if (!$options->likes && !$options->follow && !$options->comment && !$options->unfollow) {
			$options->$option = 1;
		}

		if ($options->$option != $options->getOldAttribute($option)) {
			$logEntry = new UsersLog([
				'user_id' => Yii::$app->user->id,
				'text'    => 'Изменена настройка ' . $option . ' => '.($value ? 'вкл' : 'выкл').' аккаунта ' . $account->login
			]);
			$logEntry->save();

			AccountProcessLog::log([
				'worker_ip' => ip2long($account->server),
				'instagram_id' => $account->instagram_id,
				'user_id' => Yii::$app->user->id,
				'code' => ($value ? Logger::Z020_OPTION_ON : Logger::Z020_OPTION_OFF),
				'description' => $option,
			]);
		}

		if ($options->save()) {

			if ($account->is_paused && $account->getStatus()){
				$account->is_paused = 0;
				$account->pause_until = 0;
				$account->save();
				RPCHelper::stopAccount($account, 'changeOptions');
				RPCHelper::startAccount($account, 'changeOptions');
			}

			if ($account->getStatus() && count($action_log_rows)) {
				Yii::$app->db->createCommand()->batchInsert(
					AccountActionLog::tableName(),
					['account_id', 'action', 'operation'],
					$action_log_rows
				)->execute();
			}
			$account->use_trusted = 0;
			$account->save();

			return [
				'status' => 'ok',
				'data'   => [
					'likes'         => [
						'checked' => !!$options->likes
					],
					'follow'        => [
						'checked' => !!$options->follow
					],
					'comment'       => [
						'checked' => !!$options->comment
					],
					'unfollow'      => [
						'checked' => !!$options->unfollow
					],
					'likeFollowers' => [
						'checked'  => !!$options->likeFollowers,
						'disabled' => !!$options->unfollow
					],
					'likeMutuals'   => [
						'checked'  => !!$options->likeMutuals,
						'disabled' => !!$options->unfollow
					],
					'mutual'        => [
						'checked' => !!$options->mutual
					],
					'autounfollow'  => [
						'checked' => !!$options->autounfollow
					],
				]
			];
		}

		return [
			'status' => 'error'
		];
	}

	public function actionChangePassword($validate = false)
	{
		$model = new UserPasswordChangeForm();
		$model->load(Yii::$app->request->post());
		if ($validate) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}
		if ($model->validate() && $model->save()) {
			$logEntry = new UsersLog(['user_id' => Yii::$app->user->id, 'text' => 'Изменен пароль от пользователя']);
			$logEntry->save();
			return $this->redirect(['/', 'show' => 'changed']);
		} else {
			return $this->goBack();
		}
	}

	public function actionResetPassword()
	{
		/** @var Users $user */
		$user = Users::findByMail($_POST['email']);
		if ($user === null) {
			return Yii::t('app', 'There is no user with this email');
		}
		$newPassword = Yii::$app->security->generateRandomString(15);
		$user->password = md5($newPassword);
		if (!$user->save()) {
			return Yii::t('app', 'Password reset error');
		};
		$logEntry = new UsersLog(['user_id' => $user->id, 'text' => 'Сброшен пароль']);
		$logEntry->save();
		$token = new UserTokens();
		$token->generate($user->id);
		$token->save();
		$body = [
			Yii::t('mails', 'Hello.'),
			Yii::t('mails', 'Your account password was reseted'),
			Yii::t('mails', 'E-mail: {mail}', ['mail' => $user->mail]),
			Yii::t('mails', 'New password: {password}', ['password' => $newPassword]),
			Yii::t('mails', 'Use new password to log in. You can change the password in account settings.')
		];
		$caption = Yii::t('mails', 'Password reset');
		Yii::$app->mailer->compose()
			->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['company']])
			->setTo($user['mail'])
			->setSubject($caption)
			->setTextBody(
				strip_tags(implode(PHP_EOL, $body))
			)->setHtmlBody(
				Yii::$app->controller->renderPartial('//layouts/mail_' . Yii::$app->language, [
						'caption' => $caption,
						'body'    => $body,
						'token'   => $token->token
					]
				)
			)->send();
		$mailModel = new Mail();
		$mailModel->mail = $user->mail;
		$mailModel->type = 'passwordReset';
		$mailModel->save();

		return '';
	}

	public function actionLogin()
	{
		if (!Yii::$app->user->isGuest || NO_DB_CONNECTION) {
			return $this->goHome();
		}
		$model = new LoginForm();

		$loaded = false;

		if (Yii::$app->request->isPost) {
			$loaded = $model->load(Yii::$app->request->post());
		} else {
			if (Yii::$app->request->get('mail') && Yii::$app->request->get('password')) {
				$model->mail = Yii::$app->request->get('mail');
				$model->password = Yii::$app->request->get('password');

				$loaded = true;
			}
		}


		if ($loaded && $model->login()) {
			/** @var UsersInfo $modelInfo */
			$modelInfo = UsersInfo::findOne(['user_id' => Yii::$app->user->id]);
			if ($modelInfo === null) {
				if (Yii::$app->user->id) {
					$modelInfo = new UsersInfo(['user_id' => Yii::$app->user->id, 'country' => Users::locationInfo('country')]);
					$modelInfo->save();
				}
			} else {
				if (!$modelInfo->country) {
					$modelInfo->country = Users::locationInfo('country');
					$modelInfo->save();
				}
			}
			if (!$modelInfo->refferer && Yii::$app->session->get('refferer', false)){
				$modelInfo->refferer = Yii::$app->session->get('refferer', '');
				$modelInfo->save();
			}
			if (!$modelInfo->refferer && Yii::$app->request->referrer &&
					strpos(Yii::$app->request->referrer, Yii::$app->params['host']) === false){
				$modelInfo->refferer = Yii::$app->request->referrer;
				$modelInfo->save();
			}
			if (Yii::$app->request->get('noredir')) {
				return '';
			}

			return $this->userRedirect();
		} else {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return $model->getErrors();
		}
	}

	public function userRedirect()
	{
		// TODO: костыль поменьше
		if (strpos(Yii::$app->request->referrer, 'partnership') !== false) {
			return $this->redirect('/partnership/');
		} else if (strpos(Yii::$app->request->referrer, 'instashpion') !== false) {
			return $this->redirect('/instashpion/');
		} else {
			return $this->goHome();
		}
	}

	public function actionRegister()
	{
		if (NO_DB_CONNECTION){
			return $this->goHome();
		}
		$model = new RegisterForm();
		$model->load(Yii::$app->request->post());
		if (!$model->validate() || !$model->register()) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return $model->getErrors();
		} else {

			// GA событие - регистрация и отправка пользовательских переменных
			Counters::gaRegistration();
			//Counters::roiStatsSendEvent('register');
			Counters::roiStatsRegister();

			$loginFormModel = new LoginForm();
			if ($loginFormModel->load(Yii::$app->request->post()) && $loginFormModel->login())
			{
				/** @var UsersInfo $modelInfo */
				if ($modelInfo === null) {
					if (Yii::$app->user->id) {
						$modelInfo = new UsersInfo(['user_id' => Yii::$app->user->id, 'country' => Users::locationInfo('country')]);
						$modelInfo->save();
					}
				} else {
					if (!$modelInfo->country) {
						$modelInfo->country = Users::locationInfo('country');
						$modelInfo->save();
					}
				}
				if (!$modelInfo->refferer && Yii::$app->session->get('refferer', false)){
					$modelInfo->refferer = Yii::$app->session->get('refferer', '');
					$modelInfo->save();
				}
				if (!$modelInfo->refferer && Yii::$app->request->referrer &&
						strpos(Yii::$app->request->referrer, Yii::$app->params['host']) === false){
					$modelInfo->refferer = Yii::$app->request->referrer;
					$modelInfo->save();
				}
			}

			// Партнёрка
			$ref_cookie = Yii::$app->request->cookies->get('ref_key');
			if (!is_null($ref_cookie)) {
				file_get_contents(Url::toRoute([
					'/partnership/internal/register',
					'secret' => Yii::$app->params['partnership']['internal_secret'],
					'ref_key' => $ref_cookie->value,
					'user_id' => Yii::$app->user->id,
				], true));
			}

			if (isset($_COOKIE['history'])) {
				$history = '{' . implode(',', array_slice(json_decode($_COOKIE['history']), 0, 3)) . '}';
			} else {
				$history = '{}';
			}

			$model = new ShpionBalance();

			$model->date = time();
			$model->victims = $history;
			$model->is_bonus = 0;
			$model->is_guest = 1;
			$model->user_id = Yii::$app->user->id;

			$model->save();

			if (!is_null(Yii::$app->request->post('cb', null))) {
				return $this->redirect(Yii::$app->request->post('cb'));
			}

			return $this->userRedirect();
		}
	}

	public function actionAjaxvalidate()
	{
		if (!Yii::$app->request->isAjax) {
			throw new BadRequestHttpException();
		}

		Yii::$app->response->format = Response::FORMAT_JSON;

		$model = new LoginForm();
		$model->load(Yii::$app->request->post());
		return ActiveForm::validate($model);
	}

	public function actionAjaxregvalidate()
	{
		if (!Yii::$app->request->isAjax) {
			throw new BadRequestHttpException();
		}

		Yii::$app->response->format = Response::FORMAT_JSON;

		$model = new RegisterForm();
		$model->load(Yii::$app->request->post());
		return ActiveForm::validate($model);
	}

	public function actionLogout()
	{
		Yii::$app->response->getHeaders()->set('Access-Control-Allow-Origin', '*');
		Yii::$app->response->getHeaders()->set('Access-Control-Allow-Methods', 'POST, GET');
		Yii::$app->response->getHeaders()->set('Access-Control-Max-Age', '1000');
		Yii::$app->response->getHeaders()->set('Access-Control-Allow-Headers', 'Content-Type, Authorization, X-Requested-With');

		// Считываем гостевой баланс для шпиона
		$balance = 3;
		if (\Yii::$app->request->cookies->has('shpion_balance')) {
			$balance = \Yii::$app->request->cookies->get('shpion_balance')->value;
		}

		// Делаем logout
		Yii::$app->user->logout();

		// Записываем исходный
		\Yii::$app->response->cookies->add(
			new \yii\web\Cookie([
				'name' => 'shpion_balance',
				'value' => $balance
			])
		);

		if (Yii::$app->request->get('noredir')) {
			return '';
		}
		return $this->userRedirect();
	}

	public function actionContact()
	{
		if (!Yii::$app->request->isAjax) {
			throw new BadRequestHttpException();
		}

		if (Yii::$app->user->isGuest) {
			return false;
		}
		/** @var Users $user */
		$user = Users::findIdentity(Yii::$app->user->id);
		$data = Yii::$app->request->post();
		//return(print_r($data,true));
		if (isset(ContactForm::getSubjects()[$data['subject']])) {
			$subj = ContactForm::getSubjects()[$data['subject']];
		} else {
			$subj = ContactForm::getSubjects()['default'];
		}

		// Проверяем, указан ли аккаунт при определённых темах. Если указан - добавляем в тело письма
		if ($data['subject'] == 1 || $data['subject'] == 7) {
			if($data['instagram_account'] != ''
				&& $data['instagram_account']>=0
				&& isset(ContactForm::getAccounts()[$data['instagram_account']])) {
				$data['text'] = 'Аккаунт: ' . ContactForm::getAccounts()[$data['instagram_account']] . PHP_EOL . PHP_EOL .$data['text'];
				//$subj .= ' ' . ContactForm::getAccounts()[$data['instagram_account']];
			} else {
				return 'error';
			}
		}

		$subj = html_entity_decode($subj, ENT_QUOTES);

		//return $subj;
		//return $data['subject'];
		if (!$this->sendRequest($subj, $data['text'], [preg_replace('#\s#u', '_', $subj)])) {
			return 'error';
		}
		$logEntry = new UsersLog(['user_id' => Yii::$app->user->id, 'text' => 'Отправлен запрос с темой ' . $subj]);
		$logEntry->save();
		return 'ok';
	}

	protected function sendRequest($subject, $text, $tags, $status = \Freshdesk\Model\Ticket::STATUS_OPEN)
	{
		try {
			$oldModel = SupportRequest::findOne(['user_id' => Yii::$app->user->identity->getId(), 'text' => $text]);
			if ($oldModel !== null || !$text) {
				return false;
			}
			$client = new Ticket(new Connection(Yii::$app->params['fleshdeskApiKey'])
			);
			$ticket = new \Freshdesk\Model\Ticket([
					'description' => $text,
					'subject' => $subject,
					'email' => Yii::$app->user->identity->mail,
					'cc_emails' => [],
					'status' => $status,
					'priority' => 2,
					'tags' => ['name' => implode(',',$tags)],
					'responder_id' => Yii::$app->params['fleshdeskResponser']
			]);
			$client->createNewTicket($ticket);
			$model = new SupportRequest([
				'user_id' => Yii::$app->user->identity->getId(), 'text' => $text,'subject' => $subject
			]);
			return $model->save();
		} catch (\Exception $e) {
			return false;
		}
	}

	public function actionRevision($id) {
		if (Yii::$app->user->isGuest) {
			return '';
		}
		$account = Account::find()
			->where('id = ' . $id)
			->one();

		$revision = new Account();

		$revision->login 	= $account->login;
		$revision->password = UHelper::unpack($account->password);
		$revision->user_id 	= $account->user_id;

		$result = $revision->revision();

		if (!empty($result['error'])) {
			if ($result['error'] == 'Sorry, there was a problem with your request.')
			{
				$account->auth_block = 1;
				$account->save();

				return json_encode(['status' => 'error', 'message' => 'problemRequest', 'account_id' => $account->id, 'login' => $account->login]);
			} else {
				if ($account->auth_block > 0) {
					$account->auth_block = 0;
					$account->save();
				}
			}

			return json_encode(['status' => 'error', 'message' => $result['error'], 'account_id' => $account->id, 'login' => $account->login]);
		}

		$account->auth_block = 0;
		$account->is_new = 0;
		$account->save();

		return json_encode(['status' => 'ok', 'account_id' => $account->id, 'login' => $account->login]);
	}

	public function actionStats($id = null)
	{
		if (!Yii::$app->user->isGuest) {
			$user_id = Yii::$app->user->id;
			if (!empty(Yii::$app->request->queryParams['preview'])) {
				if (Yii::$app->user->identity->isAdmin) {
					$user_id = Yii::$app->request->queryParams['preview'];
				}
			}
			if (!empty($id)) {
				$account = Account::find()->where('id = ' . $id)->one();
				/* @var $account Account */
				$message = $account->message;
				if ($message) {
					if ('problemRequest' == trim($message)) {
						return Yii::t('js', trim($message), [
							'account' => $account->login,
							'email' => $account->user->mail
						]);
					}
					return Yii::t('main', trim($message));
				} else {
					return 'ok';
				}
			} else {
				Yii::$app->session->close(); // Предотвращает блокировку сессии для других запросов

				$accounts = Account::find()
					->select([
						'accounts.id',
						'accounts.instagram_id',
						'accounts.likes',
						'accounts.comments',
						'accounts.follow',
						'accounts.unfollow',
						'accounts.message',
						'accounts.comment_status',
						'accounts.follow_status',
						'accounts.comments_block_date',
						'accounts.comments_block_expire',
						'accounts.follow_block_date',
						'accounts.follow_block_expire',
						'accounts.unfollow_block_date',
						'accounts.unfollow_block_expire',
						'accounts.unfollow_status',
						'accounts.account_media',
						'accounts.account_followers',
						'accounts.account_follows',
						'accounts.lastfollowers',
						'accounts.user_id',
						'accounts.timer',
						new Expression('sum(media_count.new_comments) as new_comments')
					])
					->with('options')
					->leftJoin('media_count', 'media_count.account_id = accounts.id')
					->where(['accounts.user_id' => $user_id])
					->groupBy('accounts.id')
					->all();

				$account_statuses = Account::getStatusesByInstagramIdsList(ArrayHelper::getColumn($accounts, 'instagram_id'));
				if ($account_statuses === null) {
					$account_statuses = [];
				}

				$stats = [];
				foreach ($accounts as $account) {
					/* @var $account Account */

					$statusRaw = ArrayHelper::getValue($account_statuses, intval($account->instagram_id), false);
					$options = $account->options;
					$status = ($statusRaw === true ? 1 : 0);


					if ($account->comment_status && $account->follow_status){
						$hours = ceil(($account->pause_until - time())/3600);
						if ($hours > 0) {
							$account->comment_status =
							$account->follow_status = Yii::t('views', 'There are some temporary restrictions by Instagram on using follows and comments and they are paused on {hours} hours. The actions will start automatically when pause will be ended.', ['hours' => $hours]) . ' ' . Yii::t('views', 'Time is not subs on that period.');
						} else {
							$account->comment_status = $account->follow_status = 0;
						}
					} elseif($account->comment_status){
						$hours = ceil(($account->comments_block_date + $account->comments_block_expire * 3600 - time())/3600);
						if ($hours > 0) {
							$account->comment_status = Yii::t('views', 'There are some restrictions on using of the action by Instagram and it is paused for {hours} hours. The actions will start automatically when pause will be ended.', ['hours' => $hours]);
							if ($account->options->follow == false) {
								$account->comment_status .= ' ' . Yii::t('views', 'Time is not subs on that period.');
							}
						} else {
							$account->comment_status = 0;
						}
					} elseif($account->follow_status){
						$hours = ceil(($account->follow_block_date + $account->follow_block_expire * 3600 - time())/3600);
						if ($hours > 0) {
							$account->follow_status = Yii::t('views', 'There are some restrictions on using of the action by Instagram and it is paused for {hours} hours. The actions will start automatically when pause will be ended.', ['hours' => $hours]);
							if ($account->options->comment == false) {
								$account->follow_status .= ' ' . Yii::t('views', 'Time is not subs on that period.');
							}
						} else {
							$account->follow_status = 0;
						}
					}

					if ($account->unfollow_status) {
						$expire = $account->getUnfollowsPauseExpire();
						if ($expire > 0) {
							$account->unfollow_status = $account->getUnfollowsStatusMessage();
						} else {
							$account->unfollow_status = 0;
						}
					}

					// Todo: Костыль - убрать при рефакторинге dashboad'a
					if (!$options->follow) {
						$account->follow_status = 0;
					}

					// Todo: Костыль - убрать при рефакторинге dashboad'a
					if (!$options->unfollow) {
						$account->unfollow_status = 0;
					}

					$user = Users::findOne($account->user_id);

					$stats[$account->id] = [
						'account_media'     => $account->account_media,
						'account_followers' => $account->account_followers,
						'account_follows'   => $account->account_follows,
						'new_comments'      => $account->new_comments < 0 ? 0 : $account->new_comments, //$account->getNewComments(),
						'likes'             => $options->likes ? $account->likes : '-',
						'comments'          => $options->comment ? $account->comments : '-',
						'comment_status'    => $account->comment_status,
						'follow_status'     => $account->follow_status,
						'hideTimer'         => !$user->use_timer,
						'timerText'         => Account::getStaticTimerText($account->timer),
						'unfollow_status'   => $account->unfollow_status,
						'follow'            => $options->follow ? $account->follow : '-',
						'unfollow'          => $options->unfollow ? $account->unfollow : '-',
						'message'           => $account->message,
						'lastfollowers'     => $account->getLastFollowers(),
						'status'            => $status,
						'option_likes'      => $options->likes,
						'option_comment'    => $options->comment,
						'option_follow'     => $options->follow,
						'option_unfollow'   => $options->unfollow,
					];
				}
				return json_encode($stats);
			}
		}
		return '';
	}

//	public function actionAddimages()
//	{
//		if (empty($_POST['users']) || !is_array($_POST['users'])) {
//			return 'error';
//		}
//		$accounts = Account::find()->where(['id' => $_POST['users']])->all();
//		$params = [];
//		foreach ($accounts as $account) {
//			/* @var $account Account */
//			$params[$account->instagram_id] = $account->getServerParams();
//		}
////		$data    = file_get_contents('/var/www/html/userdata/uploads/1.jpg');
////		$escaped = bin2hex($data);
//		list(, $escaped) = explode(',', $_POST['image']);
//		if (empty($escaped)) {
//			return 'error';
//		}
//		$result = Yii::$app->postdb->createCommand()->insert(
//			'imagesqueue', [
//			'image'         => new Expression("decode('{$escaped}' , 'base64')"),
//			'status'        => 'waiting',
//			'comment'       => $_POST['caption'],
//			'users'         => json_encode($params),
//			'fail_users'    => new Expression("ARRAY[]::text[]"),
//			'success_users' => new Expression("ARRAY[]::text[]"),
//			'added'         => new Expression("now()"),
//			'pause_from'    => $_POST['pauseFrom'],
//			'pause_to'      => $_POST['pauseTo'],
//			'user_id'       => Yii::$app->user->id,
//		])->execute();
//		if (!$result) {
//			return 'error';
//		}
//		if (!$this->client) {
//			$this->client = new \GearmanClient();
//			$this->client->addServer(Yii::$app->params['gearmanServer'], '4730');
//			$this->client->setCompleteCallback([$this, 'gearmanCompleteCallback']);
//		}
//		$this->client->addTask('addQueue', json_encode(['user_id' => Yii::$app->user->id]));
//		return $this->client->runTasks() ? 'ok' : 'error';
//	}


	public function actionAddimages()
	{

		if (empty($_POST['users']) || !is_array($_POST['users'])) {
			return 'error';
		}

		/** @var Account[] $accounts */
		$accounts = Account::find()->where(['id' => $_POST['users']])->all();
		if (empty($accounts)){
			return 'error';
		}
		$params = [];
		foreach ($accounts as $account) {
			$params[$account->instagram_id] = RPCHelper::getServerParams($account);
		}
//		$data    = file_get_contents('/var/www/html/userdata/uploads/1.jpg');
//		$escaped = bin2hex($data);
		list(, $escaped) = explode(',', $_POST['image']);
		if (empty($escaped)) {
			return 'error';
		}

		$result = Yii::$app->postdb->createCommand()->insert('imagesqueue', [
			'image'         => new Expression("decode('{$escaped}' , 'base64')"),
			'status'        => 'waiting',
			'comment'       => $_POST['caption'],
			'users'         => json_encode($params),
			'fail_users'    => new Expression("ARRAY[]::text[]"),
			'success_users' => new Expression("ARRAY[]::text[]"),
			'added'         => new Expression("now()"),
			'pause_from'    => $_POST['pauseFrom'],
			'pause_to'      => $_POST['pauseTo'],
			'user_id'       => Yii::$app->user->id,
		])->execute();
		if (!$result) {
			return 'error';
		}

		RPCHelper::runAccountTask($accounts[0], Yii::$app->user->id);

		return RPCHelper::runAccountTask($accounts[0], Yii::$app->user->id) ? 'ok' : 'error';
	}


	public function actionReportAction()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;
		if (Yii::$app->user->isGuest) {
			throw new ForbiddenHttpException();
		}

		$text = Yii::$app->request->post('text', null);
		if (empty($text)) {
			return ['status' => 'error'];
		}

		$device = Yii::t('app', 'desktop');
		/** @var DeviceDetect $dd */
		$dd = new DeviceDetect();
		if ($dd->isMobile()) {
			$device = Yii::t('app', 'smartphone');
		} elseif ($dd->isTablet()) {
			$device = Yii::t('app', 'tablet');
		}
		if (!$this->sendRequest(Yii::t('app', 'Promo action reply'), $text, [Yii::t('app', 'ideas'), $device], \Freshdesk\Model\Ticket::STATUS_PENDING)) {
			return ['status' => 'error'];
		}
		return ['status' => 'ok'];
	}

	public function gearmanCompleteCallback()
	{

	}

	public function actionRevert()
	{
		if (Yii::$app->request->get('secret', null) !== 'terces') {
			throw new NotFoundHttpException();
		}

		return strrev(Yii::$app->request->get('string', ''));
	}

	public function actionSendMail()
	{
		if (Yii::$app->request->get('secret', null) !== '7d78ad3534fb0b49c3') {
			throw new NotFoundHttpException();
		}

		Yii::$app->response->format = Response::FORMAT_JSON;

		$account_id = Yii::$app->request->get('account_id', null);
		$type = Yii::$app->request->get('type', null);
		if (is_null($account_id) || is_null($type)) {
			throw new BadRequestHttpException('Wrong params');
		}

		$availableTypes = [Account::CHECKPOINT_ERROR, Account::PASSWORD_ERROR, Account::BAD_GEO, Account::BAD_HASHTAGS, Account::PROBLEM_REQUEST, 'resetAuthProblem'];
		if (!in_array($type, $availableTypes)) {
			throw new BadRequestHttpException('Wrong type');
		}

		/** @var Account $account */
		$account = Account::findOne($account_id);
		if ($account === null) {
			throw new NotFoundHttpException('Account not found');
		}

		/* @var $mailFind Mail */
		$mailFind = Mail::findOne(['instagram_id' => $account->instagram_id, 'type' => $type]);
		if ($mailFind !== null) {
			throw new ConflictHttpException('This mail already sent');
		}

		$caption = Yii::t('mails', 'Zengram.net. Need your attention');
		$body = [];
		$options = [];
		switch ($type) {
			case 'resetAuthProblem':
				$body = [
					Yii::t('mails', 'Hello. We are pleased that your project account @{account} has passed authentication and is now working to promote your account.', [
						'account' => $account->login,
					])
				];
				break;
			case Account::PROBLEM_REQUEST:
				  $body = [
					  Yii::t('js', 'On 10/07/2016 there are some problems of accounts authorization from instagram (5% of cases). Currently instagram denied authorization for your account @{account}. This can happen on any device or service that you are using. We found out that the problem is solved by itself during the day. We offer you is to set the settings of your project and get familiar with our service. Once your account is authorized, the project will automatically start, which we will inform you by email @{email}.', [
						  'account' => $account->login,
						  'email' => $account->user->mail,
					  ]),
					  Yii::t('mails', 'Balance during idle time will not be charged.')
				  ];
				  break;
			case Account::CHECKPOINT_ERROR:
				$body = [
					Yii::t('mails', 'Hello, {login}', ['login' => $account->login]),
					Yii::t('mails', 'We inform you, that works on your account {login} are currently stopped.', ['login' => $account->login]),
					Yii::t('mails', 'For further work you should go to the Instagram site or Instagram app on your mobile device. You will see a form of account verification:'),
					Yii::t('mails', 'Confirm your account and run your project again.')
				];
				$options['checkpoint'] = true;
				break;

			case Account::PASSWORD_ERROR:
				$body = [
					Yii::t('mails', 'Hello, {login}.', ['login' => $account->login]),
					Yii::t('mails', 'We inform you, that works on your account ({login}) are currently stopped due to incorrect password.', ['login' => $account->login]),
					Yii::t('mails', 'For further work you should go to the account settings and re-enter the password to your account.')
				];
				break;

			case Account::BAD_GEO:
				$body = [
					Yii::t('mails', 'Hello, {login}.', ['login' => $account->login]),
					Yii::t('mails', 'We inform you, that you have chosen Instagram cities with very few users media.'),
					Yii::t('mails', 'To speed up the promotion of your account, we recommend choosing additional cities.')
				];
				break;

			case Account::BAD_HASHTAGS:
				$body = [
					Yii::t('mails', 'Hello, {login}.', ['login' => $account->login]),
					Yii::t('mails', 'We inform you, that you have chosen Instagram hashtags with very few users media.'),
					Yii::t('mails', 'To speed up the promotion of your account, we recommend choosing additional hashtags.')
				];
				break;
		}

		$token = new UserTokens();
		$token->generate($account->user_id);
		$token->save();

		$error = false;
		$mailModel = new Mail();
		try {
			Yii::$app->mailer->compose()
				->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['company']])
				->setTo($account->user->mail)
				->setSubject($caption)
				->setTextBody(
					implode(PHP_EOL, $body)
				)->setHtmlBody(
					Yii::$app->controller->renderPartial(
						'//layouts/mail_' . Yii::$app->language,
						[
							'caption' => $caption,
							'body'    => $body,
							'token'   => $token->token,
							'options' => $options
						]
					)
				)->send();

			$mailModel->mail = $account->user->mail;
			$mailModel->type = $type;
			$mailModel->instagram_id = $account->instagram_id;
			$mailModel->save();
		} catch (\Swift_RfcComplianceException $e) {
			$error = 'Mail transport error';
		} catch (\Swift_TransportException $e) {
			$error = 'Mail transport error';
		}

		return [
			'status'  => $error !== false ? 409 : 200,
			'message' => $error !== false ? $error : 'Success',
			'debug'   => $mailModel->getErrors()
		];
	}

	public function actionBlog($alias = null)
	{
		if (!is_null($alias)) {
			$post = Post::findOne(['alias' => $alias]);
			if (is_null($post)) {
				throw new NotFoundHttpException(Yii::t('app', 'Record not found'));
			}
			return $this->render('blog_entry', [
				'post' => $post
			]);
		}

		return $this->render('blog', [
			'posts' => Post::find()->orderBy(['date' => SORT_DESC])->all()
		]);
	}

	public function actionLanguageJs()
	{
		return $this->renderPartial('language-js', [
			'file' => $this->js_lang_file
		]);
	}

	public function actionCurrentBalance()
	{
		return Yii::$app->user->identity->balance;
	}

	public function actionIsValidTargeting($account_id)
	{
		// Получаем тип таргетинга
		$account = Account::findOne($account_id);
		//echo $account->options->search_by;
		$error = '';

		if(Yii::$app->user->identity->balance <= 0){
			$error = 'no_money';
			return $error;
		}

		if (!$account->options->unfollow) {
			switch ($account->options->search_by){
				case 1:
					if (count($account->places) == 0){
						$error = 'no_places';
					}
					break;
				case 4:
					if (count($account->hashtags) == 0){
						$error = 'no_hashtags';
					}
					break;
				case 8:
					if (count($account->competitors) == 0){
						$error = 'no_competitors';
					}
					break;
			}
		}
		return $error;
	}
}
