<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "mail_triggers".
 *
 * @property integer $id
 * @property integer $action
 * @property integer $interval
 * @property integer $permanent
 * @property string  $name
 */
class MailTriggers extends ActiveRecord
{
	const ACTION_AFTER_REG      = 1;
	const ACTION_FREE_DAYS      = 2;
	const ACTION_NO_USE_BALANCE = 3;
	const ACTION_NO_PAYMENT     = 4;
	const ACTION_STATS          = 5;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'mail_triggers';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['action', 'interval', 'permanent'], 'integer'],
			[['name'], 'string'],
		];
	}

	public static function getActionDescriptions()
	{
		return [
			self::ACTION_AFTER_REG => 'Регистрация, но не добавлен аккаунт инстаграм',
			self::ACTION_FREE_DAYS => 'Использованы 3 бесплатных дня, но нет оплат',
			self::ACTION_NO_USE_BALANCE => 'Есть оплата, не используются платные дни',
			self::ACTION_NO_PAYMENT => 'Платные дни израсходованы, и нет оплат',
		];
	}

	public static function getActionDescription($action)
	{
		return self::getActionDescriptions()[$action];
	}
}
