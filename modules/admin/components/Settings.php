<?php namespace app\modules\admin\components;

use app\commands\CronController;
use yii\base\Exception;
use yii\base\Model;

class Settings extends Model
{
	public $popular_category;
	public $expired_time;
	public $post_count;
	public $cron_active;

	public $time_from;
	public $time_until;

	public $count_per_iteration;

	public $comment_account_id;
	public $name;
	public $id;

	const STATUS_ACTIVE = 1;
	const STATUS_PAUSE  = 2;
	const STATUS_STOP   = 0;

	public function rules()
	{
		return [
			[['popular_category', 'expired_time', 'post_count', 'cron_active'], 'required'],
			[['expired_time'], 'integer', 'min' => 10],
			[['comment_account_id', 'name'], 'string'],
			[['cron_active'], 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_STOP, self::STATUS_PAUSE]],
			[['time_from', 'time_until'], 'integer', 'min' => 0, 'max' => 24],
			['popular_category', 'in', 'range' => [30, 100, 500, 1000]],
			[['post_count', 'count_per_iteration'], 'integer', 'min' => 1],
			[['id'], 'integer']
		];
	}

	public function attributeLabels()
	{
		return [
			'popular_category'    => 'Категория звёзд',
			'expired_time'        => 'Время жизни комментария (минут)',
			'post_count'          => 'Количество постов от одного аккаунта',
			'cron_active'         => 'Комментирование запущено',
			'time_from'           => 'Время начала комментирования',
			'time_until'          => 'Время окончания комментирования',
			'count_per_iteration' => 'Комментариев за 10 минут',
			'comment_account_id'  => 'Аккаунт для выборки комментариев',
			'name'                => 'Наименование проекта',
		];
	}

	public function getFileName()
	{
		$file = \Yii::getAlias('@app/modules/admin/config/settings.json');
		if (!is_file($file)) {
			touch($file);
		}
		return $file;
	}

	public function restore()
	{
		$file = $this->getFileName();
		$data = json_decode(file_get_contents($file), true);
		if (!is_null($data)) {
			$this->load([$this->formName() => $data]);
		}
	}

	public function save($validate = true)
	{
		if ($validate && !$this->validate()) {
			return false;
		}

		// Проверка на запуск
		$file = $this->getFileName();

		$data = json_decode(file_get_contents($file), true);

		if ($this->cron_active && !$data['cron_active']) {

			try {

				$script = \Yii::getAlias('@app/yii cron/truncate-comments');

				shell_exec('php '.$script);

			} catch (Exception $e) {

			}

		}

		file_put_contents($file, json_encode($this->toArray()));
		return true;
	}
}