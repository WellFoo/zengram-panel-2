<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $account_id
 * @property string  $name
 *
 * This is the model class for table "account_hashtags".
 */

class AccountHashtags extends ActiveRecord
{
	public static function tableName()
	{
		return 'account_hashtags';
	}

	public function rules()
	{
		return [
			[['account_id', 'name'], 'required']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function beforeValidate()
	{
		$this->name = mb_ereg_replace('\W+', '', $this->name);
		return parent::beforeValidate();
	}
}