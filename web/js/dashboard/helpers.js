function reInitPopovers(parrentNode)
{
	setTimeout(function () {
		$(parrentNode).find("dd").popover('destroy');

		setTimeout(function() {
			$(parrentNode).find("[data-toggle='popover']").popover({
				placement: 'top',
				trigger: 'hover',
				container: 'body'
			});
		}, 200);

	}, 300);
}

window.tryRevision = 0;
window.showedProblemRequestMessage = 0;
window.showedEtcErrortMessage = 0;

function revisionHandler(response)
{
	if (response.status == 'ok') {
		window.accounts[response.account_id].start();
		$('#accountAdded').modal('show');
	} else if (response.status == 'error' && window.tryRevision <= 6)
	{
		window.tryRevision++;
		if (response.message == 'problemRequest' && window.showedProblemRequestMessage < 1) {
			window.accounts[response.account_id].showMessage(response.account_id, 'problemRequest', true);
			window.showedProblemRequestMessage = 1;
		} else {
			if (window.showedEtcErrortMessage < 1) {
				window.accounts[response.account_id].showMessage(response.account_id, response.message, true);
				window.showedEtcErrortMessage = 1;
			}
		}

		setTimeout(function () {
			accountRevision(response.account_id, revisionHandler);
		}, 10);
	}
}

function accountRevision(account, callback)
{
	var url = "/site/revision/" + account;
	$.get(url, callback, 'JSON');
}

function sendAddAccount(form, params)
{

	$.ajax({
		url: '/account/add',
		data: params,
		async: true,
		type: 'post',
		beforeSend: function ()
		{
			submitting = true;
			form.find('.alert').hide();
            form.find('button[type=submit]').addClass('loading');
		},
		success: function (data)
		{
			form.find('button[type=submit]').removeClass('loading');
			if (data.result !== undefined)
			{
				if (data.account !== undefined)
				{
					window.accounts[data.model.id] = new Account({
						id:                 data.model.id,
						login:              data.model.login,
						likesOption:        ((data.options.likes > 0) ? true : false),
						commentsOption:     ((data.options.comment > 0) ? true : false),
						followsOption:      ((data.options.follow > 0) ? true : false),
						unfollowsOption:    ((data.options.unfollow > 0) ? true : false),
						avatar:             data.model.account_avatar,
						info: {
							followers:      data.model.account_followers,
							newFollowers:   0,
							followings:     data.model.account_follows
						},
						counters: {
							likes:          data.model.likes,
							comments:       data.model.comments,
							follows:        data.model.follows,
							unfollows:      data.model.unfollows
						},
						speed:              (data.options.speed || 0),
						photos:             data.model.account_media,
						show: true,
						demo: false
					});

					Accounts.push(window.accounts[data.model.id]);

					setTimeout(updateAccounts, 300);

					$('.addAccount').modal('hide');
					$('#accountAdded').find('#login').text(data.login);

					if (data.city !== undefined) {
						$('#accountAdded').find('#city-block').show();
						$('#accountAdded').find('#city').text(data.city);
					}

					if (data.settings !== undefined) {
						$('#accountAdded').find('#settings-button').attr('href', data.settings);
					}

					if (window.demo) {
						window.demo.hide();
					}
					accountRevision(data.account_id, revisionHandler);
				}
			}
			if (data.errors !== undefined)
			{
				form.find('.alert').html('').show();

				if (data.errors.hasOwnProperty('error') && data.errors.error[0] == 'checkpoint_required')
				{
					var text = '<div style="overflow: hidden; line-height: 30px; text-align: left; color: #333333;">' +
						'<img class="pull-right" src="' + window.Zengram.t('/img/telefon.jpg') + '" style="width: 250px;">' +
						window.Zengram.t('It is necessary to verify @{account} Instagram account. Make this action on <a target="_blank" href="https://instagram.com">Instagram.com</a> or in your Instagram app and we will start work.', {account: form.find('input[name*=login]').val()}) +
						'</div>';

					$('#alertModalContent').html(text);
					$('#addAccount').modal('hide');
					$('#alertModal').modal();
					delete data.errors.error;
				}

				for (var input in data.errors)
				{
					if (data.errors.hasOwnProperty(input)) {
						form.find('.alert').append(data.errors[input] + '<br>');
					}
				}
			}
		},
		complete: function () {
			$('#addAccount-form button').removeClass('disabled');
		},
		dataType: 'json'
	});
}

function updateAccounts()
{
	var url = "/site/stats" + '?' + jQuery.param(yii.getQueryParams(window.location.toString()));

	$.getJSON(url, function (response)
	{
		$.each(response, function (account_id, data)
		{
			var account = window.accounts[account_id];

			//todo: Сделать методы для сброса сообщений об ошибках опций (clearErrors() чистит все сообщения включая общие)
			account.attr('errors.likes', false);
			account.attr('errors.comments', false);
			account.attr('errors.follows', false);
			account.attr('errors.unfollows', false);

			if (data.comment_status === 0) {
				account.addError('comments', false);
			} else {
				account.addError('comments', data.comment_status);
			}

			if (data.follow_status === 0) {
				account.addError('follows', false);
			} else {
				account.addError('follows', data.follow_status);
			}

			if (data.unfollow_status === 0) {
				account.addError('unfollows', false);
			} else {
				account.addError('unfollows', data.unfollow_status);
			}

			if (data.likes != '-') { account.setCounter('likes', data.likes); }
			if (data.comment != '-') { account.setCounter('comments', data.comments); }
			if (data.follow != '-') { account.setCounter('follows', data.follow); }
			if (data.unfollow != '-') { account.setCounter('unfollows', data.unfollow); }

			account.attr('photos', data.account_media);
			account.attr('newComments', ((data.new_comments) ? data.new_comments : 0));
			account.attr('isActive', (data.status !== 0));

			account.attr('hideTimer', data.hideTimer);
			account.attr('timerText', data.timerText);

			if (data.message) {
				account.showMessage(account, data.message, false);
			}

			account.updateOptionState('likes',     (data.option_likes == 1));
			account.updateOptionState('comments',  (data.option_comment == 1));
			account.updateOptionState('follows',   (data.option_follow == 1));
			account.updateOptionState('unfollows', (data.option_unfollow == 1));
		});
	});
}

function updateBalance()
{
	var url = "/site/balance?" + jQuery.param(yii.getQueryParams(window.location.toString()));

	$.getJSON(url, function (data)
	{
		$('#balance_d').text(data['d']);
		$('#balance_H').text(data['H']);
		$('#balance_i').text(data['i']);

		if (data['d'] == '00' && data['H'] == '00' && data['i'] == '00') {
			$('.btnew.btn-comment').on('click.low-balance', function ()
			{
				$('#low-balance').modal();
				return false;
			});
		} else {
			$('.btnew.btn-comment').off('.low-balance');
		}
		if (data['d'] == '00' && parseInt(data['H']) < 5) {
			$('.btnew[data-target=\\#photo-upload]').attr('data-target', '#low-balance');
		} else {
			$('.btnew[data-target=\\#low-balance]').attr('data-target', '#photo-upload');
		}
	});
}

function backendFormatOption(option)
{
	if (option == 'comments') {
		return 'comment';
	} else if (option == 'unfollows') {
		return 'unfollow';
	} else if (option == 'follows') {
		return 'follow';
	}

	return option;
}

function frontendFormatOption(option)
{
	if (option == 'comment') {
		return 'comments';
	} else if (option == 'unfollow') {
		return 'unfollows';
	} else if (option == 'follow') {
		return 'follows';
	}

	return option;
}