<?php

namespace app\controllers;

use app\components\RPCHelper;
use app\models\Account;
use app\models\AccountComments;
use app\models\AccountCompetitors;
use app\models\AccountHashtags;
use app\models\AccountPlaceGeotag;
use app\models\AccountPlace;
use app\models\AutoFillDorons;
use app\models\Comments;
use app\models\FbGeotag;
use app\models\Options;
use app\models\Place;
use app\models\Regions;
use app\components\Controller;
use app\models\UsersLog;
use app\models\UsersInfo;
use Yii;
use yii\db\IntegrityException;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class OptionsController extends Controller
{
	public function actionIndex($id)
	{
		$account = $this->checkPermits($id);
		$account->options->skipRelations = true;
		if ($account->options->load(Yii::$app->request->post())
			&& $account->options->save()
		) {
			$logEntry = new UsersLog(['user_id' => Yii::$app->user->id, 'text' => 'Изменены настройки аккаунта '.
					$account->login]);
			$logEntry->save();
			return Yii::$app->getResponse()->redirect(array('/'));
		}
		return $this->render('index_geotags', [
			'account' => $account
		]);
	}

	public function actionDelete($id)
	{
		$account = $this->checkPermits($id);
		$logEntry = new UsersLog(['user_id' => Yii::$app->user->id, 'text' => 'Удален аккаунт '.$account->login]);
		$logEntry->save();
		/** @var UsersInfo $modelInfo */
		$modelInfo = UsersInfo::findOne(['user_id' => Yii::$app->user->id]);
		if ($modelInfo === null){
			$modelInfo = new UsersInfo(['user_id' => Yii::$app->user->id]);
		}
		$modelInfo->deleted_account = 1;
		$modelInfo->save();
		//$account->accountStop();
		RPCHelper::stopAccount($account, 'user');
		$account->delete();
		return $this->goHome();
	}

	public function actionComment($id)
	{
		if (!Yii::$app->request->isAjax) {
			throw new BadRequestHttpException();
		}

		Yii::$app->response->format = Response::FORMAT_JSON;
		$account = $this->checkPermits($id);

		$action = Yii::$app->request->post('action', null);
		if ($action === null) {
			throw new BadRequestHttpException(Yii::t('app', 'Error request params'));
		}

		if ($action != 'list') {
			$account->use_trusted = 0;
			$account-> save();
		}

		$comment = Yii::$app->request->post('comment', []);

		switch ($action) {
			case 'create':
				$result = $this->commentCreate($id, $comment);
				break;
			case 'update':
				$result = $this->commentUpdate($id, $comment);
				break;
			case 'delete':
				$result = $this->commentDelete($id, $comment);
				break;
			case 'list':
				$offset = Yii::$app->request->post('offset', 0);
				$limit = Yii::$app->request->post('limit', 0);
				$result = $this->getCommentsList($id, $limit, $offset);
				break;
			case 'switch-firm':
				$useFirm = Yii::$app->request->post('firm', 1);
				$result = $this->commentsSetFirm($account, $useFirm);
				break;
			default:
				throw new BadRequestHttpException(Yii::t('app', 'Error request params'));
		}

		return $result;
	}

	private function getCommentsList($account_id, $limit = 0, $offset = 0)
	{
		$query = AccountComments::find()
			->where(['account_id' => $account_id]);

		$limit = intval($limit);
		if ($limit > 0) {
			$query->limit($limit);
		}
		$offset = intval($offset);
		if ($offset > 0) {
			$query->offset($offset);
		}

		return $query
			->orderBy(['id' => SORT_DESC])
			->all();
	}

	/**
	 * @param $account Account
	 * @param $useFirm boolean
	 *
	 * @return array
	 * @throws \yii\db\Exception
	 * @throws \yii\web\BadRequestHttpException
	 */
	private function commentsSetFirm($account, $useFirm)
	{
		if (!!$account->options->firmcomments !== !!$useFirm) {
			if ($useFirm) {
				if (!$account->hasFirmComments()) {
					$account->fillFirmComments();
				}
			} else {
				$account->clearFirmComments();
			}
			$account->options->firmcomments = $useFirm ? 1 : 0;
			$account->options->skipRelations = true;
			$account->options->save();
		}

		return [
			'action' => 'firm',
			'count' => $account->getCommentsCount()
		];
	}

	private function commentCreate($account_id, $data)
	{
		if (!isset($data['text'])) {
			throw new BadRequestHttpException(Yii::t('app', 'Error request params'));
		}

		$comment = new AccountComments();
		$comment->account_id = $account_id;
		$comment->text = $data['text'];
		if (!$comment->save()) {
			return ['error' => Yii::t('app', 'Unable to save comment')];
		}

		return [
			'action' => 'create',
			'id' => $comment->id,
			'text' => $comment->text
		];
	}

	private function commentUpdate($account_id, $data)
	{
		if (!isset($data['id']) || !isset($data['text'])) {
			throw new BadRequestHttpException(Yii::t('app', 'Error request params'));
		}

		/* @var $comment AccountComments */
		$comment = AccountComments::findOne([
			'id' => $data['id'],
			'account_id' => $account_id
		]);
		if ($comment === null) {
			$comment = new Comments();
			$comment->account_id = $account_id;
		}

		$comment->text = $data['text'];
		$comment->save();

		return [
			'action' => 'update',
			'id' => $comment->id,
			'text' => $comment->text
		];
	}

	private function commentDelete($account_id, $data)
	{
		if (!isset($data['id'])) {
			throw new BadRequestHttpException(Yii::t('app', 'Error request params'));
		}

		/* @var $comment AccountComments */
		$comment = AccountComments::findOne([
			'id' => $data['id'],
			'account_id' => $account_id
		]);
		if ($comment === null) {
			throw new BadRequestHttpException(Yii::t('app', 'Comment #{id} not found.', ['id' => $data['id']]));
		}
		$comment->delete();

		return [
			'action' => 'delete',
			'id' => $comment->id
		];
	}

	public function actionPlaces($id)
	{
		if (!Yii::$app->request->isAjax) {
			throw new BadRequestHttpException();
		}

		Yii::$app->response->format = Response::FORMAT_JSON;
		$account = $this->checkPermits($id);

		$action = ArrayHelper::getValue(Yii::$app->request->post(), 'action', null);
		if ($action === null) {
			throw new BadRequestHttpException(Yii::t('app', 'Error request params'));
		}

		if ($action != 'list'){
			$account->use_trusted = 0;
			$account-> save();
		}

		$place = ArrayHelper::getValue(Yii::$app->request->post(), 'place', []);

		switch ($action) {
			case 'list':
				$result = $this->getPlacesList($account);
				break;
			case 'create':
				$result = self::placeCreate($account, $place);
				break;
			case 'delete':
				$result = $this->placeDelete($account, $place);
				break;
			default:
				throw new BadRequestHttpException(Yii::t('app', 'Error request params'));
		}

		$result['type'] = 'places';
		return $result;
	}

	/**
	 * @param $account Account
	 * @return array
	 */
	private function getPlacesList($account)
	{
		$list = [];
		foreach($account->places as $place) {
			$data = $place->toOptionsArray();
			$data = ArrayHelper::merge($data, self::getPlaceCircle($account, $place));
			$list[] = $data;
		}
		return [
			'action' => 'list',
			'data'   => $list
		];
	}

	/**
	 * @param $account Account
	 * @param $data    array
	 * @return array
	 * @throws BadRequestHttpException
	 */
	public static function placeCreate($account, $data)
	{
		$place = self::getPlace($data);

		if (isset($place['error'])) {
			$place['action'] = 'create';
			return $place;
		}

		try {
			$account->link('places', $place);
		} catch (IntegrityException $e) {
			return [
				'action' => 'create',
				'error'  => Yii::t('app', '{city} is already in your list', ['city' => $place->name])
			];
		}

		$result = [
			'action' => 'create',
			'data'   => $place->toOptionsArray()
		];
		$result['data'] = ArrayHelper::merge($result['data'], self::getPlaceCircle($account, $place));
		return $result;
	}

	/**
	 * @param $account Account
	 * @param $place   Place
	 * @return array
	 */
	private static function getPlaceCircle($account, $place)
	{
		$accPlace = AccountPlace::findOne([
			'account_id' => $account->id,
			'place_id'   => $place->id,
		]);
		if (is_null($accPlace)) {
			$accPlace = new AccountPlace([
				'account_id'     => $account->id,
				'place_id'       => $place->id,
				'circle_enabled' => true,
			]);
			$accPlace->setCircleData([
				'lat' => floatval($place->lat),
				'lng' => floatval($place->lng),
			], $place->radius);
			$accPlace->save();
		}

		Yii::warning($place->id);
		$result['circle'] = $accPlace->getCircleData();
		$result['circle']['enabled'] = $accPlace->circle_enabled;
		return $result;
	}

	/**
	 * @param $data array
	 * @return Place|array|null
	 * @throws BadRequestHttpException
	 */
	public static function getPlace($data)
	{
		$place = Place::findOne(['googleId' => [$data['text'], $data['googleId']]]);
		if ($place === null) {
			$place = new Place();
			$place->googleId = $data['text'];
			$place->name = $data['name'];
			$place->country = $data['country'];
			$place->region = $data['region'];
			$place->lat = $data['center']['lat'];
			$place->lng = $data['center']['lng'];
			$place->radius = $data['radius'];
			$place->northEastLat = $data['corners']['ne']['lat'];
			$place->northEastLng = $data['corners']['ne']['lng'];
			$place->southWestLat = $data['corners']['sw']['lat'];
			$place->southWestLng = $data['corners']['sw']['lng'];

			if ($place->validate()) {
				$place->save();
			} else {
				return [
					'error' => Yii::t('app', 'Add city error'),
					'allErrors' => $place->getErrors()
				];
			}
		}
		if ($place->northEastLat != $data['corners']['ne']['lat']){
			$place->northEastLat = $data['corners']['ne']['lat'];
			$place->save();
		}
		if ($place->googleId != $data['text']){
			$place->googleId = $data['text'];
			$place->save();
		}
		if ($place->northEastLng != $data['corners']['ne']['lng']){
			$place->northEastLng = $data['corners']['ne']['lng'];
			$place->save();
		}
		if ($place->southWestLat != $data['corners']['sw']['lat']){
			$place->southWestLat = $data['corners']['sw']['lat'];
			$place->save();
		}
		if ($place->southWestLng != $data['corners']['sw']['lng']){
			$place->southWestLng = $data['corners']['sw']['lng'];
			$place->save();
		}
		if (!$place->country && $data['country']){
			$place->country = $data['country'];
			if ($place->validate()) {
				$place->save();
			}
		}
		if (!$place->region && $data['region']){
			if ($data['region'] == 'false'){
				$data['region'] = $data['country'];
			}
			$place->region = $data['region'];
			if ($place->validate()) {
				$place->save();
			}
		}
		return $place;
	}

	/**
	 * @param $account Account
	 * @param $data    array
	 * @return array
	 * @throws BadRequestHttpException
	 * @throws \Exception
	 */
	private function placeDelete($account, $data)
	{
		if (!isset($data['id'])) {
			throw new BadRequestHttpException(Yii::t('app', 'Error request params'));
		}

		Yii::$app->db->createCommand(sprintf(
			'DELETE FROM %s WHERE account_id = %d AND place_id = %d',
			AccountPlace::tableName(),
			$account->id,
			$data['id']
		))->execute();
		Yii::$app->db->createCommand(sprintf(
			'DELETE FROM %s WHERE account_id = %d AND place_id = %d',
			AccountPlaceGeotag::tableName(),
			$account->id,
			$data['id']
		))->execute();

		/** @var Place $place */
		$place = Place::findOne($data['id']);
		if ($place === null) {
			return [
				'action' => 'error',
				'text' => Yii::t('app', 'City ID #{id} not found', ['id' => $data['id']])
			];
		}

		$account->unlink('places', $place, true);

		return [
			'action' => 'delete',
			'id' => $place->id
		];
	}

	public function actionSearchBy($id)
	{
		if (!Yii::$app->request->isAjax) {
			throw new BadRequestHttpException();
		}

		Yii::$app->response->format = Response::FORMAT_JSON;
		$account = $this->checkPermits($id);

		$value = Yii::$app->request->post('newValue', Options::SEARCH_PLACE);
		$account->options->search_by = $value;

		if ($account->options->validate()) {
			$account->options->skipRelations = true;
			$account->options->save();
		} else {
			throw new BadRequestHttpException(Yii::t('app', 'Error request params'));
		}

		$account->use_trusted = 0;
		$account-> save();


		return [
			'action' => 'search_by',
			'value' => $account->options->search_by
		];
	}

	public function actionHashtags($id)
	{
		if (!Yii::$app->request->isAjax) {
			throw new BadRequestHttpException();
		}

		Yii::$app->response->format = Response::FORMAT_JSON;
		$account = $this->checkPermits($id);

		$action = Yii::$app->request->post('action', null);
		if ($action === null) {
			throw new BadRequestHttpException(Yii::t('app', 'Error request params'));
		}

		if ($action != 'list'){
			$account->use_trusted = 0;
			$account-> save();
		}

		$hashtag = Yii::$app->request->post('hashtag', []);

		switch ($action) {
			case 'list':
				$result = ['action' => 'list', 'data' => $account->hashtags];
				break;
			case 'create':
				$result = $this->hashtagCreate($id, $hashtag);
				break;
			case 'delete':
				$result = $this->hashtagDelete($id, $hashtag);
				break;
			default:
				throw new BadRequestHttpException(Yii::t('app', 'Error request params'));
		}

		$result['type'] = 'hashtags';
		return $result;
	}

	private function hashtagCreate($account_id, $data)
	{
		if (!isset($data['name']) || empty($data['name'])) {
			throw new BadRequestHttpException(Yii::t('app', 'Error request params'));
		}

		$tags = mb_split('[,;|]+', $data['name']);

		$result = [];
		foreach ($tags as $name) {
			/** @var AccountHashtags $tag */
			$tag = $this->hashtagAdd($account_id, $name);
			if ($tag !== false) {
				$result[] = $tag;
			}
		}

		if (!count($result)) {
			throw new BadRequestHttpException();
		}

		return [
			'action' => 'create',
			'data'   => $result
		];
	}

	private function hashtagAdd($account_id, $name)
	{
		$hashtag = new AccountHashtags();

		$hashtag->account_id = $account_id;
		$hashtag->name = $name;

		if ($hashtag->validate() && $hashtag->save()) {
			return $hashtag;
		}

		return false;
	}

	private function hashtagDelete($account_id, $data)
	{
		if (!isset($data['id'])) {
			throw new BadRequestHttpException(Yii::t('app', 'Error request params'));
		}

		AccountHashtags::deleteAll([
			'account_id' => $account_id,
			'id' => $data['id']
		]);

		return [
			'action' => 'delete',
			'id' => $data['id']
		];
	}

	public function actionCompetitors($id)
	{
		if (!Yii::$app->request->isAjax) {
			throw new BadRequestHttpException();
		}

		Yii::$app->response->format = Response::FORMAT_JSON;
		$account = $this->checkPermits($id);

		$action = ArrayHelper::getValue(Yii::$app->request->post(), 'action', null);
		if ($action === null) {
			throw new BadRequestHttpException(Yii::t('app', 'Error request params'));
		}

		if ($action != 'list'){
			$account->use_trusted = 0;
			$account-> save();
		}

		$competitor = ArrayHelper::getValue(Yii::$app->request->post(), 'competitor', []);

		switch ($action) {
			case 'list':
				$result = $this->competitorsList($account);
				break;
			case 'create':
				$result = $this->competitorCreate($account, $competitor);
				break;
			case 'delete':
				$result = $this->competitorDelete($id, $competitor);
				break;
			default:
				throw new BadRequestHttpException(Yii::t('app', 'Error request params'));
		}

		$result['type'] = 'competitors';
		return $result;
	}

	/**
	 * @param $account Account
	 * @return array
	 */
	private function competitorsList($account)
	{
		$list = [];
		foreach ($account->competitors as $item) {
			/** @var AccountCompetitors $item */
			$list[] = [
				'id'       => $item->id,
				'name'     => $item->login,
				'complete' => $item->complete
			];
		}

		return [
			'action' => 'list',
			'data'   => $list
		];
	}

	private function competitorCreate($account, $data)
	{
		if (!isset($data['name'])) {
			throw new BadRequestHttpException(Yii::t('app', 'Error request params'));
		}

		$names = explode(',', trim($data['name']));

		$data = [];

		foreach ($names as $name) {
			$competitor = new AccountCompetitors();
			$competitor->account    = $account;
			$competitor->account_id = $account->id;
			$competitor->login      = trim($name);

			if ($competitor->validate()) {
				$competitor->save();
				$data[] = [
					'id' => $competitor->id,
					'name' => $competitor->login,
				];
			}
		}

		return [
			'action' => 'create',
			'data' => $data
		];
	}

	private function competitorDelete($account_id, $data)
	{
		if (!isset($data['id'])) {
			throw new BadRequestHttpException(Yii::t('app', 'Error request params'));
		}

		AccountCompetitors::deleteAll([
			'account_id' => $account_id,
			'id'         => (int)$data['id']
		]);

		return [
			'action' => 'delete',
			'id'     => $data['id']
		];
	}

	public function actionRegions($id)
	{
		if (!Yii::$app->request->isAjax) {
			throw new BadRequestHttpException();
		}

		Yii::$app->response->format = Response::FORMAT_JSON;

		/* @var $account Account */
		$account = $this->checkPermits($id);

		$action = ArrayHelper::getValue(Yii::$app->request->post(), 'action', null);
		$region_id = ArrayHelper::getValue(Yii::$app->request->post(), 'id', null);
		if ($action === null || $region_id === null) {
			throw new BadRequestHttpException(Yii::t('app', 'Error request params'));
		}

		/* @var $region Regions */
		$region = Regions::findOne($region_id);
		if ($region === null) {
			throw new NotFoundHttpException(Yii::t('app', 'Region not found'));
		}

		$account->use_trusted = 0;
		$account-> save();

		switch ($action) {
			case 'append':
				$account->link('regions', $region);
				break;
			case 'remove':
				$account->unlink('regions', $region, true);
				break;
			default:
				return ['status' => 'error'];
		}
		return ['status' => 'ok'];
	}

	public function actionGeotagsList($id, $place_id)
	{
		if (!Yii::$app->request->isAjax) {
			throw new BadRequestHttpException();
		}

		Yii::$app->response->format = Response::FORMAT_JSON;

		$account = $this->checkPermits($id);

		$action = Yii::$app->request->post('action', 'list');
		try {
			switch ($action) {
				case 'save':
					$data = Yii::$app->request->post('data', []);
					$result = $this->saveGeotagsSettings($account, $place_id, $data);
					break;

				case 'search':
					$query  = Yii::$app->request->post('query', '');
					$coords = Yii::$app->request->post('coords', null);
					$result = $this->searchAccountsGeotags($account, Place::findOne($place_id), $query, $coords);
					break;

				case 'autocomplete':
					$query  = Yii::$app->request->post('query', '');
					$coords = Yii::$app->request->post('coords', null);
					$result = $this->searchAutocomplete($account, Place::findOne($place_id), $query, $coords);
					break;

				case 'switch-tag':
					$tag_id = Yii::$app->request->post('tag_id');
					$active = Yii::$app->request->post('active', false);
					$result = $this->switchGeotag($account, $place_id, $tag_id, $active);
					break;

				case 'circle-changed':
					$active = Yii::$app->request->post('active', false);
					$center = Yii::$app->request->post('center', []);
					$radius = Yii::$app->request->post('radius');
					$result = $this->circleChanged($account, $place_id, $active, $center, $radius);
					$enabledTags = Yii::$app->request->post('enabledTags', []);
					$this->switchGeotagsMultiple($account, $place_id, $enabledTags);
					break;

				case 'hint-tour-end';
					$result = $this->geotagsHintsComplete();
					break;

				default:
				case 'list':
					$coords = Yii::$app->request->get('coords', null);
					$result = $this->getAccountGeotags($account, $place_id, $coords);
					//
					break;
			}
		} catch (\Exception $e) {
			if (YII_DEBUG) {
				throw $e;
			}
			$result = [
				'status' => 'error',
				'message' => $e->getMessage()
			];
		}

		return $result;
	}

	private function geotagsHintsComplete()
	{
		$hints = Yii::$app->user->identity->hints;
		$hints->geotags = true;
		return [
			'status' => $hints->save() ? 'ok' : 'error'
		];
	}

	/**
	 * @param Account    $account
	 * @param integer    $place_id
	 * @param array|null $coords
	 * @return array
	 */
	public function getAccountGeotags($account, $place_id, $coords = null)
	{
		$data = AccountPlaceGeotag::find()
			->where(['account_id' => $account->id, 'place_id' => $place_id])
			->orderBy([
				'enabled'      => SORT_DESC,
				'fb_geotag_id' => SORT_ASC,
			])
			->with('tag')
			->all();

		if (count($data) === 0) {
			set_time_limit(0);
			$place = Place::findOne($place_id);
			return $this->searchAutocomplete($account, $place, $place->name, $coords);
		}

		$result = [];
		foreach ($data as $item) {
			$result[] = AccountPlaceGeotag::toOptionsArray($item);
		}

		return $result;
	}

	/**
	 * @param Account $account
	 * @param integer $place_id
	 * @param integer $tag_id
	 * @param boolean $state
	 * @return array
	 */
	public function switchGeotag($account, $place_id, $tag_id, $state)
	{
		$tag = AccountPlaceGeotag::findOne([
			'account_id'   => $account->id,
			'place_id'     => $place_id,
			'fb_geotag_id' => $tag_id,
		]);
		if (is_null($tag)) {
			return ['status' => 'error'];
		}
		$tag->enabled = $state ? 1 : 0;
		return [
			'status' => $tag->save() ? 'ok' : 'error'
		];
	}

	/**
	 * @param Account $account
	 * @param integer $place_id
	 * @param array   $enabled_tags
	 */
	public function switchGeotagsMultiple($account, $place_id, $enabled_tags)
	{
		Yii::$app->db->createCommand(sprintf(
			'UPDATE %s SET enabled = false WHERE account_id = %d AND place_id = %d',
			AccountPlaceGeotag::tableName(),
			$account->id,
			$place_id
		))->execute();

		if (is_array($enabled_tags) && count($enabled_tags)) {
			Yii::$app->db->createCommand(sprintf(
				'UPDATE %s SET enabled = true WHERE account_id = %d AND place_id = %d AND fb_geotag_id IN (%s)',
				AccountPlaceGeotag::tableName(),
				$account->id,
				$place_id,
				implode(',', $enabled_tags)
			))->execute();
		}
	}

	/**
	 * @param Account $account
	 * @param integer $place_id
	 * @param boolean $state
	 * @param array $center
	 * @param float $radius
	 * @return array
	 */
	public function circleChanged($account, $place_id, $state, $center, $radius)
	{
		$place = AccountPlace::findOne([
			'account_id' => $account->id,
			'place_id'   => $place_id,
		]);
		if (is_null($place)) {
			return ['status' => 'error'];
		}
		$place->circle_enabled = $state ? 1 : 0;
		if (!empty($center['lat']) && !empty($center['lng'])) {
			$place->setCircleData($center, $radius);
		}
		return [
			'status' => $place->save() ? 'ok' : 'error'
		];
	}

	/**
	 * @param Account    $account
	 * @param Place      $place
	 * @param string     $query
	 * @param array|null $coords
	 * @return array
	 */
	private function searchAccountsGeotags($account, $place, $query, $coords = null)
	{
		$data = ['query' => $query];
		if (is_array($coords)) {
			$data['coords'] = [
				'lat' => ArrayHelper::getValue($coords, 'lat'),
				'lng' => ArrayHelper::getValue($coords, 'lng')
			];
		}

		$result = $account->getPostInfo('geotags', $account->user_id, $data);
		$data = [];
		foreach ($result['data']['items'] as $item) {
			if (empty($item['location']) || empty($item['location']['lat']) || empty($item['location']['lng'])) {
				continue;
			}
			if (!$place->containsPoint($item['location']['lat'], $item['location']['lng'])) {
				continue;
			}
			$data[] = $this->getFbGeotag($account, $place, $item['location']);
		}

		$result = [];
		foreach ($data as $item) {
			$result[] = AccountPlaceGeotag::toOptionsArray($item);
		}

		return $result;
	}

	/**
	 * @param $account   Account
	 * @param $place     Place
	 * @param string     $query
	 * @param array|null $coords
	 * @return array
	 */
	private function searchAutocomplete($account, $place, $query, $coords = null)
	{
		$result = [];
		if (mb_strlen($query) < 3) {
			return $result;
		}

		$place = Place::findOne($place->id);
		$tagsList = self::getGeotagsList($account, $query, $coords, $place);

		$ids = [];
		foreach ($tagsList as $item)
		{
			$tag = ArrayHelper::getValue($item, 'location');
			if (empty($tag) || empty($tag['lat']) || empty($tag['lng'])) {
				continue;
			}
			$ids[] = $tag['pk'];
		}
		Yii::warning($ids);

		$subQuery = FbGeotag::find()
			->select('id')
			->where(['in', 'fb_id', $ids])
			->createCommand()
			->getRawSql();

		$userGeotagsQuery = AccountPlaceGeotag::find()
			->with('tag')
			->where([
				'place_id' => $place->id,
				'account_id' => $account->id,
			])->andWhere('fb_geotag_id IN (' . $subQuery . ')');

		foreach ($userGeotagsQuery->all() as $userTag) {
			$result[] = AccountPlaceGeotag::toOptionsArray($userTag);
		}

		if (empty($result) && !empty($ids)) {
			$this->fillGeotagsList($account, $place, $ids);
			foreach ($userGeotagsQuery->all() as $userTag) {
				$result[] = AccountPlaceGeotag::toOptionsArray($userTag);
			}
		}

		return $result;
	}

	/**
	 * @param Account $account
	 * @param Place   $place
	 * @param array   $ids
	 */
	public function fillGeotagsList($account, $place, $ids)
	{
		Yii::$app->db->createCommand(sprintf(
			'INSERT INTO account_place_geotags (account_id, place_id, fb_geotag_id, enabled)
			SELECT %d, %d, id, true FROM fb_geotags WHERE fb_id IN (%s)
			ON CONFLICT (account_id, place_id, fb_geotag_id) DO NOTHING;',
			$account->id,
			$place->id,
			implode(',', $ids)
		))->execute();
	}

	/**
	 * @param Account    $account
	 * @param string     $query
	 * @param array|null $coords
	 * @param Place|null $place
	 * @return array
	 */
	public static function getGeotagsList($account, $query, $coords = null, $place = null)
	{
		$data = ['query' => $query];
		if (is_array($coords)) {
			$data['coords'] = [
				'lat' => ArrayHelper::getValue($coords, 'lat'),
				'lng' => ArrayHelper::getValue($coords, 'lng')
			];
		}

		$cacheKey = md5(mb_strtolower(serialize($data)));
		$cacheData = Yii::$app->cache_mongo->get($cacheKey);
		if ($cacheData !== false) {
			return $cacheData;
		}

		$postInfo = $account->getPostInfo('geotags', $account->user_id, $data);

		if (empty($postInfo['data']) || empty($postInfo['data']['items'])) {
			return [];
		}

		$result = $postInfo['data']['items'];
		if (!count($result)) {
			return [];
		}

		if ($place !== null) {
			if (!self::saveGeotags($account, $place, $result)) {
				return [];
			}
		}

		Yii::$app->cache_mongo->set($cacheKey, $result, 86400);

		return $result;
	}

	/**
	 * @param Account $account
	 * @param Place   $place
	 * @param array   $data
	 * @return boolean
	 */
	public static function saveGeotags($account, $place, &$data)
	{
		$values = [];
		$ids = [];
		foreach ($data as $key => $item) {
			$tag = ArrayHelper::getValue($item, 'location');
			if (empty($tag) || empty($tag['lat']) || empty($tag['lng'])) {
				unset($data[$key]);
				continue;
			}
			if (!$place->containsPoint($tag['lat'], $tag['lng'])) {
				unset($data[$key]);
				continue;
			}
			$values[] = sprintf(
				"(%d, %s, '(%f,%f)', %s)",
				$tag['pk'],
				Yii::$app->db->quoteValue($tag['name']),
				$tag['lat'],
				$tag['lng'],
				Yii::$app->db->quoteValue($tag['address'])
			);
			$ids[] = $tag['pk'];
		}

		if (!count($values)) {
			return false;
		}

		Yii::$app->db->createCommand('
				INSERT INTO fb_geotags
					(fb_id, "name", coords, address)
				VALUES ' . implode(',', $values) . '
				ON CONFLICT (fb_id) DO UPDATE SET
					"name"  = EXCLUDED."name",
					coords  = EXCLUDED.coords,
					address = EXCLUDED.address;'
		)->execute();

		Yii::$app->db->createCommand('
			INSERT INTO account_place_geotags
			SELECT ' . $account->id . ', ' . $place->id . ', id, false FROM fb_geotags
			WHERE fb_id IN (' . implode(',', $ids) . ')
			ON CONFLICT (account_id, place_id, fb_geotag_id) DO NOTHING;'
		)->execute();

		return true;
	}

	/**
	 * @param $account  Account
	 * @param $place_id integer
	 * @param $data     array
	 * @return array
	 */
	public function saveGeotagsSettings($account, $place_id, $data)
	{
		set_time_limit(0);
		$circle = ArrayHelper::getValue($data, 'circle');
		if (!is_null($circle)) {
			$accPlace = AccountPlace::findOne([
				'account_id' => $account->id,
				'place_id' => $place_id,
			]);
			if (is_null($accPlace)) {
				$accPlace = new AccountPlace([
					'account_id' => $account->id,
					'place_id' => $place_id,
				]);
			}
			$accPlace->circle_enabled = !!$circle['enabled'];
			$accPlace->setCircleData($circle['center'], $circle['radius']);
			$accPlace->save();
		}

		$items = ArrayHelper::getValue($data, 'tags', []);
		foreach ($items as $item) {
			$accTag = AccountPlaceGeotag::findOne([
				'account_id'   => $account->id,
				'place_id'     => $place_id,
				'fb_geotag_id' => $item['id'],
			]);
			if (is_null($accTag)) {
				$accTag = new AccountPlaceGeotag([
					'account_id'   => $account->id,
					'place_id'     => $place_id,
					'fb_geotag_id' => $item['id'],
				]);
			}
			$accTag->enabled = boolval($item['enabled']);
			Yii::warning($item['enabled']);
			Yii::warning($accTag->enabled);
			$accTag->save(false);
		}

		return ['status' => 'ok'];
	}

	/**
	 * @param $account    Account
	 * @param $place      Place
	 * @param $data       array
	 * @return AccountPlaceGeotag
	 */
	private function getFbGeotag($account, $place, $data)
	{
		$tag = FbGeotag::findOne(['fb_id' => $data['pk']]);
		if (is_null($tag)) {
			if (empty($data['address'])) {
				$address = $data['city'];
			} else {
				$address = $data['address'] . (!empty($data['city']) ? ', ' . $data['city'] : '');
			}
			$tag = new FbGeotag([
				'fb_id'      => $data['pk'],
				'name'       => $data['name'],
				'coords'     => $data['lat'] . ',' . $data['lng'],
				'address'    => $address,
			]);
			$tag->save();
		}

		$accTag = AccountPlaceGeotag::findOne([
			'account_id'   => $account->id,
			'place_id'     => $place->id,
			'fb_geotag_id' => $tag->id
		]);
		if (is_null($accTag)) {
			$accTag = new AccountPlaceGeotag([
				'account_id'   => $account->id,
				'place_id'     => $place->id,
				'fb_geotag_id' => $tag->id,
				'enabled'      => true
			]);
			$accTag->save();
		}

		return $accTag;
	}

	public function actionReset($id = null)
	{
		if ($id === null) {
			return $this->goHome();
		}

		/* @var $account Account */
		$account = $this->checkPermits($id);

		$account->options->likes = 1;
		$account->options->comment = 0;
		$account->options->follow = 1;
		$account->options->unfollow = 0;
		$account->options->mutual = 1;
		$account->options->autounfollow = 1;
		$account->options->firmcomments = 1;
		$account->options->prev_mask = 0;
		$account->options->search_by = Options::SEARCH_PLACE;
		$account->options->skipRelations = true;
		$account->options->save();

		Yii::$app->db->createCommand(sprintf(
			"DELETE FROM comments WHERE id IN (
				SELECT type_id FROM relations WHERE account_id = %d AND type = 'comments'
			);",
			$id
		))->execute();
		Yii::$app->db->createCommand(sprintf(
			"DELETE FROM relations WHERE account_id = %d AND type = 'comments';",
			$id
		))->execute();

		$account->unlinkAll('commentsList', true);
		$account->fillFirmComments();

		AccountHashtags::deleteAll(['account_id' => $id]);
		$account->unlinkAll('regions', true);
		$account->unlinkAll('places', true);

		$account->message = '';
		$account->save();

		return $this->goHome();
	}

	public function actionPropagateComments($id)
	{
		set_time_limit(0);
		/* @var $account Account */
		$account = $this->checkPermits($id);

		$query = Account::find()->where(['user_id' => $account->user_id]);
		foreach ($query->each(20) as $user_account)
		{
			/** @var Account $user_account */
			if ($user_account->id === $account->id) {
				continue;
			}
			$user_account->unlinkAll('commentsList', true);
			Yii::$app->db->createCommand(sprintf(
				'INSERT INTO %1$s (account_id, text, is_firm, spam)
				SELECT %2$d, text, 0, 0 FROM %1$s
				WHERE account_id = %3$d;',
				AccountComments::tableName(),
				$user_account->id,
				$account->id
			))->execute();
		}
		return $this->redirect(['/options/index', 'id' => $account->id]);
	}

	/**
	 * @param int $id account Id
	 *
	 * @throws \yii\web\BadRequestHttpException
	 * @throws \yii\web\ForbiddenHttpException
	 *
	 * @return Account
	 */
	private function checkPermits($id)
	{
		if (Yii::$app->user->isGuest) {
			throw new ForbiddenHttpException(Yii::t('app', 'You do not have permission to watch this'));
		}

		$account = Account::findOne($id);
		if ($account === null) {
			throw new BadRequestHttpException(Yii::t('app', 'Account id #{id} not found', ['id' => $id]));
		}

		if (Yii::$app->user->identity->isAdmin) {
			return $account;
		}

		/* @var $account Account */
		if ($account->user_id !== Yii::$app->user->id) {
			throw new ForbiddenHttpException(Yii::t('app', 'You do not have permission to watch this'));
		}

		return $account;
	}

	public function actionAddAutoFillDonors()
	{
		if (empty(Yii::$app->request->get('donor_login'))){
			throw new BadRequestHttpException(Yii::t('app', 'Error request params'));
		}


		$aFDonor = new AutoFillDorons();
		$aFDonor->donor_login = Yii::$app->request->get('donor_login');
		$aFDonor->account_id = Yii::$app->request->get('account_id');
		$aFDonor->user_id = Yii::$app->user->id;
		$account = $this->checkPermits($aFDonor->account_id);
		$aFDonor->account = $account;

		if ($aFDonor->validate()) {
			$aFDonor->save();
			$url = '/options/index/' . Yii::$app->request->get('account_id') .'#auto-fill-settings';
			return $this->redirect($url);
		}else{
			echo 'Возникла ошибка';
			var_dump($aFDonor->errors);
		}
	}

	public function actionDeleteAutoFillDonors()
	{
		// *TODO* потом нужно сделать через ajax
		$donorId = Yii::$app->request->get('donor_id');
		$accountID = Yii::$app->request->get('account_id');
		// Удаляем донора
		// *TODO* сделать проверку владельца аккаунта
		Yii::$app->db->createCommand()->delete('auto_fill_dorons', ['id'=> $donorId, 'account_id' => $accountID])->execute();
		$url = '/options/index/' . Yii::$app->request->get('account_id') .'#auto-fill-settings';
		return $this->redirect($url);
	}

	public function actionSaveAutoFillDonorsFreq()
	{
		// *TODO* нужно сделать проверку, что акк принадлежит юзеру
		$accountID = Yii::$app->request->get('account_id');
		$freq = Yii::$app->request->get('freq');
		$option = Options::findOne(['account_id' => $accountID]);
		/** @var Options $option */
		if ($freq >= 1) {
			$option->auto_fill_freq = (int)$freq;
			$option->save();
		}
		$url = '/options/index/' . Yii::$app->request->get('account_id') .'#auto-fill-settings';
		return $this->redirect($url);
	}

	public function actionChangeAutoFillMode()
	{
		// *TODO* нужно сделать проверку, что акк принадлежит юзеру
		$accountID = Yii::$app->request->get('account_id');
		$mode = Yii::$app->request->get('mode');
		$option = Options::findOne(['account_id' => $accountID]);
		/** @var Options $option */
		$option->auto_fill_mode = $mode;
		if ($mode == 1){
			// Проверяем, установлена ли дата
			if ($option->auto_fill_start_date == null){
				$option->auto_fill_start_date = date('Y-m-d H:i:s');
			}
		}
		$option->save();

		// Перезапускаем аккаунт, если он работает
		$account = Account::findOne($accountID);
		/** @var Account $account */
		if($account->monitoring_status == 'work') {
			//$account->accountStop(true);
			//$account->accountStart(true);
			RPCHelper::stopAccount($account, 'user');
			RPCHelper::startAccount($account, 'user');
		}

		$url = '/options/index/' . Yii::$app->request->get('account_id') .'#auto-fill-settings';
		return $this->redirect($url);
	}

	public function actionChangeSettingsAutoFill()
	{
		// *TODO* нужно сделать проверку, что акк принадлежит юзеру
		$accountID = Yii::$app->request->get('account_id');

		$follows_max_during_period = Yii::$app->request->get('follows_max_during_period', 0);
		$follows_min_during_period = Yii::$app->request->get('follows_min_during_period', 0);
		$likes_after_follow_min = Yii::$app->request->get('likes_after_follow_min', 0);
		$likes_after_follow_max = Yii::$app->request->get('likes_after_follow_max', 0);
		$follow_percent_clients = Yii::$app->request->get('follow_percent_clients', 0);

		$option = Options::findOne(['account_id' => $accountID]);
		/** @var Options $option */
		$option->auto_fill_follows_max_during_period = $follows_max_during_period;
		$option->auto_fill_follows_min_during_period = $follows_min_during_period;
		$option->auto_fill_likes_after_follow_min = $likes_after_follow_min;
		$option->auto_fill_likes_after_follow_max = $likes_after_follow_max;
		$option->auto_fill_follow_percent_clients = $follow_percent_clients;
		$option->save();

		// Обновляем настройки на воркере, вызывая метод из Gearman Trait
		//$account = Account::findOne($accountID);
		/** @var Account $account */
		//$account->findUser($account->login);

		$url = '/options/index/' . Yii::$app->request->get('account_id') .'#auto-fill-settings';
		return $this->redirect($url);
	}
}
