<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "account_action_log".
 *
 * @property integer $id
 * @property integer $account_id
 * @property string $action
 * @property string $operation
 * @property string $time
 */
class AccountActionLog extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'account_action_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_id', 'action', 'operation'], 'required'],
            [['account_id'], 'integer'],
            [['action', 'operation'], 'string'],
            [['time'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'account_id' => Yii::t('app', 'Account ID'),
            'action' => Yii::t('app', 'Action'),
            'operation' => Yii::t('app', 'Operation'),
            'time' => Yii::t('app', 'Time'),
        ];
    }

	public static function log($data)
	{
		$log = new AccountActionLog($data);
		$log->save();
		return $log;
    }


	/**
	 *
	 * Возвращает массив формата:
	 * [
	 * common - 40593
	 * likes - 40593
	 * follows - 0
	 * comments - 0
	 * unfollows - 40593
	 * ]
	 *
	 * Указано время работы акка по функциям в секундах. common - общее время работы.
	 *
	 * @param $account_id
	 * @param $date_from
	 * @param $date_to
	 * @return array
	 */
	public static function getActivityTime($account_id, $date_from = null, $date_to = null)
	{
		if (is_null($date_from)) $date_from = date('Y-m-d');
		if (is_null($date_to)) $date_to = date('Y-m-d', time() + 86400);

		$stat = AccountStats::findOne([
			'account_id' => $account_id,
			'date' => $date_from
		]);
		return self::getActionsTime($stat, $date_from, $date_to);
    }

	public static function getActionsTime($stat, $date_from, $date_to)
	{
		if (!$stat) {
			return [
				'common' => 0
			];
		}

		$actions_log = \Yii::$app->db->createCommand(sprintf(
			'SELECT
				action, operation, extract(epoch FROM time) AS time
			FROM
 				account_action_log
 			WHERE
 				time >= \'%s\' AND time < \'%s\' AND account_id = %d
 			GROUP BY time, action, operation
 			
 			ORDER BY action ASC, time ASC;',
			$date_from,
			$date_to,
			$stat->account_id
		))->queryAll(\PDO::FETCH_ASSOC);

		$common = [];

		foreach ($actions_log as $row) {
			if (!empty($common[intval($row['time'])])) {
				continue;
			} else {
				$row['action'] = 'common';
				$common[$row['time']] = $row;
			}
		}

		array_merge($actions_log, $common);
		
		$start_time = strtotime($date_from);

		$operations = [
			'common' => [
				'name' => 'common',
				'duration' => 0,
				'start' => 0,
				'active' => null,
			],
			'likes' => [
				'name' => 'likes',
				'duration' => 0,
				'start' => 0,
				'active' => null,
			],
			'follows' => [
				'name' => 'follows',
				'duration' => 0,
				'start' => 0,
				'active' => null,
			],
			'comments' => [
				'name' => 'comments',
				'duration' => 0,
				'start' => 0,
				'active' => null,
			],
			'unfollows' => [
				'name' => 'unfollows',
				'duration' => 0,
				'start' => 0,
				'active' => null,
			],
		];

		foreach ($common as $row) {

			$action = $row['action'];

			$date = new \DateTime();
			$date->setTimestamp((int) $row['time']);
			$date->setTimezone(new \DateTimeZone('GMT'));

			$action_time = strtotime($date->format('Y-m-d H:i:s'));

			// Костыль
			if ($action !== 'likes' and $action !== 'common') {
				$action .= 's';
			}

			// Если не задан начальный статус, вкл или выкл
			if (is_null($operations[$action]['active'])) {
				$operations[$action]['active'] = ($row['operation'] === 'stop');

				// Если вкл, значит принимаем время старта
				if ($operations[$action]['active']) {
					$operations[$action]['start'] = $start_time;
				}
			}

			if (!$operations[$action]['active'] && $row['operation'] === 'start') {
				// Если выкл и стартуем
				$operations[$action]['active'] = true;
				$operations[$action]['start'] = $action_time;
			} else if ($operations[$action]['active'] && $row['operation'] === 'stop') {
				// Если вкл и останавливаем
				$operations[$action]['active'] = false;
				$operations[$action]['duration'] += $action_time - $operations[$action]['start'];
				$operations[$action]['start'] = (0);
			}
		}

		$time = strtotime($date_to);

		if ($time > time()) $time = time();
		
		foreach ($operations as $operation => $data) {
			if ($operation == 'common') {
				$option = ($stat->likes > 0 || $stat->follows > 0 || $stat->comments > 0 || $stat->unfollows > 0);
			} else {
				$option = !empty($stat->$operation);
			}

			// Если нет информации о включении и есть действия, значит работало, и до сих пор работает
			if (is_null($data['active']) && $option) {
				$operations[$operation]['active'] = true;
				$operations[$operation]['start'] = $start_time;
			}

			// Если включена, и время старта меньше текущего времени, значит до сих пор работает
			if ($operations[$operation]['active'] && $operations[$operation]['start'] < $time) {
				$operations[$operation]['duration'] += $time - $operations[$operation]['start'];
			}
		}

		//unset($operations['commons']);

		return ArrayHelper::map($operations, 'name', 'duration');
	}
}
