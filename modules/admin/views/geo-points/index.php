<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View
 * @var $places array
 */

$this->title = 'Рабочие точки';
?>

<div class="site-admin">
	<h1><?= Html::encode($this->title) ?></h1>

	<?php ActiveForm::begin([
		'action' => ['/admin/geo-points/remove'],
		'validateOnSubmit' => false,
		'options' => [
			'onsubmit' => 'return confirm("Вы уверены, что хотите удалить выбранные списки?");'
		]
	]) ?>
	<form method="post" action="<?= Url::to(['/admin/geo-points/remove']) ?>">
	<table class="table table-hover">
		<thead>
			<tr>
				<th><input type="checkbox" onchange="$(this).parents('table').find('input[name]').prop('checked', this.checked);"></th>
				<th>Название города</th>
				<th class="text-center" colspan="2">Кол-во записей</th>
				<th width="1%"></th>
			</tr>
			<tr>
				<th></th>
				<th></th>
				<th width="1%" class="text-center">сетка</th>
				<th width="1%" class="text-center">рабочие</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($places as $place) : ?>
			<tr>
				<td><input type="checkbox" name="id[]" value="<?= $place['id'] ?>"></td>
				<td><?= Html::a($place['name'], ['/admin/geo-points', 'id' => $place['id']]) ?></td>
				<td class="text-center"><?= $place['covers'] ?></td>
				<td class="text-center <?= $place['points'] < 5 ? 'warning' : '' ?>">
					<?= $place['points'] ?>
				</td>
				<td>
					<?= Html::a(
							Html::tag('span', '', ['class' => 'glyphicon glyphicon-trash']),
							['/admin/geo-points/remove', 'id' => $place['id']],
							[
								'class' => 'btn btn-link',
								'onclick' => 'return confirm("Вы уверены, что хотите удалить список?")'
							]
					) ?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<?= Html::submitButton('Удалить выбранное', [
		'class' => 'btn btn-danger'
	]) ?>
	<?php ActiveForm::end(); ?>
</div>
