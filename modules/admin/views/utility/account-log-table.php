<?php

/* @var $this yii\web\View
 * @var $dataProvider
 */

use app\models\AccountProcessLog;
use core\logger\Logger;
use yii\bootstrap\Html;

echo \yii\grid\GridView::widget([
	'dataProvider' => $dataProvider,
	'rowOptions' => function ($item) {
		/** @var AccountProcessLog $item */

		$result = 'level-'.$item->level;

		if ($item->environment != 0) {
			$result .= ' extender-group';
		}

		return [
			'class' => $result,
		];
	},
	'layout' => '
		{pager}
		{summary}
		{items}
	    {pager}'
	,
	'columns'      => [
		//'environment',
		[
			'attribute' => 'worker_ip',
			'format' => 'raw',
			'value' => function($item) {
				/** @var $item AccountProcessLog */
				return Html::tag(
					'span',
					long2ip($item->worker_ip),
					[
						'class' => ($item->code == 0) ? ' no-code' : ''
					]);
			},
		],
		/*'instagram_id',*/
		[
			'attribute' => 'user_id',
			'format' => 'raw',
			'value' => function($item) {
				/** @var $item AccountProcessLog */
				return Html::a(
					$item->user_id,
					['/admin/users?UsersSearch[id]=' . $item->user_id]
				);
			},
		]
		,
		[
			'attribute' => 'date',
			'format' => 'raw',
			'value' => function($item) {
				/** @var $item AccountProcessLog */
				return '<div class="date">'.date('Y-m-d H:i:s', $item->date).'</div>';
			},
		],
		[
			'attribute' => 'description',
			'format' => 'raw',
			'value' => function($item) {
				/** @var $item AccountProcessLog */
				if (empty($item->code)) {
					return $item->description;
				}

				return Logger::getMessage($item->code, $item->description);
			},
		],
	],
]);