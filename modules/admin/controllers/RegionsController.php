<?php

namespace app\modules\admin\controllers;

use app\components\Controller;
use app\models\Regions;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class RegionsController extends Controller
{
	/**
	 * Initializes the object.
	 * This method is invoked at the end of the constructor after the object is initialized with the
	 * given configuration.
	 */
	public function init()
	{
		parent::init();

		if (!Yii::$app->user->identity->isAdmin) {
			throw new ForbiddenHttpException('У вас нет прав для просмотра этой страницы.');
		}
	}

	public function actionIndex($id = null)
	{
		if ($id === null) {
			return $this->render('index');
		} else {
			$region = Regions::findOne($id);
			if ($region === null) {
				throw new NotFoundHttpException('Регион с ID ' . $id . ' не найден');
			}
			return $this->render('region', [
				'region' => $region
			]);
		}
	}

	public function actionCreate()
	{
		$model = new Regions();
		$model->load(Yii::$app->request->post());

		// Если места заданы через запятую, разбиваем всё на массив и сохраняем
		if (strpos($model->name, ',') !== false){
			$places = explode(',',$model->name);
			$model->name = trim($places[0]);
			if ($model->validate()) {
				$model->save();
			}
			unset($places[0]);

			foreach($places as $place){
				$model = new Regions();
				$model->load(Yii::$app->request->post());
				if (trim($place) == '') {
					continue;
				}
				$model->name = trim($place);
				if ($model->validate()) {
					$model->save();
				}
			}


		}
		if ($model->validate()) {
			$model->save();
		}
		return $this->redirect(['/admin/regions']);
	}

	public function actionRemove($id)
	{
		/* @var $model Regions */
		$model = Regions::findOne($id);
		if ($model !== null) {
			$model->delete();
		}
		return $this->redirect(['/admin/regions']);
	}

}