<?php

namespace app\controllers;

use app\components\Counters;
use app\components\YMComponent;
use app\models\Discounts;
use app\models\Prices;
use app\models\Invoice;
use app\components\Controller;
use app\components\UniversalAnalyticsCookieParser;
use app\models\Users;
use app\models\UsersInfo;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use Yii;
use yii\web\ForbiddenHttpException;
use \YandexMoney\API;

class PaymentController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		if (Yii::$app->language === 'ru') {
			return [
				'resultpaymentrobo' => [
					'class'    => '\robokassa\ResultAction',
					'callback' => [$this, 'resultCallback'],
				],
				'success'           => [
					'class'    => '\robokassa\SuccessAction',
					'callback' => [$this, 'successCallback'],
				],
				'fail'              => [
					'class'    => '\robokassa\FailAction',
					'callback' => [$this, 'failCallback'],
				],
			];
		}

		return [];
	}

	public function beforeAction($action)
	{
		$this->enableCsrfValidation = false;
		if (Yii::$app->user->isGuest && $action == 'index') {
			Yii::$app->response->redirect(['/#loginModal']);
		}

		if (defined('NO_DB_CONNECTION') && NO_DB_CONNECTION && $action != 'index') {
			return $this->goHome();
		}

		return parent::beforeAction($action);
	}

	public function actionSuccessPayment($id)
	{
		/** @var Invoice $model */
		$model = Invoice::findOne(['id' => $id]);
		if ($model === null || $model->user_id !== Yii::$app->user->id) {
			return $this->goHome();
		}

		$model->redirected = 1;
		$model->save();
		return $this->render('success', [
			'model' => $model,
		]);
	}

	public function actionInvoice($id = null)
	{
		if ($id === null) {
			/** @var Discounts $discount */
			$discount = Discounts::findOne(Yii::$app->request->get('_discount', null));
			if (!is_null($discount) && !$discount->checkDate()) {
				$discount = null;
			}

			$priceId = Yii::$app->request->get('price');
			if (!$priceId) {
				Yii::$app->response->redirect(['/page/price/']);
			}
			$model = new Invoice();
			/** @var \app\models\Prices $price */
			$price = Prices::findOne(['id' => $priceId]);
			if (!$price) {
				Yii::$app->response->redirect(['/page/price/']);
			}
			$model->load(
				[
					'user_id' => Yii::$app->user->identity->getId(),
					'price_id' => $_GET['price'],
					'sum' => $price->price,
					'agent' => 'robokassa',
					'discount_id' => (!is_null($discount) ? $discount->id : null),
				], '');

			$gaData['client_id'] = UniversalAnalyticsCookieParser::getCid();
			$gaData['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
			$gaData['user_lang'] = mb_strtolower(mb_substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,5));
			$gaData['roistat_visit'] = Counters::roiCookieVisit();
			$model->ga_data = json_encode($gaData);

			$model->save();

			// Создаём платёж для roistats

			$roiInvoice = [
				'id' => $model->id,
				'name' => 'Количество дней: ' . $price->value,
				'status' => 1,
				'price' => $price->price,
				'client_id' => $model->user_id
			];
			Counters::roiInvoice($roiInvoice);
			Counters::roiStatsSendEvent('create_invoice');

			$merchant = Yii::$app->get('robokassa');
			return $merchant->payment($model->sum, $model->id, Yii::t('app', 'Balance recharge on Zengram.net for #{user}', ['user' => Yii::$app->user->identity->getId()])/*,null,$user->mail*/);
		} else {
			$model = Invoice::findOne(['id' => $id]);
		}
		/** @var \app\models\Invoice $model */
		if ($model->load(Yii::$app->request->post())) {
			/** @var \robokassa\Merchant $merchant */
			$merchant = Yii::$app->get('robokassa');
			return $merchant->payment($model->sum, $model->id, Yii::t('app', 'Balance recharge on Zengram.net for #{user}', ['user' => Yii::$app->user->identity->getId()])/*,null,$user->mail*/);
		} else {
			return $this->render(
				'invoice', [
				'model' => $model,
				'price' => Prices::findOne(['id' => $model->price_id]),
			]);
		}
	}

	public function action2coInvoice()
	{
		$priceId = Yii::$app->request->get('price');
		if (!$priceId) {
			Yii::$app->response->redirect(['/page/price/']);
		}
		/** @var \app\models\Prices $price */
		$price = Prices::findOne(['id' => $priceId]);
		if (!$price) {
			Yii::$app->response->redirect(['/page/price/']);
		}

		/** @var Discounts $discount */
		$discount = Discounts::findOne(Yii::$app->request->get('_discount', null));
		if (!is_null($discount) && !$discount->checkDate()) {
			$discount = null;
		}
		$invoice = new Invoice();

		$invoice->load([
			'user_id'     => Yii::$app->user->identity->getId(),
			'price_id'    => $priceId,
			'sum'         => $price->price,
			'agent'       => '2co',
			'discount_id' => (!is_null($discount) ? $discount->id : null),
		], '');

		$gaData['client_id'] = UniversalAnalyticsCookieParser::getCid();
		$gaData['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
		$gaData['user_lang'] = mb_strtolower(mb_substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,5));
		//$gaData['roistat_visit'] = Counters::roiCookieVisit();
		$invoice->ga_data = json_encode($gaData);

		$invoice->save();

		//Counters::roiStatsSendEvent('create_invoice');

		/** @var \Yii\twocheckout\TwoCheckout $model */
		$model = Yii::$app->get('TwoCheckout');
		$product = [];
		$product['currency_code'] = 'USD';
		$product['mode'] = '2CO';
		$product['fixed'] = '1';
		$product['user_id'] = Yii::$app->user->identity->getId();
		$product['sid'] = $model->sellerId;
		$product['demo'] = YII_ENV_DEV;
		$product['li_0_price'] = $price->price;
		$product['li_0_name'] = Yii::t('app', '{value} days on Zengram.net for #{user}', ['value' => $price->value, 'user' => Yii::$app->user->identity->getId()]);
		$product['li_0_product_id'] = $invoice->id;
		$product['li_0_quantity'] = '1';
		$product['li_0_tangible'] = 'N';
		$model->charge->form($product, 'auto');
	}

	public function actionInstashpion($method = 'yandex') {
		if (Yii::$app->user->isGuest) {
			throw new ForbiddenHttpException();
		}

		/** @var \app\models\Prices $price */
		$price = Prices::findOne(['id' => 9]);
		if (!$price) {
			Yii::$app->response->redirect(['/instashpion']);
		}

		/** @var Discounts $discount */
		$discount = Discounts::findOne(Yii::$app->request->get('_discount', null));
		if (!is_null($discount) && !$discount->checkDate()) {
			$discount = null;
		}
		$invoice = new Invoice();

		$invoice->load([
			'user_id'     => Yii::$app->user->identity->id,
			'price_id'    => 9,
			'sum'         => $price->price,
			'agent'       => $method,
			'discount_id' => null,
		], '');

		$gaData['client_id'] = UniversalAnalyticsCookieParser::getCid();
		$gaData['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
		$gaData['user_lang'] = mb_strtolower(mb_substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,5));
		$invoice->ga_data = json_encode($gaData);

		$invoice->save();
		$data = [
			'invoice' => $invoice,
			'price'   => $price,
		];

		$render = $method;

		if ($method == 'yandex') {
			$data['type'] = 'PC';
		} else if ($method == 'yandex-cards') {
			$render = 'yandex';
			$data['type'] = 'AC';
		}

		return $this->render($render, $data);
	}

	public function actionInterkassa()
	{
		if (Yii::$app->user->isGuest) {
			throw new ForbiddenHttpException();
		}

		$priceId = Yii::$app->request->get('price');
		if (!$priceId) {
			Yii::$app->response->redirect(['/page/price/']);
		}
		/** @var \app\models\Prices $price */
		$price = Prices::findOne(['id' => $priceId]);
		if (!$price) {
			Yii::$app->response->redirect(['/page/price/']);
		}

		/** @var Discounts $discount */
		$discount = Discounts::findOne(Yii::$app->request->get('_discount', null));
		if (!is_null($discount) && !$discount->checkDate()) {
			$discount = null;
		}
		$invoice = new Invoice();

		$invoice->load([
			'user_id'     => Yii::$app->user->identity->id,
			'price_id'    => $priceId,
			'sum'         => $price->price,
			'agent'       => 'interkassa',
			'discount_id' => (!is_null($discount) ? $discount->id : null),
		], '');

		$gaData['client_id'] = UniversalAnalyticsCookieParser::getCid();
		$gaData['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
		$gaData['user_lang'] = mb_strtolower(mb_substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,5));
		$gaData['roistat_visit'] = Counters::roiCookieVisit();
		$invoice->ga_data = json_encode($gaData);

		$invoice->save();

		$roiInvoice = [
			'id' => $invoice->id,
			'name' => 'Количество дней: ' . $price->value,
			'status' => 1,
			'price' => $price->price,
			'client_id' => $invoice->user_id
		];
		Counters::roiInvoice($roiInvoice);
		Counters::roiStatsSendEvent('create_invoice');


		return $this->render('interkassa', [
			'invoice' => $invoice,
			'price'   => $price,
		]);
	}

	public function actionInterkassaCallback()
	{

		$params = Yii::$app->request->get();
		$model = $this->loadModel($params['ik_pm_no']);
		if ($model->status != Invoice::STATUS_SUCCESS) {
			if ($params['ik_inv_st'] !== 'success') {
				return 'Not success';
			}
			$price = Prices::findOne(['id' => $model->price_id]);
			/** @var \app\models\Prices $price */
			if ($price->price != $params['ik_am']) {
				return 'Incorrect price';
			}

			$param_sign = $params['ik_sign'];

			unset($params['ik_sign']);
			ksort($params, SORT_STRING); // сортируем по ключам в алфавитном порядке элементы массива
			if (YII_ENV_DEV) {
				array_push($params, Yii::$app->params['interkassaTestKey']); // добавляем в конец массива "секретный ключ"
			} else {
				array_push($params, Yii::$app->params['interkassaSecretKey']); // добавляем в конец массива "секретный ключ"
			}
			$signString = implode(':', $params); // конкатенируем значения через символ ":"
			$sign = base64_encode(md5($signString, true)); // берем MD5 хэш в бинарном виде по сформированной строке и кодируем в BASE64

			//проверка подписи
			if ($param_sign !== $sign) {
				return 'Incorrect sign';
			}

			if (!$model->apply('interkassa')) {
				return 'Balance add error';
			}

			/** @var UsersInfo $usersInfo */
			$usersInfo = UsersInfo::findOne(['user_id' => $model->user_id]);
			Counters::trigger('payment_success_interkassa', $usersInfo->refferer);

			// Народ, проверьте плз, что тут всё правильно подставлено
			$user_id = $model->user_id; // id-шник клиента в зенграме
			$transaction_id = $model->id; // id-шник транзакции в invoice
			$myprice = $price->price; // Цена
			$days = $price->value; // Размер пакет услуг (сколько дней куплено)
			$method = 'InterkassaCallback';

			// Получаем ip-шник для корректного определения страны
			$user = Users::findOne($user_id);
			/** @var Users $user */
			$ip = $user->registerIP;

			// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
			// Передача информации о транзакции в GA
			Counters::gaTransaction($user_id, $transaction_id, $myprice, $days, $method, $ip, $model->ga_data);
			// ----------------------------------------------------

			// RoiStat успешная оплата
			$visit_id = null;
			if (!empty($model->ga_data)) {
				$gaData = json_decode($model->ga_data, true);
				if (isset($gaData['roistat_visit'])){
					$visit_id = $gaData['roistat_visit'];
				}
			}

			$roiInvoice = [
				'id' => $model->id,
				'name' => 'Количество дней: ' . $price->value,
				'status' => 2,
				'price' => $price->price,
				'client_id' => $model->user_id
			];
			Counters::roiInvoice($roiInvoice, $visit_id);
			Counters::roiStatsSendEvent('success_payment', $visit_id);

			return 'ok';
		} else {
			return 'Status has not changed';
		}
	}

	public function actionPaypal($id = null)
	{
		if ($id === null) {
			/** @var Discounts $discount */
			$discount = Discounts::findOne(Yii::$app->request->get('_discount', null));
			if (!is_null($discount) && !$discount->checkDate()) {
				$discount = null;
			}

			$priceId = Yii::$app->request->get('price');
			if (!$priceId) {
				Yii::$app->response->redirect(['/page/price/']);
			}
			$model = new Invoice();
			/** @var \app\models\Prices $price */
			$price = Prices::findOne(['id' => $priceId]);
			if (!$price) {
				Yii::$app->response->redirect(['/page/price/']);
			}
			$model->load(
				[
					'user_id'     => Yii::$app->user->identity->getId(),
					'price_id'    => $_GET['price'],
					'sum'         => $price->price,
					'agent'       => 'paypal',
					'discount_id' => (!is_null($discount) ? $discount->id : null),
				], '');
		} else {
			/** @var \app\models\Invoice $model */
			$model = Invoice::findOne(['id' => $id]);
			$price = Prices::findOne(['id' => $model->price_id]);
		}
		$params = [
			'currency'    => Yii::t('app', 'USD'), // only support currency same PayPal
			'description' => Yii::t('app', 'Balance recharge on Zengram.net for #{user}', ['user' => Yii::$app->user->identity->getId()]),
			'total_price' => $model->sum,
			'items'       => [
				[
					'name'     => Yii::t('app', '{value} days on Zengram.net for #{user}', ['value' => $price->value, 'user' => Yii::$app->user->identity->getId()]),
					'quantity' => 1,
					'price'    => $price->price,
				],
			],
		];


		$response = Yii::$app->get('payPalRest')->getLinkCheckOut($params);
		$model->payment_id = $response['payment_id'];

		$gaData['client_id'] = UniversalAnalyticsCookieParser::getCid();
		$gaData['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
		$gaData['user_lang'] = mb_strtolower(mb_substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,5));
		$gaData['roistat_visit'] = Counters::roiCookieVisit();
		$model->ga_data = json_encode($gaData);

		$model->save();

		$roiInvoice = [
			'id' => $model->id,
			'name' => 'Количество дней: ' . $price->value,
			'status' => 1,
			'price' => $price->price,
			'client_id' => $model->user_id
		];
		Counters::roiInvoice($roiInvoice);
		Counters::roiStatsSendEvent('create_invoice');

		return Yii::$app->response->redirect($response['redirect_url']);
	}

	public function actionSuccess_paypal()
	{
		$model = $this->loadModelPayment(Yii::$app->request->get('paymentId'));
		if ($model->status != Invoice::STATUS_SUCCESS) {
			/** @var \app\models\Prices $price */
			$price = Prices::findOne(['id' => $model->price_id]);

			/** @var \betsuno\paypal\RestAPI $rest */
			$rest = Yii::$app->get('payPalRest');
			if (!$rest->checkPayment(['payment_id' => $model->payment_id, 'price' => $price->price])) {
				throw new BadRequestHttpException();
			}
			$params = [
				'payment_id' => $model->payment_id,
				'payer_id'   => Yii::$app->request->get('PayerID'),
			];
			/** @var \PayPal\Api\Payment $response */
			$response = $rest->executePayment($params);
			if ($response->state == 'approved') {
				if (!$model->apply('paypal')) {
					return 'Balance add error<script>setTimeout(function(){window.location.href = "/";},1000)</script>';
				}
				/** @var UsersInfo $usersInfo */
				$usersInfo = UsersInfo::findOne(['user_id' => $model->user_id]);
				Counters::trigger('payment_success', $usersInfo->refferer);


				// Народ, проверьте плз, что тут всё правильно подставлено
				$user_id = $model->user_id; // id-шник клиента в зенграме
				$transaction_id = $model->id; // id-шник транзакции в invoice
				$myprice = $price->price; // Цена

				$days = $price->value; // Размер пакет услуг (сколько дней куплено)
				$method = 'Success_paypal';

				// Получаем ip-шник для корректного определения страны
				$user = Users::findOne($user_id);
				/** @var Users $user */
				$ip = $user->registerIP;


				// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
				// Передача информации о транзакции в GA
				Counters::gaTransaction($user_id, $transaction_id, $myprice, $days, $method, $ip, $model->ga_data);
				// ----------------------------------------------------

				// RoiStat успешная оплата
				$visit_id = null;
				if (!empty($model->ga_data)) {
					$gaData = json_decode($model->ga_data, true);
					if (isset($gaData['roistat_visit'])){
						$visit_id = $gaData['roistat_visit'];
					}
				}


				$roiInvoice = [
					'id' => $model->id,
					'name' => 'Количество дней: ' . $price->value,
					'status' => 2,
					'price' => $price->price,
					'client_id' => $model->user_id
				];
				Counters::roiInvoice($roiInvoice, $visit_id);
				Counters::roiStatsSendEvent('success_payment', $visit_id);


				return Yii::$app->response->redirect(['/payment/success-payment/', 'id' => $model->id]);
			} else {
				return 'Paypal accept error<script>setTimeout(function(){window.location.href = "/";},1)</script>';
			}
		} else {
			return Yii::$app->response->redirect(['/payment/success-payment/', 'id' => $model->id]);
		}
	}

	public function actionCancel_paypal()
	{
		Counters::trigger('payment_fail');

		return Yii::$app->response->redirect(['/page/price/']);
	}

	public function actionYandexCards()
	{
		$priceId = Yii::$app->request->get('price');
		if (!$priceId) {
			Yii::$app->response->redirect(['/page/price/']);
		}

		/** @var \app\models\Prices $price */
		$price = Prices::findOne(['id' => $priceId]);
		if (!$price) {
			Yii::$app->response->redirect(['/page/price/']);
		}

		/** @var Discounts $discount */
		$discount = Discounts::findOne(Yii::$app->request->get('_discount', null));
		if (!is_null($discount) && !$discount->checkDate()) {
			$discount = null;
		}

		$invoice = new Invoice();
		$invoice->load([
			'user_id'     => Yii::$app->user->identity->getId(),
			'price_id'    => $priceId,
			'sum'         => $price->price,
			'agent'       => 'yandex_card',
			'discount_id' => (!is_null($discount) ? $discount->id : null),
		], '');

		$gaData['client_id'] = UniversalAnalyticsCookieParser::getCid();
		$gaData['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
		$gaData['user_lang'] = mb_strtolower(mb_substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,5));
		$gaData['roistat_visit'] = Counters::roiCookieVisit();
		$invoice->ga_data = json_encode($gaData);

		$invoice->save();

		$roiInvoice = [
			'id' => $invoice->id,
			'name' => 'Количество дней: ' . $price->value,
			'status' => 1,
			'price' => $price->price,
			'client_id' => $invoice->user_id
		];
		Counters::roiInvoice($roiInvoice);
		Counters::roiStatsSendEvent('create_invoice');

		return $this->render('yandex', [
			'price' => $price,
			'model' => $invoice,
			'type' => 'AC'
		]);
/*      // платежи как агрегатор
		$ym = new YMComponent(['client_id' => '9CC705C3CD092AB01E3BA8F959ACB69B4D6098C73236EB221141A5852E21F942']);
		$user = Users::findOne(['id' => Yii::$app->user->identity->getId()]);
		$user->instance_id = $ym->initCard($user->instance_id);
		$user->save();
		$result_request = $ym->getExternal()->request([
			'pattern_id' => 'p2p',
			'to' => '410013915663570',
			'amount' => $price->price,
			'invoice_id' => $invoice->id,
			'message' => Yii::t('app', '{value} days on Zengram.net for #{user}', ['value' => $price->value, 'user' => Yii::$app->user->identity->getId()])
		]);
		if (empty($result_request->request_id)){
			return 'Error';
		}
		$result_process = $ym->getExternal()->process([
			'request_id' => $result_request->request_id,
			'ext_auth_success_uri' => Url::to(['payment/payment-success']),
			'ext_auth_fail_uri' => Url::to(['payment/payment-fail'])
		]);*/
	}

	public function actionYandex()
	{
		$priceId = Yii::$app->request->get('price');
		if (!$priceId) {
			Yii::$app->response->redirect(['/page/price/']);
		}

		/** @var \app\models\Prices $price */
		$price = Prices::findOne(['id' => $priceId]);
		if (!$price) {
			Yii::$app->response->redirect(['/page/price/']);
		}

		/** @var Discounts $discount */
		$discount = Discounts::findOne(Yii::$app->request->get('_discount', null));
		if (!is_null($discount) && !$discount->checkDate()) {
			$discount = null;
		}

		$invoice = new Invoice();
		$invoice->load([
			'user_id'     => Yii::$app->user->identity->getId(),
			'price_id'    => $priceId,
			'sum'         => $price->price,
			'agent'       => 'yandex_card',
			'discount_id' => (!is_null($discount) ? $discount->id : null),
		], '');

		$gaData['client_id'] = UniversalAnalyticsCookieParser::getCid();
		$gaData['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
		$gaData['user_lang'] = mb_strtolower(mb_substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 5));
		$gaData['roistat_visit'] = Counters::roiCookieVisit();
		$invoice->ga_data = json_encode($gaData);

		$invoice->save();

		$roiInvoice = [
			'id' => $invoice->id,
			'name' => 'Количество дней: ' . $price->value,
			'status' => 1,
			'price' => $price->price,
			'client_id' => $invoice->user_id
		];

		Counters::roiInvoice($roiInvoice);
		Counters::roiStatsSendEvent('create_invoice');

		return $this->render('yandex', [
			'price' => $price,
			'model' => $invoice,
			'type' => 'PC'
		]);
	}

	public function actionYmResult()
	{
		// Для получения токена TODO потом выпилить
		$code = Yii::$app->request->get('code', false);
		if ($code !== false) {
			echo 'Полученный код:<br>' . $code .'<br>';


			$redirect_uri = 'https://zengram.ru/payment/ym-result';
			$client_id = '9CC705C3CD092AB01E3BA8F959ACB69B4D6098C73236EB221141A5852E21F942';
			$client_secret = 'A331CB8EFE6DAEF67EB40D38E02F17AC9E574A80323CAF364183489B221A1DD085ED8BDF1D291AE58C08088523D6FF98BAD4735A1C9464B4D1A04B2EF49B97ED';

			$access_token_response = API::getAccessToken($client_id, $code, $redirect_uri, $client_secret);
			if(property_exists($access_token_response, "error")) {
				// process error
				echo 'Оишбка подключения';
			}
			$access_token = $access_token_response->access_token;
			var_dump($access_token);

			die();
		}
		// ---

		if (Yii::$app->request->post('test_notification', false) === 'true'){
			return 'Ошибка при подтверждении платежа. Тестовая оплата';
		}

		if (Yii::$app->request->post('unaccepted', false) === 'true'){
			return 'Ошибка при подтверждении платежа. Не принято';
		}

		//проверка notification_type&operation_id&amount&currency&datetime&sender&codepro&notification_secret&label
		$hash_params =  [
			Yii::$app->request->post('notification_type'),
			Yii::$app->request->post('operation_id'),
			Yii::$app->request->post('amount'),
			Yii::$app->request->post('currency'),
			Yii::$app->request->post('datetime'),
			Yii::$app->request->post('sender'),
			Yii::$app->request->post('codepro'),
			Yii::$app->params['ym-secret'],
			Yii::$app->request->post('label')
		];
		$hash = sha1(implode('&', $hash_params));
		if ($hash != Yii::$app->request->post('sha1_hash')){
			return 'Ошибка при подтверждении платежа. Неверный хеш';
		}

		$payment_id = Yii::$app->request->post('label');

		/** @var Invoice $invoice */
		$invoice = Invoice::findOne(['id' => $payment_id]);
		if ($invoice === null){
			return 'Ошибка при подтверждении платежа. Несуществующий invoice';
		}
		if ($invoice->status == Invoice::STATUS_SUCCESS) {
			return 'Ошибка при подтверждении платежа. Уже подтвеждено';
		}

		/** @var \app\models\Prices $price */
		$price = Prices::findOne(['id' => $invoice->price_id]);
		if (!$price){
			return 'Ошибка при подтверждении платежа. Неверная цена';
		}
		if (floatval($price->price) !== floatval(Yii::$app->request->post('amount')) && floatval($price->price) !== floatval(Yii::$app->request->post('withdraw_amount'))) {
			return 'Ошибка при подтверждении платежа. Неверная сумма';
		}

		if (!empty(Yii::$app->request->post('operation_id'))) {
			$invoice->payment_id = 'YM-' . Yii::$app->request->post('operation_id');
		}
		if (!$invoice->apply('yandex')) {
			$invoice->status = Invoice::STATUS_FAIL;
			$invoice->save();
			return 'Ошибка пополнения';
		}
		/** @var UsersInfo $usersInfo */
		$usersInfo = UsersInfo::findOne(['user_id' => $invoice->user_id]);
		Counters::trigger('payment_success', $usersInfo->refferer);


		// Народ, проверьте плз, что тут всё правильно подставлено
		$user_id = $invoice->user_id; // id-шник клиента в зенграме
		$transaction_id = $invoice->id; // id-шник транзакции в invoice
		$myprice = $price->price; // Цена
		$days = $price->value; // Размер пакет услуг (сколько дней куплено)
		$method = 'Success';

		// Получаем ip-шник для корректного определения страны
		$user = Users::findOne($user_id);
		/** @var Users $user */
		$ip = $user->registerIP;


		// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		// Передача информации о транзакции в GA
		Counters::gaTransaction($user_id, $transaction_id, $myprice, $days, $method, $ip, $invoice->ga_data);

		// roistats
		$visit_id = null;
		if (!empty($invoice->ga_data)) {
			$gaData = json_decode($invoice->ga_data, true);
			if (isset($gaData['roistat_visit'])){
				$visit_id = $gaData['roistat_visit'];
			}
		}

		$roiInvoice = [
			'id' => $invoice->id,
			'name' => 'Количество дней: ' . $price->value,
			'status' => 2,
			'price' => $price->price,
			'client_id' => $invoice->user_id
		];
		Counters::roiInvoice($roiInvoice, $visit_id);
		Counters::roiStatsSendEvent('success_payment', $visit_id);

		return "OK";
	}

	/**
	 * @return string|\yii\web\Response
	 */
	public function actionSuccess()
	{
		/** @var \Yii\twocheckout\TwoCheckout $model */
		$model_2co = Yii::$app->get('TwoCheckout');
		if (!$model_2co->approve($_REQUEST)) {
			return '2checkout wrong params<script>setTimeout(function(){window.location.href = "/";},1000)</script>';
		}
		/** @var Invoice $invoice */
		$invoice = Invoice::findOne(['id' => $_REQUEST['li_0_product_id']]);
		if ($invoice === null){
			return '2checkout wrong invoice<script>setTimeout(function(){window.location.href = "/";},1000)</script>';
		}
		if ($invoice->status == Invoice::STATUS_SUCCESS) {
			return '2checkout accept error<script>setTimeout(function(){window.location.href = "/";},1000)</script>';
		}
		/** @var \app\models\Prices $price */
		$price = Prices::findOne(['id' => $invoice->price_id]);
		if (!$price || floatval($price->price) !== floatval($_REQUEST['total'])) {
			return '2checkout wrong price<script>setTimeout(function(){window.location.href = "/";},1000)</script>';
		}

		if ($invoice->user_id != $_REQUEST['user_id']){
			return '2checkout wrong user<script>setTimeout(function(){window.location.href = "/";},1000)</script>';
		}
		if (!empty($_REQUEST['order_number'])) {
			$invoice->payment_id = '2CO-' . $_REQUEST['order_number'];
		}
		if (!$invoice->apply('2co')) {
			$invoice->status = Invoice::STATUS_FAIL;
			$invoice->save();
			return 'Balance add error<script>setTimeout(function(){window.location.href = "/";},1000)</script>';
		}
		/** @var UsersInfo $usersInfo */
		$usersInfo = UsersInfo::findOne(['user_id' => $invoice->user_id]);
		Counters::trigger('payment_success', $usersInfo->refferer);


		// Народ, проверьте плз, что тут всё правильно подставлено
		$user_id = $invoice->user_id; // id-шник клиента в зенграме
		$transaction_id = $invoice->id; // id-шник транзакции в invoice
		$myprice = $price->price; // Цена
		$days = $price->value; // Размер пакет услуг (сколько дней куплено)
		$method = 'Success';

		// Получаем ip-шник для корректного определения страны
		$user = Users::findOne($user_id);
		/** @var Users $user */
		$ip = $user->registerIP;


		// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		// Передача информации о транзакции в GA
		Counters::gaTransaction($user_id, $transaction_id, $myprice, $days, $method, $ip, $invoice->ga_data);
		// ----------------------------------------------------

		// roistats
		$visit_id = null;
		if (!empty($invoice->ga_data)) {
			$gaData = json_decode($invoice->ga_data, true);
			if (isset($gaData['roistat_visit'])){
				$visit_id = $gaData['roistat_visit'];
			}
		}

		$roiInvoice = [
			'id' => $invoice->id,
			'name' => 'Количество дней: ' . $price->value,
			'status' => 2,
			'price' => $price->price,
			'client_id' => $invoice->user_id

		];
		Counters::roiInvoice($roiInvoice, $visit_id);
		Counters::roiStatsSendEvent('success_payment', $visit_id);

		return Yii::$app->response->redirect(['/payment/success-payment/', 'id' => $invoice->id]);
	}

	public function actionCancel()
	{
		Counters::trigger('payment_fail');

		return 'Cancel<script>setTimeout(function(){window.location.href = "/";},1)</script>';
	}

	public function actionCancelInterkassa()
	{
		Counters::trigger('payment_fail_interkassa');

		return 'Cancel<script>setTimeout(function(){window.location.href = "/";},1)</script>';
	}

	/**
	 * Callback.
	 *
	 * @param \robokassa\Merchant $merchant merchant.
	 * @param integer $nInvId invoice ID.
	 * @param float $nOutSum sum.
	 * @param array $shp user attributes.
	 *
	 * @return string|\yii\web\Response
	 */
	public function successCallback($merchant, $nInvId, $nOutSum, $shp)
	{
		$model = $this->loadModel($nInvId);
		if ($model->status == Invoice::STATUS_PENDING) {
			$model->status = Invoice::STATUS_ACCEPTED;
			$model->save(false);
		}
//		else {
//			return 'Status had not changed<script>setTimeout(function(){window.location.href = "/";},1000)</script>';
//		}
		return Yii::$app->response->redirect(['/payment/success-payment/', 'id' => $nInvId]);
	}

	public function resultCallback($merchant, $nInvId, $nOutSum, $shp)
	{
		$model = $this->loadModel($nInvId);
		if ($model->status != Invoice::STATUS_SUCCESS) {
			$price = Prices::findOne(['id' => $model->price_id]);
			/** @var \app\models\Prices $price */
			if ($price->price != $nOutSum) {
				throw new BadRequestHttpException;
			}
			if (!$model->apply('robokassa')) {
				return 'Balance add error';
			}

			/** @var UsersInfo $usersInfo */
			$usersInfo = UsersInfo::findOne(['user_id' => $model->user_id]);

			Counters::trigger('payment_success', $usersInfo->refferer);

			// Народ, проверьте плз, что тут всё правильно подставлено
			$user_id = $model->user_id; // id-шник клиента в зенграме
			$transaction_id = $nInvId; // id-шник транзакции в invoice
			$myprice = $price->price; // Цена
			$days = $price->value; // Размер пакет услуг (сколько дней куплено)

			// Получаем ip-шник для корректного определения страны
			$user = Users::findOne($user_id);
			/** @var Users $user */
			$ip = $user->registerIP;
			$method = 'resultCallback';

			// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
			// Передача информации о транзакции в GA
			Counters::gaTransaction($user_id, $transaction_id, $myprice, $days, $method, $ip, $model->ga_data);
			// ----------------------------------------------------

			// roistats
			$visit_id = null;
			if (!empty($model->ga_data)) {
				$gaData = json_decode($model->ga_data, true);
				if (isset($gaData['roistat_visit'])){
					$visit_id = $gaData['roistat_visit'];
				}
			}

			$roiInvoice = [
				'id' => $model->id,
				'name' => 'Количество дней: ' . $price->value,
				'status' => 2,
				'price' => $price->price,
				'client_id' => $model->user_id
			];
			Counters::roiInvoice($roiInvoice, $visit_id);
			Counters::roiStatsSendEvent('success_payment', $visit_id);

			return 'Ok';
		} else {
			return 'Status has not changed';
		}
	}

	public function actionTest()
	{

		//1075661455.1444986834
		//$test = 'GA1.2.1549864361.1460563889';
		//$test = UniversalAnalyticsCookieParser::getCid();
		//print_r($test);
		//UniversalAnalyticsCookieParser::getCid();
		//Counters::triggerMeasurement('Окончание бесплатного периода', 2414);

		// Народ, проверьте плз, что тут всё правильно подставлено

		Counters::triggerMeasurement('Окончание бесплатного периода', 2414);
		die();
		$user_id = 2414; // id-шник клиента в зенграме
		$transaction_id = (int)Yii::$app->request->get('tid', 114);  // id-шник транзакции в invoice
		$myprice = 1; // Цена
		$days = 3; // Размер пакет услуг (сколько дней куплено)
		$method = 'test';
		$ip = '154.119.80.15';


		// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		// Передача информации о транзакции в GA
		// ----------------------------------------------------

		//echo $_SERVER['HTTP_USER_AGENT'];
		//echo 'Запрос отправлен успешно или типа того';
		$model = $this->loadModel(3493);
		//echo $user_agent . '<br>';
		//echo $client_id . '<br>';
		$model->ga_data = '';
		Counters::gaTransaction($user_id, $transaction_id, $myprice, $days, $method, $ip, $model->ga_data);

		//Counters::roiStatsSendEvent('success_payment');

	}

	public function failCallback($merchant, $nInvId, $nOutSum, $shp)
	{
		Counters::trigger('payment_fail');

		$model = $this->loadModel($nInvId);
		if ($model->status == Invoice::STATUS_PENDING) {
			$model->status = Invoice::STATUS_FAIL;
			$model->save(false);
			return 'Ok<script>setTimeout(function(){window.location.href = "/";},1)</script>';
		} else {
			return 'Status has not changed<script>setTimeout(function(){window.location.href = "/";},1000)</script>';
		}
	}

	/**
	 * @param integer $id
	 *
	 * @return Invoice
	 * @throws \yii\web\BadRequestHttpException
	 */
	protected function loadModel($id)
	{
		$model = Invoice::findOne(['id' => $id]);
		if ($model === null) {
			throw new BadRequestHttpException;
		}
		return $model;
	}

	/**
	 * @param string $payment_id
	 *
	 * @return Invoice
	 * @throws \yii\web\BadRequestHttpException
	 */
	protected function loadModelPayment($payment_id)
	{
		$model = Invoice::findOne(['payment_id' => $payment_id]);
		if ($model === null) {
			throw new BadRequestHttpException;
		}
		return $model;
	}
}