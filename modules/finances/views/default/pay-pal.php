<?php
/**
 * @var $this \yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $dateFrom int
 * @var $dateTo int
 * @var $service string
 * @var $error string|null
 */

$this->title = 'Платежи PayPal';

Yii::$app->formatter->datetimeFormat = 'dd MMMM yyyy HH:mm:ss'
?>

<h2><?= $this->title ?></h2>

<?php \yii\widgets\ActiveForm::begin([
	'method' => 'get',
	'action' => [''],
	'options' => [
		'class' => 'form-horizontal',
		'style' => 'margin: 20px 0;'
	]
]); ?>

<div class="row">
	<div class="col-xs-9">
		<div class="col-xs-6">
			<?= \yii\bootstrap\Html::dropDownList('service', $service, \app\models\PaymentHistoryItem::$services, [
				'prompt' => '',
				'class' => 'form-control'
			]) ?>
		</div>

		<div class="input-group col-xs-6">
			<label for="date-from" class="input-group-addon">с</label>
			<input name="from" value="<?= $dateFrom ? date('Y-m-d', $dateFrom) : '' ?>" id="date-from"
			       class="form-control date-picker" data-date-format="yyyy-mm-dd">
			<label for="date-to" class="input-group-addon">до</label>
			<input name="to" value="<?= $dateTo ? date('Y-m-d', $dateTo) : '' ?>" id="date-to"
			       class="form-control date-picker" data-date-format="yyyy-mm-dd">
			<span class="input-group-btn">
				<button type="submit" class="btn btn-success">Вперед</button>
			</span>
		</div>
	</div>
</div>

<?php 
\yii\widgets\ActiveForm::end();

echo \yii\grid\GridView::widget([
	'dataProvider' => $dataProvider,
	'columns' => [
		'date:datetime',
		'service',
		[
			'attribute' => 'amount',
			'format' => 'raw',
			'value' => function ($item) {
				/** @var $item \app\models\PaymentHistoryItem */
				return \yii\helpers\Html::a(
					Yii::$app->formatter->asCurrency($item->amount, $item->currency),
					'https://www.paypal.com/ru/cgi-bin/webscr?cmd=_view-a-trans&id=' . $item->operation_id,
					['target' => '_blank']
				);
			}
		]
	]
]);
?>