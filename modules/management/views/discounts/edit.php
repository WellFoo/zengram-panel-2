<?php
/**
 * @var \yii\web\View $this
 * @var \app\models\Discounts $model
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->registerCssFile('@web/css/datepicker.css');
$this->registerJsFile('@web/js/bootstrap-datepicker.js', ['position' => $this::POS_BEGIN]);

$this->title = $model->isNewRecord ? 'Новая скидка' : 'Скидка';
?>

<div class="discounts-edit">
	<h2><?= $this->title ?></h2>

	<?php $form = ActiveForm::begin([
		'layout' => 'horizontal',
		'fieldConfig' => [
			'horizontalCssClasses' => [
				'offset' => 'col-sm-offset-2',
				'label' => 'col-sm-2',
			]
		]
	]); ?>

	<?= $form->field($model, 'code') ?>
	<?= $form->field($model, 'date_start')->textInput([
		'class' => 'form-control date-picker',
		'data' => [
			'date-format' => 'yyyy-mm-dd'
		]
	]) ?>
	<?= $form->field($model, 'date_end')->textInput([
		'class' => 'form-control date-picker',
		'data' => [
			'date-format' => 'yyyy-mm-dd'
		]
	]) ?>
	<?= $form->field($model, 'size') ?>

	<div>
		<?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => 'btn btn-success']) ?>
		<?= Html::a('Отмена', ['index'], ['class' => 'btn btn-default']) ?>
	</div>

	<?php $form::end(); ?>
</div>

<script language="JavaScript">
$(function () {
	$('.date-picker').datepicker();
});
</script>
