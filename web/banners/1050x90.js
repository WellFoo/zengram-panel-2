(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 1050,
	height: 90,
	fps: 30,
	color: "#FFFFFF",
	manifest: [
		{src:"https://zengram.ru/banners/images/logo.png?1458653357495", id:"logo"}
	]
};



// symbols:



(lib.logo = function() {
	this.initialize(img.logo);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,217,50);


(lib.Symbol29 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#92FFDD").s().p("AgPBjQgEAAgDgCQgCgDAAgEIAAgSIgBihIAAgBQABgEACgCQADgCADAAIAqAAIAADFg");
	this.shape.setTransform(11.3,10.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#86E4C2").s().p("AgGBjIAAjFIAAAAIANAAIAADFg");
	this.shape_1.setTransform(14.7,10.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFB095").s().p("AgpBvIgCgBQgEgBgDgCQgEgCgCgFIAAgBIAAAAIBRABQAEAAAEgEQAEgEAAgFIAAgKQAAgFgEgDQgEgDgEAAIhRAAIgCgFIAAAAIAAgBIgBAAIgDgBIgBgBIgCAAIgCAAIgBgFIABgDIB1AAQAHAAAEgDQAEgFAAgGIAAgPQAAgFgEgFQgEgEgHAAIh1AAIAAgDIgBgDIAAAAIgEgBQgBAAAAAAQgBAAAAAAQgBAAAAAAQgBAAAAAAIgEgEIAAgBICCAAQAHAAAEgFQAEgDAAgHIAAgKQAAgGgEgEQgEgEgHgBIiCAAIABgDIAAgBQACgEAFgBIABAAIADAAIABgEIAAgDIBtgBQAGAAADgDQAEgEAAgFIgBghIAGgBQAHAAAGgDIABAAIABAnQgBAGgDAFQgEAEAAABQAAABAHAEQAHAFAAAHIAAAXQAAAFgEAGQgDADgFABQgBABAAAAQgBAAAAAAQAAAAAAAAQAAAAAAAAQAAAAAAAAQAAABAAAAQABAAAAAAQABABABAAQAEACAEAFQADAFAAAFIAAAcQAAAGgFAGQgFAFgHAAQgEAAgDACQgCADAAAFIAAAVQAAAIgFAEQgEAFgHAAg");
	this.shape_2.setTransform(-17.8,5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFD2B1").s().p("AgiDcIgFgBIheggIBSACQAHgBAEgEQAFgFAAgHIAAgWQAAgEACgDQADgCAEgBQAHAAAFgEQAFgGAAgHIAAgcQAAgFgDgFQgEgFgEgCQgBAAgBAAQAAAAgBgBQAAAAAAAAQAAgBAAAAQAAAAAAgBQAAAAAAAAQAAgBABAAQAAAAABAAQAFgCADgDQAEgFAAgGIAAgWQAAgIgHgFQgHgDAAgBQAAgCAEgEQADgFABgFIgBglIAEgEQAEgDACgEQACgEgCgFIgZhmQgCgHACgFIARgmQADgGAFgEQAFgDAEAAIAEAAQAJAAAGAHQAGAFAAAJIAAAfQAAAEACAFIA+BzIAaBPQABADADACQACACADAAIAtAAIAAACIABCjIgmgBIgGABIgbALIgFABgAiSC0IgCgEQACAFAEADQgCgCgCgCgAiUCvIAAgBIAAggIBTAAQAEAAAEAEQAEADAAAFIAAAKQAAAEgEAFQgEADgEAAgAiWCJIAAABIAAAAIAAgBgAifB/IAAgrIB3AAQAHAAAEADQAEAFAAAGIAAAOQAAAGgEAFQgEAEgHAAgAisBFIAAgDIAAgkICEAAQAHAAAEAEQAEAEAAAHIAAAKQAAAGgEAEQgEAFgHgBgAifgPQAAgHAEgEQADgDAEgBIAEgBIBsAAIABAgQAAAFgEAEQgDACgGAAIhvABg");
	this.shape_3.setTransform(-8.6,-2.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#517786").s().p("AgrB/QgHgHAAgJIgBjdQAAgJAGgGQAEgHAJAAIBUAAIAAEIIhPABQgKAAgGgGg");
	this.shape_4.setTransform(20.7,11.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-25.9,-24.6,51.8,49.3);


(lib.Symbol28 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAoBKIAAg8IgdAAIgsA8IgnAAIAyhAQgNgEgHgGQgHgGgCgIQgDgIAAgKQAAgUANgLQAMgKAYAAIBOAAIAACTgAgPgrQgGAEAAAKQAAAKAGAFQAFAGAKAAIAoAAIAAgoIgoAAQgKAAgFAFg");
	this.shape.setTransform(804.6,20.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAgBKIAAhrIgBAAIg2BrIgpAAIAAiTIAhAAIAABrIABAAIA2hrIApAAIAACTg");
	this.shape_1.setTransform(789.2,20.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgPBKIAAh5IgxAAIAAgaICBAAIAAAaIgxAAIAAB5g");
	this.shape_2.setTransform(774.1,20.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgjBFQgRgKgJgSQgJgRAAgXQAAgWAJgTQAKgSAQgJQARgKAVAAQAdAAARAOQARAOAEAaIghAAQgDgOgIgHQgKgIgNAAQgRAAgLAOQgLAOAAAYQAAAZALANQAKAOARAAQAPAAAJgIQAJgJADgRIAhAAQgEAcgTAQQgRAQgdAAQgVAAgQgJg");
	this.shape_3.setTransform(759.5,20.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAkBKQgGgEgCgKQgLAKgNAEQgLAFgPgBQgYAAgOgMQgNgLAAgVQAAgPAGgJQAHgJAJgCQAKgFAMgCIAXgDIAQgDIAKgDQADgBACgCIABgEIABgHQAAgLgIgGQgHgFgOAAQgQgBgIAHQgHAGgCAOIgfAAQACgbASgMQASgMAbABQAQAAANAEQAOAFAJAKQAHAKABAQIAABKQgBAGACACQACADAFAAIACAAIAFgBIAAAXIgNADIgJAAIgBAAQgJAAgGgEgAAJAFIgPADIgPADQgIACgFAFQgFAFAAAKQAAAKAHAGQAHAGANAAQAKAAAIgDQAIgDAGgGQAFgFABgHQABgHgBgHIAAgRQgHAEgKABg");
	this.shape_4.setTransform(743.6,20.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAfBKIAAg/IgNADIgRADIgRACQgXgBgLgLQgNgJAAgWIAAgxIAhAAIAAAlQAAAPAHAHQAHAHANAAIALgBIANgDIAKgEIAAg6IAhAAIAACTg");
	this.shape_5.setTransform(727.2,20.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgtBnIgNgCIAAgcIAKACIAHAAQAIAAAFgGQAFgFADgIIAFgPIg6iPIAkAAIAmBsIAmhsIAjAAIg4CRIgLAcQgGAOgIAJQgKAJgTAAIgJAAg");
	this.shape_6.setTransform(712.1,23.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgnBFQgRgKgIgRQgKgSAAgYQAAgXAKgRQAIgSASgKQARgKAVAAQAXAAARAKQAQAKAKARQAJASAAAXQAAAYgJASQgKARgQAKQgRAKgXAAQgVAAgSgKgAgeglQgKAOgBAXQABAZAKAOQAMAOASAAQATAAALgOQAMgOAAgZQAAgYgMgOQgLgNgTgBQgSABgMAOg");
	this.shape_7.setTransform(688,20.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgzBKIAAiTIBnAAIAAAaIhGAAIAAB5g");
	this.shape_8.setTransform(674,20.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgkBEQgRgKgJgRQgIgSAAgWQAAgWAJgSQAJgSARgKQAQgLATAAQAWAAAQALQAQAKAIASQAJATAAAYIAAADIhsAAQAAAXALAMQALANARAAQANAAAJgHQAJgGAFgOIAfAAQgEAPgJAMQgKALgOAHQgOAGgRAAQgVAAgQgLgAAmgNQgCgTgKgKQgKgLgQgBQgPAAgJALQgKALgDATIBLAAIAAAAg");
	this.shape_9.setTransform(659.1,20.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AhjBKIAAiTIAhAAIAAB5IAzAAIAAh5IAfAAIAAB5IAzAAIAAh5IAhAAIAACTg");
	this.shape_10.setTransform(639.1,20.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AAlBKQgHgEgCgKQgLAKgNAEQgMAFgOgBQgYAAgNgMQgNgLgBgVQAAgPAGgJQAHgJAKgCQAJgFAMgCIAXgDIAQgDIAKgDQADgBACgCIABgEIABgHQAAgLgHgGQgIgFgOAAQgQgBgIAHQgHAGgCAOIgfAAQACgbASgMQASgMAbABQAQAAAOAEQAOAFAHAKQAJAKAAAQIAABKQAAAGABACQACADAFAAIACAAIAFgBIAAAXIgNADIgJAAIgBAAQgJAAgFgEgAAKAFIgQADIgPADQgIACgFAFQgFAFAAAKQAAAKAHAGQAHAGANAAQAKAAAHgDQAJgDAGgGQAFgFABgHQABgHgBgHIAAgRQgIAEgIABg");
	this.shape_11.setTransform(619.6,20.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AhUBnIAAjNIBcAAQAXAAAPAHQAPAGAHAMQAHAMAAAPQAAAPgIAMQgIALgNAGQASAEAKALQAKANABAUQAAASgJAOQgIANgOAIQgPAIgUAAgAgwBJIA0AAQAMAAAKgCQAKgDAGgHQAGgHABgNQgBgOgGgHQgGgHgLgCQgKgDgMAAIgzAAgAgwgQIAwAAQATABAKgHQAKgGABgQQgBgNgFgGQgGgGgJgCQgIgCgLABIgwAAg");
	this.shape_12.setTransform(601.5,17.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AA0BKIAAhhIAAAAIgkBhIgfAAIgkhhIAAAAIAABhIggAAIAAiTIArAAIAoB1IAAAAIAph1IArAAIAACTg");
	this.shape_13.setTransform(572.6,20.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgtBnIgNgCIAAgcIAKACIAHAAQAIAAAFgGQAFgFADgIIAFgPIg6iPIAkAAIAmBsIAmhsIAjAAIg4CRIgLAcQgGAOgIAJQgKAJgTAAIgJAAg");
	this.shape_14.setTransform(554.8,23.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AA0BKIAAhhIAAAAIgkBhIgfAAIgkhhIAAAAIAABhIggAAIAAiTIArAAIAoB1IAAAAIAph1IArAAIAACTg");
	this.shape_15.setTransform(537.1,20.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AAgBKIAAhrIgBAAIg2BrIgpAAIAAiTIAhAAIAABrIABAAIA2hrIApAAIAACTg");
	this.shape_16.setTransform(518.6,20.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AAgBKIAAhBIg/AAIAABBIghAAIAAiTIAhAAIAAA6IA/AAIAAg6IAhAAIAACTg");
	this.shape_17.setTransform(501.9,20.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AAgBKIAAhrIgBAAIg2BrIgpAAIAAiTIAhAAIAABrIABAAIA2hrIApAAIAACTg");
	this.shape_18.setTransform(485.3,20.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AA0BKIAAhhIAAAAIgkBhIgfAAIgkhhIAAAAIAABhIggAAIAAiTIArAAIAoB1IAAAAIAph1IArAAIAACTg");
	this.shape_19.setTransform(466.8,20.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgpAPIAAgdIBTAAIAAAdg");
	this.shape_20.setTransform(442.9,20.1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgkBEQgRgKgJgRQgIgSAAgWQAAgWAJgSQAJgSARgKQAQgLATAAQAWAAAQALQAQAKAIASQAJATAAAYIAAADIhsAAQAAAXALAMQALANARAAQANAAAJgHQAJgGAFgOIAfAAQgEAPgJAMQgKALgOAHQgOAGgRAAQgVAAgQgLgAAmgNQgCgTgKgKQgKgLgQgBQgPAAgJALQgKALgDATIBLAAIAAAAg");
	this.shape_21.setTransform(421.2,20.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AAgBKIAAhrIgBAAIg2BrIgpAAIAAiTIAhAAIAABrIABAAIA2hrIApAAIAACTg");
	this.shape_22.setTransform(404.7,20.6);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AAgBKIAAhBIg/AAIAABBIghAAIAAiTIAhAAIAAA6IA/AAIAAg6IAhAAIAACTg");
	this.shape_23.setTransform(388.1,20.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgkBEQgRgKgJgRQgIgSAAgWQAAgWAJgSQAJgSARgKQAQgLATAAQAWAAAQALQAQAKAIASQAJATAAAYIAAADIhsAAQAAAXALAMQALANARAAQANAAAJgHQAJgGAFgOIAfAAQgEAPgJAMQgKALgOAHQgOAGgRAAQgVAAgQgLgAAmgNQgCgTgKgKQgKgLgQgBQgPAAgJALQgKALgDATIBLAAIAAAAg");
	this.shape_24.setTransform(371.8,20.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("ABLBKIguhBIgOAQIAAAxIgdAAIAAgxIgOgQIguBBIgoAAIBAhUIg5g/IAoAAIA0A/IABAAIAAg/IAdAAIAAA/IABAAIA0g/IAoAAIg5A/IBABUg");
	this.shape_25.setTransform(352.4,20.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AAgBKIAAhrIgBAAIg2BrIgpAAIAAiTIAhAAIAABrIABAAIA2hrIApAAIAACTg");
	this.shape_26.setTransform(332.8,20.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AhBBKIAAiTIBQAAQAVAAAMAJQAMAKAAARQAAAMgGAIQgFAHgLAFIAAAAQAOAEAHAIQAHAKAAAPQAAAUgOALQgOALgbAAgAggAxIArAAQALAAAGgFQAGgFAAgKQAAgKgGgFQgGgGgLAAIgrAAgAgggOIAoAAQAJAAAFgFQAFgEAAgJQAAgJgFgEQgFgEgKAAIgnAAg");
	this.shape_27.setTransform(316.8,20.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AA1BiIAAgwIhpAAIAAAwIgfAAIAAhKIATAAQAFgNAFgNQADgPACgUQACgTABgZIAAgQIBuAAIAAB5IAUAAIAABKgAgPg9QAAARgCARIgHAeIgGAVIA9AAIAAhgIguAAg");
	this.shape_28.setTransform(299.6,23);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgmBFQgRgKgJgRQgKgSAAgYQAAgXAKgRQAJgSARgKQARgKAVAAQAXAAARAKQAQAKAKARQAJASAAAXQAAAYgJASQgKARgQAKQgRAKgXAAQgWAAgQgKgAgdglQgLAOAAAXQAAAZALAOQALAOASAAQATAAAMgOQAKgOABgZQgBgYgKgOQgLgNgUgBQgSABgLAOg");
	this.shape_29.setTransform(282.4,20.6);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AhIBoIAAjLIAeAAIAAAUQAJgMANgGQANgGAMAAQAVAAAQAKQAOAKAJARQAJASgBAZQABAXgJAQQgIARgPAKQgPAJgUAAQgNABgNgGQgMgGgJgMIAABLgAgeg/QgLAOAAAZQAAAYALAMQALAOATAAQASAAALgOQALgMAAgXQAAgZgLgOQgLgOgSgBQgTABgLANg");
	this.shape_30.setTransform(265.3,23.2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AAgBKIAAh5Ig/AAIAAB5IghAAIAAiTICBAAIAACTg");
	this.shape_31.setTransform(247.7,20.6);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgkBEQgRgKgJgRQgIgSAAgWQAAgWAJgSQAJgSARgKQAQgLATAAQAWAAAQALQAQAKAIASQAJATAAAYIAAADIhsAAQAAAXALAMQALANARAAQANAAAJgHQAJgGAFgOIAfAAQgEAPgJAMQgKALgOAHQgOAGgRAAQgVAAgQgLgAAmgNQgCgTgKgKQgKgLgQgBQgPAAgJALQgKALgDATIBLAAIAAAAg");
	this.shape_32.setTransform(223.4,20.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgmBFQgSgKgIgRQgKgSAAgYQAAgXAKgRQAIgSASgKQARgKAVAAQAXAAARAKQAQAKAKARQAJASAAAXQAAAYgJASQgKARgQAKQgRAKgXAAQgWAAgQgKgAgdglQgMAOAAAXQAAAZAMAOQALAOASAAQATAAAMgOQAKgOABgZQgBgYgKgOQgLgNgUgBQgSABgLAOg");
	this.shape_33.setTransform(206.7,20.6);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AAdBKIgthDIgTAVIAAAuIghAAIAAiTIAhAAIAABCIA5hCIApAAIg7A8IBBBXg");
	this.shape_34.setTransform(191.2,20.6);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgjBFQgRgKgJgSQgJgRAAgXQAAgWAJgTQAKgSAQgJQARgKAVAAQAdAAARAOQASAOADAaIgiAAQgCgOgIgHQgKgIgNAAQgRAAgLAOQgLAOAAAYQAAAZAKANQALAOARAAQAPAAAJgIQAKgJACgRIAhAAQgEAcgTAQQgRAQgdAAQgVAAgQgJg");
	this.shape_35.setTransform(174.6,20.6);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgkBEQgRgKgJgRQgIgSAAgWQAAgWAJgSQAJgSARgKQAQgLATAAQAWAAAQALQAQAKAIASQAJATAAAYIAAADIhsAAQAAAXALAMQALANARAAQANAAAJgHQAJgGAFgOIAfAAQgEAPgJAMQgKALgOAHQgOAGgRAAQgVAAgQgLgAAmgNQgCgTgKgKQgKgLgQgBQgPAAgJALQgKALgDATIBLAAIAAAAg");
	this.shape_36.setTransform(158.5,20.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AAfBKIAAg/IgNADIgRADIgRACQgXgBgLgLQgNgJAAgWIAAgxIAhAAIAAAlQAAAPAHAHQAHAHANAAIALgBIANgDIAKgEIAAg6IAhAAIAACTg");
	this.shape_37.setTransform(142.3,20.6);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AAgBKIAAhrIgBAAIg2BrIgpAAIAAiTIAhAAIAABrIABAAIA2hrIApAAIAACTg");
	this.shape_38.setTransform(126.4,20.6);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgPBKIAAh5IgxAAIAAgaICBAAIAAAaIgxAAIAAB5g");
	this.shape_39.setTransform(111.4,20.6);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AAkBKQgGgEgCgKQgLAKgNAEQgLAFgPgBQgYAAgOgMQgNgLAAgVQAAgPAGgJQAHgJAJgCQAKgFAMgCIAXgDIAQgDIAKgDQADgBACgCIABgEIABgHQAAgLgIgGQgHgFgOAAQgQgBgIAHQgHAGgCAOIgfAAQACgbASgMQASgMAbABQAQAAANAEQAOAFAJAKQAHAKABAQIAABKQgBAGACACQACADAFAAIACAAIAFgBIAAAXIgNADIgJAAIgBAAQgJAAgGgEgAAJAFIgPADIgPADQgIACgFAFQgFAFAAAKQAAAKAHAGQAHAGANAAQAKAAAIgDQAIgDAGgGQAFgFABgHQABgHgBgHIAAgRQgHAEgKABg");
	this.shape_40.setTransform(97,20.6);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AA0BKIAAhhIAAAAIgkBhIgfAAIgkhhIAAAAIAABhIggAAIAAiTIArAAIAoB1IAAAAIAph1IArAAIAACTg");
	this.shape_41.setTransform(78.4,20.6);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgnBFQgQgKgKgRQgJgSAAgYQAAgXAJgRQAJgSASgKQARgKAVAAQAXAAARAKQAQAKAKARQAJASAAAXQAAAYgJASQgKARgQAKQgRAKgXAAQgWAAgRgKgAgeglQgLAOAAAXQAAAZALAOQAMAOASAAQAUAAAKgOQALgOAAgZQAAgYgLgOQgLgNgTgBQgSABgMAOg");
	this.shape_42.setTransform(59.6,20.6);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AgPBKIAAh5IgxAAIAAgaICBAAIAAAaIgxAAIAAB5g");
	this.shape_43.setTransform(44.2,20.6);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AhBBKIAAiTIBQAAQAVAAAMAJQAMAKAAARQAAAMgGAIQgFAHgLAFIAAAAQAOAEAHAIQAHAKAAAPQAAAUgOALQgOALgbAAgAggAxIArAAQALAAAGgFQAGgFAAgKQAAgKgGgFQgGgGgLAAIgrAAgAgggOIAoAAQAJAAAFgFQAFgEAAgJQAAgJgFgEQgFgEgKAAIgnAAg");
	this.shape_44.setTransform(29.8,20.6);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AA8BnIgUg3IhQAAIgUA3IgmAAIBQjNIAlAAIBQDNgAAeAVIgehVIgfBVIA9AAg");
	this.shape_45.setTransform(11.7,17.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,815.7,36.3);


(lib.Symbol27 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#1C1C1B").s().p("AgKAzIAAgGIgGAAIAAgGIAGAAIAAAGIAoAAIAAAGgAAeAtIAAgGIASAAIAAAGgAAxAnIAAgGIALAAIAAgvIgMAAIAAgGIATAAIAAA7gAgXAnIAAgMIgGAAIAAgSIAYAAIAAAGIgSAAIAAAMIAXAAIAAAGIgRAAIAAAGgAgjAJIAAgLIgZAAIAAgGIgGAAIAAgMIAGAAIAAAMIA3AAIAAAGIgYAAIAAALgAAkgUIAAgGIgMAAIAAgGIAMAAIAAAGIAMAAIAAAGgAg8gUIAAgGIA4AAIAAgGIAEAAIAAAGIASAAIAAAGgAAMggIAAgGIgGAAIAAgGIAGAAIAAAGIAMAAIAAAGgAgKggIAAgSIAQAAIAAAGIgKAAIAAAMg");
	this.shape.setTransform(18.2,23.6,2.422,2.422,90);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// Layer 1 copy
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhdCiIAAgrIABAAIgDgBIgOAAIAAgtIgPAAIAAhkIAPAAIAAgOIAOAAIACgCIAAgOIAbAAIAAgOIAtAAIAAgQIAdAAIAAg8IAOAAIAAgOIAdAAIAAANIAQAAIAACIIANAAIAAgPIAtAAIAAAqIgQAAIAAAPIgPAAIAAAcIgOAAIAAAeIgNAAIAAAdIgPAAIgBABIAAAsg");
	this.shape_1.setTransform(18.2,23.6);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(5.7,7.3,25,32.6);


(lib.Symbol23 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#2382AC").s().p("AhkCNIAAkYIBGAAQA/gBAiAlQAiAkAABDQAABCgiAlQgiAmg9AAgAhABvIAfAAQAvAAAageQAYgcAAg1QAAg1gYgcQgZgdgwAAIgfAAg");
	this.shape.setTransform(1.6,22.2,0.364,0.364);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#2382AC").s().p("AgbBnQAggQAAgeIgEAAQgHAAgHgGQgGgFAAgLQAAgJAHgGQAGgHAIABQAMgBAHALQAHAJAAARQAAAYgNATQgNARgVALgAgOhPQgGgHAAgKQAAgKAGgHQAHgGAHAAQAKAAAGAGQAHAHAAAKQAAAKgHAHQgGAGgKAAQgHAAgHgGg");
	this.shape_1.setTransform(-5.2,22.9,0.364,0.364);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("Ah/EpQh7g1gxh9Qgyh7A1h7QA1h7B9gyQB6gxB7A1QB8A1AxB9QAyB6g1B8Qg1B7h9AxQg8AYg7AAQg/AAhAgbg");
	this.shape_2.setTransform(-0.7,22.1,0.364,0.364);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#E7E7E7").s().p("AhhiiIEKDnIlRBeg");
	this.shape_3.setTransform(10,23.9,0.364,0.364);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FDE4D3").s().p("Ah7A4QgXgBgRgQQgQgRAAgWQAAgWAQgQQARgQAXAAID3AAQAXAAARAQQAQAQAAAWQAAAWgQARQgRAQgXABg");
	this.shape_4.setTransform(65.7,77.8,0.24,0.24);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FDE4D3").s().p("Ah7A4QgXgBgRgQQgQgRAAgWQAAgWAQgQQARgQAXAAID3AAQAXAAARAQQAQAQAAAWQAAAWgQARQgRAQgXABg");
	this.shape_5.setTransform(13.4,77.8,0.24,0.24);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#ACEEEB").s().p("AiyAwIAAhgIFlAAIAABgg");
	this.shape_6.setTransform(52.6,74.6,0.24,0.24);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#FEFFFF").p("AwoIxIAAxhMAhRAAAIAARhg");
	this.shape_7.setTransform(39.2,64.6,0.24,0.24);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#ACEEEB").s().p("AhgATIAAglIDBAAIAAAlg");
	this.shape_8.setTransform(34.8,61.9,0.24,0.24);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#ACEEEB").s().p("AiHAbIAAg1IEOAAIAAA1g");
	this.shape_9.setTransform(24.4,61.9,0.24,0.24);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#ACEEEB").s().p("AhgATIAAglIDBAAIAAAlg");
	this.shape_10.setTransform(54.7,61.9,0.24,0.24);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#ACEEEB").s().p("AhhATIAAglIDDAAIAAAlg");
	this.shape_11.setTransform(44.3,61.9,0.24,0.24);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#ACEEEB").s().p("AiHCIIAAkOIEPAAIAAEOg");
	this.shape_12.setTransform(34.8,66.7,0.24,0.24);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#ACEEEB").s().p("AiHCIIAAkOIEOAAIAAEOg");
	this.shape_13.setTransform(24.4,66.7,0.24,0.24);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#ACEEEB").s().p("AiGCIIAAkOIEOAAIAAEOg");
	this.shape_14.setTransform(54.8,66.7,0.24,0.24);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#ACEEEB").s().p("AiGCIIAAkOIENAAIAAEOg");
	this.shape_15.setTransform(44.3,66.7,0.24,0.24);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#6EC5C5").s().p("AtvCMIAAkXIbfAAIAAEXg");
	this.shape_16.setTransform(39.3,74.6,0.24,0.24);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#6EC5C5").s().p("AifAuIAAhbIE/AAIAABbg");
	this.shape_17.setTransform(39.6,58,0.24,0.24);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#2382AC").s().p("AhbIxIAAxhIC3AAIAARhg");
	this.shape_18.setTransform(62.7,64.6,0.24,0.24);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#2382AC").s().p("AhcIxIAAxhIC5AAIAARhg");
	this.shape_19.setTransform(15.9,64.6,0.24,0.24);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#6EC5C5").s().p("AhbBCIAAiDIC3AAIAACDg");
	this.shape_20.setTransform(58.4,53.6,0.24,0.24);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#ACEEEB").s().p("AtXBCIAAiDIavAAIAACDg");
	this.shape_21.setTransform(35.7,53.6,0.24,0.24);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#CECECE").s().p("AxjA7QgyAAgigjQgkghAAgxMAm3AAAQAAAxgkAhQgiAjgyAAg");
	this.shape_22.setTransform(38.9,83.6,0.24,0.24);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#ADADAD").s().p("AzbAkIAAhHMAm3AAAIAABHg");
	this.shape_23.setTransform(38.9,81.3,0.24,0.24);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FEFFFF").s().p("AwoIxIAAxhMAhRAAAIAARhg");
	this.shape_24.setTransform(39.2,64.6,0.24,0.24);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#CECECE").s().p("AxAKTQgfAAgVgVQgXgWAAgfIAAyRQAAgfAXgWQAVgVAfAAMAiCAAAQAeAAAWAVQAVAWABAfIAASRQgBAfgVAWQgWAVgeAAg");
	this.shape_25.setTransform(39.2,64.6,0.24,0.24);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FDE4D3").s().p("Ai3C4QhNhMAAhsQAAhrBNhMQBMhNBrAAQBsAABMBNQBNBMAABrQAABshNBMQhMBNhsAAQhrAAhMhNg");
	this.shape_26.setTransform(67.2,80.4,0.24,0.24);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FDE4D3").s().p("Ai3C4QhNhMAAhsQAAhrBNhMQBMhNBrAAQBsAABMBNQBNBMAABrQAABshNBMQhMBNhsAAQhrAAhMhNg");
	this.shape_27.setTransform(11.9,80.4,0.24,0.24);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AiCAoQg3gwAAgvQAAglA3AAQAhgBBhAHQAzAABQgGQA3AAAAAlQAAAvg3AwQg7A1hIAAQhHAAg7g1g");
	this.shape_28.setTransform(39.4,34.9,0.24,0.24);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#D45954").s().p("AgTFGQi6gPiYhcQiehghJiUQAdhwBThRQBXhVB1gUQB1gUChBBQCfBBBKBcQAQAUAbAoQAbApAPATQA3BEBCAPQBvAcAhghQhdCBilBDQiJA4iWAAQggAAgfgDg");
	this.shape_29.setTransform(48,7.9,0.24,0.24);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#D45954").s().p("AgPCdQgOhCghhtQgjhSgRgvQgfhXAYgvQAfg9A+AUQAxAQArA3QAsA4AQBMQAPBGgMBIQgMBPgqBTQgiBDg3BMQAZgmgYiFg");
	this.shape_30.setTransform(53.6,18.8,0.24,0.24);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#D45954").s().p("AmyCMQAGjJBjipQA2heBMhDQBShJBfgfQBjgiBoAVQBtAVBHBIQAPAQATAEQAcAGAMAGQgOBShFBHQg6A7haAtQgZANiMA7QhjAqg6AmQh9BWhACAQhCCGAVCPQhYivAGjKg");
	this.shape_31.setTransform(33.1,15,0.24,0.24);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FDE4D3").s().p("AhcBdQgngmAAg3QAAg1AngnQAngnA1AAQA2AAAnAnQAnAnAAA1QAAA2gnAnQgnAng2AAQg1AAgngng");
	this.shape_32.setTransform(54.2,26.6,0.24,0.24);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FDE4D3").s().p("AhcBdQgngnAAg2QAAg1AngnQAngnA1AAQA2AAAnAnQAnAnAAA1QAAA3gnAmQgmAng3AAQg1AAgngng");
	this.shape_33.setTransform(24.6,26.6,0.24,0.24);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgYAZQgKgLAAgOQAAgNAKgLQALgKANAAQAOAAALAKQAKALAAANQAAAOgKALQgLAKgOAAQgNAAgLgKg");
	this.shape_34.setTransform(46.6,23.7,0.24,0.24);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#3D3D3D").s().p("AgPAlQgPgQAQgjIgPgCQAhAAAZgXQAFAQgUAiQgOAdgJAAQgDAAgDgDg");
	this.shape_35.setTransform(48.1,23.6,0.24,0.24);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#D45954").s().p("AAfgcQhFgdh0AqQAfgqAtgQQAtgQArAOQAuAPAnArQAmApAVA7QhGhZg1gWg");
	this.shape_36.setTransform(47.7,18.6,0.24,0.24);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#3D3D3D").s().p("AgNALQgUgiAFgQQAZAXAhAAIgQACQARAkgQAPQgCADgDAAQgKAAgNgdg");
	this.shape_37.setTransform(30.6,23.6,0.24,0.24);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#D45954").s().p("AhdgRQAmgrAugPQArgOAtAQQAsAQAgAqQh0gqhEAdQg2AWhHBZQAWg7Angpg");
	this.shape_38.setTransform(31,18.6,0.24,0.24);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FCD3B7").s().p("AhKBLQgfgfAAgsQAAgqAfggQAfgfArAAQAsAAAfAfQAfAgAAAqQAAAsgfAfQggAfgrAAQgrAAgfgfg");
	this.shape_39.setTransform(49.3,29.8,0.24,0.24);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FCD3B7").s().p("AhKBLQgfgfAAgsQAAgqAfggQAggfAqAAQAsAAAfAfQAfAgAAAqQAAAsgfAfQgfAfgsAAQgqAAgggfg");
	this.shape_40.setTransform(29.6,29.8,0.24,0.24);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#3D3D3D").s().p("AgzBFQgWgdAAgoQAAgnAWgdQAWgdAdAAQAeAAAWAdQAWAdAAAnQAAAogWAdQgWAdgeAAQgdAAgWgdg");
	this.shape_41.setTransform(46.2,24.6,0.24,0.24);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#3D3D3D").s().p("AAoAcQgSgPgWAAQgUAAgTAPQgSAQgJAaQgHgUAAgWQAAgnAWgcQAWgdAdgBQAfABAVAdQAWAcAAAnQAAAWgHAUQgJgagSgQg");
	this.shape_42.setTransform(32.5,23.9,0.24,0.24);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FDE4D3").s().p("AmFKbQgfgXgcgaQhFhCgbhfQhYkqgIijQgNkeBajBQCXlFGcAQQGdgQCXFFQBaDBgNEeQgICjhYEqQgbBfhFBCQgcAagfAXQiuB/jYAAQjXAAiuh/g");
	this.shape_43.setTransform(39.4,23,0.24,0.24);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FCC6A4").s().p("AjXBqQADiYgChiQAAgIAGAAIGhAAQAGAAAAAIQgCBjADCXQhpAvhvgBQhuABhpgvg");
	this.shape_44.setTransform(39.4,42.2,0.24,0.24);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FDE4D3").s().p("AraD/IGyiHQAZgIARgWQAOgVAFgcQALhCADijQAChdgBhaQAAgGAGAAIGtAAQAHAAgBAGIABC5QADCkALA/QAFAcAPAVQAQAWAZAIIGyCHQlcB7l/AAQl+AAlch7g");
	this.shape_45.setTransform(39.4,46.7,0.24,0.24);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#ACEEEB").s().p("AABI/IoCgCQoFgDAAgDQACgIgHg5QgIhAACgvQAIijB7haQA1gnBKgXIHpiXQAZgIARgWQAOgUAFgdQALhEADijQAChdgBhaQAAgGAGAAIGtAAQAHAAgBAGIABC6QADCkALBAQAFAdAPAUQAQAWAZAIIHoCWQBSAaA+AyQBzBbgECcQgBAsgMA/QgKA1ABAJIACAGQAAAAAAABQAAAAAAAAQAAABgBAAQAAAAgBAAg");
	this.shape_46.setTransform(39.4,51.4,0.24,0.24);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FDE4D3").s().p("AwDIuQgBAAAAAAQgBAAAAAAQAAgBAAAAQAAAAAAgBIACgFQABgFgCg6QgCg9AHgvQAWieB5hYQAzgmBHgWIHbiTQAZgHAPgVQAPgVAEgbQALhCADieQAChegBhUQAAgBAAAAQAAgBAAAAQAAgBABAAQAAgBABAAQAAAAABgBQAAAAABAAQAAgBABAAQAAAAABAAIGgAAQABAAAAAAQABAAAAABQABAAAAAAQABABAAAAQABAAAAABQAAAAABABQAAAAAAABQAAAAAAABQgBBZACBbQADCfALA/QAFAcAOAUQAPAVAZAHIHaCTQBOAXA+AyQBvBYALCXQADAtgGA7QgFA2ACAHQAAAEv7ADg");
	this.shape_47.setTransform(39.3,51.4,0.24,0.24);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#D45954").s().p("AqANoIAA7PIUBAAIAAbPg");
	this.shape_48.setTransform(39.4,36,0.24,0.24);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-12.6,0,86.1,86.7);


(lib.Symbol21 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Isolation Mode
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3D494E").s().p("ABdAhQgHgNAAgUQAAgSAHgOQAJgOAKAAQALAAAHAOQAIAOAAASQAAAUgIANQgHAPgLAAQgKAAgJgPgAiCAhQgIgNAAgUQAAgSAIgOQAIgOALAAQALAAAHAOQAIAOAAASQAAAUgIANQgHAPgLAAQgLAAgIgPg");
	this.shape.setTransform(85,72.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#604839").s().p("ABUAoQgEgDAFgJQAJgQAXgFQAVgFATAIQAJAEgDACQgCADgGgCQgSgGgQAGQgQAGgMANQgEAFgDAAIgCgBgAhcgJQgMgOgPgFQgRgGgSAFQgGADgCgDQgCgCAIgEQATgIAVAFQAXAFAJAQQAFAIgDAEIgDABQgDAAgEgFg");
	this.shape_1.setTransform(84.6,62.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#D9B293").s().p("AgzgSQAXAUAcAAQAdAAAYgUQgQAlglAAQgkAAgPglg");
	this.shape_2.setTransform(85,84.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 7
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#512A15").s().p("ABbAZIhbgUIhbAUIgSAAIBtgwIAAgBIAAABIAAgBIAAABIBvAwg");
	this.shape_3.setTransform(85.3,93.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("ABbBQIg2hDIgigpIgDgFIB8gyIACgBIAdAeIACABIg+CKgAicg1IACgBIAdgeIACABIB7AyIgCAFIgiApIg2BEIgDAEg");
	this.shape_4.setTransform(85.2,127.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#21282C").s().p("Ai0gRIBEgNIh9i6IACgBQAmgNArgJIgCACIA/CLIADgEIAyC7IAoCbIAoibIAzi7IADAEIA/iLIgCgCQAqAJApAOIAAAAIh9C6IBDANIi0EBg");
	this.shape_5.setTransform(85.2,145.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#A93239").s().p("AgnBKIAWjrIgTgUIAigrIACgEIADAEIAiArIgUAUIAYDrIgpCbg");
	this.shape_6.setTransform(85.2,147.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#B3C1CA").s().p("AARhTIAUgUIA2BDIgzC8gAhagjIA1hEIAUAUIgXDrgAAAiWIgCAEIACgFIADAFg");
	this.shape_7.setTransform(85.2,139.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#313F48").s().p("AEJDlIoTAAIAkigIANg2QAoimAUgUQAWgXB/gaIAmgIIgCABIB9C6IhDANIC1EBIgCAAg");
	this.shape_8.setTransform(58.4,147);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E3E4E5").s().p("ADpCnQgLgvAFhnQAZhHgrhcQgNgdgSgbIgQgVIATgKQBSgiAtB7QATA0AGBDQAFA8gHAqIgBABQglgMgLAlQgMAlAJBlQgjgdgLgtgAkZBnQgLglglAMIAAgBQgIgqAFg8QAGhCATg1QAth8BTAjIASAKQgaAfgVAuQgrBcAZBHQAFBmgLAwQgLAtgjAdQAJhlgMglg");
	this.shape_9.setTransform(85.1,57.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#EED2B6").s().p("Ah7F6IgDgBQhOgjgUgZQgdgmgYhMQgKgggHgjIgDABQgaADgWgZQgWgYgEgnQgEgnAQgbQAMgWARgIIAAABQAlgMAMAlQALAjgIBmQAigdALgtQALgugFhnQgZhJArhbQAVguAbggIgSgJQBLg6BoAAIAAAAQBoAABMA5IgTAKIAQAVQATAbANAeQArBbgaBJQgFBnAMAuQALAtAiAdQgIhmALgjQAMglAlAMIAAgBQARAIAMAWQARAbgEAnQgEAngWAYQgWAZgbgDIgCgBQgHAjgKAgQgZBNgdAlQgTAZhOAjIgDABQhQAkgsAAQgrAAhQgkgABcEJIATAAIhugxIAAgBIgBAAIAAAAIAAABIhtAxIATAAIBagTg");
	this.shape_10.setTransform(85.2,69.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#D19E84").s().p("Ah7AfIAAhvQBQAkArAAQAsAABQgkIAABvIh8Ayg");
	this.shape_11.setTransform(85.2,115.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#2B3A46").s().p("AkKDlIC1kCIhEgNIB+i6IAkAIQB/AaAXAWQATAVApClIALA0IAlCjg");
	this.shape_12.setTransform(111.9,147.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#658D2B").s().p("ApXKpQh2h2hBiYQhDieAAitQAAirBDieQBBiZB2h1QB1h2CZhAQCehDCrAAQCtAACdBDQCZBAB1B2QB2B1BACZQBDCeABCrQgBCthDCeQhACYh2B2QgvAvg2AnIgLgzQgpiogTgUQgXgXh/gaIgmgHIAAgBQgpgOgqgJIgdgdIgDAAIAAhwIAEgCQBOgjATgZQAdglAZhMQAKghAHgjIACABQAbAEAVgZQAXgZAEglQADgmgQgeQgMgWgRgIQAHgqgFg9QgGhDgSg1Qguh7hSAiQhMg5hpAAIgBAAQhnABhLA5QhTgjgtB7QgTA1gFBDQgGA+AIAqQgRAHgMAWQgQAeAEAmQAEAlAWAZQAVAZAbgEIACgBQAHAjALAhQAYBLAdAmQAUAZBNAjIAEACIAABwIgCAAIgeAdQgqAJgnANIgoAJQh/AagWAXQgUAUgoCoIgNA1Qg3gngwgxg");
	this.shape_13.setTransform(85,77);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,170,170);


(lib.Symbol20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Isolation Mode
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3D494E").s().p("ABbAfQgHgNAAgSQAAgRAHgOQAIgNALAAQAKAAAIANQAHAOAAARQAAASgHANQgIAOgKAAQgLAAgIgOgAh+AfQgIgNAAgSQAAgRAIgOQAIgNAKAAQAKAAAIANQAHAOAAARQAAASgHANQgIAOgKAAQgKAAgIgOg");
	this.shape.setTransform(84.5,78);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E08C30").s().p("ACiAQQgVgfgpAIQgJACgBgFQgBgFAJgDQASgEAUAJQATAIAJAPQAFAIgCABIgBABQgCAAgCgEgAimATQgBgBAEgIQAKgPATgIQATgJASAEQAKADgBAFQgBAFgJgCQgqgIgUAfQgDAEgCAAIgBgBg");
	this.shape_1.setTransform(84.5,69.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#D9B293").s().p("AAAASQgjAAgPgjQAWASAcABQAdAAAWgTQgPAjgjAAIgBAAg");
	this.shape_2.setTransform(84.5,89.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 20
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#5B9E3D").s().p("AiiDuIAAj8QBOAAA4hBQA1hBAChcIAAgBIAwAUQATAIATAMIACABIATBGQARBCAIA8QAIA/gMBeQgFAvgIAig");
	this.shape_3.setTransform(101.3,146.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#8ABF43").s().p("AiNDuQgIgigGgvQgLheAIg/QAHg9AShBQARg5ACgNIACgBQATgMATgIIAjgPIANgFQACBdA1BBQA4BBBOAAIAAD8g");
	this.shape_4.setTransform(68.7,146.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#EED2B6").s().p("AAAFqQgqAAhOg2IgDgCQhBgsgYgfQgngygUguQgLgZgHgbIgIACQgaAEgWgZQgVgZgEgmQgFglAQgeQAIgNAJgIQALgKAPgCIAGgBIAAgBQAHAOAJANQAQh5BFgdQAZgLBOgOQA1gJA1gnIAGgFQAdgWASglQACAWAGAWQAsCmDIBIIAGgVIAGABQAPACALAKQAJAIAHANQARAegFAlQgDAmgWAZQgWAZgbgEIgIgCQgHAbgLAZQgUAugnAyQgYAehAAsIgEADQhOA2gqAAIAAAAgAg7DmQAZASAiABIAAAAQAjAAAZgTQAZgTAAgbIipAAQAAAbAZATgAkakIIAAABIAAAAIAAgBg");
	this.shape_5.setTransform(85.2,78.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#E5C1A6").s().p("AEyExQAHgiAGgvQAMhegIhAQgIg+gShAIgShFIgCgBQBIAnAWAiQAYBEAOCaIADAgQAFA6ACAygAmiExQACgyAFg6IADggQAPiaAXhEQAWgjBJgnIgCACQgCAMgRA5QgSBAgHA+QgIBAALBeQAGAvAIAigAiEgNQg3hBgChdIgNAGQAqgTAngNIAAhqIADACQBOA1AoABIABAAQArgBBOg1IAEgDIAABtIBAAaQgCBbg3BBQg3A/hOAAQhMAAg4g/g");
	this.shape_6.setTransform(85,139.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#56392C").s().p("Ak2EpIAAABIgGAAQgPACgLAKQgdhCABhPQAAiaBthrQBBhBBRgaIgBgSQAAgwAjgjQAhgiAwAAIAAAAQAwAAAiAiQAjAjAAAwIgBASQBRAaBBBBQBtBrAACaQAABPgcBCQgLgKgPgCIgGAAIgGAUQjIhIgsimQgGgVgCgXQgSAlgdAXIgGAFQg1Ang1AJQhOAOgZAKQhFAegQB4QgJgNgHgNgAkaCHIAAAAIAAgCIAAACg");
	this.shape_7.setTransform(85.2,38.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#369F93").s().p("AGZL8QgPiagXhGQgWgihJgnQgSgMgUgIIgwgUIAAABIhBgaIAAhtQBBgsAXgeQAngyAVguQAKgZAIgbIAIACQAaAEAWgZQAWgZAEgkQAEgngQgeQgHgNgJgIQAchDAAhOQAAiahthtQhBhChSgaIACgRQAAgxgjgiQgigjgxAAIgBAAQguABgiAiQgjAiABAxIABARQhSAahBBCQhtBtAACaQAABOAcBDQgJAIgHANQgQAeAEAnQAEAkAVAZQAXAZAagEIAHgCQAIAbAKAZQAVAuAnAyQAXAfBCAsIAABrQgnANgqASIgjAPQgTAIgTAMQhJAngWAiQgXBGgPCaIgDAgQhng5hVhVQh2h2hBiYQhDieAAitQAAirBDieQBBiZB2h1QB1h2CZhAQCehDCrAAQCtAACdBDQCZBAB1B2QB2B1BACZQBDCeABCrQgBCthDCeQhACYh2B2QhVBVhnA5IgDggg");
	this.shape_8.setTransform(85,79.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAAAgQgigBgZgSQgZgRAAgbICpAAQAAAbgZARQgZATgjAAIAAAAg");
	this.shape_9.setTransform(85.2,100.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,170,170.2);


(lib.Symbol19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Isolation Mode
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E08C30").s().p("AguAYQgCgCAGgKQALgSAXgKQAWgLAWAFQAMADgBAHQgCAGgLgCQgwgLgZAmQgEAFgBAAIgCAAg");
	this.shape.setTransform(71.4,63.8,0.99,0.99);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E08C30").s().p("AAoATQgZgmgxALQgKACgCgGQgBgHALgDQAXgFAVALQAXAKAMASQAFAKgCACIgBAAQgCAAgDgFg");
	this.shape_1.setTransform(98.3,63.8,0.99,0.99);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3D494E").s().p("ABZAmQgIgQgBgWQABgVAIgQQAKgQAMAAQANAAAJAQQAJAQAAAVQAAAWgJAQQgJAQgNAAQgMAAgKgQgAiEAmQgJgQAAgWQAAgVAJgQQAKgQAMAAQANAAAIAQQAKAQgBAVQABAWgKAQQgIAQgNAAQgMAAgKgQg");
	this.shape_2.setTransform(84.8,73.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 5
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAjCDIAAgBIgbgmIgEgGIgEgHIB8g/IACgBIBBAmIhUBDIgzApgAiMBeIgsgkIgGgFIBBgmIACABIB7A/IgKARIgBABIgYAhIgWAfgAgaiSQgKgGgBgIIBLAAQAAAIgMAGQgKAGgQAAQgOAAgMgGg");
	this.shape_3.setTransform(85,117.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#353A44").s().p("AhQCyIgFhDIABgCIBKkVIBRhDQAZAHAXAIIh7C5IBBAOIizEBg");
	this.shape_4.setTransform(97,146.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#2A968C").s().p("AglClIAEgwIABgSIAAgDIATj1IgVgWIAAAAIgBgBIAYggIABgCIAKgQIAEAGIAEAHIAbAlIAAABIgLALIgFAFIgFAGIATD1IAAACIAFBDIgmA6g");
	this.shape_5.setTransform(85,147.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#B3C1CA").s().p("Ah7iiIBTBEIAXggIAAABIAAAAIATAWIgRD1IAAACIgBATgAAfhnIAFgGIAFgFIALgLIAVAfIAzgpIhKEVg");
	this.shape_6.setTransform(83.3,143.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#2B2F34").s().p("AkKDqIAkiiIANg0QAoimAUgUQAWgXB/gaQAtgKAsgIIAsAkIBrFFIgEAwIAnA6g");
	this.shape_7.setTransform(58.3,146.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#5D0206").s().p("Ah7G5IgDgBQhNgjgUgZQgUgagUgyQgXg7gNhEIgDgUIAHAUIACAFQAOAoATAfQAnBAAsAAQAeAAA4gaQA4gZAjAAQAlAAA4AZQA4AaAeAAQAsAAAnhBQASgeAOgnIACgGIAHgUIgDAUQgMBEgXA7QgUAzgUAZQgUAZhOAjIgDABQhQAkgsAAQgrAAhQgkgAgZFwQALAGAOAAQAQAAALgGQALgGAAgIIhKAAQAAAIALAGgADpBVQgLgwAFhkQAWhAgehNIAAAAQgbg2g/gIQgqgEhXATIAAAAQhvgbhCAkQgUALgOAQIgIAMQgdBNAWA/QAFBkgLAwQgLAtgjAeQAJhmgMglQgLgjglAMIAAgBQgIgqAFg9QAGhDATg1QAVg6AegYQBIhmBagpQAvgWAvgDQAmgCAmAJQBRATA+A+QA8A8ATBOIAAAAIAKAYQATA1AGBDQAFA9gIAqIAAABQglgMgLAjQgMAlAJBmQgjgegLgtg");
	this.shape_8.setTransform(85,65.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#EED2B6").s().p("ABcELQg4gagkAAQgkAAg3AaQg4AZgeAAQgtAAgnhAQgSgfgOgnIgCgGIAAAAIgHgTIADAUQgZADgVgYQgWgZgEgmQgEgnAQgeQAMgUARgHIAAAAQAlgMAMAmQALAjgIBlQAigdALgtQALgwgFhlQgVg/AdhMIAIgNQANgQAVgLQBBgkBwAbIAAAAQBWgTArAFQA+AHAcA2IAAABQAdBNgWA/QgFBlAMAwQALAtAiAdQgIhlALgjQAMgmAlAMIAAgBQARAIAMAUQARAegEAnQgEAmgWAZQgVAYgZgDIADgUIgHATIgBAAIgBAGQgOAogTAeQgmBAgtAAQgeAAg4gZg");
	this.shape_9.setTransform(85,68.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#D19E84").s().p("Ah7ANIAAhYQBQAjArAAQAsAABQgjIAABYIh8A/g");
	this.shape_10.setTransform(85,117.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#27292F").s().p("AkKDlIC1kCIhDgNIB9i6IAlAIQB/AaAVAWQAUAVAoClIAMA1IAlCig");
	this.shape_11.setTransform(111.8,147.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#716867").s().p("AAAgBIABAAIgBADIAAgDg");
	this.shape_12.setTransform(114.6,83.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#786F6E").s().p("ApXKpQh2h1hBiZQhDieAAitQAAirBDieQBBiYB2h2QB1h1CZhBQCehDCrAAQCtAACdBDQCZBBB1B1QB2B2BACYQBDCeABCrQgBCthDCeQhACZh2B1QgwAwg2AoIgMg1QgoiogUgUQgWgXh/gaIgmgHQgYgIgYgHIhBgmIgCABIAAhbIADgBQBOgjATgZQAVgZAUgzQAXg7AMhEQAZADAVgYQAWgYAEglQAEgmgRgeQgLgXgSgHQAIgqgGg9QgGhDgSg1IgKgYIAAAAQgThOg8g8Qg/g+hQgTQgngJglACQgvADgvAWQhaAphIBmQgeAYgWA6QgTA1gFBDQgFA9AIAqQgSAIgMAWQgPAeADAmQAEAlAWAYQAVAYAZgDQAMBEAXA7QAUAyAUAaQAVAZBNAjIADABIAABbIgCgBIhBAmIAGAFQgtAHguAKQiAAagWAXQgTAUgpCoIgMA1Qg2gogwgwgAknBBIABAAIABAFIgCgFg");
	this.shape_13.setTransform(85,76.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,170,170);


(lib.Symbol18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Isolation Mode
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#D9B293").s().p("Ag+gVQAbAXAjAAQAiAAAcgXQgRAsgtAAQgrgBgTgrg");
	this.shape.setTransform(85.4,86.9,0.94,0.94);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#9F6D5C").s().p("ACzARQgUgRgZgDQgYgCgXAJQgNAGgDgHQgDgEANgJQAVgPAhAHQAfAHAUAUQAIAKgEACIgCAAQgEAAgFgEgAi9AVQgEgCAIgKQAUgUAfgHQAhgHAVAPQANAJgEAEQgDAHgMgGQgXgJgXACQgaADgUARQgFAEgEAAIgCAAg");
	this.shape_1.setTransform(84.1,63.6,0.94,0.94);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3D494E").s().p("ABwAoQgJgRAAgXQAAgWAJgRQAIgRANAAQANAAAKARQAIARABAWQgBAXgIARQgKARgNAAQgNAAgIgRgAiaAoQgKgRABgXQgBgWAKgRQAJgRAMAAQANAAAKARQAIARABAWQgBAXgIARQgKARgNAAQgMAAgJgRg");
	this.shape_2.setTransform(85.4,73.4,0.94,0.94);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 5
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#A0B6C1").s().p("AkKDsIAAgBIAwhaIgeksIAUgTIA5BIIA8iFIB5AWQB/AaAVAXQAUAUApCmIAMA0IAkCigAiWCFICcAAIAAgkIicAAg");
	this.shape_3.setTransform(111.7,146.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#2DA192").s().p("AguCLIAdksIgTgTIAkgwIAlAwIgTATIAdEsIgvBag");
	this.shape_4.setTransform(85,147);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#778E9A").s().p("ABzARIAAghICfAAIAAAhgAkRARIAAghICfAAIAAAhg");
	this.shape_5.setTransform(85,157.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#BACBD3").s().p("AkKDsIAkiiIAMg0QApimATgUQAWgXB/gaQA+gNA7gJIA8CFIA5hIIAUATIgeEsIAwBaIAAABgAgFCFICcAAIAAgkIicAAg");
	this.shape_6.setTransform(58.3,146.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#D19E84").s().p("Ah7AfIAAhvQBQAkArAAQAsAABQgkIAABvIh8Ayg");
	this.shape_7.setTransform(85,115.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAlCNIglgvIB8gzIACgBIAfAgIgDAGIg8CFgAiZBQIgDgGIAfggIACABIB7AzIgkAvIg5BIgAAABegAhHi3QgegMAAgRIDLAAQAAARgfAMQgdAMgqAAQgoAAgfgMg");
	this.shape_8.setTransform(85,114.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#EED2B6").s().p("Ah7E4IgDgCQhNgjgVgZQgcglgZhMQgKghgHgiIgDAAQgaAEgWgZQgWgYgEglQgDgmAPgfQARgeAagDIAKAAQAQACAKAIQAMAJABAuQAAAWgCAVIAWAAQgDgVABgZQADgxAYgRIAAg2QgDAAgEgJQgIgRAAgnQAAgyA6gdQA+geBzAAIACAAQBzAAA9AeQA7AdAAAyQAAAngHARQgEAJgEAAIAAA2QAYARADAxQACAZgDAVIAVAAQgCgVAAgWQACguAKgJQALgIAPgCIAIAAIAGAAQAaADAQAeQARAfgEAmQgFAlgVAYQgWAZgagEIgDAAQgHAigKAhQgYBMgeAlQgTAZhOAjIgDACQhQAjgsAAQgrAAhQgjgAhHDEQAfANAoAAQAqAAAdgNQAfgMAAgRIjLAAQAAARAeAMg");
	this.shape_9.setTransform(85,76.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#EF961C").s().p("AHmLMQgpiogTgUQgWgXh/gaIh7gWIADgGIgfggIgCABIAAhxIADgBQBOgjATgZQAegmAYhMQAKghAHgiIADAAQAaAEAWgZQAVgYAFglQAEgmgRgeQgQgegagEIgGAAIAHAAIAAigQgBiSighLQgygYg8gOIgvgIIAAgBIAAAAIgCAAIAAABQhOAIhPAmQihBLABCSIAACgIADAAQgaAEgRAeQgPAeADAmQAEAlAWAYQAWAZAagEIADAAQAHAiAKAhQAZBMAcAmQAVAZBNAjIADABIAABxIgCgBIgfAgIADAGQg7AJhAANQh/AagWAXQgUAUgoCoIgMA1Qg2gogwgwQh2h1hBiZQhDieAAitQAAirBDieQBBiYB2h2QB1h1CZhBQCehDCrAAQCtAACdBDQCZBBB1B1QB2B2BACYQBDCeABCrQgBCthDCeQhACZh2B1QgwAwg2AoIgMg1g");
	this.shape_10.setTransform(85,76.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#7F5538").s().p("AD3ELQAEgVgDgYQgDgxgYgRIAAg2QAFAAADgJQAHgRAAgnQAAgwg6gdQg9geh0gBIgBAAQh0ABg9AeQg7AdAAAwQAAAnAIARQAEAJADAAIAAA2QgYARgDAxQgBAYADAVIgVAAQACgUgBgXQAAgtgMgJQgLgJgPgBIgKAAIgEAAIAAigQAAiQCghMQBPglBPgJIAAAAIAAAAIABAAIAAAAIAvAJQA9AOAxAXQChBMAACQIAACgIgGgBIgIABQgPABgLAJQgLAJgBAtQAAAXACAUg");
	this.shape_11.setTransform(84.9,48.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,170,170);


(lib.Symbol15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgbAxQgNgGgGgNQgHgNAAgRQAAgQAHgNQAGgNANgHQAMgHAPAAQARAAAMAHQAMAHAHANQAGANAAAQQAAARgGANQgHANgMAGQgMAIgRAAQgPAAgMgIgAgVgbQgIALAAAQQAAARAIALQAIAKANAAQAOAAAIgKQAIgKAAgSQAAgRgIgKQgIgKgOAAQgNAAgIAKg");
	this.shape.setTransform(247.6,22.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAXA2IAAgvIgtAAIAAAvIgYAAIAAhrIAYAAIAAArIAtAAIAAgrIAYAAIAABrg");
	this.shape_1.setTransform(235.4,22.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgKA2IAAhYIgkAAIAAgTIBdAAIAAATIgkAAIAABYg");
	this.shape_2.setTransform(224.5,22.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAaA1QgEgDgCgHQgHAHgKADQgIAEgLgBQgRAAgKgIQgJgJAAgPQAAgLAEgGQAFgGAHgCQAHgDAJgCIAQgCIALgCIAIgCIADgCIABgDIAAgFQAAgIgFgEQgGgEgJAAQgLgBgGAFQgGAEgBALIgXAAQACgUANgJQANgIAUAAQALAAAKADQAKAEAFAHQAGAHAAAMIAAA1IABAGQABABAAAAQABAAAAABQABAAAAAAQABAAABAAIACAAIADgBIAAARIgIACIgHAAIgCABQgGAAgEgEgAAHAEIgLABQgGABgFACQgGABgDAEQgEADAAAHQAAAIAFAEQAFAEAKAAQAHAAAFgCQAGgCAEgEQAEgEABgFIAAgKIAAgMQgGACgGACg");
	this.shape_3.setTransform(214,22.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgtA2IgGgBIAAgTIAEABIAEAAQAEAAADgCQADgCADgGQACgGAAgLIACg+IBOAAIAABrIgYAAIAAhYIgfAAIgCAtQAAAWgJAMQgIALgPAAIgIgBg");
	this.shape_4.setTransform(201.5,22.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAXA2IAAhYIgtAAIAABYIgYAAIAAhrIBdAAIAABrg");
	this.shape_5.setTransform(190,22.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgZAyQgNgIgGgMQgGgNAAgQQAAgQAGgNQAHgOAMgGQAMgIAPAAQAUAAAOALQAMAKACATIgYAAQgCgKgGgGQgGgFgKAAQgMAAgIAKQgIAKAAARQAAASAIAJQAIAKALABQALAAAGgGQAIgHABgMIAYAAQgDAVgNALQgOAMgUAAQgPgBgLgGg");
	this.shape_6.setTransform(178.2,22.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgaAxQgLgHgHgNQgGgMAAgQQAAgPAGgNQAHgOAMgIQAMgHANAAQAQAAAMAHQALAIAHAOQAFANAAAQIAAADIhOAAQABARAIAIQAHAJANAAQAJAAAGgEQAHgFADgLIAXAAQgDAMgGAJQgIAIgKAFQgKAEgMAAQgPAAgMgIgAAcgJQgCgNgHgJQgHgHgMAAQgKAAgHAHQgIAIgBAOIA2AAIAAAAg");
	this.shape_7.setTransform(166.5,22.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgbBGQgNgHgGgPQgDgHgCgJIgBgSIABgVQACgLADgLQADgLAFgIQAHgLAKgFQALgGANgBIAPgBQAFgBACgEIAYAAQgCAKgEAFQgEAFgHADQgGACgIAAIgRACQgJABgHAFQgIAGgFAJQgFAIgCAMIAAAAQAHgLAJgGQAJgGAOAAQAOAAALAHQALAHAGAMQAHALAAARQAAARgHAMQgGANgMAHQgLAHgQAAQgQAAgMgIgAgUgDQgIAIAAARQAAARAIAKQAIAKAMAAQANAAAIgKQAIgKAAgRQAAgRgIgIQgIgKgNAAQgMAAgIAKg");
	this.shape_8.setTransform(154.5,20.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AguA2IAAhrIAYAAIAAAoIAgAAQASAAAJAJQAKAHAAAQQAAAQgKAJQgJAKgSAAgAgWAjIAdAAQAIAAAEgEQAEgFgBgGQAAgIgDgEQgFgFgIAAIgcAAg");
	this.shape_9.setTransform(137,22.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgKA2IAAhYIgkAAIAAgTIBdAAIAAATIgkAAIAABYg");
	this.shape_10.setTransform(126.1,22.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AAaA1QgEgDgCgHQgHAHgKADQgIAEgLgBQgRAAgKgIQgJgJAAgPQAAgLAEgGQAFgGAHgCQAHgDAJgCIAQgCIALgCIAIgCIADgCIABgDIAAgFQAAgIgFgEQgGgEgJAAQgLgBgGAFQgGAEgBALIgXAAQACgUANgJQANgIAUAAQALAAAKADQAKAEAFAHQAGAHAAAMIAAA1IABAGQABABAAAAQABAAAAABQABAAAAAAQABAAABAAIACAAIADgBIAAARIgIACIgHAAIgCABQgGAAgEgEgAAHAEIgLABQgGABgFACQgGABgDAEQgEADAAAHQAAAIAFAEQAFAEAKAAQAHAAAFgCQAGgCAEgEQAEgEABgFIAAgKIAAgMQgGACgGACg");
	this.shape_11.setTransform(115.6,22.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgvA2IAAhrIA6AAQAPAAAJAHQAIAHAAANQAAAJgEAFQgDAFgJAEIAAAAQALADAFAFQAFAHAAALQAAAPgLAIQgKAIgTAAgAgXAkIAfAAQAIAAAEgEQAEgEAAgHQAAgHgEgEQgEgEgIAAIgfAAgAgXgKIAdAAQAGAAAEgDQADgDAAgHQABgGgEgDQgEgDgHAAIgcAAg");
	this.shape_12.setTransform(104,22.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgbAxQgNgGgGgNQgHgNAAgRQAAgQAHgNQAGgNANgHQAMgHAPAAQARAAAMAHQAMAHAHANQAGANAAAQQAAARgGANQgHANgMAGQgMAIgRAAQgPAAgMgIgAgVgbQgIALAAAQQAAARAIALQAIAKANAAQAOAAAIgKQAIgKAAgSQAAgRgIgKQgIgKgOAAQgNAAgIAKg");
	this.shape_13.setTransform(91.7,22.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgbBGQgNgHgGgPQgDgHgCgJIgBgSIABgVQACgLADgLQADgLAFgIQAHgLAKgFQALgGANgBIAPgBQAFgBACgEIAYAAQgCAKgEAFQgEAFgHADQgGACgIAAIgRACQgJABgHAFQgIAGgFAJQgFAIgCAMIAAAAQAHgLAJgGQAJgGAOAAQAOAAALAHQALAHAGAMQAHALAAARQAAARgHAMQgGANgMAHQgLAHgQAAQgQAAgMgIgAgUgDQgIAIAAARQAAARAIAKQAIAKAMAAQANAAAIgKQAIgKAAgRQAAgRgIgIQgIgKgNAAQgMAAgIAKg");
	this.shape_14.setTransform(79.4,20.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgbAxQgNgGgGgNQgHgNAAgRQAAgQAHgNQAGgNANgHQAMgHAPAAQARAAAMAHQAMAHAHANQAGANAAAQQAAARgGANQgHANgMAGQgMAIgRAAQgPAAgMgIgAgVgbQgIALAAAQQAAARAIALQAIAKANAAQAOAAAIgKQAIgKAAgSQAAgRgIgKQgIgKgOAAQgNAAgIAKg");
	this.shape_15.setTransform(66.9,22.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("Ag0BLIAAiSIAXAAIAAAOQAGgJAIgEQAKgEAJAAQAPAAALAHQALAHAGANQAGANAAASQAAAQgGALQgGANgLAHQgKAHgPAAQgJAAgJgEQgJgEgGgJIAAA2gAgVgtQgIAKAAASQAAARAIAJQAIAKANAAQANAAAIgKQAIgJAAgQQAAgSgIgLQgIgKgNAAQgNAAgIAKg");
	this.shape_16.setTransform(54.5,24.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AAXA2IAAhYIgtAAIAABYIgYAAIAAhrIBdAAIAABrg");
	this.shape_17.setTransform(41.8,22.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgbAxQgNgGgGgNQgHgNAAgRQAAgQAHgNQAGgNANgHQAMgHAPAAQARAAAMAHQAMAHAHANQAGANAAAQQAAARgGANQgHANgMAGQgMAIgRAAQgPAAgMgIgAgVgbQgIALAAAQQAAARAIALQAIAKANAAQAOAAAIgKQAIgKAAgSQAAgRgIgKQgIgKgOAAQgNAAgIAKg");
	this.shape_18.setTransform(29.6,22.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAiBKIAAh9IhDAAIAAB9IgaAAIAAiTIB3AAIAACTg");
	this.shape_19.setTransform(15.9,20.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 2
	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#55B553").ss(6.2,1,1).p("A0KlJMAoVAAAQByAAAAByIAAGvQAAByhyAAMgoVAAAQhyAAAAhyIAAmvQAAhyByAAg");
	this.shape_20.setTransform(132.1,21.5,0.94,0.65);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#55B553").s().p("A0KFKQhyAAABhyIAAmvQgBhxBygBMAoVAAAQBxABABBxIAAGvQgBByhxAAg");
	this.shape_21.setTransform(132.1,21.5,0.94,0.65);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3.1,-3.1,270.3,49.1);


(lib.Symbol14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_44 = function() {
		/* Stop at This Frame
		The  timeline will stop/pause at the frame where you insert this code.
		Can also be used to stop/pause the timeline of movieclips.
		*/
		
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(44).call(this.frame_44).wait(1));

	// 8629
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#55B553").s().p("AhOB2QgbgoAAhOQAAhMAbgoQAbgoAzgBQA0AAAbAoQAbAoAABOQAABNgbAoQgbAog0AAQgzAAgbgogAgfhnQgLAMgFASQgFASgBAVQgCAUABARQgBA7AOAbQAOAbAbAAQAUgBAMgMQALgMAFgSQAFgTABgTQACgTgBgPQABgOgCgTQgBgUgFgTQgEgSgMgNQgMgMgUAAQgTAAgMALg");
	this.shape.setTransform(170.1,20.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#55B553").s().p("AALCaIAAjTIhLAAIAAglQARABATgEQATgEANgNQAPgNAFgaIApAAIAAEzg");
	this.shape_1.setTransform(143.5,20.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#55B553").s().p("AhOB2QgbgoAAhOQAAhMAbgoQAbgoAzgBQA0AAAbAoQAbAoAABOQAABNgbAoQgbAog0AAQgzAAgbgogAgfhnQgLAMgFASQgFASgBAVQgCAUABARQgBA7AOAbQAOAbAbAAQAUgBAMgMQALgMAFgSQAFgTABgTQACgTgBgPQABgOgCgTQgBgUgFgTQgEgSgMgNQgMgMgUAAQgTAAgMALg");
	this.shape_2.setTransform(121.2,20.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#55B553").s().p("AhOB2QgbgoAAhOQAAhMAbgoQAbgoAzgBQA0AAAbAoQAbAoAABOQAABNgbAoQgbAog0AAQgzAAgbgogAgfhnQgLAMgFASQgFASgBAVQgCAUABARQgBA7AOAbQAOAbAbAAQAUgBAMgMQALgMAFgSQAFgTABgTQACgTgBgPQABgOgCgTQgBgUgFgTQgEgSgMgNQgMgMgUAAQgTAAgMALg");
	this.shape_3.setTransform(96.7,20.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#55B553").s().p("AAVCbIAAhJIiBAAIAAgyICCi6IAwAAIAADBIAnAAIAAArIgnAAIAABJgAhCAnIBYAAIAAiDg");
	this.shape_4.setTransform(169.9,20.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#55B553").s().p("Ag8CZQABghAJgjQAKglAQgjQAQgiATgfQAVggAXgVIifAAIAAguIDQAAIAAApQgXAZgVAfQgTAfgPAhQgPAkgIAkQgIAjAAAkg");
	this.shape_5.setTransform(145.5,20.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#55B553").s().p("Ag4CSQgYgNgNgXQgNgWAAgfIAAgHIAwAAQABAhAPAQQAPAQAbAAQAaAAAOgPQAPgOAAgaQAAgTgIgLQgJgLgOgFQgOgGgOABIgIAAIgIAAIAAgkIAEAAQAQAAAMgDQAPgCAJgKQAJgJABgUQgBgUgMgMQgNgMgUgBQgYAAgNAQQgNAPgBAeIgwAAQACgwAagbQAagbAtAAQAcAAAVAKQAWALAMASQAMATAAAZQAAAWgMARQgLAQgUAHQAaAHAPARQAOATAAAcQAAAdgNAXQgOAWgZANQgYAMgfAAQggAAgYgMg");
	this.shape_6.setTransform(96.5,20.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#55B553").s().p("Ag3CQQgXgMgNgUQgNgVgCgdIAyAAQADAYAPAOQAOAOAYAAQAaAAAPgSQAOgRAAggQAAgegPgRQgPgPgaAAQgRAAgMAGQgMAHgKANIgsgCIAdijICdAAIAAArIh5AAIgOBPQAMgMAPgFQAOgFAPAAQAdABAWAMQAVAMAMAXQAMAVAAAeQAAAhgOAYQgOAZgZAOQgZAOgeAAQgfAAgXgLg");
	this.shape_7.setTransform(169.8,20.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#55B553").s().p("Ag3CSQgYgMgOgUQgOgWAAgcQAAgdAPgTQAPgTAbgIQgVgHgMgRQgLgRAAgXQAAgYAMgTQAMgTAWgJQAWgKAbgBQAcABAVAKQAVAJAMATQAMASAAAXQAAAYgLARQgLAQgWAJQAbAJAPAQQAPARAAAfQAAAcgOAXQgOAVgYAMQgYAMggAAQgfAAgYgMgAgoAWQgQAPAAAZQAAAaAQAPQAQAPAZAAQAaAAAPgOQAPgOAAgbQAAgagQgPQgPgPgaAAQgZAAgPAPgAgihpQgNANAAAUQAAAVANALQAOAMAUABQAWgBANgLQANgMAAgVQAAgUgNgNQgNgMgWABQgVgBgNAMg");
	this.shape_8.setTransform(145.6,20.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#55B553").s().p("AhoCbQABgjAMgXQAMgXAUgSQAVgRAagRIAdgVQAPgJALgPQALgPAAgVQgBgYgNgNQgMgPgYAAQgUAAgLAMQgMAKgEARQgEAQAAASIgxAAIAAgHQABgiALgYQAOgYAWgOQAXgNAfAAQAdAAAXAMQAWALAMAUQAMAWABAcQgBAcgMAUQgMASgSAOQgUAPgWAOIgXARQgNAKgLALQgLAMgFAMICUAAIAAAtg");
	this.shape_9.setTransform(120.5,20.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#55B553").s().p("AhNB2QgcgnAAhLQAAgyANgkQAOgkAagUQAZgTAiAAQAoAAAXAVQAZAWAFAnIgxAAQgEgWgLgKQgMgLgUAAQgSABgLALQgNALgGARQgGARgDASIgEAgQAMgSASgKQARgKAWAAQAcAAAVANQAWANALAWQAMAVAAAeQAAAggMAYQgOAZgXANQgXAOgfAAQg0AAgcgogAgkAGQgOAQAAAdQAAAeAPASQAPARAXAAQAYAAAOgRQAPgRABgeQgBgdgPgRQgOgPgZAAQgXAAgPAPg");
	this.shape_10.setTransform(170.2,20.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#55B553").s().p("Ag0CUQgVgLgMgSQgNgTgBgYIAwAAQACATAMALQANAMAVABQARgBAMgLQAMgLAGgRQAHgRACgSQADgSAAgPQgMAUgRAJQgQAKgVAAQgcAAgWgNQgVgNgMgWQgMgVgBgeQABggANgYQANgZAYgOQAXgNAfAAQAfAAAVANQAVAOAMAWQANAXAFAaQAFAbgBAbQABAagGAdQgFAdgNAYQgNAZgWAPQgWAPgfAAQgbAAgUgKgAgphhQgOARAAAeQAAAcAOARQAPAPAYAAQAXgBAOgOQAPgRAAgcQAAgegPgRQgOgSgYAAQgYAAgOASg");
	this.shape_11.setTransform(145.4,20.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#55B553").s().p("Ag9CZQACghAJgjQAKglAQgjQAQgiATgfQAVggAXgVIieAAIAAguIDPAAIAAApQgXAZgVAfQgTAfgQAhQgOAkgIAkQgHAjgBAkg");
	this.shape_12.setTransform(170,20.3);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#55B553").s().p("AhNB2QgcgnAAhLQAAgyANgkQAOgkAagUQAZgTAiAAQAoAAAXAVQAZAWAFAnIgxAAQgEgWgLgKQgMgLgUAAQgSABgLALQgNALgGARQgGARgDASIgEAgQAMgSASgKQARgKAWAAQAcAAAVANQAWANALAWQAMAVAAAeQAAAggNAYQgNAZgXANQgXAOgfAAQg0AAgcgogAgkAGQgOAQAAAdQAAAeAPASQAPARAXAAQAYAAAOgRQAPgRABgeQgBgdgPgRQgOgPgZAAQgXAAgPAPg");
	this.shape_13.setTransform(96.8,20.3);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#55B553").s().p("AhoCbQABgjAMgXQAMgXAVgSQATgRAbgRIAdgVQAPgJALgPQALgPAAgVQAAgYgOgNQgNgPgXAAQgUAAgMAMQgLAKgEARQgEAQAAASIgxAAIAAgHQAAgiAMgYQANgYAXgOQAYgNAdAAQAeAAAXAMQAWALANAUQAMAWAAAcQAAAcgMAUQgMASgUAOQgSAPgWAOIgYARQgMAKgMALQgLAMgFAMICTAAIAAAtg");
	this.shape_14.setTransform(145,20.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#55B553").s().p("AhNB2QgdgnAAhLQAAgyAOgkQANgkAagUQAagTAiAAQAnAAAYAVQAZAWAEAnIgwAAQgEgWgMgKQgLgLgTAAQgTABgMALQgMALgGARQgHARgCASIgEAgQAMgSASgKQARgKAWAAQAcAAAVANQAWANAMAWQAMAVgBAeQABAggNAYQgOAZgXANQgYAOgeAAQg0AAgcgogAgkAGQgOAQAAAdQABAeAOASQAOARAYAAQAZAAAOgRQAOgRABgeQAAgdgPgRQgPgPgaAAQgWAAgPAPg");
	this.shape_15.setTransform(121.3,20.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1,p:{x:143.5}},{t:this.shape,p:{x:170.1}}]}).to({state:[{t:this.shape_6,p:{x:96.5}},{t:this.shape_1,p:{x:119}},{t:this.shape_5},{t:this.shape_4,p:{x:169.9}}]},14).to({state:[{t:this.shape_4,p:{x:96.5}},{t:this.shape_9},{t:this.shape_8,p:{x:145.6}},{t:this.shape_7,p:{x:169.8}}]},5).to({state:[{t:this.shape_7,p:{x:96.4}},{t:this.shape_6,p:{x:120.9}},{t:this.shape_11,p:{x:145.4}},{t:this.shape_10}]},5).to({state:[{t:this.shape_13},{t:this.shape_4,p:{x:121}},{t:this.shape,p:{x:145.6}},{t:this.shape_12,p:{x:170}}]},5).to({state:[{t:this.shape_12,p:{x:96.6}},{t:this.shape_7,p:{x:120.9}},{t:this.shape_1,p:{x:143.5}},{t:this.shape_8,p:{x:170.1}}]},5).to({state:[{t:this.shape_8,p:{x:96.7}},{t:this.shape_15},{t:this.shape_14},{t:this.shape_11,p:{x:169.9}}]},5).to({state:[{t:this.shape_8,p:{x:96.7}},{t:this.shape_15},{t:this.shape_14},{t:this.shape_11,p:{x:169.9}}]},5).wait(1));

	// клиентов уже пользуются сервисом
	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#55B553").s().p("AAbAmIAAgxIAAAAIgTAxIgPAAIgSgxIgBAAIAAAxIgQAAIAAhLIAWAAIAUA7IAAAAIAVg7IAXAAIAABLg");
	this.shape_16.setTransform(259.4,62.3);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#55B553").s().p("AgTAjQgJgEgFgKQgEgJAAgMQAAgLAEgJQAFgJAJgGQAJgEAKAAQAMAAAIAEQAJAGAFAJQAFAIgBAMQABAMgFAJQgFAKgJAEQgIAGgMAAQgKAAgJgGgAgPgTQgFAHgBAMQABANAFAGQAHAIAIAAQAKAAAGgIQAFgGAAgNQAAgLgFgIQgGgHgKAAQgIAAgHAHg");
	this.shape_17.setTransform(249.7,62.3);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#55B553").s().p("AgRAkQgJgGgEgJQgFgJAAgLQAAgLAFgKQAEgJAJgFQAJgFAJgBQAPABAJAHQAJAIACANIgRAAQgCgIgEgDQgEgEgHAAQgIAAgGAHQgGAIAAALQAAAMAGAIQAFAHAIAAQAIAAAEgFQAFgEACgIIAQAAQgBANgKAJQgJAIgPAAQgKAAgIgEg");
	this.shape_18.setTransform(241.1,62.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#55B553").s().p("AARAmIAAg2IgBAAIgbA2IgWAAIAAhLIARAAIAAA2IABAAIAbg2IAWAAIAABLg");
	this.shape_19.setTransform(232.6,62.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#55B553").s().p("AghAmIAAhLIApAAQALAAAFAFQAHAFgBAJQABAGgDAEQgDAEgFACQAHACADADQAEAGAAAHQAAALgHAGQgHAFgPAAgAgQAZIAVAAQAHAAACgCQADgDAAgFQAAgFgDgDQgCgDgHAAIgVAAgAgQgGIAUAAQAEAAADgDQADgCAAgFQAAgEgDgCQgDgCgFAAIgTAAg");
	this.shape_20.setTransform(224.3,62.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#55B553").s().p("AglA2IAAhoIAQAAIAAAKQAEgHAHgDQAHgCAFAAQALAAAIAEQAIAGAEAJQAFAJAAAMQAAAMgFAIQgEAJgIAFQgHAFgLAAQgGAAgGgDQgGgDgFgHIAAAogAgPggQgGAIAAAMQAAAMAGAGQAGAHAJAAQAJAAAGgHQAFgGAAgMQAAgMgFgIQgGgHgJAAQgJAAgGAHg");
	this.shape_21.setTransform(215.5,63.6);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#55B553").s().p("AgSAjQgJgFgEgJQgFgJAAgMQAAgKAFgJQAFgKAIgFQAJgGAJABQALAAAIAEQAJAGAEAKQAEAKABAKIAAADIg4AAQABALAFAHQAGAGAIAAQAHAAAEgEQAFgDACgHIARAAQgDAJgEAFQgFAGgIADQgHAEgJAAQgKgBgIgFgAAUgGQgBgKgGgFQgFgGgIAAQgHAAgFAGQgFAFgBAKIAmAAIAAAAg");
	this.shape_22.setTransform(206.6,62.3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#55B553").s().p("AgRAkQgJgGgEgJQgFgJAAgLQAAgLAFgKQAEgJAJgFQAJgFAJgBQAPABAJAHQAJAIACANIgRAAQgCgIgEgDQgEgEgHAAQgIAAgGAHQgGAIAAALQAAAMAGAIQAFAHAIAAQAIAAAEgFQAFgEACgIIAQAAQgBANgKAJQgJAIgPAAQgKAAgIgEg");
	this.shape_23.setTransform(198.3,62.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#55B553").s().p("AAUAmIAAgfIgOAAIgWAfIgUAAIAZghQgGgCgEgDQgEgCgBgEQgBgFAAgFQAAgKAGgGQAHgFAMAAIAnAAIAABLgAgHgWQgDADAAAFQAAAFADADQADADAEAAIAUAAIAAgVIgUAAQgEAAgDACg");
	this.shape_24.setTransform(185.2,62.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#55B553").s().p("AgRAkQgJgGgEgJQgFgJAAgLQAAgLAFgKQAEgJAJgFQAJgFAJgBQAPABAJAHQAJAIACANIgRAAQgCgIgEgDQgEgEgHAAQgIAAgGAHQgGAIAAALQAAAMAGAIQAFAHAIAAQAIAAAEgFQAFgEACgIIAQAAQgBANgKAJQgJAIgPAAQgKAAgIgEg");
	this.shape_25.setTransform(177.4,62.3);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#55B553").s().p("AgHAmIAAg+IgaAAIAAgNIBDAAIAAANIgaAAIAAA+g");
	this.shape_26.setTransform(169.8,62.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#55B553").s().p("AgJAfQgKgKgBgRIgPAAIAAAiIgRAAIAAhLIARAAIAAAeIAPAAQACgPAKgJQAIgJAPABQALAAAJAEQAIAGAFAJQAEAIAAAMQAAAMgEAJQgFAKgIAEQgJAGgLAAQgPgBgJgJgAAAgTQgEAHABAMQgBANAEAGQAFAIAKAAQAKAAAFgIQAFgGAAgNQAAgLgFgIQgFgHgKAAQgKAAgFAHg");
	this.shape_27.setTransform(160.4,62.3);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#55B553").s().p("AgXA1IgGgBIAAgOIAFABIAEAAQAEAAACgEQACgCACgEIACgIIgdhJIATAAIASA3IAUg3IASAAIgdBKIgGAOQgDAHgDAGQgFAEgJAAIgGAAg");
	this.shape_28.setTransform(150.3,63.8);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#55B553").s().p("AgYAhQgIgHAAgMIARAAQABAFACADQADADAEABIAFABQAHAAAFgDQAEgDAAgFQAAgFgDgEQgDgDgJAAIgHAAIAAgKIAGAAQAFAAAFgDQADgCABgGQAAgEgDgDQgEgCgHAAQgFAAgDADQgEADgBAFIgSAAQACgJAFgGQAEgFAIgCQAHgCAGAAQAJAAAGACQAGACAFAEQADAFAAAIQABAEgCADIgEAGQgEACgCABIAAAAIAHACIAFAFQACAEABAGQgBAJgFAFQgEAGgIADQgIACgHAAQgPAAgKgHg");
	this.shape_29.setTransform(142.5,62.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#55B553").s().p("AggAmIAAhLIARAAIAAAcIAWAAQANAAAGAGQAHAFAAALQAAAMgHAHQgGAGgNAAgAgPAZIAUAAQAFAAADgDQADgDAAgFQAAgFgDgEQgDgDgGAAIgTAAg");
	this.shape_30.setTransform(134.8,62.3);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#55B553").s().p("AggAmIgEgBIAAgNIADABIADAAIAFgCQACgBACgFQABgEABgHIABgsIA3AAIAABLIgRAAIAAg+IgWAAIgBAgQAAAPgGAJQgGAIgLAAIgGgBg");
	this.shape_31.setTransform(125.9,62.4);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#55B553").s().p("AgTAjQgJgEgFgKQgEgJAAgMQAAgLAEgJQAFgJAJgGQAJgEAKAAQAMAAAIAEQAJAGAFAJQAFAIgBAMQABAMgFAJQgFAKgJAEQgIAGgMAAQgKAAgJgGgAgPgTQgFAHAAAMQAAANAFAGQAGAIAJAAQAKAAAGgIQAFgGABgNQgBgLgFgIQgGgHgKAAQgJAAgGAHg");
	this.shape_32.setTransform(117.6,62.3);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#55B553").s().p("AARAmIAAg+IghAAIAAA+IgRAAIAAhLIBDAAIAABLg");
	this.shape_33.setTransform(108.8,62.3);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#55B553").s().p("AgSAjQgJgFgEgJQgFgJAAgMQAAgKAFgJQAFgKAIgFQAJgGAJABQALAAAIAEQAJAGAEAKQAEAKABAKIAAADIg4AAQABALAFAHQAGAGAIAAQAHAAAEgEQAFgDACgHIARAAQgDAJgEAFQgFAGgIADQgHAEgJAAQgKgBgIgFgAAUgGQgBgKgGgFQgFgGgIAAQgHAAgFAGQgFAFgBAKIAmAAIAAAAg");
	this.shape_34.setTransform(96.2,62.3);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#55B553").s().p("AAnAmIgYghIgHAIIAAAZIgPAAIAAgZIgHgIIgYAhIgUAAIAhgqIgdghIAUAAIAbAhIAAAAIAAghIAPAAIAAAhIAAAAIAbghIAVAAIgeAhIAhAqg");
	this.shape_35.setTransform(86.2,62.3);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#55B553").s().p("AgXA1IgHgBIAAgOIAGABIAEAAQADAAADgEQADgCABgEIACgIIgdhJIASAAIAUA3IATg3IASAAIgdBKIgFAOQgDAHgEAGQgFAEgKAAIgFAAg");
	this.shape_36.setTransform(76.5,63.8);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#55B553").s().p("AghAmIAAhLIApAAQALAAAFAFQAHAFgBAJQAAAGgCAEQgDAEgFACQAHACADADQAEAGAAAHQAAALgHAGQgHAFgPAAgAgQAZIAVAAQAHAAACgCQADgDAAgFQAAgFgDgDQgCgDgHAAIgVAAgAgQgGIAUAAQAEAAADgDQADgCAAgFQAAgEgDgCQgDgCgFAAIgTAAg");
	this.shape_37.setTransform(64.4,62.3);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#55B553").s().p("AgTAjQgJgEgEgKQgGgJABgMQgBgLAGgJQAEgJAJgGQAIgEALAAQAMAAAJAEQAIAGAFAJQAFAIAAAMQAAAMgFAJQgFAKgIAEQgJAGgMAAQgLAAgIgGgAgOgTQgHAHABAMQgBANAHAGQAGAIAIAAQAKAAAFgIQAGgGABgNQgBgLgGgIQgFgHgKAAQgIAAgGAHg");
	this.shape_38.setTransform(55.7,62.3);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#55B553").s().p("AgHAmIAAg+IgZAAIAAgNIBBAAIAAANIgZAAIAAA+g");
	this.shape_39.setTransform(47.8,62.3);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#55B553").s().p("AARAmIAAghIghAAIAAAhIgRAAIAAhLIARAAIAAAeIAhAAIAAgeIARAAIAABLg");
	this.shape_40.setTransform(40,62.3);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#55B553").s().p("AgSAjQgJgFgEgJQgFgJAAgMQAAgKAFgJQAFgKAIgFQAJgGAJABQALAAAIAEQAJAGAEAKQAEAKABAKIAAADIg4AAQABALAFAHQAGAGAIAAQAHAAAEgEQAFgDACgHIARAAQgDAJgEAFQgFAGgIADQgHAEgJAAQgKgBgIgFgAAUgGQgBgKgGgFQgFgGgIAAQgHAAgFAGQgFAFgBAKIAmAAIAAAAg");
	this.shape_41.setTransform(31.5,62.3);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#55B553").s().p("AAQAmIAAg2IAAAAIgbA2IgVAAIAAhLIARAAIAAA2IAAAAIAbg2IAVAAIAABLg");
	this.shape_42.setTransform(23,62.3);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#55B553").s().p("AggAmIgEgBIAAgNIADABIADAAIAFgCQACgBACgFQABgEABgHIABgsIA3AAIAABLIgRAAIAAg+IgWAAIgBAgQAAAPgGAJQgGAIgLAAIgGgBg");
	this.shape_43.setTransform(14.1,62.4);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#55B553").s().p("AAPAmIgWgiIgLAKIAAAYIgRAAIAAhLIARAAIAAAiIAdgiIAVAAIgeAfIAiAsg");
	this.shape_44.setTransform(6.6,62.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16}]}).wait(45));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-6,266.7,77.4);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#55B553").s().p("AAVAeIAAgmIgPAmIgLAAIgPgmIAAAmIgOAAIAAg8IASAAIAQAwIARgwIASAAIAAA8g");
	this.shape.setTransform(273.4,62.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#55B553").s().p("AAVAgQgEAAgCgBQgDgCgBgEQgEAEgGACQgDABgGAAQgKAAgGgFQgFgFAAgIQAAgGACgEQADgEAEAAIAJgCIAJgCIAGgBIAEgBIACgBIABgCIAAgDQAAgFgDgCQgDgCgFAAQgGAAgEACQgDADgBAGIgNAAQACgLAHgFQAHgFALAAQAGAAAGACQAFACAEAEQADAEAAAHIAAAdIABAEIACABIABAAIACgBIAAAKIgFABIgEAAgAAEACIgGABIgGABIgFADQgCACAAAEQAAAFADACQADADAFAAQAEAAACgCIAGgDQACgDAAgCIABgGIAAgHIgHACg");
	this.shape_1.setTransform(266,62.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#55B553").s().p("AgdArIAAhTIANAAIAAAIQADgFAFgDQAGgCAEAAQAJAAAGAEQAGAEADAIQAEAHAAAKQAAAJgEAGQgDAHgGAEQgGAEgIAAQgEAAgGgCQgFgCgEgFIAAAfgAgLgZQgFAGAAAKQAAAJAFAFQAEAFAHAAQAIAAAEgFQAEgFAAgJQAAgKgEgGQgFgGgHAAQgHAAgEAGg");
	this.shape_2.setTransform(259,63.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#55B553").s().p("AgUAeIAAg8IApAAIAAALIgbAAIAAAxg");
	this.shape_3.setTransform(252.9,62.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#55B553").s().p("AAVAgQgEAAgCgBQgDgCgBgEQgEAEgGACQgDABgGAAQgKAAgGgFQgFgFAAgIQAAgGACgEQADgEAEAAIAJgCIAJgCIAGgBIAEgBIACgBIABgCIAAgDQAAgFgDgCQgDgCgFAAQgGAAgEACQgDADgBAGIgNAAQACgLAHgFQAHgFALAAQAGAAAGACQAFACAEAEQADAEAAAHIAAAdIABAEIACABIABAAIACgBIAAAKIgFABIgEAAgAAEACIgGABIgGABIgFADQgCACAAAEQAAAFADACQADADAFAAQAEAAACgCIAGgDQACgDAAgCIABgGIAAgHIgHACg");
	this.shape_4.setTransform(246.9,62.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#55B553").s().p("AgFAeIAAgxIgVAAIAAgLIA1AAIAAALIgVAAIAAAxg");
	this.shape_5.setTransform(240.6,62.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#55B553").s().p("AgOAcQgGgEgEgHQgEgHAAgKQAAgIAEgHQAEgIAGgEQAIgEAHAAQAMAAAHAGQAHAGACALIgOAAQgBgGgEgDQgDgEgGAAQgGABgFAFQgEAGAAAJQAAAKAEAGQAEAFAHAAQAGAAAEgDQADgEABgHIAOAAQgCAMgHAHQgIAGgLAAQgIAAgHgEg");
	this.shape_6.setTransform(234.6,62.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#55B553").s().p("AANAeIAAgbIgZAAIAAAbIgOAAIAAg8IAOAAIAAAZIAZAAIAAgZIAOAAIAAA8g");
	this.shape_7.setTransform(227.7,62.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#55B553").s().p("AANAeIAAgrIAAAAIgVArIgSAAIAAg8IAOAAIAAArIAWgrIARAAIAAA8g");
	this.shape_8.setTransform(220.8,62.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#55B553").s().p("AgaAeIAAg8IAgAAQAJAAAFAFQAEADABAIQAAAEgDAEQgCADgEACQAFABADACQADAEAAAHQAAAIgGAFQgGAEgLAAgAgMAUIAQAAQAFAAACgCQADgCAAgEQAAgFgDgCQgCgCgFAAIgQAAgAgMgFIAPAAQAEAAACgCQACgCAAgEQAAgDgCgBQgCgCgEAAIgPAAg");
	this.shape_9.setTransform(210.9,62.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#55B553").s().p("AANAeIAAgrIAAAAIgVArIgSAAIAAg8IAOAAIAAArIAWgrIARAAIAAA8g");
	this.shape_10.setTransform(200.6,62.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#55B553").s().p("AAMAeIgSgbIgIAJIAAASIgNAAIAAg8IANAAIAAAcIAXgcIARAAIgZAZIAbAjg");
	this.shape_11.setTransform(194.3,62.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#55B553").s().p("AgFAeIAAgxIgVAAIAAgLIA1AAIAAALIgVAAIAAAxg");
	this.shape_12.setTransform(187.9,62.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#55B553").s().p("AgSAqIgFAAIAAgMIAEABIADAAQADAAACgCIADgGIACgGIgXg6IAPAAIAOArIAQgrIAOAAIgXA7IgEALQgDAGgCAEQgEADgIAAIgEAAg");
	this.shape_13.setTransform(182.1,63.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#55B553").s().p("AgdArIAAhTIANAAIAAAIQADgFAGgDQAFgCAEAAQAIAAAHAEQAGAEAEAIQADAHAAAKQAAAJgDAGQgDAHgHAEQgGAEgJAAQgEAAgFgCQgFgCgEgFIAAAfgAgMgZQgEAGAAAKQAAAJAEAFQAFAFAHAAQAHAAAFgFQAEgFABgJQAAgKgGgGQgEgGgHAAQgHAAgFAGg");
	this.shape_14.setTransform(175.4,63.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#55B553").s().p("AAMAeIgSgbIgIAJIAAASIgNAAIAAg8IANAAIAAAcIAXgcIARAAIgZAZIAbAjg");
	this.shape_15.setTransform(168.8,62.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#55B553").s().p("AgOAcQgHgEgDgHQgEgHAAgKQAAgIAEgHQAEgIAGgEQAIgEAHAAQAMAAAHAGQAHAGABALIgNAAQgBgGgEgDQgEgEgFAAQgGABgEAFQgFAGAAAJQAAAKAEAGQAEAFAHAAQAFAAAEgDQAEgEACgHIANAAQgCAMgHAHQgIAGgLAAQgIAAgHgEg");
	this.shape_16.setTransform(161.9,62.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#55B553").s().p("AAVAgQgEAAgCgBQgDgCgBgEQgEAEgGACQgDABgGAAQgKAAgGgFQgFgFAAgIQAAgGACgEQADgEAEAAIAJgCIAJgCIAGgBIAEgBIACgBIABgCIAAgDQAAgFgDgCQgDgCgFAAQgGAAgEACQgDADgBAGIgNAAQACgLAHgFQAHgFALAAQAGAAAGACQAFACAEAEQADAEAAAHIAAAdIABAEIACABIABAAIACgBIAAAKIgFABIgEAAgAAEACIgGABIgGABIgFADQgCACAAAEQAAAFADACQADADAFAAQAEAAACgCIAGgDQACgDAAgCIABgGIAAgHIgHACg");
	this.shape_17.setTransform(155.3,62.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#55B553").s().p("AgdArIAAhTIANAAIAAAIQADgFAGgDQAFgCAEAAQAJAAAGAEQAGAEAEAIQADAHAAAKQAAAJgDAGQgDAHgHAEQgGAEgJAAQgDAAgFgCQgGgCgDgFIAAAfgAgMgZQgEAGAAAKQAAAJAEAFQAFAFAHAAQAHAAAFgFQAFgFAAgJQgBgKgFgGQgEgGgHAAQgHAAgFAGg");
	this.shape_18.setTransform(148.3,63.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#55B553").s().p("AgOAcQgGgEgFgHQgDgHAAgKQAAgIAEgHQADgIAIgEQAGgEAIAAQAMAAAHAGQAHAGABALIgNAAQgCgGgDgDQgEgEgFAAQgGABgEAFQgFAGAAAJQAAAKAEAGQAFAFAFAAQAHAAADgDQAFgEABgHIANAAQgBAMgIAHQgIAGgMAAQgHAAgHgEg");
	this.shape_19.setTransform(137.9,62.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#55B553").s().p("AANAeIAAgrIAAAAIgVArIgSAAIAAg8IAOAAIAAArIAWgrIARAAIAAA8g");
	this.shape_20.setTransform(131,62.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#55B553").s().p("AgaAeIAAg8IAgAAQAJAAAFAFQAEADABAIQAAAEgDAEQgCADgEACQAFABADACQADAEAAAHQAAAIgGAFQgGAEgLAAgAgMAUIAQAAQAFAAACgCQADgCAAgEQAAgFgDgCQgCgCgFAAIgQAAgAgMgFIAPAAQAEAAACgCQACgCAAgEQAAgDgCgBQgCgCgEAAIgPAAg");
	this.shape_21.setTransform(124.4,62.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#55B553").s().p("AgdArIAAhTIANAAIAAAIQADgFAFgDQAGgCAEAAQAIAAAHAEQAGAEADAIQAEAHAAAKQAAAJgEAGQgDAHgGAEQgGAEgIAAQgFAAgEgCQgGgCgDgFIAAAfgAgMgZQgEAGAAAKQAAAJAEAFQAFAFAHAAQAIAAAEgFQAFgFgBgJQAAgKgEgGQgFgGgHAAQgHAAgFAGg");
	this.shape_22.setTransform(117.4,63.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#55B553").s().p("AgOAcQgHgEgEgHQgDgIAAgJQAAgIAEgHQAEgIAGgEQAHgEAHAAQAJAAAGAEQAHAEAEAIQADAHAAAJIAAACIgrAAQAAAJAEAFQAFAFAGAAQAFAAAEgCQADgDACgGIANAAQgBAHgEAEQgEAFgGADQgGACgHAAQgHAAgHgEgAAQgEQgBgIgEgFQgEgEgHAAQgFAAgEAEQgEAEgBAJIAeAAIAAAAg");
	this.shape_23.setTransform(110.3,62.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#55B553").s().p("AgOAcQgGgEgFgHQgDgHAAgKQAAgIAEgHQADgIAIgEQAGgEAIAAQAMAAAHAGQAHAGABALIgNAAQgCgGgDgDQgEgEgFAAQgGABgFAFQgEAGAAAJQAAAKAEAGQAFAFAFAAQAHAAADgDQAFgEAAgHIAOAAQgBAMgIAHQgIAGgMAAQgHAAgHgEg");
	this.shape_24.setTransform(103.7,62.7);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#55B553").s().p("AANArIAAgrIAAAAIgVArIgSAAIAAg8IAOAAIAAArIAWgrIARAAIAAA8gAgMgdQgFgFgBgIIAHAAQABAEADADQADACAEAAQAFAAADgCQADgDABgEIAHAAQgBAIgEAFQgFAEgJAAQgHAAgFgEg");
	this.shape_25.setTransform(93.5,61.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#55B553").s().p("AANAeIAAgrIAAAAIgVArIgSAAIAAg8IAOAAIAAArIAWgrIARAAIAAA8g");
	this.shape_26.setTransform(86.6,62.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#55B553").s().p("AAMAeIgSgbIgHAJIAAASIgOAAIAAg8IAOAAIAAAcIAWgcIAQAAIgYAZIAbAjg");
	this.shape_27.setTransform(80.3,62.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#55B553").s().p("AgOAcQgGgEgFgHQgDgHAAgKQAAgIAEgHQAEgIAGgEQAIgEAHAAQAMAAAHAGQAHAGABALIgNAAQgCgGgDgDQgDgEgGAAQgGABgEAFQgFAGAAAJQAAAKAEAGQAFAFAFAAQAHAAADgDQAFgEABgHIANAAQgCAMgHAHQgIAGgMAAQgHAAgHgEg");
	this.shape_28.setTransform(73.5,62.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#55B553").s().p("AgOAcQgHgEgEgHQgDgIAAgJQAAgIAEgHQAEgIAGgEQAHgEAHAAQAJAAAGAEQAHAEAEAIQADAHAAAJIAAACIgrAAQAAAJAEAFQAFAFAGAAQAFAAAEgCQADgDACgGIANAAQgBAHgEAEQgEAFgGADQgGACgHAAQgHAAgHgEgAAQgEQgBgIgEgFQgEgEgHAAQgFAAgEAEQgEAEgBAJIAeAAIAAAAg");
	this.shape_29.setTransform(66.8,62.7);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#55B553").s().p("AANAeIAAgaIgGABIgHACIgGABQgJgBgFgFQgFgCAAgJIAAgVIAOAAIAAAQQAAAGACADQADADAGAAIADgBIAFgBIAFgBIAAgZIANAAIAAA8g");
	this.shape_30.setTransform(60.1,62.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#55B553").s().p("AANAeIAAgrIAAAAIgVArIgSAAIAAg8IAOAAIAAArIAWgrIARAAIAAA8g");
	this.shape_31.setTransform(53.5,62.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#55B553").s().p("AgFAeIAAgxIgVAAIAAgLIA1AAIAAALIgVAAIAAAxg");
	this.shape_32.setTransform(47.3,62.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#55B553").s().p("AAVAgQgEAAgCgBQgDgCgBgEQgEAEgGACQgDABgGAAQgKAAgGgFQgFgFAAgIQAAgGACgEQADgEAEAAIAJgCIAJgCIAGgBIAEgBIACgBIABgCIAAgDQAAgFgDgCQgDgCgFAAQgGAAgEACQgDADgBAGIgNAAQACgLAHgFQAHgFALAAQAGAAAGACQAFACAEAEQADAEAAAHIAAAdIABAEIACABIABAAIACgBIAAAKIgFABIgEAAgAAEACIgGABIgGABIgFADQgCACAAAEQAAAFADACQADADAFAAQAEAAACgCIAGgDQACgDAAgCIABgGIAAgHIgHACg");
	this.shape_33.setTransform(41.3,62.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#55B553").s().p("AAVAeIAAgmIgPAmIgLAAIgPgmIAAAmIgOAAIAAg8IASAAIAQAwIARgwIASAAIAAA8g");
	this.shape_34.setTransform(33.6,62.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#55B553").s().p("AgPAcQgHgEgEgHQgEgIAAgJQAAgJAEgHQAEgHAHgEQAHgEAIAAQAJAAAHAEQAHAEAEAHQAEAHAAAJQAAAJgEAIQgEAHgHAEQgHAEgJAAQgIAAgHgEgAgLgPQgFAGAAAJQAAAKAFAGQAEAFAHAAQAIAAAEgFQAFgGAAgKQAAgJgFgGQgEgGgIAAQgHAAgEAGg");
	this.shape_35.setTransform(25.9,62.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#55B553").s().p("AgFAeIAAgxIgVAAIAAgLIA1AAIAAALIgVAAIAAAxg");
	this.shape_36.setTransform(19.5,62.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#55B553").s().p("AgaAeIAAg8IAgAAQAJAAAFAFQAEADABAIQAAAEgDAEQgCADgEACQAFABADACQADAEAAAHQAAAIgGAFQgGAEgLAAgAgMAUIAQAAQAFAAACgCQADgCAAgEQAAgFgDgCQgCgCgFAAIgQAAgAgMgFIAPAAQAEAAACgCQACgCAAgEQAAgDgCgBQgCgCgEAAIgPAAg");
	this.shape_37.setTransform(13.6,62.7);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#55B553").s().p("AAZAqIgIgWIggAAIgJAWIgQAAIAihTIAOAAIAgBTgAAMAIIgMgiIgMAiIAYAAg");
	this.shape_38.setTransform(6.1,61.5);

	this.instance = new lib.logo();
	this.instance.setTransform(36.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,-6,279.7,76.4);


(lib.Symbol5copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgxA4IAAhvIA9AAQAQAAAIAHQAJAHAAANQAAAKgEAFQgEAGgIADIAAABQALADAFAFQAFAIAAALQAAAQgKAIQgLAIgUAAgAgYAlIAhAAQAIAAAEgEQAFgDAAgIQAAgIgFgEQgEgEgIAAIghAAgAgYgKIAeAAQAHAAAEgEQADgDABgHQAAgGgEgDQgEgDgIAAIgdAAg");
	this.shape.setTransform(806.2,47.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgdA0QgNgHgHgNQgHgOAAgSQAAgRAHgNQAHgOANgHQANgHAQgBQASABAMAHQANAHAHAOQAHANAAARQAAASgHAOQgHANgNAHQgMAIgSAAQgQAAgNgIgAgWgcQgIALgBARQABATAIAKQAJALANAAQAPAAAIgLQAIgKAAgTQAAgSgIgKQgIgLgPAAQgNAAgJALg");
	this.shape_1.setTransform(793.3,47.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgLA4IAAhcIgmAAIAAgTIBjAAIAAATIgmAAIAABcg");
	this.shape_2.setTransform(781.7,47.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAZA4IAAgxIgwAAIAAAxIgaAAIAAhvIAaAAIAAAsIAwAAIAAgsIAYAAIAABvg");
	this.shape_3.setTransform(770.2,47.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgiBNIgKgBIAAgVIAIABIAGABQAFAAAEgEQAEgFACgGIAEgLIgshsIAbAAIAdBRIAdhRIAaAAIgqBtIgJAWQgEAKgGAHQgHAHgPAAIgHgBg");
	this.shape_4.setTransform(758.2,49.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAcA4QgFgDgCgIQgIAIgKADQgIADgLAAQgTAAgKgJQgJgJgBgQQABgLAEgHQAFgGAHgCQAHgDAJgCIASgDIAMgCQAFAAACgBIAEgDIABgDIABgFQgBgJgFgEQgGgEgKAAQgMAAgGAEQgGAFgBALIgYAAQACgVAOgIQANgJAVAAQAMgBAKAEQAKADAHAIQAFAHABANIAAA4IABAGQABACADAAIADAAIADAAIAAARIgJACIgHAAQgIAAgEgDgAAHAEIgLACIgMACQgGACgDAEQgEADAAAHQAAAJAGAEQAFAEAJAAQAIAAAFgCQAHgDAEgEQAEgEABgFIABgKIAAgNQgHADgHABg");
	this.shape_5.setTransform(746.6,47.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAXA4IgigzIgQAQIAAAjIgZAAIAAhvIAZAAIAAAyIAsgyIAfAAIgsAtIAwBCg");
	this.shape_6.setTransform(735.1,47.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AAXA4IgigzIgPAQIAAAjIgaAAIAAhvIAaAAIAAAyIArgyIAeAAIgrAtIAwBCg");
	this.shape_7.setTransform(723.3,47.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAbA4QgEgDgBgIQgJAIgKADQgIADgMAAQgRAAgLgJQgKgJAAgQQAAgLAFgHQAEgGAIgCQAIgDAJgCIARgDIAMgCQAFAAACgBIAEgDIABgDIABgFQAAgJgGgEQgGgEgKAAQgMAAgGAEQgGAFgBALIgYAAQACgVANgIQAOgJAVAAQALgBALAEQAKADAHAIQAFAHAAANIAAA4IABAGQACACAEAAIACAAIADAAIAAARIgJACIgHAAQgHAAgGgDgAAHAEIgLACIgMACQgGACgDAEQgEADAAAHQAAAJAFAEQAGAEAJAAQAJAAAEgCQAHgDAEgEQAEgEABgFIABgKIAAgNQgHADgHABg");
	this.shape_8.setTransform(710.8,47.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAcA4IgcgqIgbAqIgeAAIArg5Igng2IAeAAIAXAjIAYgjIAdAAIgnA0IAsA7g");
	this.shape_9.setTransform(692.4,47.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAqA4IAAhvIAZAAIAABvgAhCA4IAAhvIAZAAIAAApIAfAAQAQAAAKAKQAKAHAAARQAAARgKAJQgKAKgQAAgAgpAlIAcAAQAJAAADgFQACgFAAgHQAAgHgCgFQgEgEgJgBIgbAAg");
	this.shape_10.setTransform(678.5,47.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AAYA4IAAgxIgvAAIAAAxIgaAAIAAhvIAaAAIAAAsIAvAAIAAgsIAZAAIAABvg");
	this.shape_11.setTransform(664.3,47.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAZA4IAAgxIgxAAIAAAxIgZAAIAAhvIAZAAIAAAsIAxAAIAAgsIAZAAIAABvg");
	this.shape_12.setTransform(651.6,47.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAcA4QgFgDgBgIQgJAIgKADQgIADgMAAQgRAAgLgJQgJgJgBgQQABgLAEgHQAFgGAHgCQAHgDAKgCIARgDIAMgCQAFAAACgBIAEgDIABgDIABgFQgBgJgFgEQgGgEgKAAQgMAAgGAEQgGAFgBALIgYAAQACgVANgIQAOgJAVAAQAMgBAKAEQAKADAHAIQAFAHAAANIAAA4IACAGQABACADAAIADAAIADAAIAAARIgJACIgHAAQgHAAgFgDgAAHAEIgLACIgMACQgFACgEAEQgEADAAAHQAAAJAGAEQAFAEAJAAQAJAAAEgCQAHgDAEgEQAEgEABgFIABgKIAAgNQgHADgHABg");
	this.shape_13.setTransform(639.4,47.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgxA4IAAhvIA9AAQAQAAAIAHQAJAHAAANQAAAKgEAFQgEAGgIADIAAABQALADAFAFQAFAIAAALQAAAQgKAIQgLAIgUAAgAgYAlIAhAAQAIAAAEgEQAFgDAAgIQAAgIgFgEQgEgEgIAAIghAAgAgYgKIAeAAQAHAAAEgEQADgDABgHQAAgGgEgDQgEgDgIAAIgdAAg");
	this.shape_14.setTransform(627.2,47.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgdA0QgNgHgHgNQgHgOAAgSQAAgRAHgNQAHgOANgHQANgHAQgBQASABAMAHQANAHAHAOQAHANAAARQAAASgHAOQgHANgNAHQgMAIgSAAQgQAAgNgIgAgWgcQgIALgBARQABATAIAKQAJALANAAQAPAAAIgLQAIgKAAgTQAAgSgIgKQgIgLgPAAQgNAAgJALg");
	this.shape_15.setTransform(614.3,47.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("Ag3BPIAAiZIAYAAIAAAOQAGgJAKgEQAKgFAJAAQAQABALAHQAMAHAGANQAHAOAAATQAAARgHAMQgGANgLAIQgLAGgQABQgJAAgKgFQgJgDgHgKIAAA5gAgWgvQgJAKAAAUQAAARAJAJQAIALAOAAQAOAAAIgLQAIgJAAgRQAAgTgIgKQgIgMgOAAQgOAAgIALg");
	this.shape_16.setTransform(601.3,49.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AAZA4IAAhQIgBAAIgoBQIghAAIAAhvIAaAAIAABQIAAAAIAphQIAfAAIAABvg");
	this.shape_17.setTransform(588,47.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AAXA4IgigzIgPAQIAAAjIgaAAIAAhvIAaAAIAAAyIArgyIAeAAIgsAtIAyBCg");
	this.shape_18.setTransform(576.5,47.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgdA0QgNgHgHgNQgHgOAAgSQAAgRAHgNQAHgOANgHQANgHAQgBQASABAMAHQANAHAHAOQAHANAAARQAAASgHAOQgHANgNAHQgMAIgSAAQgQAAgNgIgAgWgcQgIALgBARQABATAIAKQAJALANAAQAPAAAIgLQAIgKAAgTQAAgSgIgKQgIgLgPAAQgNAAgJALg");
	this.shape_19.setTransform(563.3,47.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgvA5IgHgCIAAgUIAEABIAFAAQAEAAADgBQAEgDACgGQADgGAAgMIAChAIBSAAIAABvIgZAAIAAhcIghAAIgCAwQAAAWgJAMQgJAMgPAAIgJAAg");
	this.shape_20.setTransform(550,47.3);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgdBJQgNgHgHgQQgDgHgBgJIgBgUIABgVQABgMADgMQADgLAGgJQAHgLALgGQALgFAOgBIAQgCQAFgBACgDIAZAAQgBAJgFAGQgEAFgHADQgHACgIABIgSABQgJABgIAGQgIAGgFAJQgFAJgDAMIABAAQAGgLAKgGQAKgGAOAAQAPAAALAHQAMAHAHANQAGALAAASQAAASgGANQgHANgMAHQgNAHgQAAQgQAAgOgIgAgVgDQgIAIAAASQAAASAIAKQAJAKAMAAQAOAAAIgKQAIgKAAgSQAAgSgIgIQgIgKgOgBQgMABgJAKg");
	this.shape_21.setTransform(537.7,44.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AAcA4QgFgDgCgIQgIAIgKADQgIADgLAAQgTAAgKgJQgJgJgBgQQABgLAEgHQAFgGAHgCQAHgDAJgCIASgDIAMgCQAFAAACgBIAEgDIABgDIABgFQgBgJgFgEQgGgEgKAAQgMAAgGAEQgGAFgBALIgYAAQADgVANgIQANgJAVAAQAMgBAKAEQAKADAHAIQAFAHABANIAAA4IABAGQABACADAAIADAAIADAAIAAARIgJACIgHAAQgIAAgEgDgAAHAEIgLACIgMACQgGACgDAEQgEADAAAHQAAAJAGAEQAFAEAJAAQAIAAAFgCQAHgDAEgEQAEgEABgFIABgKIAAgNQgHADgHABg");
	this.shape_22.setTransform(525.2,47.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgjAxQgNgKAAgSIAZAAQABAIAEAEQAEAEAGACQAFABAEAAQAKAAAHgEQAGgEAAgJQAAgHgEgEQgFgGgNAAIgMAAIAAgQIAKAAQAIAAAGgDQAGgDABgJQAAgGgFgEQgFgEgLAAQgIAAgFAEQgFAFgCAIIgZAAQABgOAIgJQAHgHALgEQALgCAJAAQAMAAAKACQAKAEAGAGQAFAIABAKQgBAHgCAFQgDAFgEADQgEADgFABIAAABQAGABAFACQAFACADAGQADAGAAAJQAAAMgHAIQgHAIgLAFQgLAEgMgBQgXABgNgLg");
	this.shape_23.setTransform(513.1,47.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgmA7QgOgUAAgnQAAgmAOgTQANgVAZAAQAaAAANAVQAOATAAAmQAAAmgOAVQgNATgaAAQgZAAgNgTgAgPgzQgFAGgDAJQgCAJgBAKIgBASQAAAeAHANQAHAOANAAQAKgBAFgGQAGgGADgJQACgJABgKIAAgRIAAgQQgBgJgCgKQgDgJgGgGQgFgGgKAAQgJAAgGAFg");
	this.shape_24.setTransform(495.2,45.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgfALIAAgVIA/AAIAAAVg");
	this.shape_25.setTransform(478.7,46.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgdA0QgNgHgHgNQgHgOAAgSQAAgRAHgNQAHgOANgHQANgHAQgBQASABAMAHQANAHAHAOQAHANAAARQAAASgHAOQgHANgNAHQgMAIgSAAQgQAAgNgIgAgWgcQgIALgBARQABATAIAKQAJALANAAQAPAAAIgLQAIgKAAgTQAAgSgIgKQgIgLgPAAQgNAAgJALg");
	this.shape_26.setTransform(461.8,47.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AAYA4IAAgxIgvAAIAAAxIgZAAIAAhvIAZAAIAAAsIAvAAIAAgsIAaAAIAABvg");
	this.shape_27.setTransform(449,47.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgaA0QgNgIgGgMQgHgOgBgRQABgRAHgNQAGgOANgIQANgIAPABQAWAAANALQANAKADAUIgZAAQgCgLgHgGQgHgFgKAAQgMAAgIAKQgJAMAAARQABATAHAKQAIALAMAAQAMgBAHgGQAHgHACgMIAYAAQgCAVgOAMQgOAMgWAAQgPAAgMgHg");
	this.shape_28.setTransform(436.6,47.1);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AAcA4QgFgDgCgIQgIAIgKADQgIADgLAAQgTAAgJgJQgKgJgBgQQABgLAEgHQAFgGAHgCQAHgDAJgCIASgDIAMgCQAFAAACgBIAEgDIABgDIABgFQgBgJgFgEQgGgEgKAAQgMAAgGAEQgGAFgBALIgYAAQADgVANgIQANgJAVAAQAMgBAKAEQAKADAGAIQAGAHABANIAAA4IABAGQABACADAAIADAAIADAAIAAARIgJACIgHAAQgIAAgEgDgAAIAEIgMACIgMACQgGACgDAEQgEADAAAHQAAAJAGAEQAEAEALAAQAHAAAFgCQAHgDAEgEQAEgEABgFIABgKIAAgNQgHADgGABg");
	this.shape_29.setTransform(424.5,47.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AAYA4IAAhcIgvAAIAABcIgaAAIAAhvIBiAAIAABvg");
	this.shape_30.setTransform(411.8,47.1);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgdA0QgNgHgHgNQgHgOAAgSQAAgRAHgNQAHgOANgHQANgHAQgBQASABAMAHQANAHAHAOQAHANAAARQAAASgHAOQgHANgNAHQgMAIgSAAQgQAAgNgIgAgWgcQgIALgBARQABATAIAKQAJALANAAQAPAAAIgLQAIgKAAgTQAAgSgIgKQgIgLgPAAQgNAAgJALg");
	this.shape_31.setTransform(399,47.1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgjAxQgNgKAAgSIAZAAQABAIAEAEQAEAEAGACQAFABAEAAQAKAAAHgEQAGgEAAgJQAAgHgEgEQgFgGgNAAIgMAAIAAgQIAKAAQAIAAAGgDQAGgDABgJQAAgGgFgEQgFgEgLAAQgIAAgFAEQgFAFgCAIIgZAAQABgOAIgJQAHgHALgEQALgCAJAAQAMAAAKACQAKAEAGAGQAFAIABAKQgBAHgCAFQgDAFgEADQgEADgFABIAAABQAGABAFACQAFACADAGQADAGAAAJQAAAMgHAIQgHAIgLAFQgLAEgMgBQgXABgNgLg");
	this.shape_32.setTransform(386.7,47.1);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgbA0QgMgIgIgNQgGgNAAgRQAAgQAHgOQAHgOANgIQAMgHAOgBQAQABANAHQALAIAHAOQAGAOABASIAAADIhSAAQABARAHAJQAJAJANAAQAKABAGgFQAHgFAEgLIAYAAQgEAMgHAJQgHAJgLAEQgKAFgNAAQgPAAgNgIgAAdgJQgCgOgHgJQgHgIgNAAQgKAAgIAIQgHAIgDAPIA5AAIAAAAg");
	this.shape_33.setTransform(375,47.1);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgdBJQgNgHgHgQQgDgHgBgJIgBgUIABgVQABgMADgMQADgLAGgJQAHgLALgGQALgFAOgBIAQgCQAFgBACgDIAZAAQgBAJgFAGQgEAFgHADQgHACgIABIgSABQgJABgIAGQgIAGgFAJQgFAJgDAMIABAAQAGgLAKgGQAKgGAOAAQAPAAALAHQAMAHAHANQAGALAAASQAAASgGANQgHANgMAHQgNAHgQAAQgQAAgOgIgAgVgDQgIAIAAASQAAASAIAKQAJAKAMAAQAOAAAIgKQAIgKAAgSQAAgSgIgIQgIgKgOgBQgMABgJAKg");
	this.shape_34.setTransform(362.4,44.9);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgdA0QgNgHgHgNQgHgOAAgSQAAgRAHgNQAHgOANgHQANgHAQgBQASABAMAHQANAHAHAOQAHANAAARQAAASgHAOQgHANgNAHQgMAIgSAAQgQAAgNgIgAgWgcQgIALgBARQABATAIAKQAJALANAAQAPAAAIgLQAIgKAAgTQAAgSgIgKQgIgLgPAAQgNAAgJALg");
	this.shape_35.setTransform(343.1,47.1);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AAYA4IAAgxIgvAAIAAAxIgZAAIAAhvIAZAAIAAAsIAvAAIAAgsIAaAAIAABvg");
	this.shape_36.setTransform(330.3,47.1);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgLA4IAAhcIgmAAIAAgTIBiAAIAAATIglAAIAABcg");
	this.shape_37.setTransform(318.9,47.1);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgOAtQgOgOgCgYIgVAAIAAAxIgZAAIAAhvIAZAAIAAAtIAVAAQADgXAPgNQAMgMAXgBQAQAAANAIQAMAHAHAOQAGANAAARQAAASgGAOQgHANgMAHQgNAIgQAAQgXgBgOgOgAAAgcQgGALAAARQAAATAGAKQAIALAPAAQAOAAAHgLQAIgKAAgTQAAgSgIgKQgIgLgOAAQgOAAgIALg");
	this.shape_38.setTransform(305.2,47.1);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgvA5IgHgCIAAgUIAEABIAFAAQAEAAADgBQAEgDACgGQADgGAAgMIAChAIBSAAIAABvIgZAAIAAhcIghAAIgCAwQAAAWgJAMQgJAMgPAAIgJAAg");
	this.shape_39.setTransform(289.3,47.3);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AgdA0QgNgHgHgNQgHgOAAgSQAAgRAHgNQAHgOANgHQANgHAQgBQASABAMAHQANAHAHAOQAHANAAARQAAASgHAOQgHANgNAHQgMAIgSAAQgQAAgNgIgAgWgcQgIALgBARQABATAIAKQAJALANAAQAPAAAIgLQAIgKAAgTQAAgSgIgKQgIgLgPAAQgNAAgJALg");
	this.shape_40.setTransform(276.9,47.1);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgaA0QgNgIgHgMQgGgOgBgRQABgRAGgNQAIgOAMgIQANgIAQABQAVAAANALQANAKADAUIgZAAQgCgLgHgGQgHgFgJAAQgNAAgIAKQgJAMAAARQABATAHAKQAIALAMAAQAMgBAHgGQAHgHACgMIAYAAQgCAVgOAMQgOAMgWAAQgPAAgMgHg");
	this.shape_41.setTransform(264.4,47.1);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgdBJQgNgHgHgQQgDgHgBgJIgBgUIABgVQABgMADgMQADgLAGgJQAHgLALgGQALgFAOgBIAQgCQAFgBACgDIAZAAQgBAJgFAGQgEAFgHADQgHACgIABIgSABQgJABgIAGQgIAGgFAJQgFAJgDAMIABAAQAGgLAKgGQAKgGAOAAQAPAAALAHQAMAHAHANQAGALAAASQAAASgGANQgHANgMAHQgNAHgQAAQgQAAgOgIgAgVgDQgIAIAAASQAAASAIAKQAJAKAMAAQAOAAAIgKQAIgKAAgSQAAgSgIgIQgIgKgOgBQgMABgJAKg");
	this.shape_42.setTransform(251.7,44.9);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AAtBOIgPgqIg8AAIgQAqIgbAAIA8ibIAbAAIA9CbgAAXAPIgXg/IgXA/IAuAAg");
	this.shape_43.setTransform(237.8,45);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(228.5,31,585.8,28.5);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAcA4QgFgDgCgIQgHAIgLADQgIADgLABQgTgBgJgJQgKgJgBgPQABgLAEgIQAFgGAHgBQAHgEAJgCIASgDIAMgBQAFgBACgBIAEgDIABgDIABgFQgBgJgFgEQgGgEgKAAQgMAAgGAEQgGAFgBALIgYAAQADgVANgJQANgIAVgBQALAAALAEQAKAEAGAHQAGAHABANIAAA4IABAGQABACADAAIADAAIADAAIAAARIgJACIgHABQgIgBgEgDgAAIAEIgMACIgMACQgGABgDAFQgEADAAAIQAAAHAGAFQAEAEALABQAHgBAFgCQAHgCAEgEQAEgFABgEIABgLIAAgNQgHADgGABg");
	this.shape.setTransform(366.9,0.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAoBKIAAgkIhPAAIAAAkIgYAAIAAg4IAPAAQAEgJADgKQADgMABgOQACgPAAgTIAAgMIBTAAIAABbIAQAAIAAA4gAgLguQAAANgCANQgCANgDAJIgFAQIAvAAIAAhIIgjAAg");
	this.shape_1.setTransform(353.9,1.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgdA0QgNgHgHgNQgHgOAAgSQAAgRAHgNQAHgOANgHQANgHAQgBQASABAMAHQANAHAHAOQAHANAAARQAAASgHAOQgHANgNAHQgMAIgSAAQgQAAgNgIgAgWgcQgIALgBARQABATAIAKQAJALANAAQAPAAAIgLQAIgKAAgTQAAgSgIgKQgIgLgPAAQgNAAgJALg");
	this.shape_2.setTransform(340.8,0.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("Ag3BPIAAiaIAYAAIAAAPQAGgJAKgFQAKgEAJAAQAQAAALAIQAMAHAGANQAHAOAAATQAAARgHAMQgGANgLAHQgLAIgQAAQgJAAgKgEQgJgFgHgJIAAA5gAgWgvQgJAKAAAUQAAARAJAKQAIAKAOAAQAOAAAIgKQAIgKAAgRQAAgTgIgLQgIgKgOgBQgOAAgIALg");
	this.shape_3.setTransform(327.8,2.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgdA0QgNgHgHgNQgHgOAAgSQAAgRAHgNQAHgOANgHQANgHAQgBQASABAMAHQANAHAHAOQAHANAAARQAAASgHAOQgHANgNAHQgMAIgSAAQgQAAgNgIgAgWgcQgIALgBARQABATAIAKQAJALANAAQAPAAAIgLQAIgKAAgTQAAgSgIgKQgIgLgPAAQgNAAgJALg");
	this.shape_4.setTransform(314.3,0.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgmA4IAAhvIBNAAIAAATIg0AAIAABcg");
	this.shape_5.setTransform(303.7,0.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgdA0QgNgHgHgNQgHgOAAgSQAAgRAHgNQAHgOANgHQANgHAQgBQASABAMAHQANAHAHAOQAHANAAARQAAASgHAOQgHANgNAHQgMAIgSAAQgQAAgNgIgAgWgcQgIALgBARQABATAIAKQAJALANAAQAPAAAIgLQAIgKAAgTQAAgSgIgKQgIgLgPAAQgNAAgJALg");
	this.shape_6.setTransform(285.8,0.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgmA4IAAhvIBNAAIAAATIg0AAIAABcg");
	this.shape_7.setTransform(275.2,0.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgbA0QgNgIgGgNQgHgNAAgRQAAgQAHgOQAHgOAMgIQANgHAOgBQARABALAHQAMAIAIAOQAGAOAAASIAAADIhSAAQAAARAJAJQAHAJANAAQAKABAHgFQAGgFAEgLIAYAAQgCAMgIAJQgHAJgLAEQgLAFgMAAQgQAAgMgIgAAdgJQgBgOgIgJQgIgIgMAAQgLAAgHAIQgIAIgBAPIA4AAIAAAAg");
	this.shape_8.setTransform(263.9,0.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AhLA4IAAhvIAZAAIAABbIAnAAIAAhbIAXAAIAABbIAnAAIAAhbIAZAAIAABvg");
	this.shape_9.setTransform(248.8,0.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAbA4QgEgDgCgIQgHAIgLADQgIADgLABQgSgBgKgJQgLgJAAgPQAAgLAFgIQAEgGAIgBQAIgEAIgCIASgDIAMgBQAFgBADgBIADgDIABgDIAAgFQABgJgGgEQgGgEgKAAQgMAAgGAEQgGAFgBALIgYAAQADgVANgJQANgIAVgBQALAAALAEQALAEAFAHQAHAHAAANIAAA4IAAAGQACACAEAAIACAAIADAAIAAARIgJACIgHABQgIgBgFgDgAAIAEIgMACIgMACQgFABgEAFQgEADAAAIQAAAHAFAFQAFAEALABQAHgBAFgCQAHgCAFgEQADgFABgEIAAgLIAAgNQgGADgGABg");
	this.shape_10.setTransform(233.9,0.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("Ag/BOIAAibIBGAAQAQAAAMAFQALAFAGAJQAFAJAAAMQAAALgGAJQgGAIgKAEQAOAEAHAIQAIAJAAAQQAAAOgGAKQgGAKgLAGQgLAGgPAAgAgkA3IAnAAQAJAAAIgCQAIgCAEgFQAFgFAAgKQAAgLgFgFQgFgFgIgCQgHgCgJAAIgnAAgAgkgMIAkAAQAPABAHgFQAIgFAAgMQAAgKgEgEQgEgFgHgBQgHgCgIAAIgkAAg");
	this.shape_11.setTransform(220.1,-2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgaA0QgNgHgGgNQgHgOgBgRQABgRAHgNQAGgPANgHQANgIAPAAQAWAAANAMQANAKADAUIgZAAQgCgLgHgGQgHgFgKAAQgMAAgIALQgJALAAARQABATAHAKQAIALAMAAQAMAAAHgHQAHgHACgMIAYAAQgCAVgOAMQgOAMgWABQgPgBgMgHg");
	this.shape_12.setTransform(199.9,0.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAZA4IAAhQIgBAAIgoBQIghAAIAAhvIAZAAIAABQIABAAIAphQIAfAAIAABvg");
	this.shape_13.setTransform(181.3,0.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AAXA4IgigzIgQAQIAAAjIgZAAIAAhvIAZAAIAAAyIAsgyIAeAAIgrAtIAxBCg");
	this.shape_14.setTransform(169.8,0.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AAZA4IAAhQIgBAAIgpBQIgfAAIAAhvIAYAAIAABQIACAAIAohQIAgAAIAABvg");
	this.shape_15.setTransform(156.8,0.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AAXA4IAAgwIgJADIgNACIgNABQgRAAgJgJQgJgGgBgRIAAglIAaAAIAAAcQAAALAEAGQAGAFAKAAIAIgBIAJgCIAIgDIAAgsIAaAAIAABvg");
	this.shape_16.setTransform(144.4,0.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgaA0QgNgHgGgNQgHgOAAgRQAAgRAHgNQAGgPANgHQANgIAPAAQAWAAANAMQANAKADAUIgZAAQgCgLgHgGQgHgFgKAAQgMAAgIALQgJALAAARQABATAHAKQAIALAMAAQAMAAAHgHQAHgHACgMIAZAAQgDAVgOAMQgOAMgWABQgPgBgMgHg");
	this.shape_17.setTransform(132.7,0.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AAYA4IAAhQIAAAAIgpBQIgfAAIAAhvIAZAAIAABQIABAAIAohQIAfAAIAABvg");
	this.shape_18.setTransform(120.1,0.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAZA4IAAhcIgxAAIAABcIgZAAIAAhvIBiAAIAABvg");
	this.shape_19.setTransform(107.5,0.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AAoBKIAAgkIhPAAIAAAkIgYAAIAAg4IAPAAQAEgJADgKQADgMABgOQACgPAAgTIAAgMIBTAAIAABbIAQAAIAAA4gAgLguQAAANgCANQgCANgDAJQgCALgDAFIAvAAIAAhIIgjAAg");
	this.shape_20.setTransform(94.5,1.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgdA0QgNgHgHgNQgHgOAAgSQAAgRAHgNQAHgOANgHQANgHAQgBQASABAMAHQANAHAHAOQAHANAAARQAAASgHAOQgHANgNAHQgMAIgSAAQgQAAgNgIgAgWgcQgIALgBARQABATAIAKQAJALANAAQAPAAAIgLQAIgKAAgTQAAgSgIgKQgIgLgPAAQgNAAgJALg");
	this.shape_21.setTransform(81.4,0.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AAYA4IAAhcIgvAAIAABcIgaAAIAAhvIBiAAIAABvg");
	this.shape_22.setTransform(68.6,0.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgbA0QgMgIgIgNQgGgNAAgRQAAgQAHgOQAHgOANgIQAMgHAOgBQAQABANAHQALAIAHAOQAGAOABASIAAADIhSAAQABARAHAJQAJAJANAAQAKABAGgFQAHgFAEgLIAYAAQgEAMgHAJQgHAJgLAEQgKAFgNAAQgPAAgNgIgAAdgJQgCgOgHgJQgHgIgNAAQgKAAgIAIQgHAIgDAPIA5AAIAAAAg");
	this.shape_23.setTransform(50.1,0.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AAqA4IAAhvIAZAAIAABvgAhCA4IAAhvIAZAAIAAApIAfAAQAQAAAKAKQAKAHAAARQAAARgKAJQgKAKgQAAgAgpAlIAcAAQAJAAADgFQACgFAAgHQAAgHgCgFQgEgEgJgBIgbAAg");
	this.shape_24.setTransform(35.9,0.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgxA4IAAhvIA9AAQAQAAAIAHQAJAHAAANQAAAKgEAFQgEAGgIADIAAABQALADAFAFQAFAIAAALQAAAQgKAIQgLAIgUAAgAgYAlIAhAAQAIAAAEgEQAFgDAAgIQAAgIgFgEQgEgEgIAAIghAAgAgYgKIAeAAQAHAAAEgEQADgDABgHQAAgGgEgDQgEgDgIAAIgdAAg");
	this.shape_25.setTransform(22.1,0.1);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AAYA4IAAhQIAAAAIgpBQIgfAAIAAhvIAZAAIAABQIABAAIAohQIAfAAIAABvg");
	this.shape_26.setTransform(9.4,0.1);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AA5A4IgjgxIgKAMIAAAlIgXAAIAAglIgKgMIgjAxIgeAAIAxg/IgsgwIAfAAIAnAwIAAAAIAAgwIAXAAIAAAwIABAAIAmgwIAfAAIgrAwIAwA/g");
	this.shape_27.setTransform(-5.4,0.1);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgdA0QgNgHgHgNQgHgOAAgSQAAgRAHgNQAHgOANgHQANgHAQgBQASABAMAHQANAHAHAOQAHANAAARQAAASgHAOQgHANgNAHQgMAIgSAAQgQAAgNgIgAgWgcQgIALgBARQABATAIAKQAJALANAAQAPAAAIgLQAIgKAAgTQAAgSgIgKQgIgLgPAAQgNAAgJALg");
	this.shape_28.setTransform(-26.6,0.1);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AAWA4IgigzIgOAQIAAAjIgaAAIAAhvIAaAAIAAAyIArgyIAeAAIgsAtIAyBCg");
	this.shape_29.setTransform(-38.3,0.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgwA4IAAhvIAZAAIAAApIAhAAQATAAAKAKQAKAHAAARQAAARgKAJQgKAKgTAAgAgXAlIAeAAQAJAAAEgFQAEgFAAgHQAAgHgFgFQgEgEgIgBIgeAAg");
	this.shape_30.setTransform(-50.5,0.1);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgvA5IgHgCIAAgTIAEABIAFAAQAEAAADgCQAEgDACgGQADgGAAgLIAChCIBSAAIAABwIgZAAIAAhcIghAAIgCAwQAAAWgJANQgJAMgPgBIgJAAg");
	this.shape_31.setTransform(-63.6,0.3);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgdA0QgNgHgHgNQgHgOAAgSQAAgRAHgNQAHgOANgHQANgHAQgBQASABAMAHQANAHAHAOQAHANAAARQAAASgHAOQgHANgNAHQgMAIgSAAQgQAAgNgIgAgWgcQgIALgBARQABATAIAKQAJALANAAQAPAAAIgLQAIgKAAgTQAAgSgIgKQgIgLgPAAQgNAAgJALg");
	this.shape_32.setTransform(-75.9,0.1);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgMBOIAAiDIgyAAIAAgYIB9AAIAAAYIgyAAIAACDg");
	this.shape_33.setTransform(-89,-2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-97.5,-16,472.5,28.5);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Isolation Mode
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#122A33").s().p("A/DHSMAnTgmFIW0XjMgnUAmEg");
	this.shape.setTransform(13.5,80.5,0.14,0.14);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#DBA577").s().p("At6OHIB4h0QAqgsAfg7QAvhaAEhmIgiu8QAAhMAXhNQAmh6BbhYIGbmOIAWgSQAdgUAjgRQBwg3CMACIIuAAIAZADQAdAGAVAPQBBAugxB0IgjBTQgtBahSAvQg3AghCAIImpAVQgnAGgaAaQgmAlgBBLIADBbIAQBFQAZBSAqBAIBgCTQAUAlAMAxQApCegrDaIgkDRQgaCghGCDQguBXg6A9In7Htg");
	this.shape_1.setTransform(34,42.2,0.14,0.14);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#DBA57D").s().p("AhEEkQhfAAhDhEQhChEADhhQADheBFhEIB+h4QBGhEBdAAQBgAABCBFQBCBEgCBgQgDBehGBDIh9B6QhFBDhdAAIgCAAg");
	this.shape_2.setTransform(65.6,52.9,0.14,0.14);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#DBA57D").s().p("AkdEUQhBhEAEhhQADhhBIhDIDijcQBGhFBigBQBgAABCBEQBCBEgDBhQgEBhhHBDIjkDcQhFBFhhABIgCAAQhgAAhChEg");
	this.shape_3.setTransform(64.7,44.5,0.14,0.14);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#DBA57D").s().p("Ai4GVQhdgEhChEQhChEgBhdQgBhcBBg/IF2lqQBBg/BdAEQBcADBCBFQBCBEABBcQABBdhBA/Il2FqQg+A7hWAAIgJAAg");
	this.shape_4.setTransform(64.7,35.2,0.14,0.14);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#DBA57D").s().p("AiGFkQhgAAhChEQhChEAChgQADhgBGhCIECj6QBEhEBgABQBgAABCBEQBCBEgDBgQgCBghGBCIkDD6QhDBDhfAAIgBAAg");
	this.shape_5.setTransform(63.3,26.8,0.14,0.14);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#302825").s().p("AhYgUICxAAIixApg");
	this.shape_6.setTransform(52.1,31.3,0.14,0.14);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#4A403B").s().p("AhYgUICxAAIAAApg");
	this.shape_7.setTransform(47.8,31.3,0.14,0.14);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#C6C6C6").s().p("Ahvg2IAGAAQA3gEAyggQBUg4Ach5IAAAAQAAASgFAnQgJBDgSBEQg7DRiECGg");
	this.shape_8.setTransform(51.5,45.6,0.14,0.14);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#E3E3E3").s().p("AhPhPQgThEgIhDIgGg1QAdB5BUA4QAzAgA2AEIAGAAIAAFCQiFiHg6jUg");
	this.shape_9.setTransform(48.3,45.6,0.14,0.14);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#302825").s().p("AB1BjQglishQgIQg1gGgvATQg6AXgwADIAAlaQBzAABUA2QBpBFBKClQAZA4AIBaQAEAtgCAjIgBAAQgSAKgOAoQgXBGAICGIgRAJQAAipgZh5g");
	this.shape_10.setTransform(52.8,28.9,0.14,0.14);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#4A403B").s().p("AieF8QAJiGgYhGQgOgpgSgJIgBAAQgBgjADgtQAIhaAZg4QBIijBxhHQBXg2BqAAIAAFaQgwgDg5gXQgwgTg2AGQhPAIglCsQgZB5AACpg");
	this.shape_11.setTransform(47,28.9,0.14,0.14);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#DDAE8A").s().p("Aj6G/IAAtVQAwgDA6gXQAvgTA3AGQBOAIAlCuQAZB5AACnIARgJQgIiEAXhGQAOgoASgKQA8gaAUAxQAQAmgKBKQgIA1gfApQgfAlgqANQgQBmgWBIQgbBUgeAaQgoAkhcApQhmArg4AAgAiyitICygqIiyAAg");
	this.shape_12.setTransform(53.4,34,0.14,0.14);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#ECC29E").s().p("AD7G/Qg5AAhlgrQhdgpgogkQgegagahUQgXhIgQhmQgqgNgfglQgfgpgHg1QgLhKAQgmQAUgxA8AaQATAJANApQAYBGgICEIARAJQAAinAZh5QAkiuBOgIQA3gGAwATQA5AXAxADIAANVgAC/itIAAgqIizAAg");
	this.shape_13.setTransform(46.3,34,0.14,0.14);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#454545").s().p("AscGAQANhMAXhqQAtjRAqiVIALgRIAKgMIAEgGIADgCIAJgJIATgQIAygiIB5g6IAhgMIABgBQBSgfBpgdIAGA2QAIBDATBDQA6DVCGCHQCFiGA8jSQAShDAJhDQAFgoAAgRIAAgBQBcAaBOAcIACAAIAOAGIAEABIB/A4IArAZIAsAgIAZAYIACADIAFAFIAIAMIAFAJQA2CIAoDWQAUBtAKBRg");
	this.shape_14.setTransform(49.9,47.2,0.14,0.14);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#CEA082").s().p("AhviJIABAAQA5AABjgsIAACGIBCARQgcB4hUA3QgyAhg3ADIgGABg");
	this.shape_15.setTransform(51.5,42.2,0.14,0.14);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#E2BB9C").s().p("ABqC1Qg2gDgzghQhUg3gdh4IBEgRIAAiGQBiAsA5AAIABAAIAAE/g");
	this.shape_16.setTransform(48.3,42.2,0.14,0.14);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFCF8").s().p("AgDBgIAAi/IAHAAIAAC/g");
	this.shape_17.setTransform(39.2,12.4,0.14,0.14);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#E3E3E3").s().p("AhbAbIAAg1IC3AAIAAA1g");
	this.shape_18.setTransform(50.2,17.3,0.14,0.14);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#E3E3E3").s().p("AgzAbIAAg1IBnAAIAAA1g");
	this.shape_19.setTransform(47.5,17.3,0.14,0.14);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#E3E3E3").s().p("AivAbIAAg1IFfAAIAAA1g");
	this.shape_20.setTransform(43.4,17.3,0.14,0.14);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFCF8").s().p("AhRAAIA3g1IATATIgVAVIBuAAIAAAaIhuAAIAVAWIgTAUg");
	this.shape_21.setTransform(36.7,12.4,0.14,0.14);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#454545").p("AAVAAQAAAIgGAGQgHAGgIAAQgHAAgHgGQgGgGAAgIQAAgHAGgHQAHgGAHAAQAIAAAHAGQAGAHAAAHg");
	this.shape_22.setTransform(58.5,55.7,0.14,0.14);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#454545").p("AAVAAQAAAIgGAHQgHAGgIAAQgHAAgHgGQgGgHAAgIQAAgHAGgHQAHgFAHAAQAIAAAHAFQAGAHAAAHg");
	this.shape_23.setTransform(58.5,54.9,0.14,0.14);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#454545").p("AAVAAQAAAIgGAGQgHAGgIAAQgHAAgHgGQgGgGAAgIQAAgHAGgHQAHgGAHAAQAIAAAHAGQAGAHAAAHg");
	this.shape_24.setTransform(58.5,54,0.14,0.14);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#F0F0F0").s().p("AgIAHIAAgNIARAAIAAANg");
	this.shape_25.setTransform(55,9.6,0.14,0.14);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#F0F0F0").s().p("AgIAOIAAgbIARAAIAAAbg");
	this.shape_26.setTransform(55.5,9.5,0.14,0.14);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#F0F0F0").s().p("AgIATIAAglIARAAIAAAlg");
	this.shape_27.setTransform(55.9,9.4,0.14,0.14);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#F0F0F0").s().p("AgIAZIAAgxIARAAIAAAxg");
	this.shape_28.setTransform(56.3,9.3,0.14,0.14);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#F0F0F0").s().p("AgIAfIAAg9IARAAIAAA9g");
	this.shape_29.setTransform(56.7,9.2,0.14,0.14);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#F0F0F0").s().p("AgoASIAAgjIBRAAIAAAjg");
	this.shape_30.setTransform(59.1,9.3,0.14,0.14);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#F0F0F0").ss(0.8).p("Ag4gZIAAANIgKAAIAAAZIAKAAIAAANIB7AAIAAgzg");
	this.shape_31.setTransform(58.8,9.3,0.14,0.14);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#F0F0F0").s().p("AgBACIAAgCIAAgCIABAAIACABIAAABIAAACIgCABIgBgBgAAAgBIAAABIAAACIAAAAIABAAIAAgCIAAgBIgBAAIAAAAg");
	this.shape_32.setTransform(62.5,9.3);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#F0F0F0").s().p("AgBADIAAgBIABAAIAAAAIABAAIAAgBIAAgBIgBAAIAAAAIAAAAIAAAAQAAAAAAAAQAAAAAAAAQAAAAAAAAQABAAAAAAIAAgBIgBAAIAAAAIAAAAIgBgBIABAAIABAAIABABIgBABIgBAAIABAAIABABIgBACIgBAAIgBAAg");
	this.shape_33.setTransform(61.9,9.3);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#F0F0F0").s().p("AAAACIAAAAIAAAAIAAgBIAAAAIAAAAIAAABIAAAAgAAAAAIAAAAIAAAAIAAgBIAAAAIAAABIAAAAg");
	this.shape_34.setTransform(61.4,9.4);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#F0F0F0").s().p("AAAADIAAgDIAAgBIAAAAIAAABIAAgBIAAgBIABAAIAAAFg");
	this.shape_35.setTransform(61,9.3);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#F0F0F0").s().p("AAAADIAAgDIAAgBIAAAAIAAABIAAgBIAAgBIABAAIAAAFg");
	this.shape_36.setTransform(60.4,9.3);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f().s("#5D5D5D").ss(4).p("AxmAAMAjNAAA");
	this.shape_37.setTransform(49.8,7.9,0.14,0.14);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f().s("#5D5D5D").ss(4).p("AxmAAMAjNAAA");
	this.shape_38.setTransform(49.8,57.1,0.14,0.14);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#EF6E4E").s().p("AhKBMQggggAAgsQAAgrAggfQAfggArAAQAsAAAgAgQAfAfAAArQAAAsgfAgQggAfgsAAQgrAAgfgfg");
	this.shape_39.setTransform(37.3,17.2,0.14,0.14);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f().s("#454545").p("ABYBUIgNgyQAHgPAAgTQAAgjgZgZQgagZgiAAQgjAAgaAZQgZAZAAAjQAAAjAZAaQAaAZAjAAQAZAAAWgPg");
	this.shape_40.setTransform(41.9,54.7,0.14,0.14,0,0,0,-0.1,0);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f().s("#454545").p("AAqhWQgZAAgRASQgPgSgZAAQgYAAgRARQgRARAAAXQAAAKAFAPIAEAJQAPAfAoAcQATAOAPAIQAQgIAUgOQAngcAPgfIAFgJQAEgNAAgMQAAgXgRgRQgRgRgXAAg");
	this.shape_41.setTransform(37.4,54.9,0.14,0.14);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#C1ECF7").s().p("AxmSSMAAAgkjMAjNAAAMAAAAkjg");
	this.shape_42.setTransform(49.8,36.2,0.14,0.14);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#71A1CC").s().p("AxmCYIAAkvMAjNAAAIAAEvg");
	this.shape_43.setTransform(49.8,12.4,0.14,0.14);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#5B87B9").s().p("AxmBdIAAi5MAjNAAAIAAC5g");
	this.shape_44.setTransform(49.8,9.2,0.14,0.14);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFCF8").s().p("AxmbTMAAAg2lMAjNAAAMAAAA2lg");
	this.shape_45.setTransform(49.8,32.4,0.14,0.14);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f().s("#E3E3E3").p("ACSAAQAAA8grArQgrArg8AAQg7AAgrgrQgrgqAAg9QAAg7ArgrQArgrA7AAQA8AAArArQArArAAA7g");
	this.shape_46.setTransform(49.8,61.5,0.14,0.14);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f().s("#E3E3E3").p("ACzAAQAABKg1A0Qg0A1hKAAQhIAAg1g1Qg1g0AAhKQAAhJA1g0QA0g1BJAAQBKAAA0A1QA1A0AABJg");
	this.shape_47.setTransform(49.8,61.5,0.14,0.14);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#1A1A1A").s().p("EgO/AkdQhFAAgzgyQgygyABhGMAAAhDlQgBhGAygyQAzgyBFAAId/AAQBFAAAyAyQAzAyAABGMAAABDlQAABGgzAyQgyAyhFAAg");
	this.shape_48.setTransform(49.8,32.7,0.14,0.14);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#BF8358").s().p("AymOCMAlNgr2MgAGAmfI10VKg");
	this.shape_49.setTransform(33.8,49.7,0.14,0.14);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-14.3,0,84.9,108.1);


(lib.Symbol2copy3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EhSBAQZMAAAggyMCkDASuIAAOEg");
	this.shape.setTransform(525,105);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1050,210);


(lib.Symbol2copy2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#A1B400").s().p("EhSBAQZMAAAggyMCkDASuIAAOEg");
	this.shape.setTransform(525,105);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1050,210);


(lib.Symbol2copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#30AFB8").s().p("EhSBAQZMAAAggyMCkDASuIAAOEg");
	this.shape.setTransform(525,105);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1050,210);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF5151").s().p("EhSBAQZMAAAggyMCkDASuIAAOEg");
	this.shape.setTransform(525,105);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1050,210);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgTBuIAAgoIAnAAIAAAogAgIAxIgKheIAAhAIAkAAIAAA/IgKBfg");
	this.shape.setTransform(792.3,18.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgpBKQgSgLgKgTQgKgTAAgZQAAgZAKgSQAKgTASgLQASgKAXAAQAZAAASAKQASAKAKATQAJATAAAZQAAAZgJATQgKATgSALQgSAKgZAAQgXAAgSgKgAgfgoQgMAPAAAZQAAAaAMAPQAMAPATABQAVgBAMgPQAMgPAAgaQAAgagMgOQgMgPgVAAQgTAAgMAPg");
	this.shape_1.setTransform(778.8,21.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAjBPIAAhFIhEAAIAABFIgkAAIAAieIAkAAIAAA/IBEAAIAAg/IAjAAIAACeg");
	this.shape_2.setTransform(760.7,21.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhEBPIAAieIAkAAIAAA7IAuAAQAbAAAOANQAOAMAAAYQAAAXgOAOQgOANgaAAgAggA0IArAAQAMAAAFgHQAGgGAAgKQAAgLgGgGQgHgHgLAAIgqAAg");
	this.shape_3.setTransform(744,21.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AhCBQIgKgCIAAgcIAFACIAHAAQAFAAAFgDQAFgDAEgJQADgJAAgQIAEhcIB0AAIAACeIgkAAIAAiCIgvAAIgCBEQgBAfgMARQgNARgWAAIgLgBg");
	this.shape_4.setTransform(725.6,22.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgnBJQgRgKgKgTQgJgTAAgYQAAgXAKgUQAKgTARgLQASgLAUAAQAYAAARALQARALAJATQAJAUAAAaIAAAEIh0AAQABAYALANQAMANASAAQAPABAJgHQAJgIAFgPIAiAAQgEASgKAMQgLAMgOAHQgPAGgSAAQgXAAgSgLgAApgOQgCgUgLgLQgKgMgSAAQgQAAgKALQgLALgCAVIBQAAIAAAAg");
	this.shape_5.setTransform(708.9,21.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgQBPIAAiBIg1AAIAAgdICLAAIAAAdIg1AAIAACBg");
	this.shape_6.setTransform(693,21.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AAqBPIAAhAIgeAAIgvBAIgqAAIA2hEQgOgEgIgHQgHgGgDgJQgDgJAAgKQAAgVAOgMQANgMAaAAIBTAAIAACegAgRguQgFAEAAALQAAALAFAGQAGAFALAAIAqAAIAAgrIgqAAQgLABgGAFg");
	this.shape_7.setTransform(676.1,21.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgpBKQgSgLgKgTQgKgTAAgZQAAgZAKgSQAKgTASgLQASgKAXAAQAZAAASAKQASAKAKATQAJATAAAZQAAAZgJATQgKATgSALQgSAKgZAAQgXAAgSgKgAgfgoQgMAPAAAZQAAAaAMAPQAMAPATABQAVgBAMgPQAMgPAAgaQAAgagMgOQgMgPgVAAQgTAAgMAPg");
	this.shape_8.setTransform(659.3,21.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgQBPIAAiBIg1AAIAAgdICLAAIAAAdIg1AAIAACBg");
	this.shape_9.setTransform(643,21.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgmBJQgSgKgJgSQgKgTAAgZQAAgYAKgTQAKgUASgKQASgLAWAAQAfABATAOQASAQADAcIgjAAQgDgPgJgJQgJgHgPgBQgSAAgMAPQgMAPAAAaQAAAaALAPQAMAPASAAQAQAAAKgKQAKgJADgRIAjAAQgFAdgTARQgUARgeABQgWAAgSgLg");
	this.shape_10.setTransform(627.3,21.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgpBKQgSgLgKgTQgKgTAAgZQAAgZAKgSQAKgTASgLQASgKAXAAQAZAAASAKQASAKAKATQAJATAAAZQAAAZgJATQgKATgSALQgSAKgZAAQgXAAgSgKgAgfgoQgMAPAAAZQAAAaAMAPQAMAPATABQAVgBAMgPQAMgPAAgaQAAgagMgOQgMgPgVAAQgTAAgMAPg");
	this.shape_11.setTransform(609.3,21.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AA4BPIAAhoIAAAAIgnBoIghAAIgmhoIgBAAIAABoIgiAAIAAieIAuAAIArB+IABAAIArh+IAuAAIAACeg");
	this.shape_12.setTransform(589.3,21.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AAnBQQgHgFgCgKQgKAKgPAFQgNAEgQAAQgZAAgOgNQgOgMgBgWQABgRAGgJQAGgJALgEQALgEAMgCIAZgFIARgCQAHgBAEgCQADgCACgCIABgEIABgHQAAgMgIgGQgIgGgOAAQgSgBgIAHQgIAHgCAPIgiAAQAEgdASgMQASgNAeAAQASAAAOAFQAPAFAIALQAJAKAAASIAABPQgBAGACADQACADAFgBIADAAIAFAAIAAAZIgNACIgKABQgKAAgHgEgAAKAGIgQADIgRACQgIADgFAGQgFAEgBALQABALAHAHQAHAGAOAAQALAAAIgEQAKgDAFgGQAGgFABgIIABgOIAAgTQgJAEgKACg");
	this.shape_13.setTransform(570.1,21.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgmBJQgSgKgJgSQgKgTAAgZQAAgYAKgTQAKgUASgKQASgLAWAAQAfABATAOQASAQADAcIgjAAQgDgPgJgJQgJgHgPgBQgSAAgMAPQgMAPAAAaQAAAaALAPQAMAPASAAQAQAAAKgKQAKgJADgRIAjAAQgFAdgTARQgUARgeABQgWAAgSgLg");
	this.shape_14.setTransform(552.6,21.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AA4BPIAAhoIAAAAIgnBoIghAAIgmhoIgBAAIAABoIgiAAIAAieIAuAAIArB+IABAAIArh+IAuAAIAACeg");
	this.shape_15.setTransform(524.4,21.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AAnBQQgGgFgDgKQgLAKgOAFQgNAEgQAAQgZAAgOgNQgOgMgBgWQABgRAGgJQAHgJAKgEQALgEAMgCIAZgFIARgCQAHgBAEgCQADgCACgCIABgEIABgHQAAgMgIgGQgIgGgPAAQgRgBgIAHQgIAHgDAPIghAAQADgdATgMQATgNAdAAQARAAAPAFQAPAFAJALQAHAKABASIAABPQgBAGACADQACADAFgBIADAAIAFAAIAAAZIgNACIgKABQgKAAgHgEgAALAGIgRADIgRACQgIADgFAGQgFAEAAALQAAALAHAHQAIAGANAAQALAAAIgEQAKgDAFgGQAGgFABgIIABgOIAAgTQgJAEgJACg");
	this.shape_16.setTransform(505.2,21.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AhOBvIAAjZIAiAAIAAAWQAIgOAOgGQAOgGAOAAQAWAAAQAKQARALAJATQAJATAAAaQAAAZgJARQgJATgQAKQgQAKgVAAQgPAAgNgGQgNgGgKgNIAABQgAgghDQgMAPAAAaQAAAaAMANQAMAPAUAAQAUAAALgPQAMgNAAgZQAAgagMgQQgMgPgTAAQgUAAgMAPg");
	this.shape_17.setTransform(487.1,24.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("Ag2BPIAAieIBtAAIAAAdIhKAAIAACBg");
	this.shape_18.setTransform(471.5,21.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AAnBQQgHgFgCgKQgKAKgPAFQgNAEgQAAQgZAAgOgNQgOgMgBgWQABgRAGgJQAGgJALgEQALgEAMgCIAZgFIARgCQAHgBAEgCQADgCACgCIABgEIABgHQAAgMgIgGQgIgGgOAAQgSgBgIAHQgIAHgCAPIgiAAQAEgdASgMQASgNAeAAQASAAAOAFQAPAFAJALQAIAKAAASIAABPQgBAGACADQACADAFgBIADAAIAFAAIAAAZIgNACIgKABQgKAAgHgEgAAKAGIgRADIgQACQgIADgFAGQgFAEgBALQABALAHAHQAHAGAOAAQALAAAIgEQAKgDAFgGQAGgFABgIIABgOIAAgTQgJAEgKACg");
	this.shape_19.setTransform(455.8,21.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgQBPIAAiBIg1AAIAAgdICLAAIAAAdIg1AAIAACBg");
	this.shape_20.setTransform(439.6,21.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgmBJQgSgKgJgSQgKgTAAgZQAAgYAKgTQAKgUASgKQASgLAWAAQAfABATAOQASAQADAcIgjAAQgDgPgJgJQgJgHgPgBQgSAAgMAPQgMAPAAAaQAAAaALAPQAMAPASAAQAQAAAKgKQAKgJADgRIAjAAQgFAdgTARQgUARgeABQgWAAgSgLg");
	this.shape_21.setTransform(423.9,21.9);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AAjBPIAAhFIhEAAIAABFIgkAAIAAieIAkAAIAAA/IBEAAIAAg/IAjAAIAACeg");
	this.shape_22.setTransform(406.2,21.9);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AAjBPIAAhyIgBAAIg6ByIgtAAIAAieIAkAAIAABzIABAAIA5hzIAtAAIAACeg");
	this.shape_23.setTransform(388.4,21.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AhGBPIAAieIBXAAQAWAAANALQAMAKAAATQAAANgGAHQgFAIgMAGIAAAAQAPAEAHAIQAIALAAAQQAAAWgQAMQgPAMgcgBgAgiA1IAuAAQAMAAAGgFQAGgGAAgLQAAgKgGgHQgGgFgMAAIguAAgAgigPIArAAQAJAAAGgFQAFgFAAgJQAAgKgFgDQgGgFgKAAIgqAAg");
	this.shape_24.setTransform(362.7,21.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgQBPIAAiBIg1AAIAAgdICLAAIAAAdIg1AAIAACBg");
	this.shape_25.setTransform(337.9,21.9);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AAjBPIAAhFIhEAAIAABFIgkAAIAAieIAkAAIAAA/IBEAAIAAg/IAjAAIAACeg");
	this.shape_26.setTransform(321.8,21.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgwBtIgOgBIAAgeIALACIAHAAQAJAAAFgFQAFgHADgIIAGgQIg+iZIAnAAIApB0IAoh0IAlAAIg7CbIgMAeQgGAOgJAKQgMAKgTAAIgKgBg");
	this.shape_27.setTransform(304.8,25);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AAoBQQgIgFgBgKQgMAKgOAFQgNAEgQAAQgZAAgPgNQgOgMAAgWQABgRAGgJQAGgJALgEQAKgEANgCIAZgFIARgCQAHgBAEgCQAEgCABgCIACgEIAAgHQAAgMgIgGQgIgGgPAAQgRgBgIAHQgJAHgCAPIghAAQADgdATgMQATgNAdAAQARAAAPAFQAPAFAJALQAHAKABASIAABPQAAAGACADQACADAEgBIAEAAIAEAAIAAAZIgNACIgJABQgLAAgGgEgAALAGIgRADIgRACQgIADgFAGQgGAEABALQAAALAHAHQAIAGAOAAQALAAAHgEQAJgDAGgGQAGgFABgIIABgOIAAgTQgJAEgJACg");
	this.shape_28.setTransform(288.5,21.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AAfBPIgwhHIgVAWIAAAxIgjAAIAAieIAjAAIAABHIA+hHIArAAIg+BBIBFBdg");
	this.shape_29.setTransform(272.2,21.9);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AAfBPIgwhHIgVAWIAAAxIgjAAIAAieIAjAAIAABHIA+hHIArAAIg+BBIBFBdg");
	this.shape_30.setTransform(255.6,21.9);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AAnBQQgHgFgCgKQgKAKgPAFQgNAEgQAAQgZAAgOgNQgOgMgBgWQABgRAGgJQAGgJALgEQALgEAMgCIAZgFIARgCQAHgBAEgCQADgCACgCIABgEIABgHQAAgMgIgGQgIgGgOAAQgSgBgIAHQgIAHgCAPIgiAAQAEgdASgMQASgNAeAAQASAAAOAFQAPAFAIALQAJAKAAASIAABPQgBAGACADQACADAFgBIADAAIAFAAIAAAZIgNACIgKABQgKAAgHgEgAAKAGIgRADIgQACQgIADgFAGQgFAEgBALQABALAHAHQAHAGAOAAQALAAAIgEQAKgDAFgGQAGgFABgIIABgOIAAgTQgJAEgKACg");
	this.shape_31.setTransform(238,21.9);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AAjBwIAAhyIgBAAIg6ByIgtAAIAAieIAkAAIAABzIABAAIA5hzIAtAAIAACegAgihOQgNgLgCgXIASAAQADANAIAFQAIAGAMAAQAOAAAIgGQAIgFADgNIARAAQgCAXgMALQgNALgXgBQgWABgMgLg");
	this.shape_32.setTransform(211.5,18.6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgpBKQgSgLgKgTQgKgTAAgZQAAgZAKgSQAKgTASgLQASgKAXAAQAZAAASAKQASAKAKATQAJATAAAZQAAAZgJATQgKATgSALQgSAKgZAAQgXAAgSgKgAgfgoQgMAPAAAZQAAAaAMAPQAMAPATABQAVgBAMgPQAMgPAAgaQAAgagMgOQgMgPgVAAQgTAAgMAPg");
	this.shape_33.setTransform(193.4,21.9);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AhGBPIAAieIBWAAQAXAAAMALQAMAKABATQAAANgGAHQgFAIgMAGIAAAAQAPAEAIAIQAHALAAAQQAAAWgPAMQgPAMgdgBgAgiA1IAuAAQAMAAAGgFQAGgGAAgLQAAgKgGgHQgGgFgMAAIguAAgAgigPIArAAQAJAAAFgFQAGgFAAgJQAAgKgGgDQgEgFgMAAIgpAAg");
	this.shape_34.setTransform(176,21.9);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgmBJQgSgKgJgSQgKgTAAgZQAAgYAKgTQAKgUASgKQASgLAWAAQAfABATAOQASAQADAcIgjAAQgDgPgJgJQgJgHgPgBQgSAAgMAPQgMAPAAAaQAAAaALAPQAMAPASAAQAQAAAKgKQAKgJADgRIAjAAQgFAdgTARQgUARgeABQgWAAgSgLg");
	this.shape_35.setTransform(158.5,21.9);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AAjBPIAAhyIgBAAIg6ByIgtAAIAAieIAkAAIAABzIABAAIA5hzIAtAAIAACeg");
	this.shape_36.setTransform(132.2,21.9);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgQBPIAAiBIg1AAIAAgdICLAAIAAAdIg1AAIAACBg");
	this.shape_37.setTransform(116.2,21.9);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgwBtIgOgBIAAgeIALACIAHAAQAJAAAFgFQAFgHADgIIAGgQIg+iZIAnAAIApB0IAoh0IAlAAIg7CbIgMAeQgGAOgJAKQgMAKgTAAIgKgBg");
	this.shape_38.setTransform(100.9,25);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AhOBvIAAjZIAiAAIAAAWQAIgOAOgGQAOgGAOAAQAWAAAQAKQARALAJATQAJATAAAaQAAAZgJARQgJATgQAKQgQAKgVAAQgPAAgNgGQgNgGgKgNIAABQgAgghDQgMAPAAAaQAAAaAMANQAMAPAUAAQAUAAALgPQAMgNAAgZQAAgagMgQQgMgPgTAAQgUAAgMAPg");
	this.shape_39.setTransform(83.7,24.6);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AAfBPIgwhHIgVAWIAAAxIgjAAIAAieIAjAAIAABHIA+hHIArAAIg+BBIBFBdg");
	this.shape_40.setTransform(66.6,21.9);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgmBJQgSgKgJgSQgKgTAAgZQAAgYAKgTQAKgUASgKQASgLAWAAQAfABATAOQASAQADAcIgjAAQgDgPgJgJQgJgHgPgBQgSAAgMAPQgMAPAAAaQAAAaALAPQAMAPASAAQAQAAAKgKQAKgJADgRIAjAAQgFAdgTARQgUARgeABQgWAAgSgLg");
	this.shape_41.setTransform(48.8,21.9);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AAoBQQgIgFgBgKQgMAKgOAFQgNAEgQAAQgZAAgPgNQgNgMgBgWQAAgRAHgJQAGgJALgEQAKgEANgCIAZgFIARgCQAHgBAEgCQADgCACgCIABgEIABgHQAAgMgIgGQgIgGgOAAQgSgBgIAHQgJAHgBAPIgiAAQAEgdASgMQASgNAeAAQASAAAOAFQAPAFAIALQAJAKAAASIAABPQAAAGABADQADADAEgBIAEAAIAEAAIAAAZIgNACIgJABQgLAAgGgEgAAKAGIgRADIgQACQgIADgFAGQgGAEAAALQAAALAIAHQAHAGAPAAQAKAAAIgEQAJgDAHgGQAFgFABgIIABgOIAAgTQgJAEgKACg");
	this.shape_42.setTransform(31.7,21.9);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AhVBuIAAjbIBeAAQAXAAARAHQARAHAKAPQAKAPAAAXQAAAggTAQQgTATgiAAIg8AAIAABVgAgugEIA0AAQATAAALgJQAMgIAAgUQAAgOgGgIQgHgIgKgDQgKgDgNAAIgwAAg");
	this.shape_43.setTransform(13.1,18.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,798.7,38.6);


(lib.Symbol17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_35 = function() {
		/* Stop at This Frame
		The  timeline will stop/pause at the frame where you insert this code.
		Can also be used to stop/pause the timeline of movieclips.
		*/
		
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(35).call(this.frame_35).wait(1));

	// Layer 3
	this.instance = new lib.Symbol21("synched",0);
	this.instance.setTransform(345.1,-90,0.497,0.497,0,0,0,85.1,85);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(21).to({_off:false},0).to({y:10},9,cjs.Ease.get(1)).to({y:0},5,cjs.Ease.get(1)).wait(1));

	// Layer 2
	this.instance_1 = new lib.Symbol20("synched",0);
	this.instance_1.setTransform(274.2,-90,0.497,0.497,0,0,0,85,85);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(14).to({_off:false},0).to({y:10},9,cjs.Ease.get(1)).to({y:0},5,cjs.Ease.get(1)).wait(8));

	// Layer 4
	this.instance_2 = new lib.Symbol19("synched",0);
	this.instance_2.setTransform(203.2,-90,0.497,0.497,0,0,0,85,85);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(7).to({_off:false},0).to({y:10},9,cjs.Ease.get(1)).to({y:0},5,cjs.Ease.get(1)).wait(15));

	// Layer 5
	this.instance_3 = new lib.Symbol18("synched",0);
	this.instance_3.setTransform(132.3,-80,0.497,0.497,0,0,0,85,85);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({y:10},9,cjs.Ease.get(1)).to({y:0},5,cjs.Ease.get(1)).wait(22));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(90,-122.2,84.6,84.6);


(lib.Symbol16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 15
	this.instance = new lib.Symbol15("synched",0);
	this.instance.setTransform(132.1,21.4,1,1,0,0,0,132.1,21.4);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Бесплатно 3 дня
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#55B553").s().p("AAUAmIAAgfIgOAAIgWAfIgUAAIAZghQgGgCgEgDQgEgCgBgEQgBgFAAgFQAAgKAGgGQAHgFAMAAIAnAAIAABLgAgHgWQgDADAAAFQAAAFADADQADADAEAAIAUAAIAAgVIgUAAQgEAAgDACg");
	this.shape.setTransform(187.3,61.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#55B553").s().p("AAQAmIAAghIggAAIAAAhIgRAAIAAhLIARAAIAAAeIAgAAIAAgeIARAAIAABLg");
	this.shape_1.setTransform(179.3,61.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#55B553").s().p("AAbAyIAAgYIg1AAIAAAYIgQAAIAAglIAKAAIAEgNIAEgSIABgWIAAgJIA4AAIAAA+IAKAAIAAAlgAgHgfIgBASIgDAPIgEALIAfAAIAAgxIgXAAg");
	this.shape_2.setTransform(170.5,63);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#55B553").s().p("AgSAyQgIgFgFgIQgEgHAAgLIAAgCIAQAAQABALAFAGQAFAFAIAAQAIAAAFgFQAGgFAAgJQAAgGgDgEQgDgEgFgBQgFgCgDgBIgDAAIgDABIAAgLIACAAIAIgBQAFgBADgDQADgEAAgGQAAgHgEgEQgEgFgHABQgHgBgEAGQgFAFAAAKIgRAAQABgQAJgJQAJgKAOAAQAJABAIADQAHAEAEAGQAEAHAAAHQAAAJgEAFQgEAGgHACQAJADAFAEQAFAHAAAJQAAAKgEAHQgFAIgIAFQgJADgKAAQgKAAgIgDg");
	this.shape_3.setTransform(157.6,60.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#55B553").s().p("AgTAjQgJgEgEgKQgGgJAAgMQAAgLAGgJQAEgKAJgFQAIgEALAAQAMAAAJAEQAIAFAFAKQAEAIABAMQgBAMgEAJQgFAKgIAEQgJAFgMABQgLgBgIgFgAgOgTQgHAIAAALQAAANAHAGQAFAIAJAAQAKAAAFgIQAGgGAAgNQAAgLgGgIQgFgHgKAAQgJAAgFAHg");
	this.shape_4.setTransform(145,61.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#55B553").s().p("AAQAmIAAghIggAAIAAAhIgQAAIAAhLIAQAAIAAAeIAgAAIAAgeIARAAIAABLg");
	this.shape_5.setTransform(136.2,61.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#55B553").s().p("AgHAmIAAg+IgaAAIAAgNIBCAAIAAANIgZAAIAAA+g");
	this.shape_6.setTransform(128.5,61.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#55B553").s().p("AASAnQgDgDgBgFQgFAFgHADQgFABgIAAQgMAAgHgGQgHgGAAgKQAAgJADgEQAEgFAFAAQAFgCAFgBIAMgCIAIgBIAFgCIACgCIABgCIAAgDQAAgGgDgDQgEgDgGAAQgIAAgFAEQgDADgBAHIgRAAQACgOAJgGQAJgHAOAAQAHABAIACQAGADAFAEQAEAGAAAIIAAAmIABAEQAAAAAAAAQABABAAAAQAAAAABAAQAAAAABAAIACAAIACAAIAAAMIgHABIgEAAQgFAAgEgBgAAEADIgHABIgHABQgEABgDADQgCACAAAFQAAAGADADQAEADAGAAQAGAAACgBQAFgCADgDQADgDAAgDIAAgHIAAgJQgDACgGABg");
	this.shape_7.setTransform(121,61.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#55B553").s().p("AggAmIgEgBIAAgNIADABIADAAIAFgCQACgBACgFQABgEABgHIABgsIA3AAIAABLIgRAAIAAg+IgWAAIgBAgQAAAPgGAJQgGAIgLAAIgGgBg");
	this.shape_8.setTransform(112,61.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#55B553").s().p("AAQAmIAAg+IggAAIAAA+IgRAAIAAhLIBCAAIAABLg");
	this.shape_9.setTransform(103.7,61.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#55B553").s().p("AgRAkQgJgGgEgIQgFgKAAgLQAAgLAFgKQAEgJAJgFQAJgFAJgBQAPABAJAHQAJAIACANIgRAAQgCgHgEgEQgEgEgHAAQgIAAgGAHQgGAIAAALQAAANAGAHQAFAHAIAAQAIAAAEgFQAFgEACgIIAQAAQgBAOgKAIQgJAIgPAAQgKAAgIgEg");
	this.shape_10.setTransform(95.3,61.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#55B553").s().p("AgSAjQgJgFgEgJQgFgJAAgMQAAgKAFgJQAFgKAIgFQAJgGAJABQALAAAIAEQAJAGAEAKQAEAKABAKIAAADIg4AAQABALAFAHQAGAGAIAAQAHAAAEgDQAFgEACgHIARAAQgDAJgEAFQgFAGgIADQgHAEgJAAQgKAAgIgGgAAUgGQgBgKgGgFQgFgGgIAAQgHAAgFAGQgFAFgBAKIAmAAIAAAAg");
	this.shape_11.setTransform(86.9,61.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#55B553").s().p("AgqA1IAAhpIBNAAIAAAQIg6AAIAAAZIAeAAQARAAAKAIQAJAHAAAQQAAAQgJAIQgKAJgRAAgAgXAmIAdAAQAJAAAFgFQAEgEABgIQgBgJgEgFQgFgFgKABIgcAAg");
	this.shape_12.setTransform(77.7,60.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-3.1,-3.1,270.3,73.9);


// stage content:
(lib._1050x90 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 6
	this.instance = new lib.Symbol27("synched",0);
	this.instance.setTransform(982.1,119.1,1,1,0,0,0,18.2,23.6);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(546).to({_off:false},0).to({y:59.1},11,cjs.Ease.get(1)).to({regX:18.1,rotation:-27,x:982},6).to({regX:18.2,rotation:0,x:982.1},6).to({regX:18.1,rotation:27},6).to({regX:18.2,rotation:0},5).wait(1));

	// Бесплатно 3 дня
	this.instance_1 = new lib.Symbol16();
	this.instance_1.setTransform(887.1,116.8,1,1,0,0,0,132.1,33.9);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(443).to({_off:false},0).to({y:36.8},12,cjs.Ease.get(1)).to({y:46.8},5,cjs.Ease.get(1)).wait(121));

	// Symbol 14
	this.instance_2 = new lib.Symbol14();
	this.instance_2.setTransform(525,117.9,1,1,0,0,0,133.3,35.6);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(426).to({_off:false},0).to({y:37.9},12,cjs.Ease.get(1)).to({y:47.9},5,cjs.Ease.get(1)).wait(138));

	// Symbol 13
	this.instance_3 = new lib.Symbol13("synched",0);
	this.instance_3.setTransform(525,117.9,1,1,0,0,0,139.8,35.6);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(386).to({_off:false},0).to({y:37.9},12,cjs.Ease.get(1)).to({y:47.9},5,cjs.Ease.get(1)).wait(8).to({startPosition:0},0).to({x:164.3},13,cjs.Ease.get(-0.2)).wait(157));

	// Layer 8
	this.instance_4 = new lib.Symbol2copy3("synched",0);
	this.instance_4.setTransform(527.5,248.6,1,1,0,0,0,527.5,157.6);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(366).to({_off:false},0).to({y:37.6},20,cjs.Ease.get(1)).wait(195));

	// Layer 4
	this.instance_5 = new lib.Symbol23("synched",0);
	this.instance_5.setTransform(104,137.5,1,1,0,0,0,36.7,43.3);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(280).to({_off:false},0).to({y:37.5},9,cjs.Ease.get(1)).to({y:47.5},4,cjs.Ease.get(1)).wait(45).to({startPosition:0},0).to({scaleX:1.3,scaleY:1.3},6,cjs.Ease.get(1)).to({scaleX:1,scaleY:1},6,cjs.Ease.get(1)).to({scaleX:1.3,scaleY:1.3,y:47.6},6,cjs.Ease.get(1)).to({scaleX:1,scaleY:1,y:47.5},6,cjs.Ease.get(1)).to({_off:true},24).wait(195));

	// 4.Автоматическое продвижение - минимум Вашего участия
	this.instance_6 = new lib.Symbol28("synched",0);
	this.instance_6.setTransform(592.1,114,1,1,0,0,0,407.9,18.2);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(275).to({_off:false},0).to({y:44},11,cjs.Ease.get(1)).to({_off:true},100).wait(195));

	// Layer 5
	this.instance_7 = new lib.Symbol2copy2("synched",0);
	this.instance_7.setTransform(527.5,248.6,1,1,0,0,0,527.5,157.6);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(255).to({_off:false},0).to({y:37.6},20,cjs.Ease.get(1)).to({_off:true},111).wait(195));

	// Layer 3
	this.instance_8 = new lib.Symbol29("synched",0);
	this.instance_8.setTransform(310,120.4);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(136).to({_off:false},0).to({y:60.4},10,cjs.Ease.get(1)).wait(435));

	// Layer 1
	this.instance_9 = new lib.Symbol17();
	this.instance_9.setTransform(687.9,78.8,0.56,0.56,0,0,0,389.6,90.5);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(129).to({_off:false},0).to({_off:true},257).wait(195));

	// 2.Только живые подписчики с Вашего города. copy
	this.instance_10 = new lib.Symbol5copy("synched",0);
	this.instance_10.setTransform(452,105.1,1,1,0,0,0,336.1,19.3);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(127).to({_off:false},0).to({y:49.3},11,cjs.Ease.get(1)).to({y:45.1},4).to({_off:true},131).wait(308));

	// 2.Только живые подписчики с Вашего города.
	this.instance_11 = new lib.Symbol5("synched",0);
	this.instance_11.setTransform(452,105.1,1,1,0,0,0,336.1,19.3);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(122).to({_off:false},0).to({y:49.3},11,cjs.Ease.get(1)).to({y:45.1},4).to({_off:true},136).wait(308));

	// Layer 2
	this.instance_12 = new lib.Symbol2copy("synched",0);
	this.instance_12.setTransform(527.5,248.6,1,1,0,0,0,527.5,157.6);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(107).to({_off:false},0).to({y:37.6},20,cjs.Ease.get(1)).to({_off:true},259).wait(195));

	// instagram.png
	this.instance_13 = new lib.Symbol3("synched",0);
	this.instance_13.setTransform(-46.6,65.8,2.43,2.43,0,0,0,53,40);
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(28).to({_off:false},0).to({x:93.4,y:45.8},10,cjs.Ease.get(1)).to({scaleX:1,scaleY:1,x:106.4,y:45},7,cjs.Ease.get(1)).wait(42).to({startPosition:0},0).to({regX:53.1,scaleX:2,scaleY:2,x:126.5,y:55},11,cjs.Ease.get(1)).to({regX:53,scaleX:1,scaleY:1,x:106.4,y:45},9,cjs.Ease.get(1)).to({_off:true},20).wait(454));

	// 1.Раскрути свой аккаунт в инстаграм самостоятельно!
	this.instance_14 = new lib.Symbol1("synched",0);
	this.instance_14.setTransform(592.3,115.1,1,1,0,0,0,399.4,19.3);
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(16).to({_off:false},0).to({y:45.1},12,cjs.Ease.get(1)).to({_off:true},99).wait(454));

	// Layer 10
	this.instance_15 = new lib.Symbol2("synched",0);
	this.instance_15.setTransform(527.5,248.6,1,1,0,0,0,527.5,157.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).to({y:37.6},20,cjs.Ease.get(1)).to({_off:true},107).wait(454));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(525,136,1050,210);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;