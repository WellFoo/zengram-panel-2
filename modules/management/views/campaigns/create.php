<?php
/**
 * @var \yii\web\View $this
 * @var \app\modules\management\models\CampaignForm $model
 */

use app\modules\management\models\UniSenderList;
use dosamigos\tinymce\TinyMce;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = 'Новая рассылка';
?>
<div class="campaigns-create">
	<h2><?= $this->title ?></h2>

	<?php $form = ActiveForm::begin([]); ?>

	<?= $form->field($model, 'list_id')->dropDownList(
		ArrayHelper::map(UniSenderList::findAll(true), 'id', 'title'),
		['prompt' => 'Выберите']
	) ?>

	<?= $form->field($model, 'subject') ?>

	<?= $form->field($model, 'html_body')->widget(TinyMce::className(), [
		'options' => ['rows' => 12],
		'language' => 'ru',
		'clientOptions' => [
			'plugins' => [
				'advlist autolink lists link charmap print preview anchor',
				'searchreplace visualblocks code fullscreen',
				'insertdatetime media image table contextmenu paste',
				'fullpage'
			],
			'convert_urls' => false,
			'toolbar' => "undo redo | styleselect | bold italic | valigntop valignmiddle valignbottom | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
		]
	]) ?>

	<?= $form->field($model, 'track_read')->checkbox() ?>

	<?= $form->field($model, 'track_links')->checkbox() ?>

	<?= Html::submitButton('Начать', ['class' => 'btn btn-success']) ?>

	<?php $form::end(); ?>
</div>
