<?php

use yii\helpers\Url;

$this->title = 'Выплаты';

?>

<ul class="nav nav-tabs">
	<li class="active"><a href="<?= Url::to(['manager/index'])?>">Выплаты</a></li>
	<li><a href="<?= Url::to(['manager/statistic'])?>">Статистика</a></li>
</ul>

<?= $this->render('index.int.php') ?>