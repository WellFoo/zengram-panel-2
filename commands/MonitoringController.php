<?php

namespace app\commands;

use app\components\RPCHelper;
use app\components\SmsCenterApi;
use app\components\TaskController;
use app\models\Account;
use app\models\AccountStats;
use app\models\Invoice;
use app\models\Log;
use app\models\Mail;
use app\models\Monitoring;
use app\models\MonitoringLog;
use app\models\MonitoringWorkersOffline;
use app\models\Proxy;
use app\modules\admin\components\Settings;
use app\modules\admin\models\CommentingProject;
use app\modules\admin\models\PopularLog;
use app\modules\admin\models\MonitoringWorkers;
use Yii;
use yii\db\Expression;
use \YandexMoney\API;

class MonitoringController extends TaskController
{
	const DAY = 86400;
	public function actionScan()
	{
		// Получаем массив с работающими проектами с worker-а
		if (PHP_OS !== 'WINNT') {
			exec('ps aux | grep "php /var/www/html/yii monitoring/scan" | grep -v grep', $output);
			if (count($output) > 1) {
				print_r($output);
				exit(PHP_EOL.'Данный процесс уже запущен. Найдено процессов: '. count($output) .'. Выходим.');
			} else {
				print_r($output);
			}
		}

		$insta_ids = [];
		foreach (Yii::$app->params['workers'] as $ip) {
			$content = @file_get_contents('http://' . $ip . '/zengramManage.php?action=active-projects');
			if ($content === false) {
				continue;
			}

			$data = json_decode($content, true);
			if (!is_array($data)) {
				$data = [];
				//die('Ошибка при получении данных');
			}
			foreach ($data as $item) {
				$insta_ids[] = $item['insta_id'];
			}
		}

		$accountsQuery = Account::find()->where(['monitoring_status' => 'work']);
		foreach ($accountsQuery->each(20) as $account) {
			/** @var Account $account */
			// Ищем отвалившиеся аккаунты
			if (in_array(strval($account->instagram_id), $insta_ids)) {
				continue;
			}

			// Пишем в лог информацию, если процесс работы с аккаунтом отвалился
			echo 'Аккаунт ' . $account->instagram_id . ' был перезапущен после сбоя' . PHP_EOL;
			/** @var Log $log */
			$log = new Log();
			$log->user_id = $account->user_id;
			$log->account_id = $account->instagram_id;
			$log->message = 'Restart after crash';
			$log->save();

			// Запускаем отвалившийся проект
			//$account->accountStart(true);
			RPCHelper::startAccount($account, 'cronaftercrash');
		}
	}

	public function actionClearPreviousValues()
	{
		Yii::$app->db->createCommand('UPDATE ' . Monitoring::tableName() . ' SET last_result = "{}";')->execute();
	}

	public function actionMonitoringOfflineServers()
	{
		/** @var Monitoring[] $monitorings */
		$monitorings = Monitoring::find()->andWhere(['type' => 'offline_servers_time'])->all();
		foreach ($monitorings as $monitoring) {
			foreach (Yii::$app->params['check_servers'] as $ip => $serverName) {
				if (!$this->getServerStatus($ip, 80)) {
					echo $serverName . ' offline', PHP_EOL;
					/** @var MonitoringWorkersOffline $monitoring_offline */
					$monitoring_offline =
						MonitoringWorkersOffline::find()
							->where(['monitoring_id' => $monitoring->id])
							->andWhere(['worker' => $serverName])
							->andWhere(['complete' => 0])
							->andWhere(['checked' => 0])
							->one();
					if ($monitoring_offline !== null) {
						$monitoring_offline->offline_time += 1; //cron раз в минуту
						$monitoring_offline->save();
					} else {
						$monitoring_offline = new MonitoringWorkersOffline([
							'worker' => $serverName,
							'monitoring_id' => $monitoring->id,
							'complete' => 0,
							'checked' => 0,
							'offline_time' => 1,
						]);
						$monitoring_offline->save();
					}
				} else {
					echo $serverName . ' in normal', PHP_EOL;
					$monitoring_offline =
						MonitoringWorkersOffline::find()
							->where(['monitoring_id' => $monitoring->id])
							->andWhere(['worker' => $serverName])
							->andWhere(['complete' => 0])
							->andWhere(['checked' => 0])
							->one();
					if ($monitoring_offline !== null) {
						$monitoring_offline->complete = 1;
						$monitoring_offline->save();
					}
				}
			}
		}
	}

	public function monitoring_check_memory_on_workers()
	{
		// Проверяем превышение порога памяти
		$memLoads = [];
		foreach (\Yii::$app->params['workers'] as $key => $worker) {
			$result = RPCHelper::getServerLoad($worker);
			if($result['status'] === 'ok'){
				$percentOfUseMemory = 100 - $result['data']['mem'];
				//echo 'Сервер '.  Yii::$app->params['check_servers']["$worker"] . ' жрет ' . $percentOfUseMemory  .'% памяти'. PHP_EOL;
				$memLoads[] = $percentOfUseMemory;
				if ($percentOfUseMemory  > 49){
					// Нужно покупать новый сервер, ибо упадёт
					//echo 'Превышение! Записываем лог в базу!'. PHP_EOL;
					$log = new MonitoringWorkers();
					$log->worker = Yii::$app->params['check_servers']["$worker"];
					$log->type_event =  MonitoringWorkers::MEMORY_OVERLOAD;
					$log->description = 'Превышение допустимого порога памяти. Потребление: '. round($percentOfUseMemory).'%';
					$log->monitoring_id = 20;
					$log->save();
				}
			}else{
				//echo 'Сервер '.  Yii::$app->params['check_servers']["$worker"] . ' упал ' . PHP_EOL;
			}

		}
		$max = round(max($memLoads));
		echo 'Самый загруженный сервер: '.$max;
		return $max;
	}

	public function actionTenminutes()
	{
		$monitorings = Monitoring::find()->where(['period' => Monitoring::MONITORING_PERIOD_10MIN])->all();
		$this->monitoringProcess($monitorings);
	}

	public function actionHourly()
	{
		$monitorings = Monitoring::find()->where(['period' => Monitoring::MONITORING_PERIOD_HOURLY])->all();
		$this->monitoringProcess($monitorings);
	}

	public function actionDaily()
	{
		$monitorings = Monitoring::find()->where(['period' => Monitoring::MONITORING_PERIOD_DAILY])->all();
		$this->monitoringProcess($monitorings);
	}

	public function actionTests()
	{
		$monitorings = Monitoring::find()->where(['period' => Monitoring::MONITORING_PERIOD_TEST])->all();
		$this->monitoringProcess($monitorings);
	}

	public function monitoring_account_follows()
	{
		$query = AccountStats::find()
			->select([
				'SUM(' . AccountStats::tableName() . '.follows) as follows',
				'SUM(' . AccountStats::tableName() . '.follows_time) as follows_time',
			])
			->orWhere(['!=', AccountStats::tableName() . '.likes', 0])
			->orWhere(['!=', AccountStats::tableName() . '.comments', 0])
			->orWhere(['!=', AccountStats::tableName() . '.follows', 0])
			->orWhere(['!=', AccountStats::tableName() . '.unfollows', 0]);
		$query->andWhere(['date' => date('Y-m-d', strtotime('-1 day'))])
			->asArray();
		$result_query = $query->one();
		if (!$result_query['follows_time']) {
			return 0;
		}
		//		if ($monitoring->last_result === '{}') {
		$result = ($result_query['follows'] / $result_query['follows_time']) * 86400;
//		} else {
//			$last_result = json_decode($monitoring->last_result, true);
//			$result = (($result_query['follows'] - $last_result['follows']) / ($result_query['follows_time'] - $last_result['follows_time'])) * 3600;
//		}
//		$monitoring->last_result = json_encode($result_query);
//		$monitoring->save();
		return $result;
	}

	public function monitoring_account_unfollows()
	{
		$query = AccountStats::find()
			->select([
				'SUM(' . AccountStats::tableName() . '.unfollows) as unfollows',
				'SUM(' . AccountStats::tableName() . '.unfollows_time) as unfollows_time',
			])
			->orWhere(['!=', AccountStats::tableName() . '.likes', 0])
			->orWhere(['!=', AccountStats::tableName() . '.comments', 0])
			->orWhere(['!=', AccountStats::tableName() . '.follows', 0])
			->orWhere(['!=', AccountStats::tableName() . '.unfollows', 0]);
		$query->andWhere(['date' => date('Y-m-d', strtotime('-1 day'))])
			->asArray();
		$result_query = $query->one();
		if (!$result_query['unfollows_time']) {
			return 0;
		}
//		if ($monitoring->last_result === '{}') {
		$result = ($result_query['unfollows'] / $result_query['unfollows_time']) * 86400;
//		} else {
//			$last_result = json_decode($monitoring->last_result, true);
//			$result = (($result_query['unfollows'] - $last_result['unfollows']) / ($result_query['unfollows_time'] - $last_result['unfollows_time'])) * 3600;
//		}
//		$monitoring->last_result = json_encode($result_query);
//		$monitoring->save();
		return $result;
	}

	public function monitoring_account_likes()
	{
		$query = AccountStats::find()
			->select([
				'SUM(' . AccountStats::tableName() . '.likes) as likes',
				'SUM(' . AccountStats::tableName() . '.likes_time) as likes_time',
			])
			->orWhere(['!=', AccountStats::tableName() . '.likes', 0])
			->orWhere(['!=', AccountStats::tableName() . '.comments', 0])
			->orWhere(['!=', AccountStats::tableName() . '.follows', 0])
			->orWhere(['!=', AccountStats::tableName() . '.unfollows', 0]);
		$query->andWhere(['date' => date('Y-m-d', strtotime('-1 day'))])
			->asArray();
		$result_query = $query->one();
		if (!$result_query['likes_time']) {
			return 0;
		}
//		if ($monitoring->last_result === '{}') {
		$result = ($result_query['likes'] / $result_query['likes_time']) * 86400;
//		} else {
//			$last_result = json_decode($monitoring->last_result, true);
//			$result = (($result_query['likes'] - $last_result['likes']) / ($result_query['likes_time'] - $last_result['likes_time'])) * 3600;
//		}
//		$monitoring->last_result = json_encode($result_query);
//		$monitoring->save();
		return $result;
	}

	public function monitoring_account_comments()
	{
		$query = AccountStats::find()
			->select([
				'SUM(' . AccountStats::tableName() . '.comments) as comments',
				'SUM(' . AccountStats::tableName() . '.comments_time) as comments_time',
			])
			->orWhere(['!=', AccountStats::tableName() . '.likes', 0])
			->orWhere(['!=', AccountStats::tableName() . '.comments', 0])
			->orWhere(['!=', AccountStats::tableName() . '.follows', 0])
			->orWhere(['!=', AccountStats::tableName() . '.unfollows', 0]);
		$query->andWhere(['date' => date('Y-m-d', strtotime('-1 day'))])
			->asArray();
		$result_query = $query->one();
		if (!$result_query['comments_time']) {
			return 0;
		}
//		if ($monitoring->last_result === '{}') {
		$result = ($result_query['comments'] / $result_query['comments_time']) * 86400;
//		} else {
//			$last_result = json_decode($monitoring->last_result, true);
//			$result = (($result_query['comments'] - $last_result['comments']) / ($result_query['comments_time'] - $last_result['comments_time'])) * 3600;
//		}
//		$monitoring->last_result = json_encode($result_query);
//		$monitoring->save();
		return $result;
	}

	public function monitoring_account_came()
	{
		$query = AccountStats::find()
			->select([
				'SUM(' . AccountStats::tableName() . '.came) as came',
			]);
		$query
			->andWhere(['date' => date('Y-m-d', strtotime('-1 day'))])
			->asArray();
		$result = $query->one();
		$count = AccountStats::find()
			->select([
				'COUNT(DISTINCT ' . AccountStats::tableName() . '.account_id) as count',
			])
			->orWhere(['!=', AccountStats::tableName() . '.likes', 0])
			->orWhere(['!=', AccountStats::tableName() . '.comments', 0])
			->orWhere(['!=', AccountStats::tableName() . '.follows', 0])
			->orWhere(['!=', AccountStats::tableName() . '.unfollows', 0])
			->andWhere(['date' => date('Y-m-d', strtotime('-1 day'))])
			->asArray();
		$num_accounts = $count->one();
		return ($result['came'] / $num_accounts['count']);
	}

	public function monitoring_account_gone()
	{
		$query = AccountStats::find()
			->select([
				'SUM(' . AccountStats::tableName() . '.gone) as gone',
			]);
		$query
			->andWhere(['date' => date('Y-m-d', strtotime('-1 day'))])
			->asArray();
		$result = $query->one();
		$count = AccountStats::find()
			->select([
				'COUNT(DISTINCT ' . AccountStats::tableName() . '.account_id) as count',
			])
			->orWhere(['!=', AccountStats::tableName() . '.likes', 0])
			->orWhere(['!=', AccountStats::tableName() . '.comments', 0])
			->orWhere(['!=', AccountStats::tableName() . '.follows', 0])
			->orWhere(['!=', AccountStats::tableName() . '.unfollows', 0])
			->andWhere(['date' => date('Y-m-d', strtotime('-1 day'))])
			->asArray();
		$num_accounts = $count->one();
		return ($result['gone'] / $num_accounts['count']);
	}

	public function monitoring_proxy_average_ping()
	{
		$pings = [];
		foreach (Proxy::find()->all() as $proxy) {
			/** @var Proxy $proxy */
			preg_match('#[0-9]+(?:\.[0-9]+){3}#', $proxy->proxy, $matches);
			$ping = $this->getPing($matches[0]);
			//не учитываем пинг в 10000 - это таймаут, прокси оофлайн. по задаче не учитываем
			if ($ping < 10000) {
				$pings[$proxy->id] = $ping;
			}
		}
		return array_sum($pings) / count($pings);
	}

	public function monitoring_proxy_offline()
	{
		$offline = 0;
		foreach (Proxy::find()->all() as $proxy) {
			/** @var Proxy $proxy */
			if (!$proxy->checkStatus()) {
				$proxy->status = Proxy::STATUS_ERROR;
				$proxy->save();
				$offline++;
			}
		}
		return $offline;
	}

	public function monitoring_star_comments($monitoring)
	{
		/** @var Monitoring $monitoring */
		$count_projects = CommentingProject::find()->where(['cron_status' => Settings::STATUS_ACTIVE])->count();

		$query = PopularLog::find()->andWhere(['date' => date('Y-m-d')]);
		$value = $query->sum(PopularLog::tableName() . '.comments');

		if ($monitoring->last_result === '{}') {
			$result = $value / $count_projects;
		} elseif ($count_projects != 0) {
			$last_result = json_decode($monitoring->last_result, true);
			$result = ($value - $last_result['star_comments']) / $count_projects;
		}
		if (!isset($result) || $result < 0) {
			$result = 0;
		}
		$monitoring->last_result = json_encode(['star_comments' => $value]);
		$monitoring->save();
		return $result;
	}

	public function monitoring_empty_invoices()
	{
		/** @var Monitoring $monitoring */
		$query = Invoice::find()->where(['!=', 'status', Invoice::STATUS_SUCCESS])->andWhere(['redirected' => 1]);
		$query->andWhere(['>=', 'date', new Expression('NOW() - \'10 MINUTE\'::INTERVAL')]);
		return $query->count();
	}

	public function monitoring_invoice_fail_percent()
	{
		$success = Invoice::find()->where(['=', 'status', Invoice::STATUS_SUCCESS])->andWhere(['like', 'date', date('Y-m-d', strtotime('-1 day'))])->count();
		$fail = Invoice::find()->where(['!=', 'status', Invoice::STATUS_SUCCESS])->andWhere(['like', 'date', date('Y-m-d', strtotime('-1 day'))])->count();
		return ($fail / $success) * 100;
	}

	public function monitoring_ping_servers()
	{
		$pings = [];
		foreach (Yii::$app->params['check_servers'] as $ip => $serverName) {
			$ping = $this->getPing($ip);
			//не учитываем пинг в 10000 - это таймаут, сервер оофлайн. по задаче не учитываем
			if ($ping < 10000) {
				$pings[] = $ping;
			}
		}
		return array_sum($pings) / count($pings);
	}

	public function monitoring_offline_servers()
	{
		$count = 0;
		$message = null;

		foreach (Yii::$app->params['check_servers'] as $ip => $serverName) {
			if (!$this->getServerStatus($ip, 80)) {
				$count++;

				if ($message === null) {
					$message = 'Недоступные сервера:' . PHP_EOL;
				}

				$message .= $serverName . '(' . $ip . ') недоступен.' . PHP_EOL;
			}
		}

		return [$count, $message];
	}

	public function monitoring_offline_servers_time($monitoring)
	{
		/** @var Monitoring $monitoring */
		/** @var MonitoringWorkersOffline[] $completed */
		$completed = MonitoringWorkersOffline::find()->where(['monitoring_id' => $monitoring->id])->andWhere(['complete' => 1])->andWhere(['checked' => 0])->all();
		$sum_offline = 0;
		foreach ($completed as $item) {
			$sum_offline += $item->offline_time;
			$item->checked = 1;
			$item->save();
		}
		return $sum_offline;
	}

	public function monitoring_num_accouns_workers()
	{
		$counts = [];
		foreach (Yii::$app->params['check_workers'] as $ip) {
			$content = file_get_contents('http://' . $ip . '/zengramManage.php?action=active-projects');
			$data = json_decode($content, true);
			if (!is_array($data)) {
				continue;
			}
			$counts[] = count($data);
		}
		return array_sum($counts) / count($counts);
	}

	public function monitoring_workers_resources()
	{
		$resources = [];
		foreach (Yii::$app->params['check_workers'] as $ip) {
			$content = file_get_contents('http://' . $ip . '/zengramManage.php?action=resources');
			$data = json_decode($content, true);
			if (empty($data)) {
				continue;
			}
			if (!empty($data['mem'])) {
				$resources[] = 100 - $data['mem'];
			}
		}
		return array_sum($resources) / count($resources);
	}

	private function getPing($server)
	{
		return shell_exec('ping -q -c 1 ' . $server . ' | grep "packets transmitted" | sed s/"^[[:print:]]* time \([0-9]*\)ms$"/\\\\1/g');
	}

	private function getServerStatus($site, $port)
	{
		$fp = @fsockopen($site, $port, $errno, $errstr, 2);
		if (!$fp) {
			$result = false;
		} else {
			$result = true;
		}
		@fclose($fp);
		return $result;
	}

	private function monitoringProcess($monitorings)
	{
		/** @var Monitoring[] $monitorings */
		foreach ($monitorings as $monitoring) {
			$method = 'monitoring_' . $monitoring->type;
			if (!method_exists($this, $method)) {
				if (!$monitoring->incorrect) {
					$monitoring->incorrect = true;
					$monitoring->save();
				}
				continue;
			} elseif ($monitoring->incorrect) {
				$monitoring->incorrect = false;
			}

			$result = $this->$method($monitoring);
			$message = null;

			if (is_array($result)) {
				list($result, $message) = $result;
			}

			$result = round($result);

			if (!$this->monitoringCheckNormal($monitoring, $result)) {
				$monitoring->count++;
				$this->monitoringNotify($monitoring, $result, $message);
			} else {
				$log = new MonitoringLog([
					'monitoring_id' => $monitoring->id,
					'result' => (string)$result,
					'success' => 1,
					'notify_email' => false,
					'notify_sms' => false,
				]);
				$log->save();
				$monitoring->result = 'В норме: ' . $result;
			}
			$monitoring->save();
		}
	}

	private function monitoringCheckNormal($monitoring, $result)
	{
		if ($monitoring->comparation == Monitoring::MONITORING_COMPORATION_LESS) {
			if ($result < $monitoring->value) {
				return false;
			}
		}
		if ($monitoring->comparation == Monitoring::MONITORING_COMPORATION_MORE) {
			if ($result > $monitoring->value) {
				return false;
			}
		}
		return true;
	}

	private function monitoringNotify($monitoring, $result, $message = null)
	{
		/** @var Monitoring $monitoring */
		$result_email = false;
		$result_sms = false;

		$subject = 'Техническая метрика  ' . Yii::$app->params['host'];

		$text = 'Параметр "' . $monitoring->category . ' ' . $monitoring->title . '" '
			.($monitoring->comparation === Monitoring::MONITORING_COMPORATION_LESS ? 'ниже' : 'выше')
			.' нормы(' . $monitoring->value . '): ' . $result;

		if ($message !== null and is_string($message))
		{
			$text .= PHP_EOL . PHP_EOL . $message;
		}

		if (YII_ENV !== 'dev') {
			if ($monitoring->email) {
				$result_email = (bool)Yii::$app->mailer->compose()
					->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['company']])
					->setTo(explode(',', $monitoring->email))
					->setSubject($subject)
					->setTextBody(
						$text
					)->send();
				$mailModel = new Mail();
				$mailModel->mail = $monitoring->email;
				$mailModel->type = 'monitoring';
				$mailModel->save();
			}

			if ($monitoring->sms) {
				$sms_center = new SmsCenterApi(
					Yii::$app->params['sms_center_login'],
					Yii::$app->params['sms_center_password'],
					Yii::$app->params['adminEmail'],
					'utf-8',
					1,
					0,
					YII_ENV === 'dev');
//			$sms_id = $sms_center->send_sms($monitoring->sms, $text)[0];
//			list($sms_id, $sms_cnt, $cost, $balance) = $sms_center->send_sms($monitoring->sms, $text);
				$result_send_sms = $sms_center->send_sms($monitoring->sms, $text);
				$status = $sms_center->get_status($result_send_sms[0], $monitoring->sms);
//			list($status, $time) = $sms_center->get_status($sms_id, $monitoring->sms);
				$result_sms = !empty($status[0]);
			}
		}
		$log = new MonitoringLog([
			'monitoring_id' => $monitoring->id,
			'result' => (string)$result,
			'notify_email' => $result_email,
			'success' => 0,
			'notify_sms' => $result_sms,
		]);
		$log->save();
		$monitoring->result = 'Неверно: ' . $result;
	}

	public function actionTest()
	{
		//print_r(Yii::$app->params['workers']);
		while (true) {
			sleep(5);
			if (PHP_OS !== 'WINNT') {
				exec('ps aux | grep monitoring/test | grep -v grep', $output);
				print_r($output);
				$output = null;
			}
		}

	}

	public function actionDoubles()
	{
		$started = Account::getActiveAccounts();
		$doubles = [];
		foreach ($started as $key => $value) {
			unset($started[$key]);
			if (in_array($value, $started)) {
				$doubles[] = $value;
			}
		}

		//echo implode("\n", $doubles);
		foreach($doubles as $instaID){
			/** @var Account $account */
			$account = Account::findOne(['instagram_id' => $instaID]);
			if (is_null($account)){
				continue;
			}
			RPCHelper::stopAccount($account, 'MonitoringDoubles');
			RPCHelper::startAccount($account, 'MonitoringDoubles');
			echo $instaID.PHP_EOL;
		}
	}

	// Проверка и подтверждение платежей с яндекс денег
	public function actionCheckYapayments()
	{
		// Получаем список платеже, совершенных через систему Яндекс Деньги за последние сутки
		$yandexPayments = Invoice::find()
			->where(['agent' => 'yandex_card', 'status' => 0])
			->andWhere(['>', 'date', date('Y-m-d', time() - self::DAY)])
			->asArray()
			->all();

		$api = new API(Yii::$app->params['ym-token']);

		$yesterday = date('Y-m-d', time() - self::DAY).'T00:00:00Z';
		$operationHistoryOptions = [
			//'label' => [20105]
			'type' =>'deposition',
			'from' => $yesterday
		];

		/** @var object $operations */
		$operations = $api->operationHistory($operationHistoryOptions);
		$successPaymentIds = [];
		foreach($operations->operations as $operation){
			if(isset($operation->label)){
				$successPaymentIds[] = $operation->label;
			}
		}

		// Перебираем значение платежей
		foreach($yandexPayments as $payment){
			if(array_search($payment['id'], $successPaymentIds) !== false){
				$invoice = Invoice::findOne($payment['id']);
				/** @var  $invoice Invoice */
				$invoice->apply('yandex_card');
			}
		}
	}
}
