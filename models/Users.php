<?php

namespace app\models;

use app\modules\partnership\models\Manager;
use app\modules\partnership\models\Partner;
use app\modules\partnership\models\Referal;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;
use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string  $mail
 * @property string  $password
 * @property string  $access_token
 * @property string  $instance_id
 * @property integer $balance
 * @property integer $demo
 * @property integer $sameMade
 * @property integer $sameBlack
 * @property integer $exactGeo
 * @property integer $allowImages
 * @property integer $proxy_cat_id
 * @property string  $regdate
 * @property integer $reg_ip
 * @property string  $registerIP
 * @property boolean $is_service
 * @property boolean $is_payed
 * @property integer $balance_flow
 * @property integer $continent
 * @property string  $role
 * @property boolean $isAdmin
 * @property boolean $skip_popular_update
 * @property boolean $comment_popular
 * @property boolean $upload_photo
 *
 * @property Partner $partner
 * @property boolean $isPartner
 * @property Referal $referral
 * @property boolean $isReferral
 * @property Manager $manager
 * @property boolean $isManager
 *
 * @property string  $mailLowerCase
 *
 * @property Account[] $accounts
 * @property integer   $accountsCount
 * @property Invoice[] $invoices
 * @property Invoice[] $monthInvoices
 * @property UsersInfo $info
 * @property boolean   $antispam
 * @property boolean   $parallel_unfollows
 * @property string $gender
 * @property string $person
 * @property boolean $unfollow_mode_multiactions
 * @property boolean $email_confirmed
 * @property boolean $use_timer
 * @property boolean $follow_for_private
 * @property boolean $is_reg_from_rf
 * @property UserHints $hints
 */
class Users extends ActiveRecord implements IdentityInterface
{
	const ROLE_ADMIN     = 'admin';
	const ROLE_MANAGER   = 'manager';
	const ROLE_USER      = 'user';
	const ROLE_FINANCIER = 'financier';

	const MALE   = 'Мужчина';
	const FEMALE = 'Женщина';

	const UNDEFINED = 'Не определен';

	const INDIVIDUAL = 'Физик';
	const COMMERCIAL = 'Коммерческий';
	const EMPLOYED   = 'Самозанятый';

	public $repassword;

	private $_accountsCount = null;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'users';
	}


	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['mail', 'password'], 'required'],
			[['password', 'continent'], 'string'],
			['mail', 'string', 'max' => 255],
			['access_token', 'string', 'max' => 50],
			['mail', 'unique', 'targetAttribute' => ['mailLowerCase' => 'lower(mail)']],
			[['balance', 'balance_flow', 'reg_ip'], 'safe'],
			['proxy_cat_id', 'default', 'value' => null],
			['continent', 'default', 'value' => 'EU'],
			[['access_token', 'instance_id'], 'default', 'value' => ''],
			[['sameMade', 'sameBlack', 'exactGeo', 'allowImages', 'balance_flow' ], 'default', 'value' => 0],
			[['is_service', 'exactGeo','sameBlack',
				'sameMade', 'allowImages', 'skip_popular_update', 'parallel_unfollows',
				'comment_popular', 'upload_photo', 'antispam', 'unfollow_mode_multiactions',
				'email_confirmed', 'use_timer', 'follow_for_private', 'subscribe_stats', 'is_reg_from_rf'],
				'boolean'],
			[['subscribe_stats'], 'default', 'value' => true],
			[['delivery_stats_type'], 'integer'],
			[['delivery_stats_type'], 'default', 'value' => 1],
			['role', 'in', 'range' => [self::ROLE_ADMIN, self::ROLE_MANAGER, self::ROLE_USER, self::ROLE_FINANCIER]],
			['role', 'default', 'value' => self::ROLE_USER],
			['gender', 'in', 'range' => [self::MALE, self::FEMALE, self::UNDEFINED]],
			['person', 'in', 'range' => [self::INDIVIDUAL, self::COMMERCIAL, self::EMPLOYED, self::UNDEFINED]]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'           => Yii::t('app', 'ID'),
			'mail'         => Yii::t('app', 'Mail'),
			'password'     => Yii::t('app', 'Password'),
			'access_token' => Yii::t('app', 'Access Token'),
			'proxy_cat_id' => 'Список прокси',
			'sameBlack'    => 'Общий черный список',
			'sameMade'     => 'Общий список сделанных',
			'exactGeo'     => 'Функции точного таргетинга',
			'allowImages'  => 'Массовая загрузка',
			'is_service'   => 'Служебный',
			'role'         => 'Роль',
			'regdate'      => 'Дата регистрации',
			'is_payed'     => 'Были оплаты',
			'registerIP'   => 'IP при регистрации',
			'skip_popular_update' => 'Пропускать обновление популярных аккаунтов',
			'comment_popular' => 'Комментировать популярные аккаунты',
			'upload_photo' => 'Разрешить загружать фото',
			'antispam' => 'Включить Антиспам',
			'parallel_unfollows' => 'Параллельная работа отписок и подписок',
			'gender' => 'Пол',
			'person' => 'Лицо',
			'unfollow_mode_multiactions' => 'Одновременное выполнение Likes и Comments при включенном Unfollows',
			'use_timer' => 'Показать таймер',
			'follow_for_private' => 'Работать по приватным (делается только follow)',
			'is_reg_from_rf' => 'Зарегистрированы с поддомена rf'
		];
	}

	public function beforeSave($insert)
	{
		if ($insert) {
			$this->reg_ip = ip2long(Yii::$app->request->userIP);
		}
		return parent::beforeSave($insert);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getAccounts()
	{
		return $this->hasMany(Account::className(), ['user_id' => 'id']);
	}

	public function getAccountsCount()
	{
		if (is_null($this->_accountsCount)) {
			$this->_accountsCount = self::getAccounts()->count();
		}
		return $this->_accountsCount;
	}

	public function getInvoices()
	{
		return $this->hasMany(Invoice::className(), ['user_id' => 'id'])
				->onCondition(['status' => Invoice::STATUS_SUCCESS]);
	}

	public function getMonthInvoices()
	{
		return $this->hasMany(Invoice::className(), ['user_id' => 'id'])
				->andOnCondition(['and', ['status' => Invoice::STATUS_SUCCESS], ['>=', 'date',
						new Expression("(NOW() - '1 month'::interval)")]]);
//				->andOnCondition('mi.date >= DATE_SUB(NOW(), INTERVAL 1 MONTH)');
	}

	public function getMailLowerCase()
	{
		return mb_strtolower($this->mail);
	}

	public function getIsAdmin()
	{
		return $this->role === self::ROLE_ADMIN;
	}

	/**
	 * @inheritdoc
	 * @return Users
	 */
	public static function findIdentity($id)
	{
		return self::findOne($id);
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentityByAccessToken($token, $type = null)
	{
		return self::find()->where(['access_token' => $token])->one();
	}

	/**
	 * @inheritdoc
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @inheritdoc
	 */
	public function getAuthKey()
	{
		if (empty($this->access_token)) {
			$this->access_token = Yii::$app->security->generateRandomString(50);
			$this->save();
		}
		return $this->access_token;
	}

	/**
	 * @inheritdoc
	 */
	public function validateAuthKey($authKey)
	{
		return $this->access_token === $authKey;
	}

	/**
	 * Finds user by username
	 *
	 * @param $mail
	 *
	 * @return null|Users
	 */
	public static function findByMail($mail)
	{
		return self::find()->where(['lower(mail)' => mb_strtolower($mail)])->one();
	}

	/**
	 * Validates password
	 *
	 * @param  string $password password to validate
	 *
	 * @return boolean if password provided is valid for current user
	 */
	public function validatePassword($password)
	{
		return (trim($this->password) === trim(md5($password)));
	}

	/**
	 * @param $login
	 *
	 * @return \app\models\Users
	 */
	public static function getUserByInstagramLogin($login)
	{
		$row = Yii::$app->db->createCommand(sprintf(
			"SELECT u.* FROM accounts a
			LEFT JOIN users u ON a.user_id = u.id
			WHERE a.login = '%s';",
			$login
		))->queryOne();

		return $row;
	}

	public static function getUserBalance($id)
	{
		$user = self::findOne($id);
		if ($user === null || $user->balance < 0) {
			return 0;
		} else {
			return $user->balance;
		}
	}

	public static function subUserBalance($id)
	{
		return Yii::$app->db->createCommand(sprintf(
			"UPDATE users SET balance = balance - 60 WHERE id = '%d';",
			$id
		))->execute();

	}

	public static function addUserBalance($id, $days, $system)
	{
		$bf = new BalanceFlow([
			'user_id' => $id,
			'value'=> $days * 86400,
			'description' => 'Пополнение через '.$system,
			'type' => 'add'
		]);
		$bf->save();
		Yii::$app->db->createCommand(sprintf(
				"UPDATE users SET is_payed = 1 WHERE id = '%d'",
				$id
		))->execute();

		$user = Users::findOne($id);

		if (!is_null($user)) {
			Mail::deleteAll([
				'mail' => $user->mail,
				'type' => 'lowbalance'
			]);
		}

		return Yii::$app->db->createCommand(sprintf(
			"UPDATE users SET balance = balance + '%d' WHERE id = '%d'",
			$days * 86400,
			$id
		))->execute();
	}

	public static function hasRunnedAccount($id){
		/** @var Account[] $accounts */
		$accounts = Account::find()->where(['user_id' => $id])->all();
		$account_statuses = Account::getStatusesByInstagramIdsList(ArrayHelper::getColumn($accounts, 'instagram_id'));
		if ($account_statuses === null) {
			$account_statuses = [];
		}
		foreach ($accounts as $index=>$account) {
			$statusRaw = ArrayHelper::getValue($account_statuses, intval($account->instagram_id), false);
			if ($statusRaw === true){
				return true;
			}
		}
		return false;
	}

	public static function locationInfo($purpose = "location", $deep_detect = true, $ip = null)
	{
		$output = null;
		if (filter_var($ip, FILTER_VALIDATE_IP) === false) {
			$ip = $_SERVER["REMOTE_ADDR"];
			if ($deep_detect) {
				if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP)) {
					$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
				}
				if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP)) {
					$ip = $_SERVER['HTTP_CLIENT_IP'];
				}
			}
		}
		$purpose    = str_replace(["name", "\n", "\t", " ", "-", "_"], null, strtolower(trim($purpose)));
		$support    = ["country", "countrycode", "state", "region", "city", "location", "address", "continentcode"];
		$continents = [
			"AF" => "Africa",
			"AN" => "Antarctica",
			"AS" => "Asia",
			"EU" => "Europe",
			"OC" => "Australia (Oceania)",
			"NA" => "North America",
			"SA" => "South America",
		];
		if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
			$ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));
			if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
				switch ($purpose) {
					case "location":
						$output = [
							"city"           => @$ipdat->geoplugin_city,
							"state"          => @$ipdat->geoplugin_regionName,
							"country"        => @$ipdat->geoplugin_countryName,
							"country_code"   => @$ipdat->geoplugin_countryCode,
							"continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
							"continent_code" => @$ipdat->geoplugin_continentCode,
						];
						break;
					case "address":
						$address = [$ipdat->geoplugin_countryName];
						if (@strlen($ipdat->geoplugin_regionName) >= 1) {
							$address[] = $ipdat->geoplugin_regionName;
						}
						if (@strlen($ipdat->geoplugin_city) >= 1) {
							$address[] = $ipdat->geoplugin_city;
						}
						$output = implode(", ", array_reverse($address));
						break;
					case 'continentcode':
						$output = @$ipdat->geoplugin_continentCode;
						break;
					case "city":
						$output = @$ipdat->geoplugin_city;
						break;
					case "state":
						$output = @$ipdat->geoplugin_regionName;
						break;
					case "region":
						$output = @$ipdat->geoplugin_regionName;
						break;
					case "country":
						$output = @$ipdat->geoplugin_countryName;
						break;
					case "countrycode":
						$output = @$ipdat->geoplugin_countryCode;
						break;
				}
			}
		}
		return $output;
	}

	public function getInfo()
	{
		return $this->hasOne(UsersInfo::className(), ['user_id' => 'id']);
	}

	public function getRegisterIP()
	{
		return long2ip($this->reg_ip);
	}

	public static function retrieveInfo($param, $default)
	{
		if (Yii::$app->getModule('user') === null) {
			return $default;
		}
		return Yii::$app->user->$param;
	}

	public static function retrieveEmail()
	{
		$data = self::retrieveInfo('identity', null);

		if (!empty($data)) {
			return \Yii::$app->user->identity->mail;
		} else {
			return \Yii::$app->params['adminEmail'];
		}
	}

	/*
	 * Методы для модуля партнерки
	 */
	public function getPartner()
	{
		return self::hasOne(Partner::className(), ['user_id' => 'id']);
	}

	public function getIsPartner()
	{
		return !is_null($this->partner);
	}

	public function getReferral()
	{
		return self::hasOne(Referal::className(), ['user_id' => 'id']);
	}

	public function getIsReferral()
	{
		return !is_null($this->referral);
	}

	public function getManager()
	{
		return self::hasOne(Manager::className(), ['user_id' => 'id']);
	}

	public function getIsManager()
	{
		return !is_null($this->manager);
	}

	public function addBalance($amount)
	{

		$day_price = 0.7 * 24;

		$days = $amount / $day_price;

		self::addUserBalance($this->id, $days, 'вывод партнёрки');
	}

	public function getEmailAddress()
	{
		return $this->mail;
	}

	public function getDaysAfterRegistration()
	{
		// Здесь должно вычисляться количество дней, прошедших после регистрации
		return (int) floor((time() - strtotime($this->regdate)) / 86400);
	}

	/**
	 *
	 */
	public function getHints()
	{
		$hints = UserHints::findOne(['user_id' => $this->id]);
		if (is_null($hints)) {
			$hints = new UserHints([
				'user_id' => $this->id
			]);
			$hints->save();
		}
		return $hints;
	}

	public static function countActive()
	{
		$result = Yii::$app->db->createCommand(
			'SELECT count(*) FROM (SELECT user_id FROM accounts WHERE monitoring_status = \'work\' GROUP BY user_id) s'
		)->queryOne(\PDO::FETCH_NUM);
		return (int) $result[0];
	}
}
