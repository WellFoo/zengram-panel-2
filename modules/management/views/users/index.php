<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $users
 * @var \app\modules\management\models\UsersSearch $searchModel
 */
use app\modules\management\models\UniSenderList;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = 'Клиенты';
?>
<style>
	.well .progress {
		height: 10px;
		margin-bottom: -10px;
		background-color: #E6E6E6;
	}
</style>
<div class="users" style="margin-bottom: 50px;">
	<h2><?= $this->title ?></h2>

	<form class="well" id="export-form" data-pjax="0">
		<h4>Выгрузка в <?= Html::dropDownList(
				'service_type',
				null,
				['unisender' => 'Unisender', 'pechkin' => 'Pechkin'],
				[
					'class' => 'form-control',
					'onchange' => '$(".unisender").prop("disabled", $(this).val() === "pechkin");'
				]
			) ?></h4>
		<div class="form-group">
			<label class="control-label">Выберите существующий список</label>
			<?= Html::dropDownList(
				'uni_list_id',
				null,
				ArrayHelper::map(UniSenderList::findAll(true), 'id', 'title'),
				[
					'class' => 'form-control unisender',
					'prompt' => 'Новый',
					'onchange' => '$("#new_list_title").prop("disabled", !!$(this).val());'
				]
			) ?>
		</div>
		<div class="form-group">
			<label class="control-label">Или введите название нового</label>
			<?= Html::input('text', 'list_title', '', [
				'class' => 'form-control',
				'placeholder' => 'Новый список',
				'id' => 'new_list_title'
			]) ?>
		</div>
		<div class="form-group">
			<div class="checkbox">
				<label>
					<?= Html::checkbox('tokens') ?>
					Генерировать ключи для автоматического входа
				</label>
			</div>
		</div>
		<?= Html::submitButton('Вперед', ['class' => 'btn btn-success']) ?>
		<div class="progress" style="display: none;">
			<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
				<span class="sr-only">40% Complete (success)</span>
			</div>
		</div>
	</form>
	<?php \yii\widgets\Pjax::begin(['timeout' => false
	]); ?>
	<?= \yii\grid\GridView::widget([
		'dataProvider' => $users,
		'filterModel' => $searchModel,
		'columns' => [
			'mail',
			'regdate:date',
			[
				'attribute' => 'is_payed',
				'format' => 'boolean',
				'filter' => Html::activeDropDownList(
					$searchModel,
					'is_payed',
					['1' => 'Да', '0' => 'Нет'],
					['prompt' => '', 'class' => 'form-control']
				)
			],
			[
				'attribute' => 'reg_ip',
				'format' => 'raw',
				'label' => 'Main Ip',
				'filter' => 'Есть добавленные проекты?<br>'.Html::activeDropDownList(
						$searchModel,
						'haveProjects',
						['1' => 'Да', '0' => 'Нет'],
						['prompt' => '', 'class' => 'form-control']
					),
				'value' => function($item){
					return long2ip($item->reg_ip);
				}
			],[
				'attribute' => 'started',
				'format' => 'boolean',
				'label' => 'Запускал проекты',
				'filter' => Html::activeDropDownList(
					$searchModel,
					'started',
					['1' => 'Да', '0' => 'Нет'],
					['prompt' => '', 'class' => 'form-control']
				),
				'value' => function($item){
					return $item->isStarted;
				}
			],[
				'attribute' => 'lastAction.date',
				'format' => 'date',
				'label' => 'Активность',
				'filter' => 'Активен последние 7 дней?<br>'.Html::activeDropDownList(
						$searchModel,
						'active',
						['1' => 'Да', '0' => 'Нет'],
						['prompt' => '', 'class' => 'form-control']
					)
			], [
				'attribute' => 'is_service',
				'format' => 'boolean',
				'filter' => Html::activeDropDownList(
					$searchModel,
					'is_service',
					['1' => 'Да', '0' => 'Нет'],
					['prompt' => '', 'class' => 'form-control']
				)
			], [
				'attribute' => 'haveBalance',
				'format' => 'boolean',
				'label' => 'Нулевой баланс',
				'filter' => Html::activeDropDownList(
					$searchModel,
					'noBalance',
					['1' => 'Да', '0' => 'Нет'],
					['prompt' => '', 'class' => 'form-control']
				),
				'value' => function($item){
					return $item->balance <= 0;
				}
			],[
				'attribute' => 'tryPay',
				'format' => 'boolean',
				'label' => 'Был переход на страницу оплат',
				'filter' => Html::activeDropDownList(
					$searchModel,
					'tryPay',
					['1' => 'Да', '0' => 'Нет'],
					['prompt' => '', 'class' => 'form-control']
				),
				'value' => function($item){
					return $item->invoices === null;
				}
			],
		]
	]); ?>
	<?php \yii\widgets\Pjax::end(); ?>
</div>
<style>
	table tr.filters td{
		vertical-align: bottom;
	}
</style>

<script language="JavaScript">
	jQuery(function($) {
		'use strict';

		var $form = $('#export-form'),
			$progress = $form.find('.progress-bar');

		$form.on('submit', function() {
			startExport();
			return false;
		});

		function startExport()
		{
			$progress.attr('aria-valuenow', 0).css('width', '0%');
			$progress.parent().show();

			var xhr = new $.ajaxSettings.xhr();
			xhr.previousLength = 0;
			xhr.onreadystatechange = updateProgress;

			$.ajax({
				type: 'POST',
				url: window.location.href,
				data: $form.serialize(),
				xhr: function() {return xhr;},
				complete: function() {
					$form.find('input,select,button')
						.prop('disabled', false)
						.removeClass('disabled');
				}
			});

			$form.find('input,select,button')
				.prop('disabled', true)
				.addClass('disabled');
		}

		function updateProgress(e)
		{
			var xhr = e.target;
			if (xhr.readyState != 3) {
				return;
			}

			var responseLength = xhr.responseText.length;
			var response = xhr.responseText.substring(xhr.previousLength);
			xhr.previousLength = responseLength;

			var data = JSON.parse(response);

			var complete = data.complete * 100 / data.count;
			$progress.attr('aria-valuenow', complete)
				.css('width', complete + '%');
		}
	});
</script>