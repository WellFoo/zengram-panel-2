<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "actions_log".
 *
 * @property Users $user
 * @property ActionsSliders $image
 * @property boolean $deleted
 * @property integer $id
 * @property integer $instagram_id
 * @property integer $user_id
 * @property integer $image_id
 * @property integer $cost
 * @property string $media_code
 */
class ActionsLog extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'actions_log';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['instagram_id','user_id', 'image_id', 'media_code'], 'required'],
			[['instagram_id','user_id', 'image_id', 'cost'], 'integer'],
			[['deleted'], 'boolean'],
			[['media_code'], 'string', 'max' => 50],
			[['added'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'           => Yii::t('app', 'ID'),
			'instagram_id' => Yii::t('app', 'Instagram ID'),
			'image_id'     => Yii::t('app', 'Image ID'),
			'media_code'     => Yii::t('app', 'Media Code'),
			'added'        => Yii::t('app', 'Added'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getImage(){
		return $this->hasOne(ActionsSliders::className(), ['id' => 'image_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser()
	{
		return $this->hasOne(Users::className(), ['id' => 'user_id']);
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getAccount()
	{
		return $this->hasOne(Account::className(), ['instagram_id' => 'instagram_id']);
	}
}
