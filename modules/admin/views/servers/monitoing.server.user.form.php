<?php
/**
 * @var $this \yii\web\View
 * @var $server SnifferServers
 */

use app\models\SnifferMonitoringParameters;
use app\models\SnifferServers;
use app\models\SnifferUsers;
use yii\bootstrap\Html;

$users = SnifferUsers::getUsers();

echo Html::dropDownList('user', $server->user_id, $users, [
	'class' => 'form-control',
	'id' => 'server-user',
	'data' => [
		'id' => $server->id
	],
]);

$notifications = SnifferMonitoringParameters::getNotificationsList();

?>

<table>
	<thead>
		<tr>
			<th>Параметр</th>
			<th>Оператор</th>
			<th>Значение</th>
			<th>Текущее</th>

			<?php foreach ($notifications as $notification => $description) {	?>

			<th class="notification">
				<img
					src="/images/notification/<?= $notification ?>.png"
					alt=""
					title="<?= $description ?>"
				>
			</th>

			<?php }	?>

			<th></th>
		</tr>
	</thead>
	<tbody>

	<?php

	$parameters = SnifferMonitoringParameters::find()
		->where([
			'user_id' => $server->user_id,
			'server_id' => $server->id
		])
		->orderBy(['id' => SORT_ASC])
		->all();



	/** @var SnifferMonitoringParameters $parameter */
	foreach ($parameters as $parameter) { ?>
		<tr data-id="<?= $parameter->id ?>" class="existing-parameters">
			<td><?= Html::dropDownList(
					'parameter',
					$parameter->parameter,
					SnifferMonitoringParameters::getParametersList(),
					[
						'class' => 'form-control'
				]) ?></td>
			<td><?= Html::dropDownList(
					'operator',
					$parameter->operator,
					SnifferMonitoringParameters::getOperatorDescriptions(),
					[
					'class' => 'form-control'
				]) ?></td>
			<td><?= Html::input('text', 'value', $parameter->value, [
				'class' => 'form-control'
				]) ?></td>
			<td><?= $server->getValue($parameter->parameter) ?></td>

			<?php

			foreach ($notifications as $notification => $description) {	?>

				<td class="notification">
					<?= Html::checkbox($notification, $parameter->$notification) ?>
				</td>

			<?php }	?>

			<td class="notification">

				<?= Html::a(
					'<span class="glyphicon glyphicon-remove text-danger"></span>',
					'#',
					[
						'class' => 'remove-parameter',
						'data' => [
							'id' => $parameter->id
						]
					]
				) ?>

			</td>

		</tr>


	<?php } ?>

		<tr id="parameter-form">
			<td>

				<?= Html::dropDownList(
					'parameter',
					'',
					SnifferMonitoringParameters::getParametersList(),
					[
						'class' => 'form-control'
					]) ?></td>
			<td><?= Html::dropDownList(
					'operator',
					'',
					SnifferMonitoringParameters::getOperatorDescriptions(),
					[
						'class' => 'form-control'
					]) ?></td>
			<td><?= Html::input('text', 'value', '', [
					'class' => 'form-control'
				]) ?></td>

			<td></td>

			<?php

			foreach ($notifications as $notification => $description) {	?>

				<td class="notification">
					<?= Html::checkbox($notification, false) ?>
				</td>

			<?php }	?>

			<td class="notification">

				<?= Html::a(
					'<span class="glyphicon glyphicon-plus text-success"></span>',
					'#',
					[
						'class' => 'add-user-param',
						'data' => [
							'id' => $server->id
						]
					]
				) ?>
			</td>
		</tr>
	</tbody>
</table>

