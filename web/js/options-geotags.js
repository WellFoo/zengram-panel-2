'use strict';

// Togglers block
jQuery(function($){
	var $toggler = $('#search-by-toggler'),
		$globalTogglers = $('.projectSearchBy'),
		$placesPanel = $('#places-panel'),
		$regionsPanel = $('#regions-panel'),
		manageUrl = $('#targeting-panel').data('url');

	$('.panel-sub > .panel-header .toggle input').on('change', function() {
		$(this).parents('.panel-sub').first().toggleClass('disable', !this.checked);
	});

	$toggler.find('input').change(function(ev) {
		if ($(this).parent().hasClass('disabled')) {
			ev.preventDefault();
			return false;
		}
		$.post(
			manageUrl,
			{
				newValue: this.value,
				global:   false
			}
		);

		$placesPanel.hide('fast');
		$regionsPanel.hide('fast');
		switch (this.value) {
			case '1':
				$placesPanel.show('fast');
				break;
			case '3':
				$regionsPanel.show('fast');
				break;
		}

		$toggler.find('label')
			.removeClass('btn-success')
			.addClass('btn-default');
		$(this).closest('label').addClass('btn-success');
	});

	var $searchToggle = $('#search-by-toggle');
	var searchManageUrl = $searchToggle.data('url');
	$searchToggle.find('input').on('change.search', searchChange);

	$globalTogglers.parent('div').bind('.toggle click', function() {
		var val = $(this).find('.projectSearchBy').val();
		$.post(
			manageUrl,
			{
				newValue: val,
				global:   true
			},
			function(response) {
				$toggler.find('label').toggleClass('disabled', (Number(response.value) & 1) === 0);
				$globalTogglers.each(function() {
					$(this).prop('checked', (this.value == response.value)).change();
				});
			}
		);
	});

	function searchChange()
	{
		var $inputs = $searchToggle.find('input');
		$inputs.off('change.search');

		if (this.value == 1) {
			$searchToggle.find('#options-search-hashtag').prop('checked', !this.checked).change();
		} else {
			$searchToggle.find('#options-search-place').prop('checked', !this.checked).change();
		}

		$inputs.on('change.search', searchChange);

		var newVal = $searchToggle.find('input:checked').val();

		var byPlace = (newVal == 1);
		$('.set-panel.panel-hash')
			.toggleClass('active', !byPlace)
			.toggleClass('disable', byPlace);
		$('.set-panel.panel-city')
			.toggleClass('active', byPlace)
			.toggleClass('disable', !byPlace);

		if (byPlace && $toggler.length) {
			newVal = $toggler.find('input:checked').val();
		}

		$.post(
			searchManageUrl,
			{newValue: newVal}
		);
	}
});

// Targeting block
jQuery(document).ready(function($){
	var $settingsModal = $('#settings-modal'),
		$competitorsList = $('#competitors-list'),
		$hashtagsList = $('#hashtags-list'),
		$placesList = $('#places-list'),
		$regionsTree = $('#regions-list');

	var competitorsUrl = $competitorsList.data('url'),
		hashtagsUrl = $hashtagsList.data('url'),
		placesUrl = $placesList.data('url'),
		regionsUrl = $regionsTree.data('url');

	var competitorsData = new can.List([]);
	var hashtagsData = new can.List([]);
	var placesData = new can.List([]);
	var regionsData = new can.List(JSON.parse($('#regions-initial-data').text() || '{}'));

	var map, cluster, circleEdit, geotagsControl;

	var preventPlaceSwitch = false;

	var circle = new google.maps.Circle({
		editable:     false,
		draggable:    true,
		cursor:       'move',
		fillColor:    '#5F84D0',
		fillOpacity:  .5,
		strokeColor:  '#DB1D1D',
		strokeOpacity: .7,
		strokeWeight:  2
	});
	circle.addListener('dragstart', circleDragStart);
	circle.addListener('drag', circleDrag);
	circle.addListener('dragend', circleDragEnd);

	var activePlace;

	var useGeotags = Boolean($('#geotags-list-tpl').length);

	var CompetitorsList = can.Control.extend({
		init: function(/*el, op*/) {
			this.element.html(can.view('competitors-list-template', this.options));
		},
		'.delete click': function (el) {
			if (!confirm(window.Zengram.t('Are you sure want to remove this account?'))) {
				return;
			}
			var item  = el.parents('div.balloon').data('item');
			runAjax(competitorsUrl, {
				action: 'delete',
				competitor: {
					id: item.id
				}
			}, this);
		},
		'input keypress': function(el, event){
			if (event.keyCode === 13) {
				competitorsAddClick(el, this);
				return false;
			}
		},
		'button click': function(el) {
			competitorsAddClick(el, this);
		},
		'input hidden.bs.popover': function(el) {
			$(el).popover('destroy');
		}
	});

	var HashtagsList = can.Control.extend({
		init: function(/*el, op*/) {
			this.element.html(can.view('hashtags-list-template', this.options));
		},
		'.delete click': function (el) {
			var item  = el.parents('div.balloon').data('item');
			runAjax(hashtagsUrl, {
				action: 'delete',
				hashtag: {
					id: item.id
				}
			}, this);
		},
		'input keypress': function(el, event){
			if (event.keyCode === 13) {
				hashtagAddClick(el, this);
				return false;
			}
		},
		'button click': function(el) {
			hashtagAddClick(el, this);
		},
		'input hidden.bs.popover': function(el) {
			$(el).popover('destroy');
		}
	});

	var PlacesList = can.Control.extend({
		init: function(el/*, op*/) {
			el.html(can.view('places-list-template', this.options));

			var control = this;
			var $input = el.find('input');
			$input.typeahead({
				minLength: 2,
				//delay: 1,
				matcher: function() { return true; },
				displayText: function(item) {
					return item === undefined ? '' : item.fullName;
				},
				source: geocoderDataSource,
				afterSelect: function(place) {
					$input.data('place', place);
					addMap(place, $input, control);
				}
			});
		},
		'.delete click': function (el, evt) {
			evt.stopPropagation();
			//if (!confirm(window.Zengram.t('Are you sure want to remove this city?'))) {
			//	return;
			//}
			var item  = el.parents('div.balloon').data('item');
			runAjax(placesUrl, {
				action: 'delete',
				place: {
					id: item.id
				}
			}, this);
		},
		'.balloon click': function (el, evt) {
			evt.stopPropagation();
			if (geotagsControl.options.pending() || preventPlaceSwitch) {
				return;
			}
			if ($('#option-use-geotags').is(':checked')) {
				var item = el.data('item');
				setPlaceActive(item);
				showGeotags(false);
			}
		},
		'input hidden.bs.popover': function(el) {
			$(el).popover('destroy');
		},
		'#option-use-geotags click': function ($el, evt) {
			evt.stopPropagation();
			var state = $el.is(':checked');
			if (state && !placesData.length) {
				$el.prop('checked', false);
				var $e = this.element.find('.error-wrapper .error');
				$e.text(Zengram.t('Add a city first'));
				$e.show('fast');
				setTimeout(hideError.bind($e), 3000);
				return;
			}
			$('#tergeting-panel-container').toggleClass('extended', state);
			var $panel = $el.parents('.panel-geo').parent();
			$panel.toggleClass('col-sm-8', state);
			$panel.toggleClass('col-sm-4', !state);
			if (state && map !== undefined) {
				google.maps.event.trigger(map, 'resize');
				adjustMap(geotagsControl);
			}
			$.post(
				'/site/options/' + $el.data('account'), {
					'option': $el.data('option'),
					'value': state ? '1' : '0'
				}, function (response) {
					if (response.status !== 'ok') {
						$el.prop('checked', !state);
					}
				}
			);
			if (state && $el.data('show-hints')) {
				showHints();
			}
		}
	});

	var RegionsTree = can.Control.extend({
		init: function(el) {
			el.html(can.view('regions-list-template', this.options));
			updateRegionsTree(el);
		},
		'a.toggler click': function(el) {
			var data = el.parent('li').data('this');
			data.attr('collapsed', !data.collapsed);
		},
		'input change': function(el) {
			var data = el.parents('li').first().data('this');
			data.attr('selected', el.prop('checked'));
			disableSubtree(data, el.prop('checked'));
			updateRegionsTree(this.element);
			$.post(
				regionsUrl,
				{
					action: el.prop('checked') ? 'append' : 'remove',
					id: el.val()
				}
			);
		}
	});

	function setPlaceActive(activeItem)
	{
		activePlace = activeItem;
		placesData.each(function(item) {
			item.attr('active', activeItem.attr('id') == item.attr('id'));
		});
		initMap();
	}


	function disableSubtree(data, state)
	{
		if (!data.items || !data.items.length) {
			return;
		}
		var sub;
		for (var i = 0; i < data.items.length; i++) {
			sub = data.items[i];
			if (state && sub.selected) {
				sub.attr('selected', false);
			}
			sub.attr('disabled', state);
			disableSubtree(sub, state);
		}
	}

	can.Mustache.registerHelper('prune_context', function(options) {
		return options.fn(new can.view.Scope(options.context));
	});

	var competitorsControl = new CompetitorsList('#competitors-list', {
		items: competitorsData,
		pending: new can.compute(false)
	});
	var hashtagsControl = new HashtagsList('#hashtags-list', {
		items: hashtagsData,
		pending: new can.compute(false)
	});
	var placesControl = new PlacesList('#places-list', {
		items: placesData,
		pending: new can.compute(false)
	});

	if ($regionsTree.length) {
		new RegionsTree('#regions-list', {items: regionsData});
	}

	$('.show-all a').on('click', function(){
		var data = $(this).data();
		showSettingsModal(data.type, data.caption);
		return false;
	});

	runAjax(competitorsUrl, {action: 'list'}, competitorsControl);
	runAjax(hashtagsUrl, {action: 'list'}, hashtagsControl);
	runAjax(placesUrl, {action: 'list'}, placesControl);

	function hashtagAddClick(el, control)
	{
		var $input = el.parents('.input-group').first().find('input');

		if (!$input.val()) {
			$input.focus();
		}

		runAjax(hashtagsUrl, {
			action: 'create',
			hashtag: {
				name: $input.val()
			}
		}, control);

		return false;
	}

	function competitorsAddClick(el, control)
	{
		var $input = el.parents('.input-group').first().find('input');

		if (!$input.val()) {
			$input.focus();
		}

		runAjax(competitorsUrl, {
			action: 'create',
			competitor: {
				name: $input.val()
			}
		}, control);

		return false;
	}

	function addMap(place, $input, control)
	{
		if (place) {
			placeChanged(place, function(place_func){
				runAjax(placesUrl, place_func, control);
			});
			$input.val('');
		}
		return false;
	}

	function runAjax(url, data, control)
	{
		control.options.pending(true);
		$.post(
			url,
			data,
			ajaxCallback.bind(control),
			'JSON'
		).always(function() {
			control.options.pending(false);
		});
	}

	function ajaxCallback(response)
	{
		var list, $el = $(this.element).find('input');

		switch (response.type) {
			case 'places':
				list = placesData;
				break;

			case 'hashtags':
				list = hashtagsData;
				break;

			case 'competitors':
				list = competitorsData;
				break;
		}

		if (response.error) {
			this.options.pending(false);
			var $e = this.element.find('.error-wrapper .error');
			$e.text(response.error);
			$e.show('fast');
			setTimeout(hideError.bind($e), 3000);
			return;
		}

		var i;
		switch (response.action) {
			case 'list':
				for (i = 0; i < response.data.length; i++) {
					list.push(response.data[i]);
				}
				if (useGeotags && response.type === 'places' && response.data.length) {
					setPlaceActive(list[0]);
					showGeotags(false);
				}
				break;

			case 'create':
				if (response.data instanceof Array) {
					for (i = 0; i < response.data.length; i++) {
						list.push(response.data[i]);
					}
				} else {
					list.push(response.data);
					if (useGeotags && response.type === 'places') {
						setPlaceActive(list[list.length - 1]);
						showGeotags(true);
					}
				}
				$el.val('');
				break;

			case 'delete':
				var index = -1;
				list.each(function(el, i){
					if (el.id == response.id) {
						index = i;
					}
				});
				if (index < 0) {
					break;
				}
				list.splice(index, 1);
				if (useGeotags && response.type === 'places') {
					if (list.length) {
						setPlaceActive(list[0]);
						showGeotags(false);
					} else {
						$('#option-use-geotags').click();
					}
				}
				break;
		}
	}

	function hideError()
	{
		this.hide('fast');
	}

	function showSettingsModal(type, caption)
	{
		var $dataContainer = $settingsModal.find('.modal-inner-html'),
			listClass, listData, control;

		switch (type) {
			case 'geo':
				if ($('#search-by-toggler').find('input:checked').val() > 1) {
					listClass = RegionsTree;
					listData  = regionsData;
				} else {
					listClass = PlacesList;
					listData  = placesData;
				}
				break;

			case 'hashtags':
				listClass = HashtagsList;
				listData  = hashtagsData;
				break;

			case 'accounts':
				listClass = CompetitorsList;
				listData  = competitorsData;
				break;
		}

		$settingsModal.find('.modal-title')
			.text(caption);

		$dataContainer.html('');
		control = new listClass($dataContainer, {items: listData, pending: new can.compute(false)});

		$settingsModal.modal('show');
		$settingsModal.on('hidden.bs.modal', function() {
			control.destroy();
		});
	}

	function updateRegionsTree($el)
	{
		$el.find('input')
			.each(function() {
				$(this).prop('indeterminate', false);
			}).each(function() {
				if (!this.checked) {
					return;
				}
				$(this).parentsUntil($el, 'ul').slice(0, -1)
					.parent().prev('li')
					.children('label')
					.find('input')
					.prop('indeterminate', true);
			});
	}

	var mapInitialized = false;
	function initMap()
	{
		if (mapInitialized) {
			return;
		}

		geotagsControl = new GeotagsControl('#geotag-manager', {
			items:        geotagsData,
			pending:      can.compute(false),
			circleFilter: can.compute(true),
			autocompleteShow: can.compute(false),
			autocompletePending: can.compute(false),
			autocompleteItems: autocompleteData
		});

		mapInitialized = true;
	}

	var GeotagsControl = can.Control.extend({
		init: initView,
		'.balloon click': balloonClick,
		'.geotags-circle-filter click': circleFilterSwitch,
		'.geotags-zoom-out click': circleZoomOut,
		'.save-geotags click': saveTags,
		'.geotags-additional-search submit': additionalSearch,
		'.geotags-additional-search input keyup': additionalSearchKeyUp,
		'.geotags-additional-search input focus': additionalSearchFocus,
		'.geotags-additional-search input blur': additionalSearchBlur,
		'.locate click': locateClick,
		'.list-group-item click': autocompleteItemClick
	});

	function initView ($el, options)
	{
		$el.html(can.view('geotags-list-tpl', options));
		$el.find('.geotag-search input').val(activePlace.name);

		map = new google.maps.Map($el.find('.geotags-map')[0], {
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			mapTypeControl: false,
			streetViewControl: false,
			zoom: 8
		});

		google.maps.event.trigger(map, 'resize');
		adjustMap(this);

		cluster = new MarkerClusterer(map, [], {
			imagePath: '/img/markerclusterer/m',
			maxZoom: 15,
			minimumClusterSize: 5
		});
	}

	function adjustMap(controller)
	{
		map.setCenter(new google.maps.LatLng(activePlace.center.lat, activePlace.center.lng));
		map.fitBounds(new google.maps.LatLngBounds(
			new google.maps.LatLng(activePlace.sw.lat, activePlace.sw.lng),
			new google.maps.LatLng(activePlace.ne.lat, activePlace.ne.lng)
		));

		circle.setCenter(new google.maps.LatLng(activePlace.circle.center.lat, activePlace.circle.center.lng));
		circle.setRadius(activePlace.circle.radius);
		circle.setMap(map);
		circle.setVisible(activePlace.circle.enabled);
		drawCircleEdit(map, new google.maps.LatLng(
			activePlace.circle.center.lat,
			circle.getBounds().getSouthWest().lng()
		), activePlace.circle.enabled);

		centerMap();

		controller.options.circleFilter(activePlace.circle.enabled);
	}

	function centerMap()
	{
		map.setCenter(circle.getCenter());
		map.fitBounds(circle.getBounds());
	}

	function getPlaceCenter()
	{
		return {
			lat: activePlace.center.lat,
			lng: activePlace.center.lng
		};
	}

	function balloonClick($el)
	{
		var item = geotagsData.attr($el.data('idx'));
		item.attr('enabled', !item.attr('enabled'));
		switchTag(item);
	}

	function switchTag(item)
	{
		preventPlaceSwitch = true;
		$.post($('#geotags-list-tpl').data('url') + '?place_id=' + activePlace.id, {
				action: 'switch-tag',
				tag_id: item.attr('id'),
				active: item.attr('enabled') ? 1 : 0
			}, function () {
			}
		).always(function () {
			preventPlaceSwitch = false;
		});
	}

	function autocompleteItemClick($el, evt)
	{
		evt.stopPropagation();
		var item = autocompleteData.attr($el.data('idx'));
		item.attr('enabled', !item.attr('enabled'));
		geotagsData.each(function(tag) {
			if (tag.attr('id') === item.attr('id')) {
				tag.attr('enabled', item.attr('enabled'));
				switchTag(tag);
			}
		});
	}

	function circleFilterSwitch()
	{
		geotagsControl.options.circleFilter(!geotagsControl.options.circleFilter());
		circle.setVisible(geotagsControl.options.circleFilter());
		circleEdit.setVisible(geotagsControl.options.circleFilter());
		circleDragEnd();
	}

	function circleZoomOut()
	{
		centerMap();
	}

	var saving = false;
	function saveTags()
	{
		if (saving) {
			return;
		}
		saving = true;
		var data = {
			circle: {
				enabled: geotagsControl.options.circleFilter() ? 1 : 0,
				center:  {
					lat: circle.getCenter().lat(),
					lng: circle.getCenter().lng()
				},
				radius:  circle.getRadius()
			},
			tags: geotagsDataToObject()
		};
		$.post($('#geotags-list-tpl').data('url') + '?place_id=' + activePlace.id, {
				action: 'save',
				data:   data
			}, function () {
				activePlace.circle.enabled = geotagsControl.options.circleFilter();
				activePlace.circle.radius  = circle.getRadius();
				activePlace.circle.center.lat = circle.getCenter().lat();
				activePlace.circle.center.lng = circle.getCenter().lng();
			}
		).always(function() {
			saving = false;
		});
	}

	function additionalSearch()
	{
		if (geotagsControl.options.autocompletePending()) {
			return;
		}
		autocompleteData.splice(0);
	}

	var searchAutocompleteTimeout, searchAutocompleteTextPrev;
	function additionalSearchKeyUp($el)
	{
		if ($el.val() === searchAutocompleteTextPrev) {
			return;
		}
		searchAutocompleteTextPrev = $el.val();

		if ($el.val().length < 3) {
			return;
		}

		clearTimeout(searchAutocompleteTimeout);
		searchAutocompleteTimeout = setTimeout(searchAutocomplete.bind($el), 200);
	}

	function additionalSearchFocus()
	{
		geotagsControl.options.autocompleteShow(autocompleteData.length > 0);
	}

	function additionalSearchBlur($el)
	{
		if (!$el.val().length) {
			autocompleteData.splice(0);
		}
	}

	function searchAutocomplete()
	{
		geotagsControl.options.autocompletePending(true);
		$.post($('#geotags-list-tpl').data('url') + '?place_id=' + activePlace.id, {
			action: 'autocomplete',
			query:  this.val(),
			coords: getPlaceCenter()
		}, function (data) {
			autocompleteData.splice(0);
			if (!data.status) {
				for (var key in data) {
					if (!data.hasOwnProperty(key)) {
						continue;
					}
					var exists = tagExists(data[key].id);
					var tag = getGeotagItem(data[key], !exists);
					if (!exists) {
						geotagsData.unshift(tag);
					}
					autocompleteData.push(tag);
				}
			}
		}).always(function () {
			geotagsControl.options.autocompletePending(false);
		});
	}

	function locateClick($el, evt)
	{
		evt.stopPropagation();
		var marker = geotagsData.attr($el.parents('.balloon').data('idx')).attr('marker');
		map.setCenter(marker.position);
		map.setZoom(17);
		marker.setAnimation(google.maps.Animation.BOUNCE);
		setTimeout(function () {
			marker.setAnimation(null);
		}, 2000);
	}

	function geotagsDataToObject()
	{
		var result = [];
		geotagsData.each(function (item) {
			result.push({
				id:      item.attr('id'),
				enabled: item.attr('enabled') ? 1 : 0
			});
		});
		return result;
	}

	var geotagsData = new can.List([]);

	var autocompleteData = new can.List([]);
	autocompleteData.bind('length', function() {
		var list = this;
		if (list.length) {
			$(window).on('click.geo_autocomplete', function () {
				if (!hintsTour) {
					list.splice(0);
				}
			});
		} else {
			$(window).off('click.geo_autocomplete');
			filterGeotagsList();
		}
		geotagsControl.options.autocompleteShow(list.length > 0);
	});

	function filterGeotagsList()
	{
		var i, disabled = [];
		for (i = geotagsData.length - 1; i >= 0; i--) {
			if (!geotagsData[i].enabled) {
				disabled.push(geotagsData.splice(i, 1)[0]);
			}
		}
		disabled.reverse();
		for (i = 0; i < disabled.length; i++) {
			geotagsData.push(disabled[i]);
		}
	}

	function getGeotagItem(data, renderMarker)
	{
		var marker = drawMarker(data);
		var item = new GeotagItem({
			id:         data.id,
			coords:     data.coords,
			title:      data.title,
			address:    data.address,
			marker:     marker,
			enabled:    data.enabled
		});
		marker.geotag = item;
		if (renderMarker) {
			cluster.addMarker(marker);
		}
		return item;
	}

	function drawMarkers(data)
	{
		for (var key in data) {
			if (!data.hasOwnProperty(key)) {
				continue;
			}
			var item = getGeotagItem(data[key], true);
			geotagsData.push(item);
		}
	}

	function tagExists(id)
	{
		var result = false;
		geotagsData.each(function (item) {
			if (item.attr('id') === id) {
				result = true;
			}
		});
		return result;
	}

	function drawMarker(data)
	{
		var coords = data.coords;
		coords.lat += Math.random() * 0.0001 - 0.0001;
		coords.lng += Math.random() * 0.0001 - 0.0001;
		var marker = new google.maps.Marker({
			position: coords,
			title:    data.title + (data.address ? "\n" + data.address : ''),
			shape:    markerShape
		});
		marker.addListener('click', markerClick);
		return marker;
	}

	function markerClick()
	{
		this.geotag.attr('enabled', !this.geotag.attr('enabled'));
		switchTag(this.geotag);
	}

	var circleDragLatLng;
	var circleEditDragLatLng;
	function circleDragStart(evt)
	{
		circleDragLatLng = evt.latLng;
		circleEditDragLatLng = circleEdit.getPosition();
		map.setOptions({
			scrollwheel: false
		});
	}

	function circleDrag(evt)
	{
		window.requestAnimationFrame(function () {
			circleEdit.setPosition(new google.maps.LatLng(
				circleEditDragLatLng.lat() + evt.latLng.lat() - circleDragLatLng.lat(),
				circleEditDragLatLng.lng() + evt.latLng.lng() - circleDragLatLng.lng()
			));
		});
	}

	function circleDragEnd()
	{
		var enabledItems = [];
		geotagsData.each(function (item) {
			var enabled;
			if (geotagsControl.options.circleFilter()) {
				enabled = circle.contains(item.attr('coords'));
			} else {
				enabled = true;
			}
			item.attr('enabled', enabled);
			if (enabled) {
				enabledItems.push(item.attr('id'));
			}
		});
		preventPlaceSwitch = true;
		$.post($('#geotags-list-tpl').data('url') + '?place_id=' + activePlace.id, {
				action: 'circle-changed',
				active: geotagsControl.options.circleFilter() ? 1 : 0,
				center: {
					lat: circle.getCenter().lat(),
					lng: circle.getCenter().lng()
				},
				radius: circle.getRadius(),
				enabledTags: enabledItems
			}, function (result) {
				if (result.status !== 'ok') {
					return;
				}
				activePlace.attr('circle.center.lat', circle.getCenter().lat());
				activePlace.attr('circle.center.lng', circle.getCenter().lng());
				activePlace.attr('circle.radius', circle.getRadius());
			}
		).always(function () {
			preventPlaceSwitch = false;
		});
		map.setOptions({
			scrollwheel: true
		});
	}

	function drawCircleEdit(mapItem, coords, visible)
	{
		if (circleEdit === undefined) {
			circleEdit = new google.maps.Marker({
				map:       mapItem,
				title:     'drag to resize circle',
				draggable: true,
				zIndex:    10000,
				icon: {
					url: '/img/map_circle_editor.png',
					size: new google.maps.Size(20, 20),
					anchor: new google.maps.Point(10, 10)
				},
				shape: {
					coords: [0, 0, 20],
					type: 'circle'
				}
			});
			circleEdit.addListener('drag', circleEditChanged);
			circleEdit.addListener('dragend', circleDragEnd);
		}
		circleEdit.setPosition(coords);
		circleEdit.setVisible(visible);
	}

	function circleEditChanged()
	{
		window.requestAnimationFrame(function () {
			circle.setRadius(google.maps.geometry.spherical.computeDistanceBetween(
				circle.getCenter(),
				circleEdit.getPosition()
			));
		});
		map.setOptions({
			scrollwheel: false
		});
	}

	function showGeotags(filter)
	{
		geotagsData.splice(0);
		cluster.clearMarkers();
		autocompleteData.splice(0);
		$('.geotags-column input[type=text]').val('');

		adjustMap(geotagsControl);

		geotagsControl.options.pending(true);
		$.getJSON($('#geotags-list-tpl').data('url'), {
			place_id: activePlace.id,
			coords: getPlaceCenter()
		}, function (data) {
			drawMarkers(data);
			if (filter) {
				circleDragEnd();
			}
		}).always(function () {
			geotagsControl.options.pending(false);
			/* TODO сделать по хорошему */
			$(document).find('.geotags-column *[data-toggle="popover"]').popover(
				{
					placement: 'top',
					trigger: 'hover',
					container: 'body'
				}
			);
		});
	}

	var hintsTour = false;
	function showHints()
	{
		var mapPos = $('#geotag-manager').find('.geotags-map').offset();

		var $helperEl = $('<div id="enjoy-hint-helper">');
		$helperEl.css({
			position: 'absolute',
			'border-radius': '5px'
		});
		$helperEl.appendTo('body');

		var eHint = new EnjoyHint({
			skipButton: false,
			onEnd: tourEnd,
			onSkip: tourEnd
		});

		function tourEnd()
		{
			$helperEl.remove();
			autocompleteData.splice(0);
			centerMap();
			hintsTour = false;
			$.post($('#geotags-list-tpl').data('url') + '?place_id=' + activePlace.id, {
				action: 'hint-tour-end'
			}, function(result) {
				if (result.status === 'ok') {
					$('#option-use-geotags').data('show-hints', false);
				}
			});
		}

		var nextBtn = {
			className: 'btn btn-success',
			text: Zengram.t('Next')
		};

		eHint.setScript([
			{
				event: '',
				selector: '#places-list input[type=search]',
				description: Zengram.t('Enter a city name to add it in list'),
				showNext: true,
				showSkip: false,
				nextButton: nextBtn
			}, {
				event: '',
				selector: '#places-list .balloon.balloon-active',
				description: Zengram.t('Or select existing in list'),
				showNext: true,
				showSkip: false,
				nextButton: nextBtn
			}, {
				event: '',
				selector: '#enjoy-hint-helper',
				description: Zengram.t('You can control search circle: drag and resize by marker'),
				shape: 'circle',
				radius: 25,
				showNext: true,
				showSkip: false,
				nextButton: nextBtn,
				onBeforeStart: function() {
					var overlay = getOverlay();
					var pos = overlay.getProjection().fromLatLngToDivPixel(circleEdit.getPosition());
					$helperEl.css({
						left: (mapPos.left + pos.x) + 'px',
						top: (mapPos.top + pos.y - 10) + 'px',
						width: '20px',
						height: '20px'
					})
				}
			}, {
				event: '',
				selector: '#enjoy-hint-helper',
				description: Zengram.t('Toggle marker by click on it'),
				showNext: true,
				showSkip: false,
				nextButton: nextBtn,
				onBeforeStart: function() {
					var marker = getMarker();
					marker.setZIndex(1000);
					map.setCenter(marker.getPosition());
					map.setZoom(17);
					var overlay = getOverlay();
					var pos = overlay.getProjection().fromLatLngToDivPixel(marker.getPosition());
					$helperEl.css({
						left: (mapPos.left + pos.x - 5) + 'px',
						top: (mapPos.top + pos.y - 45) + 'px',
						width: '30px',
						height: '50px'
					})
				}
			}, {
				event: '',
				selector: '#geotag-manager .geotags-items-list .balloon:first',
				description: Zengram.t('Also you can click on tag description'),
				showNext: true,
				showSkip: false,
				nextButton: nextBtn
			}, {
				event: '',
				selector: '#geotag-manager .geotags-additional-search',
				description: Zengram.t('To search another geotags use this window'),
				showNext: true,
				showSkip: false,
				nextButton: nextBtn
			}, {
				event: '',
				selector: '#geotag-manager .geotags-autocomplete',
				description: Zengram.t('You can choose geotags found in dropdown list'),
				showNext: true,
				showSkip: false,
				nextButton: nextBtn,
				onBeforeStart: function() {
					autocompleteData.splice(0);
					var chunk = geotagsData.slice(0, 3);
					chunk.each(function(item) {
						autocompleteData.push(item);
					});
				}
			}
		]);

		setTimeout(function() {
			hintsTour = true;
			eHint.run();
		}, 100);

		function getOverlay()
		{
			var overlay = new google.maps.OverlayView();
			overlay.draw = function(){};
			overlay.setMap(map);
			return overlay;
		}

		function getMarker()
		{
			return geotagsData.attr(0).marker;
		}
	}
});

// Comments block
(function($){
	var commentsRadioList = [10, 20, 50],
		commentsRadioListIndex = parseInt(fromStorage('commentsPerPageIndex', 0)),
		commentsPerPage = commentsRadioList[commentsRadioListIndex],
		currentPage = 0,
		$firmCommentsCheckbox,
		$commentInputHolder,
		$commentIdInput,
		$commentTextInput,
		$commentAddBtn,
		$commentEditBtn,
		$commentsList,
		$commentTemplate,
		$commentsPagination,
		$commentsCountSwitch,
		$commentsShowMoreBtn,
		isSmallScreen,
		commentManageUrl,
		commentsCount;

	function fromStorage(name, def)
	{
		if (typeof window.localStorage !== 'object') {
			return def;
		}
		return window.localStorage.getItem(name) || def;
	}

	function toStorage(name, val)
	{
		if (typeof window.localStorage !== 'object') {
			return;
		}
		window.localStorage.setItem(name, val);
	}

	function commentsShowPage(page)
	{
		var data = {
			action: 'list',
			offset: (page - 1) * commentsPerPage,
			limit: commentsPerPage
		};
		$.post(
			commentManageUrl,
			data,
			commentsListCallback,
			'JSON'
		).always(commentsAjaxCallback);
	}

	function commentsShowMore()
	{
		var data = {
			action: 'list',
			offset: (currentPage++) * 3,
			limit: 3
		};
		$.post(
			commentManageUrl,
			data,
			commentsListCallback,
			'JSON'
		).always(commentsAjaxCallback);
	}

	function setCommentsPerPage(i, val)
	{
		toStorage('commentsPerPageIndex', i);
		commentsPerPage = val;
		$commentsPagination.bs3paginator({
			pages: Math.ceil(commentsCount / commentsPerPage),
			current: 1
		});
		commentsShowPage(1);
	}

	function clearInputs()
	{
		$commentIdInput.val('');
		$commentTextInput.val('');
		$commentTextInput.css('height','34px');
		toggleCommentsMode('add');
	}

	function toggleCommentsMode(mode)
	{
		var editMode = (mode === 'edit');
		$commentAddBtn.toggle(!editMode);
		$commentEditBtn.toggle(editMode);
	}

	function runAjax(data) {
		$.post(
			commentManageUrl,
			data,
			commentsSuccessCallback,
			'JSON'
		).always(commentsAjaxCallback);
		$commentInputHolder.addClass('disable');
	}

	function commentsSuccessCallback(response)
	{
		switch (response.action) {
			case 'create':
				createComment(response.id, response.text, true);
				break;

			case 'update':
				updateComment(response.id, response.text);
				break;

			case 'delete':
				deleteComment(response.id);
				break;

			case 'firm':
				useFirmChanged(response.count);
				break;
		}
	}

	function useFirmChanged(count)
	{
		$firmCommentsCheckbox.parent().removeClass('disable');
		$commentsPagination.bs3paginator({
			pages: Math.ceil(count / commentsPerPage)
		});
		commentsShowPage(1);
	}

	function commentsAjaxCallback()
	{
		$commentIdInput.val('');
		$commentTextInput.val('');
		toggleCommentsMode('add');
		$commentInputHolder.removeClass('disable');
	}

	function createComment(id, text, prepend)
	{
		var $comment = $commentTemplate.clone();
		$comment.data('id', id);
		$comment.attr({
			id: 'comment_' + id,
			style: ''
		});
		$comment.find('.content').text(text);

		if (prepend) {
			$comment.prependTo($commentsList);
			adjustListSize();
		} else {
			$comment.appendTo($commentsList);
		}
	}

	function updateComment(id, text)
	{
		var $comment = $('#comment_' + id);
		$comment.find('.content').text(text);
	}

	function deleteComment(id)
	{
		$('#comment_' + id).hide('fast', function(){
			$(this).remove();
			adjustListSize();
		});
	}

	function adjustListSize() {
		var h = 0;
		$commentsList.children().each(function () {
			h += $(this).outerHeight(true);
		});
		$commentsList.animate({
			height: h + 'px'
		}, 400);
	}

	function commentsListCallback(data)
	{
		$commentsList.css('height', $commentsList.height() + 'px');
		$commentsList.children().each(function(){
			$(this).remove();
		});

		for (var i = 0; i < data.length || i < 10; i++) {
			if (data[i]) {
				createComment(data[i].id, data[i].text);
			}
		}

		adjustListSize();
	}

	$(document).on('ready', function(){
		$firmCommentsCheckbox = $('#firmCommentsCheckbox');
		$commentInputHolder = $('#commentInputHolder');
		$commentIdInput = $commentInputHolder.find('#commentId');
		$commentTextInput = $commentInputHolder.find('#commentText');
		$commentAddBtn = $commentInputHolder.find('#commentAddBtn');
		$commentEditBtn = $commentInputHolder.find('#commentEditBtn');
		$commentsList = $('#commentsList');
		$commentTemplate = $('#commentTemplate');
		$commentsPagination = $('#commentsPagination');
		$commentsCountSwitch= $('#commentsCountSwitch');
		$commentsShowMoreBtn = $('#commentsShowMore');
		commentManageUrl = $commentTextInput.data('url');
		commentsCount = parseInt($commentsPagination.data('count'));
		isSmallScreen = (window.innerWidth < 768);

		$commentTextInput.on('keypress', function(event){
			if (event.keyCode === 13) {
				if ($commentIdInput.val().length) {
					$commentEditBtn.click();
				} else {
					$commentAddBtn.click();
				}
				return false;
			}
		});

		$commentAddBtn.on('click', function() {
			if (!$commentTextInput.val().length) {
				$commentTextInput.focus();
				return false;
			}

			var data = {
				action: 'create',
				comment: {
					text: $commentTextInput.val()
				}
			};

			runAjax(data);
			$commentTextInput.css('height','34px');
			$commentTextInput.focus();
			return false;
		});

		$commentEditBtn.on('click', function() {
			if (!$commentTextInput.val().length) {
				$commentTextInput.focus();
				return false;
			}

			var data = {
				action: 'update',
				comment: {
					text: $commentTextInput.val(),
					id: $commentIdInput.val()
				}
			};

			runAjax(data);
			return false;
		});

		$('#commentClearBtn').on('click', function(){
			clearInputs();
			return false;
		});

		$commentsList.on('click', '.delete', function(event) {
			var $target = $(event.target),
				data = {
					action: 'delete',
					comment: {
						id: $target.parents('.balloon').data('id')
					}
				};

			runAjax(data);
			return false;
		});

		$commentsList.on('click', '.edit', function(event) {
			var $comment = $(event.target).parents('.balloon');
			$commentIdInput.val($comment.data('id'));
			$commentTextInput.val($comment.find('.content').text());
			toggleCommentsMode('edit');
		});

		$firmCommentsCheckbox.on('change', function(){
			$firmCommentsCheckbox.parent().addClass('disable');
			runAjax({
				action: 'switch-firm',
				firm: $firmCommentsCheckbox[0].checked ? 1 : 0
			});
		});

		if (isSmallScreen) {
			$commentsShowMoreBtn.on('click', function(){
				commentsShowMore();
				return false;
			});
			commentsShowMore();
		} else {
			$commentsPagination.bs3paginator({
				callback: commentsShowPage,
				classes: {
					activeBtn: 'success'
				},
				pages: Math.ceil(commentsCount / commentsPerPage),
				current: 1
			});

			$commentsCountSwitch.bs3radio({
				callback: setCommentsPerPage,
				classes: {
					activeBtn: 'success'
				},
				list: commentsRadioList,
				current: commentsRadioListIndex
			});
			commentsShowPage(1);
		}
	});
})(jQuery);

var markerActiveIcon = $.extend(google.maps.Icon, {
	url:    '/img/map_marker_icon.png',
	size:   new google.maps.Size(24, 32),
	origin: new google.maps.Point(24, 0),
	anchor: new google.maps.Point(12, 32)
});
var markerDisabledIcon = $.extend(google.maps.Icon, {
	url:    '/img/map_marker_icon.png',
	size:   new google.maps.Size(24, 32),
	origin: new google.maps.Point(0, 0),
	anchor: new google.maps.Point(12, 32)
});
var markerShape = $.extend(google.maps.MarkerShape, {
	type: 'poly',
	coords: [11, 32, 2, 17, 0, 10, 2, 4, 5, 2, 8, 0, 16, 0, 19, 2, 22, 4, 24, 19, 22, 17, 12, 32]
});

var GeotagItem = can.Map.extend({
	id:         undefined,
	coords:     can.compute(giCoordsCompute),
	title:      undefined,
	address:    undefined,
	marker:     undefined,
	enabled:    can.compute(giEnabledCompute),
	visible:    undefined
});

function giCoordsCompute(value)
{
	if (!arguments.length) {
		return this.coords;
	}
	if (value instanceof google.maps.LatLng) {
		this.coords = value;
	}
	this.coords = new google.maps.LatLng(value.lat, value.lng);
}

function giEnabledCompute(value)
{
	if (!arguments.length) {
		return this.enabled;
	}
	value = Boolean(value);
	if (value) {
		this.marker.setOptions({opacity: 1});
		this.marker.setIcon(markerActiveIcon);
	} else {
		this.marker.setOptions({opacity: .5});
		this.marker.setIcon(markerDisabledIcon);
	}
	this.enabled = value;
}

google.maps.Circle.prototype.contains = function(latLng) {
	if (!this.getBounds().contains(latLng)) {
		return false;
	}
	var distance = google.maps.geometry.spherical.computeDistanceBetween(
		this.getCenter(),
		latLng
	);
	return this.getRadius() >= distance;
};

google.maps.Marker.prototype.geotag = undefined;
