<?php return [
	'Get More Instagram Followers App | Instagram Promotion App' => 'Продвижение в Instagram | Сервис по накрутке инстаграма',
	'Buy Zengram App Free - Get More Real Instagram Followers, Likes and Comments Fast' => 'Продвижение в Instagram | Сервис по накрутке инстаграма',
	'Buy Zengram app free and get more real Instagram followers, likes and comments fast. Know how to get followers and promote your Instagram account with Zengram!'
		=> 'Раскрути себя в Инстаграме вместе с zengram! Уникальный сервис по привлечению целевых подписчиков именно с твоего города! Быстро, удобно, безопасно!',
	'Zen-promo, the finest Instagram follower App helps in fast promotion of your account by following, unfollowing, liking & commenting on your behalf. Signup now!'
	=> 'Раскрути себя в Инстаграме вместе с zengram! Уникальный сервис по привлечению целевых подписчиков именно с твоего города! Быстро, удобно, безопасно!',
	'Buy Real Instagram Followers, Followers On Instagram, Get Instagram Followers, Get Likes on Instagram App, Get More Instagram Followers, How to Get Followers on Instagram, Instagram Followers, Instagram Followers App, More Followers on Instagram, Real Instagram Followers'
		=> 'Купить реальных подписчиков Instagram, подписчики Instagram, Получить подписчиков Instagram, Получить больше подписчиков Instagram, Реальные подписчики Instagram, Как получить подписчиков Instagram',

	'Build up yourself in<br>Instagram' => 'Продвижение и накрутка вашего Инстаграм!',
	'Zengram promotes your Instagram<br/>by liking, commenting,<br/>following and unfollowing<br/>on your behalf'
		=> 'Zengram продвигает ваш Инстаграм: <br/><a href="/instagram">накручивает лайки</a>, комментарии,<br/>подписывается и отписывается',
	'Free trial for {count} days' => 'Бесплатный тестовый период - {count} дня',
	'Free trial for 3 days' => 'Бесплатный тестовый период - 5 дней',
	'First {count} days are free' => 'Попробовать<br>{count} дня бесплатно',
	'First 3 days are free' => 'Попробовать<br>5 дней бесплатно',

	'Details' => 'Подробнее',
	'Our advantages' => 'Наши преимущества',
	'Start your 3-day trial right now completely free of charges!' => 'Первые пять дней бесплатно',
	'Try our service for free! Be convinced, how Zengram profitably differs from what you see before'
		=> 'Испытайте наш сервис абсолютно бесплатно! Убедитесь, как выгодно Zengram отличает от всего, что вы видели.',
	'Get Instagram followers <br class="hidden-xs">from your city' => 'Подписчики в твоем <br class="hidden-xs">городе!',
	'You choose the city from where you want Zengram to attract followers! It’s possible to target multiple locations to expand your reach.'
		=> 'Вы сами выбираете город, в котором Zengram будет привлекать подписчиков. Можно выбрать один или несколько городов.',
	'How To Get More Followers On Instagram? It&#39;s so easy' => 'Очень просто',
	'To start Zengram, it&#39;s enough to choose the city, actions (to like, follow and comment) and run the project.'
		=> 'Для запуска Zengram достаточно выбрать город, действия (лайки, фолловить, комментировать) и запустить проект.',
	'Start and forget' => 'Запусти и забудь!',
	'Zengram works on our server, so a browser is the only thing you need. Once Zengram is launched, you can turn off the device: the service will continue the work and you will still get more Instagram followers.'
		=> 'Zengram работает на сервере. Достаточно один раз запустить Zengram и можно закрыть браузер. Сервис продолжит свою работу.',
	'Perfect safety' => 'Полная безопасность',
	'We work using the secure protocol and we don&#39;t store any user data. It is absolutely safe to work with us!'
		=> 'Мы работаем по защищенному протоколу и не храним у себя данные пользователей. Работать с нами абсолютно безопасно!',
	'Fast support' => 'Быстрая поддержка',
	'You will get support anytime you need it. Feel free to contact us if you have any questions.'
		=> 'Вы будете получать помощь от нас, если она понадобится. Не стесняйтесь обращаться к нам с любыми вопросами.',

	'About us' => 'О нас',
	'Zengram is designed to attract attention to your Instagram account. Basically, it&#39;s an app aimed at getting you more followers on Instagram. In even simpler words, it&#39;s made so you can forget the question «How to Get Followers on Instagram?». You will get more Instagram comments, likes and, of course, real Instagram followers.'
		=> 'Zengram предназначен для привлечения внимания к вашей учетной записи instagram. Вы получаете больше подписчиков, лайков и комментариев.',
	'Zengram can help you to get Instagram followers fast, even today. We have done a great job to introduce you this service, that is not just a regular Instagram followers app. It&#39;s a powerful tool able not only to increase your following but also to get real Instagram comments, which indicates the highest level of engagement. There was a project for professional users in a private access for several years. Now we have made it clear, user-friendly and open to public. It stood the test of time and was acknowledged by professional promoters as a complex tool to grow Instagram followers. It does not matter if your account is for personal or business use, Zengram will render your work more efficient. If you have no time to constantly like, comment and follow manually - Zengram will assist you. By the way, if you want you can also buy real Instagram followers, in this case, Zengram will pick the best audience and will do everything for you to go viral. You will become a popular Instagram user with Zengram!'
		=> 'Когда мы начинали Zengram, мы делали это для себя. Мы хотели получить больше подписчиков в Инстаграм, не тратя много времени вручную. Мы разработали специальные инструменты для того, чтобы сделать этот процесс простым и эффективным.<br/> С помощью Zengram вы получаете настоящих подписчиков. Которым реально нравятся ваши фото и то, что вы делаете в Инстаграм.<br/> И это будут жители вашего города!<br/> Мы работаем с людьми, с брендами, с агентствами... со всеми, кому нужны подписчики в инстаграм. С Zengram вы быстро станете популярны в Инстаграм!',

	'Screenshots<br class="visible-xs"/> of our service' => 'Скриншоты <br class="visible-xs">нашего сервиса',

	'How it works' => 'Как это работает',
	'1. Add an Instagram <br class="visible-xs">account.' => '1. Добавить аккаунт <br class="visible-xs">инстаграма',
	'You just need to link your Instagram to your Zengram account, and it will appear on the control panel. It&#39;s as easy as that!'
		=> 'Вам просто нужно зайти в свой инстаграм аккаунт, и он сразу окажется на панели управления. Это очень просто!',
	'2. Set up your actions.' => '2. Настройка ваших действий',
	'You need to choose the way to discover new users — geography or hashtag search, or to copy someone else&#39;s followers. Then define the preferred actions: liking, following, commenting, unfollowing. That&#39;s all! The account is ready to get famous.'
		=> 'Вам нужно выбрать город, в котором будет работать ваш аккаунт. Выбрать действия: лайкинг, фоллоу, комментирование, анфоллоу. Все! Аккаунт готов к работе.',
	'3. Relax and enjoy yourself.' => '3. Наслаждайтесь и расслабляйтесь',
	'You just need to click the "start" button. The rest Zengram will do automatically on your behalf. You can even switch off the computer: everything works on our servers, so the flow of followers on Instagram of yours will not cease.'
		=> 'Вам просто нужно нажать кнопку "старт". Все остальное Zengram сделает за вас. Вы даже можете выключить компьютер - это не повлияет на работу. Ведь все происходит на наших серверах.',

	'Feedback' => 'Отзывы',
	'My name is Medo and I am a Wedding Planner, organizing events in exotic corners of our planet. By using the service Zengram I&#39;ve finally managed to promote my account. Although I have tried a lot of services and ways of promotion before, they all failed to actually get real Instagram followers. In Zengram all functions are very comfortable and understandable, and what&#39;s most important for me is that Instagram followers come from a certain city!'
		=> 'Раньше мы заказывали продвижение своего магазина у компании и платили порядка 15 000 рублей в месяц. Перешли на Zengram - теперь имеем тот же результат за 599 рублей в месяц. Я и моя жена очень довольны)',
	'Medo' => 'Виталий и Светлана.',
	'Wedding planner' => 'Владельцы интернет-магазина дизайнерских товаров для дома.',
	'A unique service that saves a lot of time. Now I do not have to spend all day on my phone trying to figure out the way how to get more likes on Instagram. The service imitates my social activity, and it does this thoroughly! Not a single Instagram follower has ever suspected anything. It has become much easier to promote my Instagram blog for interior&#39;s design.'
		=> 'Сколько времени освободилось. Больше не нужно часами сидеть в Инстаграм, лайкать, писать комментарии. Zengram делает это за меня целыми днями напролет! Спасибо!',
	'Megan' => 'Виталина.',
	'Interior designer' => 'Бьюти-блогер.',
	'We are the owners of a small cafe in the centre of Sheffield, struggling to get followers on Instagram. Previously, for the promotion of our Instagram profile we paid about 120 pounds and were glad that it was so cheap) After switching to Zengram we have the same result for $80. A substantial difference, isn&#39;t it? This is definitely the best app to get followers on Instagram. My wife and I are very satisfied!'
		=> 'Отличный сервис! Все очень удобно и понятно. Пробовала многое, но ваш сервис самый лучший. И подписчики приходят быстрее. И главное реально из Москвы. Какой город указала - такой и получила. Спасибо)',
	'Gabriel and Madison' => 'Александра.',
	'Cafe owners' => 'Гид по ресторанам.',

	'Buy Real Instagram Followers' => 'Купить реальных подписчиков Instagram',
	'Followers On Instagram' => 'Подписчики Instagram',
	'Get Instagram Followers' => 'Получить подписчиков Instagram',
	'Get Likes on Instagram App' => 'Получить лайки в Instagram',
	'Get More Instagram Followers' => 'Получить больше подписчиков Instagram',
	'How to Get Followers on Instagram' => 'Как получить подписчиков Instagram',
	'Instagram Followers' => 'Подписчики Instagram',
	'Instagram Followers App' => 'Подписчики Instagram',
	'More Followers on Instagram' => 'Больше подписчиков Instagram',
	'Real Instagram Followers' => 'Реальные подписчики Instagram',

	'Dear customer, there are technical works to improve the site functionality at the moment. Zengram will be unavailable for about 10 minutes. Please try again later. We apologize for any inconvenience.' => 'Уважаемый клиент, в настоящий момент ведутся работы по улучшению функционала сайта. Сайт будет недоступен примерно в течение 10 минут. Пожалуйста, попробуйте зайти попозже. Приносим извинения за предоставленные неудобства.',
	'Technical works' => 'Технические работы'
];
