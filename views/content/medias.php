<?php
/* @var app\models\Account $account */
?>
<style>
	.commenter-login {
		max-height: 1.2em;
	}
</style>
<?php if (!empty($account->medias['data'])) : ?>
	<?php if (!Yii::$app->request->isAjax): ?>
		<div class="instagram-media-row row panel-medias">
	<?php endif; ?>
	<?php for ($i = 0; $i < count($account->medias['data']); $i++): ?>
		<?php
		$media        = $account->medias['data'][$i];

		$comments_count = count($media['comments']) > $media['comment_count'] ? count($media['comments']) : $media['comment_count'];

		/* сложно, у инстаграма кривые параметры
		 * if ($media['comment_count'] > count($media['comments'])) {
			$comments_count = $media['comment_count'];
		}*/

		if (!isset($media['image_versions'])){
			$media['image_versions'] = $media['image_versions2']['candidates'];
		}
		$last_comment = end($media['comments']);
		if (!empty($media['comments'])) {
			$first_comment_id = $media['comments'][0]['pk'];
		} else {
			$first_comment_id = '';
		}
		//instagram caption=comment fix
		/*if (!empty($media['caption']['content_type']) && $media['caption']['content_type'] == 'comment') {
			$media['comment_count']--;
		}*/
		$image_url = str_replace('http://scontent', 'https://scontent', $media['image_versions'][0]['url']);
		?>
		<?php /*if ($i % 3 == 0) : */ ?><!--
			<div class="instagram-media-row row">
		--><?php /*endif; */ ?>
		<div class="instagram-holder col-md-4 col-sm-4 col-xs-4">
			<div class="instagram-photo text-center cursor-pointer" data-toggle="modal"
			     data-target="#showMedia<?= $media['id'] ?>">
				<div class="media-grid-item">
					<img data-src="<?= str_replace('http://', '//', $image_url) ?>" src="<?= '/img/ajax-loading.gif'//$image_url /*$media['image_versions'][0]['url']*/ ?>">
				</div>

				<div class="instagram-photo-footer">
					<div class="md-pull-left xs-text-center">
						<span class="fa fa-heart"></span> <?= Yii::t('views', 'Likes:') ?> <strong><?= $media['like_count']; ?></strong>
					</div>
					<div class="md-pull-right xs-text-center">
						<span><span class="fa fa-comment"></span> <?= Yii::t('views', 'Comments:') ?> <strong
								id="commentCounter<?= $media['id'] ?>"><?= $comments_count; ?></strong><sup
								id="newCommentCounter<?= $media['id'] ?>"
								class="new-comments-sup">+<?= ($media['new_comments'] < 0 ? 0 : $media['new_comments']); ?></sup></span>
					</div>
				</div>
				<?php /* if (!empty($last_comment['text'])):  ?>
                <div class="last-comment">@<?= $last_comment['user']['username']  ?>: <?= $last_comment['text']  ?></div>
                <?php endif; */ ?>
			</div>
			<!--<div class="instagram-comment">
				<?php /* if (!empty($last_comment['text'])):  ?>
					@<?= $last_comment['user']['username']  ?>: <?= $last_comment['text']  ?>
				<?php endif; */ ?>
			</div>-->
			<!--<div class="instagram-comment-more text-center"><i class="fa fa-ellipsis-h"></i></div>-->
		</div>
		<div id="showMedia<?= $media['id'] ?>" class="modal instagram-media-modal" tabindex="-1" role="dialog"
		     aria-hidden="true" data-mediaId="<?= $media['id'] ?>" data-comments="<?= $comments_count ?>">
			<div class="modal-dialog">
				<div class="modal-content <?= $media['media_type'] == '2' ? 'modal-video' : '' ?>">
					<div class="modal-body">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
								aria-hidden="true">&times;</span></button>
						<a href="#" data-current="<?= $media['id'] ?>" style="display: none;" class="prev"><span
								class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span></a>
						<a href="#" data-current="<?= $media['id'] ?>" style="display: none;" class="next"><span
								class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>

						<div class="modal-wrapper modal-media-item" id="modal-wrapper<?= $media['id'] ?>"
						     data-media-id="<?= $media['id'] ?>">
							<div class="row modal-flex">
								<div class="col-md-7">
									<?php if ($media['media_type'] == '2'): ?>
										<video class="instagram-content-modal-video" width="400" height="auto"
										       controls="">
											<source src="<?= $media['video_versions'][0]['url'] ?>" type="video/mp4">
											<img src="<?= $image_url ?>"
											     class="img img-responsive" border="0">
										</video>
									<?php else: ?>
										<img src="<?= $image_url ?>"
										     class="instagram-content-modal-image img-responsive" border="0">
									<?php endif; ?>
								</div>
								<div class="col-md-5">
									<div class="instagram-photo-header">
										<div class="md-pull-left xs-text-center">
											<span class="fa fa-heart"></span> <?= Yii::t('views', 'Likes:') ?>
											<strong><?= $media['like_count']; ?></strong>
										</div>
										<div class="md-pull-right xs-text-center">
											<span class="fa fa-comment"></span> <?= Yii::t('views', 'Comments:') ?>
											<strong><?= $media['comment_count']; ?></strong>
										</div>
									</div>
									<div id="commentWrapper<?= $media['id'] ?>" class="modal-content-comments">
										<?php if (!empty($media['caption']['text'])): ?>
											<div class="instagram-photo-title"><?= $media['caption']['text'] ?></div>
										<?php endif; ?>
										<?php
//										echo $this->render('comments', [
//											'account' => $account,
//											'media_id'  => $media['id']
//										]);
										?>
										<?php if (!empty($media['has_more_comments'])): ?>
											<div class="panel-more-comments text-center modal-content-comment instagram-modal-comment">
												<button class="btn btn-success"
												        onclick="loadMoreComments('<?= $account->id; ?>', '<?= $media['id'] ?>','<?= $first_comment_id; ?>')">
													<?= Yii::t('views', 'Load more') ?>
												</button>
												<p style="display: none" class="text-center loading-more-comments"><img
														src="/img/ajax-loader.gif"></p>
											</div>
										<?php endif; ?>
										<?php foreach ($media['comments'] as $comment): ?>
											<div class="modal-content-comment instagram-modal-comment"
											     data-comment-id="<?= $comment['pk'] ?>">
												<div class="delete">
													<span class="glyphicon glyphicon-trash" aria-hidden="true"
													      title="<?= Yii::t('views', 'Remove comment') ?>"></span>
												</div>
												<div class="comment-user col-xs-3 col-md-3 col-sm-3">
													<img class="img img-rounded"
													     src="<?= str_replace('http://scontent', 'https://scontent', $comment['user']['profile_pic_url']) ?>">
													<h5 class="cursor-pointer commenter-login" title="@<?= $comment['user']['username'] ?>"
													    onclick="quoteUser('<?= $media['id'] ?>', '@<?= $comment['user']['username'] ?>')">
														@<?= $comment['user']['username'] ?></h5>
												</div>
												<div
													class="comment-text col-xs-9 col-md-9 col-sm-9 emoji"><?= $comment['text'] ?></div>
												<div class="clearfix"></div>
											</div>
										<?php endforeach; ?>
									</div>
									<script>
										$(function () {
											var $wrapper = $('#commentWrapper<?= $media['id'] ?>');
											$wrapper.scrollTop($wrapper[0].scrollHeight);
										});
									</script>

									<div class="modal-send-comment">
										<div contenteditable="true" class="form-control modal-comment-input"
										     id="modal-comment-input<?= $media['id'] ?>"
										     placeholder="Введите комментарий"></div>
										<div class="emoji-button" data-id="emoji_widget_<?= $media['id'] ?>">☺</div>
										<div class="emoji-widget" id="emoji_widget_<?= $media['id'] ?>"></div>

										<div class="alert alert-danger" id="send-comment-error<?= $media['id'] ?>"
										     style="display: none"></div>
										<button class="btn btn-success btn-send-comment"
										        onclick="sendComment('<?= $account->id; ?>', '<?= $media['id'] ?>')">
											<?= Yii::t('app', 'Send') ?>
										</button>
										<p style="display: none" class="loading-send"><img src="/img/ajax-loader.gif">
										</p>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<?php /*if ($i % 3 == 2) : */ ?><!--
			</div>
		--><?php /*endif; */ ?>
	<?php endfor; ?>
	<?php if (!Yii::$app->request->isAjax): ?>
		</div>
	<?php endif; ?>
<?php endif; ?>
<script>
	function changeDataSrcToSrc(i, e) {
		e.src = $(e).data('src');
	}
	$(document).ready(function () {
		setTimeout(function(){
			$('[data-src]').each(changeDataSrcToSrc);
		}, 2000);
	});
</script>
<?php
if (Yii::$app->request->isAjax):
	?>
	<script>
		$(document).ready(function () {
			if ($('.instagram-holder').size() == <?php echo json_encode($account->account_media); ?>)
				$('.show-more-media').remove();
		});
	</script>
	<?php
endif; ?>
<?php if (!Yii::$app->request->isAjax): ?>
	<?php if (!empty($account->medias['more'])) : ?>
		<div class="panel-more text-center col-md-12 col-sm-12 col-xs-12">
			<button class="btn show-more-media" onclick="loadMore(<?= $account->id; ?>)"><?= Yii::t('views', 'Show more') ?></button>
			<p style="display: none" class="text-center loading-more"><img src="/img/ajax-loader.gif"></p>
		</div>
	<?php endif; ?>
<?php endif; ?>

<?php if (!empty($account->medias['retry'])) : ?>
	<div class="alert alert-warning"><?= Yii::t('views', 'Server error. Try again later') ?></div>
<?php endif; ?>
<?php if (!empty($account->medias['error'])) : ?>
	<div class="alert alert-danger"><?= Yii::t('views', 'Server error.') ?>
		<script>$(document).ready(function () {
				updateAccountError(<?= $account->id; ?>, "<?= $account->medias['error'] ?>", true);
			});</script>
	</div>
	<div id="start_<?= $account->id; ?>" class="hidden" data-login="<?= $account->login ?>"></div>
<?php endif; ?>