<?php
/**
 * @var $this \yii\web\View
 * @var $server SnifferServers
 * @var $logs array
 * @var $parameter
 * @var $limits array
 */

use app\models\SnifferLog;
use app\models\SnifferMonitoringParameters;
use app\models\SnifferServers;

// Предварительная обработка
$max_limit = 180;


$user_limits = [];
$lowest_limit = 0;
$lowest_parameter = 0;

foreach ($limits as $limit) {

	if (empty($user_limits[$limit['value']])) {
		$user_limits[$limit['value']] = [];
	}

	$user_limits[$limit['value']][] = $limit['name'] . ' (' .
		SnifferMonitoringParameters::getOperatorDescriptions()[$limit['operator']]
		. ')';

	if ($lowest_limit == 0 || $limit['value'] < $lowest_limit) {
		$lowest_limit = $limit['value'];
		$lowest_parameter = $limit['id'];
	}
}

$low_limit = SnifferMonitoringParameters::findOne($lowest_parameter);

?>

<div class="history-body">

	<div class="log-wrapper">

		<div class="log-slider clr">
			<?php

			/** @var SnifferLog $log */
			foreach ($logs as $log) {

				$value = $log->$parameter > $max_limit ? $max_limit : (int) $log->$parameter;
				$margin = $max_limit - $value;

				?>

				<div class="log <?= $low_limit->compare($log->$parameter) ? 'bad' : '' ?>"
				     data-toggle="popover"
				     data-content="Время: <?= date('Y-m-d H:i:s', $log->time) ?><br>Значение: <?= $log->$parameter ?>"
				     style="height: <?= $value ?>px; margin-top: <?= $margin ?>px;">
				</div>

			<?php }

			?>
		</div>

	</div>

	<?php

	foreach ($user_limits as $user_limit => $users) { ?>

		<div class="limit" data-toggle="popover" data-content="<?= implode('<br>', $users) ?>" style="bottom: <?= $user_limit ?>px;">

			<div class="value"><?= $user_limit ?> </div>

			<div class="line"></div>

		</div>

	<?php }

	?>

</div>