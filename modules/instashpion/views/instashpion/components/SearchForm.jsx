var SearchForm = React.createClass({
    /*
     * Состояние при инициализации
     */
    getInitialState() {
        return {
            loading:    false,
            interval:   null,
            request:    null,
            focus:      false,
            moment: ''
        }
    },

    /*
     *  Миксины
     */
    mixins: [
        Backbone.React.Component.mixin
    ],

    componentDidMount () {
        var self = this;
        setInterval(function () {
            var a = self.datePrint(app.get('from').y +'-'+ app.get('from').m +'-'+ app.get('from').d + ' ' + app.get('from').h + ':' + app.get('from').m + ':00');

            self.setState({moment: a.days + 'дн. ' + a.hours + 'ч. ' + a.minutes + 'м. ' + a.seconds +'с.'});
        }, 1000);

        this.on(this, {
            collections: {
                users: this.props.searchResult
            }
        });
    },

    componentWillUnmount () {
        this.off(this);
    },

    keyPress(e) {
        clearInterval(this.state.interval);
        var self = this;

        if (this.state.request) {
            this.state.request.abort();

            this.setState({
                loading: false
            });
        }

        if (this.refs.query.value.length > 2 && e.charCode != 13) {
            var interval = setTimeout(function () {
                self.search();
            }, 1500);

            this.setState({interval: interval});
        }
    },

    queryChange(){
        if (this.refs.query.value.length < 1) {
            this.props.searchResult.reset();
        }
    },

    /*
     * Поиск пользователя
     */
    search(e) {
        var submited = false;

        if (e) {
            e.preventDefault();
            e.stopPropagation();
        }

        var self = this;

        if (this.state.loading) {
            return false;
        }

        this.setState({
            loading:    true,
            user:       null
        });

        var request = $.ajax({
            type: 'post',
            url: '/instashpion/ajax/search',
            data: {
                query: this.refs.query.value
            },
            success: function (response) {
                self.setState({'loading' : false});
                if (response.status == 'ok') {
                    /*if (submited) {
                        self.props.searchResult.reset(response.users);

                        if (response.users[0].username == self.refs.query.value)
                        {
                            (self.select.bind(self, response.users[0].instagram_id))();
                        }
                    } else {*/
                        self.props.searchResult.reset(response.users);
                    // }
                }
            },
            jsonp: true
        });

        this.setState({request: request});
    },

    select(user_id)
    {
        var user = this.props.searchResult.get(user_id);
        this.props.searchResult.reset();

        var self = this;

        if (this.state.loading) {
            return false;
        }

        var existed = false;

        app.get('history').each((item) => {
            if (item.get('user').get('instagram_id') == user_id) {
                item.open();
                existed = true;
            }
        });

        if (existed) {
            return true;
        }

        this.props.store.get('analize').flush();

        user.set('loadedInfo', true);

        this.props.store.set({
            user: user,
            userInfoLoading: true
        });
        
        var request = $.ajax({
            type: 'post',
            url: '/instashpion/ajax/user-info',
            data: {
                instagram_id: user_id
            },
            success: function (response) {
                if (response.status == 'ok') {
                    var user = new UserModel(response.user);
                    user.set('loadedInfo', false);

                    self.props.onSelectUser(user);
                }

                self.setState({'loading' : false});
            },
            jsonp: true
        });
    },

    renderUser(user, iteration) {
        return (
            <div key = {iteration} className = "user-item" onClick = {this.select.bind(this, user.instagram_id)}>
                <div className="row">
                    <div className = "col-md-3 col-xs-3 photo">
                        <img src = {user.avatar} alt = "photo" style = {{width: '64px', borderRadius: '50%'}} />
                    </div>
                    <div className = "col-md-9 col-xs-9">
                        <span className="username">@{user.username}</span> <br />
                        <span className="name" style = {{marginTop: '10px', display: 'inline-block', fontSize: '12px'}}>{user.name}</span>
                    </div>
                </div>
            </div>
        );
    },

    renderSearchResult() {
        var style = {position: 'absolute', width: '315px', zIndex: 9, marginTop: '50px', boxShadow: '0px 5px 30px rgba(65,65,65,0.65)'};

        if (this.state.focus) {
            style.display = 'block';
        } else {
            style.display = 'none';
        }

        return (
            <div className = {"search-result " + ((this.state.loading) ? 'disabled' : '')} style = {style}>
                {_.map(this.state.users, this.renderUser)}
            </div>
        );
    },

    onFocus() {
        this.setState({'focus': true});
    },

    onBlur() {
        var self = this;

        setTimeout(() => {
            self.setState({'focus': false});
        }, 200);
    },

    showDropdown(index, e) {
        e.preventDefault();
        e.stopPropagation();

        this.refs['dropdown' + index].style.display = 'block';
    },

    hideDropdown(index, e) {
        e.preventDefault();
        e.stopPropagation();

        this.refs['dropdown' + index].style.display = 'none';
    },

    renderHistory() {
        var self = this;

        if (app.get('analize').get('process')) {
        //    return null;
        }

        return (
            <div style = {{
                position: 'relative',
                top: '20px',
                width: '100%',
                height: '1px'
            }}>
                {(() => {
                    return app.get('history').where({hidden: false}).map((model, index) => {
                        return (
                            <img
                                src = {model.get('user').get('avatar')}
                                alt = {model.get('user').get('username')}
                                style = {{
                                    width: '60px',
                                    borderRadius: '50%',
                                    marginRight: '8px',
                                    verticalAlign: 'bottom'
                                }}
                                onClick = {(() => { (app.get('history').where({hidden: false}))[index].open(); })}
                            />
                        );
                    });
                })()}

                {(() => {
                return (
                    <div style = {{
                        backgroundColor: '#eee',
                        color: '#888',
                        fontSize: '36px',
                        width: '60px',
                        height: '60px',
                        borderRadius: '50%',
                        display: ((app.get('history').findWhere({hidden: true}) && app.get('balance') > 0) ? 'inline-block' : 'none'),
                        marginRight: '8px',
                        verticalAlign: 'bottom',
                        lineHeight: '55px'
                    }} onClick = {self.showDropdown.bind(this, 1)}>+
                        <div style = {{
                            position: 'relative',
                            width: '0px',
                            height: '0px',
                            display: 'none'
                        }} ref = {'dropdown1'}>
                            <div style = {{
                                backgroundColor: '#fff',
                                width: '250px',
                                boxShadow: '1px 1px 3px rgba(65,65,65,0.6)',
                                left: '-100px',
                                top: '10px',
                                position: 'relative',
                                zIndex: 2
                            }} >
                                <div style = {{maxHeight: '320px', overflowY: 'auto'}}>
                                    {(() => {
                                        return app.get('history').map((model, index) => {
                                            if (model.get('hidden')) {
                                                return (
                                                    <div key={index} style={{
                                                        fontSize: '17px',
                                                        padding: '8px 15px',
                                                        textAlign: 'left'
                                                    }}
                                                     className='select-item-dropdown'
                                                     onClick={(() => {
                                                        app.get('history').at(index).restore();
                                                        (self.hideDropdown.bind(self, 1))();
                                                     })}>
                                                        <img src={model.get('user').get('avatar')} alt={model.get('user').get('username')}
                                                             style={{
                                                                width: '50px',
                                                                borderRadius: '50%',
                                                                marginRight: '20px',
                                                                verticalAlign: 'bottom'
                                                            }}
                                                        /> {model.get('user').get('username')}
                                                    </div>
                                                );
                                            }

                                            return null;
                                        });
                                    })()}
                                </div>

                                <div className = 'btn btn-default btn-tiny' onClick = {self.hideDropdown.bind(self, 1)}>отмена</div>
                            </div>
                        </div>
                    </div>
                );
                })()}
            </div>
        );
    },

    pay(method, e) {
        e.preventDefault();
        e.stopPropagation();

        if (method == 'robokassa') {
            this.refs.pay.action = '/payment/invoice';
        }

        if (method == 'interkassa') {
            this.refs.pay.action = '/payment/interkassa';
        }

        if (method == 'yandex') {
            this.refs.pay.action = '/payment/yandex';
        }

        if (method == 'yandex-cards') {
            this.refs.pay.action = '/payment/yandex-cards';
        }

        if (method == 'paypal') {
            this.refs.pay.action = '/payment/paypal';
        }

        this.refs.pay.submit();
    },

    renderPayModal() {
        var self = this;

        if (app.get('analize').get('showPayModal') !== true) {
            return null;
        }

        return (
            <div>
                <div className = "modal-overlay" onClick = {() => { app.get('analize').set({showPayModal: false}) }}></div>
                <div className="modal-dialog modal-reg">
                    <div className="modal-content" style = {{padding: '20px 10px 40px 10px'}}>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" onClick = {() => { app.get('analize').set({showPayModal: false}) }}>×</span></button>
                        <div className="modal-body" style = {{textAlign: 'center'}}>
                            <h2 className="modal-title" style = {{lineHeight: '1.4',fontSize: '26px',color: '#ff6666'}}>
                                Пополнить баланс
                            </h2>

                            <p style = {{
                                fontSize: '19px',
                                margin: '20px 0px'
                            }}>
                                После оплаты Вы получите 3 дополнительных анализа.
                            </p>

                            <p>
                                Выберите наиболее удобный способ оплаты для продолжения
                            </p> <br />
                            <form ref = 'pay'>
                                <input type = "hidden" name = "price" value = "9" />

                                <div id="currencies" style = {{
                                    width: '450px',
                                    display: 'inline-block',
                                    margin: '0px auto',
                                    textAlign: 'left'
                                }}>
                                    <span className="">
                                        <button type="button" className="pay-button yandex-cards-submit" onClick = {self.pay.bind(this, 'yandex-cards')}><img src="/img/currencies/BankCard.png" /></button>
                                        <button type="button" className="pay-button interkassa-submit" onClick = {self.pay.bind(this, 'interkassa')}><img src="/img/currencies/logo-sberbank-online.png" /></button>
                                        <button type="button" className="pay-button interkassa-submit" onClick = {self.pay.bind(this, 'interkassa')}><img src="/img/currencies/AlfaBank.gif" /></button>
                                        <button type="button" className="pay-button yandex-submit" onClick = {self.pay.bind(this, 'yandex')}><img src="/img/currencies/YandexMerchant.gif" /></button>
                                        <button type="button" className="pay-button interkassa-submit" onClick = {self.pay.bind(this, 'interkassa')}><img src="/img/currencies/Qiwi.gif" /></button>
                                        <button type="button" className="pay-button interkassa-submit" onClick = {self.pay.bind(this, 'interkassa')}><img src="/img/currencies/Beeline.gif" /></button>
                                        <button type="button" className="pay-button interkassa-submit" onClick = {self.pay.bind(this, 'interkassa')}><img src="/img/currencies/MTS.gif" /></button>
                                        <button type="button" className="pay-button interkassa-submit" onClick = {self.pay.bind(this, 'interkassa')}><img src="/img/currencies/megafon_icon_100.png" /></button>
                                        <button type="button" className="pay-button interkassa-submit" onClick = {self.pay.bind(this, 'interkassa')}><img src="/img/currencies/tinkoff_icon_100.png" /></button>
                                        <button type="button" className="pay-button interkassa-submit" onClick = {self.pay.bind(this, 'interkassa')}><img src="/img/currencies/bitcoin_icon_100.png" /></button>
                                        <button type="button" className="pay-button paypal-submit" onClick = {self.pay.bind(this, 'paypal')}><img src="/img/currencies/Paypal.gif" /></button>
                                    </span>
                                </div>
                            </form>

                            <br />
                        </div>
                    </div>
                </div>
            </div>
        );
    },


    datePrint: function (endtime) {
        var t = Date.parse(endtime) - Date.parse(new Date());
        var seconds = Math.floor( (t/1000) % 60 );
        var minutes = Math.floor( (t/1000/60) % 60 );
        var hours = Math.floor( (t/(1000*60*60)) % 24 );
        var days = Math.floor( t/(1000*60*60*24) );
        return {
            'total': t,
            'days': days,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds
        };
    },

    /*
     * Представление компонента
     */
    render: function() {
        var self = this;

        var declOfNum = (function(){
            var cases = [2, 0, 1, 1, 1, 2];
            var declOfNumSubFunction = function(titles, number){
                number = Math.abs(number);
                return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
            };
            return function(_titles) {
                if ( arguments.length === 1 ){
                    return function(_number){
                        return declOfNumSubFunction(_titles, _number)
                    }
                }else{
                    return declOfNumSubFunction.apply(null,arguments)
                }
            }
        })();

        var points = declOfNum(['аккаунт', 'аккаунта', 'аккаунтов'], store.get('balance'));

        return (
            <div className="banner__wrap">
                <section className="banner">
                    <h1 className="banner__title"><span className="banner__title_strong">Бесплатно</span>посмотри кто кого лайкает в Instagram</h1>
                    <p className="banner__description">Уникальный сервис Zengram моментально покажет, каким фотографиям и людям поставил лайк интересующий вас человек, а также сформирует отчет о тех, кто отправил ему лайк в ответ</p>
                    <div className="banner__form" style = {{maxWidth: '315px'}}>
                        <p className="banner__form-info" style = {{position: 'relative', top: '-15px', fontSize: '18px', width: '500px', maxWidth: '500px', left: '-95px'}}>
                            Вы можете проанализировать {store.get('balance')} {points}.
                            {(() => {
                                if (this.state.moment && app.get('timer')) {
                                    return (
                                        <span><br />
                                            {((store.get('balance') > 0) ? 'Доступно на' : <psan>Заменить аккаунты возможно через<br /></psan>)} {this.state.moment}
                                            {(() => {
                                                if (!isGuest || isUser) {
                                                    return (
                                                        <span><br />
                                                            <a className = "btn btn-default" style = {{marginTop: '8px'}} onClick = {(() => { app.get('analize').set({showPayModal: true}) })}>Пополнить</a>
                                                        </span>
                                                    );
                                                }
                                            })()}
                                        </span>
                                    );
                                } else {
                                    if (isGuest && !isUser) {
                                        return null;
                                    }
                                    return (
                                        <span><br />
                                            <a className = "btn btn-default" style = {{marginTop: '8px'}} onClick = {(() => { app.get('analize').set({showPayModal: true}) })}>Пополнить</a>
                                        </span>
                                    );
                                }
                            })()}
                        </p>

                        <p className="banner__form-info">Введите название аккаунта в Instagram, за кем хотите проследить</p>
                        <form onSubmit = {this.search} style = {{textAlign: 'left'}}>
                            <input type="text"
                                   placeholder = "@login"
                                   id = "search-query"
                                   onKeyDown={this.keyPress}
                                   onChange = {this.queryChange}
                                   ref = 'query'
                                   autoComplete={"off"}
                                   className="banner__field"
                                   style = {{textAlign: 'left', paddingLeft: '15px'}}
                                   onFocus = {this.onFocus}
                                   onBlur = {this.onBlur}
                                   disabled = {app.get('analize').get('process')}
                            />
                            <div style = {{
                                float: 'right',
                                width: '0px',
                                height: '0px',
                                position: 'relative',
                                top: '-32px',
                                left: '-30px'
                            }} className = "spinner-position">
                                <Spin label = " " loading = {this.state.loading} size = "20px" color = "#a6a6a4" />
                            </div>

                            {this.renderSearchResult()}
                        </form>
                    </div>

                    {this.renderHistory()}

                </section>
                {this.renderPayModal()}
            </div>
        );
    }
});