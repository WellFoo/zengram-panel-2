<?php namespace app\components;

class PHelper
{
	static function prepare()
	{
		return md5(mcrypt_create_iv(8, MCRYPT_RAND)).md5(mcrypt_create_iv(6, MCRYPT_RAND));
	}

	static function packChar($input, $key)
	{
		return chr(ord($input) ^ $key);
	}

	static function normalize($input)
	{
		$output = base64_encode($input);
		return substr($output, 0, -2);
	}

	static function setKey($input, $key)
	{
		$input[strlen($input) - 1] = chr($key + 48);
		return $input;
	}

	static function pack($input)
	{
		$output = self::prepare();
		$charKey = strlen($input);
		for ($i = 0; $i < $charKey; $i++) {
			$output[$i] = self::packChar($input[$i], $charKey);
		}
		$output = self::setKey($output, $charKey);
		return self::normalize($output);
	}
}
