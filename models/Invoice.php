<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\web\User;

/**
 * This is the model class for table "invoice".
 *
 * @property integer $id
 * @property integer $price_id
 * @property integer $status
 * @property integer $sum
 * @property integer $value
 * @property integer $user_id
 * @property string  $payment_id
 * @property string  $date
 * @property string  $agent
 * @property string  $ga_data
 * @property integer $discount_id
 * @property Prices  $price
 * @property Discounts $discount
 * @property boolean $redirected
 */
class Invoice extends ActiveRecord
{
	const STATUS_PENDING = 0;
	const STATUS_FAIL = 1;
	const STATUS_ACCEPTED = 2;
	const STATUS_SUCCESS = 3;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'invoice';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['price_id', 'status', 'user_id', 'bonus', 'redirected'], 'integer'],
			['sum', 'number'],
			[['sum', 'user_id', 'price_id'], 'required'],
			[['payment_id'], 'default', 'value' => 0],
			[['date', 'discount_id'], 'safe'],
			[['agent', 'ga_data'], 'string'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'      => Yii::t('app', 'ID заказа'),
			'status'  => Yii::t('app', 'Статус заказа'),
			'sum'     => Yii::t('app', 'Сумма заказа'),
			'user_id' => Yii::t('app', 'ID пользователя'),
			'date'    => Yii::t('app', 'Дата заказа'),
			'agent'   => Yii::t('app', 'Название плетежной системы'),
		];
	}

	public function getPrice()
	{
		return $this->hasOne(Prices::className(), ['id' => 'price_id']);
	}

	public function getUser()
	{
		return $this->hasOne(Users::className(), ['id' => 'user_id']);
	}

	public function getDiscount()
	{
		return $this->hasOne(Discounts::className(), ['id' => 'discount_id']);
	}

	public function apply($agent)
	{
		$this->status = Invoice::STATUS_SUCCESS;
		$this->save(false);

		$bonus = 0;
		if (!is_null($this->discount)) {
			$bonus = $this->discount->calc($this->price->value);
		}

		/* @var $user Users */
		$user = Users::findOne($this->user_id);

		if (!is_null($user)) {
			file_get_contents(Url::toRoute([
				'/partnership/internal/payment',
				'secret' => Yii::$app->params['partnership']['internal_secret'],
				'user_id' => $this->user_id,
				'description' => $user->mail,
				'amount' => $this->sum
			], true));
		}

		// Если пользователь покупал анализы для шпиона
		if ($this->price->id == 9) {
			$model = new ShpionBalance();

			$model->date = time();
			$model->victims = '{}';
			$model->is_bonus = 0;
			$model->user_id = $this->user_id;

			return $model->save();
		}

		return Users::addUserBalance(
			$this->user_id,
			$this->price->value + $bonus,
			$agent.($bonus ? ' (бонус '.$bonus.' дней)' : '')
		);
	}


}
