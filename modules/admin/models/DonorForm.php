<?php

namespace app\modules\admin\models;

use app\components\InstagramProvider;
use core\instagram\exceptions\BaseException;
use yii\base\Model;
use yii\validators\UrlValidator;

/**
 * Class DonorForm
 * @package app\modules\admin\models
 *
 * @property boolean id
 * @property boolean isNew
 */
class DonorForm extends Model
{
	/** @var Donor $_donor */
	private $_donor;
	private $_insta_id;
	private $_avatar;

	public $login;
	public $sex;
	public $languages = [];
	public $random_posting;
	public $descriptions = [];
	public $avatars;
	public $medias;
	public $links = '';

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			// step_1
			[['login', 'languages', 'sex', 'links'], 'required', 'on' => 'step_1'],
			['languages', 'each', 'rule' => ['in', 'range' => [Donor::LANG_EN, Donor::LANG_ES, Donor::LANG_RU]], 'on' => 'step_1'],
			['sex', 'in', 'range' => [Donor::MALE, Donor::FEMALE], 'on' => 'step_1'],
			['descriptions', 'validateDescriptions', 'on' => 'step_1'],
			['links', 'validateLinks', 'on' => 'step_1'],
			['login', 'checkLogin', 'on' => 'step_1'],

			// step_2
			[['random_posting', 'medias'], 'required', 'on' => 'step_2'],
			['random_posting', 'boolean', 'on' => 'step_2'],
			['medias', 'validateMedias', 'on' => 'step_2'],
		];
	}

	public function validateDescriptions($attribute, $params)
	{
		if (!is_array($this->languages)) {
			return;
		}

		if (!is_array($this->descriptions)) {
			$this->addError($attribute, 'Не все описания заполнены');
			return;
		}

		foreach ($this->languages as $lang) {
			if (empty($this->descriptions[$lang])) {
				$this->addError($attribute, 'Не все описания заполнены');
				return;
			}
		}
	}

	public function checkLogin($attribute, $params)
	{
		if ($this->hasErrors()) {
			return;
		}

		try {
			$insta = new InstagramProvider();
			$insta->setAccount(\Yii::$app->params['service_account_id']);
			$result = $insta->findByLogin($this->$attribute);
		} catch (BaseException $e) {
			$this->addError($attribute, 'Не удалось проверить пользователя, попробуйте повторить попытку позже');
			return;
		}

		if (is_null($result)) {
			$this->addError($attribute, 'Пользователь не найден');
		} else {
			$this->_insta_id = $result['pk'];
			$this->_avatar   = $result['profile_pic_url'];
		}
	}

	public function validateMedias($attribute, $params)
	{
		if (!is_array($this->medias)) {
			$this->addError($attribute, 'Нет изображений');
			return;
		}

		$availableUsages = ['posting', 'avatar', 'not_use'];
		foreach ($this->medias as $id => $media) {
			if (empty($media['use']) || !in_array($media['use'], $availableUsages)) {
				$this->addError($attribute, 'У одной или нескольких фото не выбрано использование');
				return;
			}
		}
	}

	public function validateLinks($attribute, $params)
	{
		$links = explode("\n", $this->links);
		$validator = new UrlValidator();
		foreach ($links as $link) {
			$link = trim($link, "\r");
			if (empty($link)) {
				continue;
			}
			if (!$validator->validate($link)) {
				$this->addError($attribute, 'Не все ссылки соответсвуют формату');
				return;
			}
		}
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'login'          => 'Логин',
			'languages'      => 'Языки',
			'random_posting' => 'Порядок постинга',
			'descriptions'   => 'Описание',
			'avatars'        => 'Аватары',
			'sex'            => 'Пол',
			'links'          => 'Ссылки',
			'medias'         => 'Описания фото',
		];
	}

	public function find($id)
	{
		/** @var Donor $donor */
		$donor = Donor::findOne($id);
		if (is_null($donor)) {
			return false;
		}

		$this->login          = $donor->login;
		$this->languages      = $donor->languages;
		$this->sex            = $donor->sex;
		$this->random_posting = $donor->random_posting;
		$this->_donor = $donor;

		foreach ($donor->descriptions as $description) {
			$this->descriptions[$description->language] = $description->text;
		}

		foreach ($donor->links as $link) {
			$this->links .= $link->link.PHP_EOL;
		}

		return true;
	}

	public function getId()
	{
		if (is_null($this->_donor)) {
			return null;
		}
		return $this->_donor->id;
	}

	public function getIsNew()
	{
		return is_null($this->_donor);
	}

	public function getMediaList()
	{
		if (count($this->_donor->medias)) {
			return $this->_donor->medias;
		}

		$insta = new InstagramProvider();
		$insta->setAccount(\Yii::$app->params['service_account_id']);

		$list = [];
		$response = $insta->getUserFeed($this->_donor->insta_id);
		foreach ($response['items'] as $image) {
			$donorMedia = new DonorMedia();
			$donorMedia->donor_id = $this->_donor->id;
			$donorMedia->media_id = $image['id'];
			$donorMedia->url      = $image['image_versions2']['candidates'][0]['url'];
			$donorMedia->save();
			$list[] = $donorMedia;
		}

		return $list;
	}

	public function save()
	{
		// TODO добавить транзакции
		if (!$this->validate()) {
			return false;
		}

		if ($this->isNew) {
			$this->_donor = new Donor();
		}

		switch ($this->scenario) {
			case 'step_1':
				$this->_donor->login     = $this->login;
				$this->_donor->insta_id  = $this->_insta_id;
				$this->_donor->avatar    = $this->_avatar;
				$this->_donor->sex       = $this->sex;
				$this->_donor->languages = $this->languages;
				if ($this->_donor->save()) {
					$this->saveDescriptions();
					$this->saveLinks();
					return true;
				}
				break;

			case 'step_2':
				$this->saveMedias();
				$this->_donor->random_posting = $this->random_posting;
				$this->_donor->completed      = true;
				if ($this->_donor->save()) {
					return true;
				}
				break;
		}

		return false;
	}

	private function saveDescriptions()
	{
		$this->_donor->unlinkAll('descriptions', true);
		foreach ($this->descriptions as $lang => $text) {
			$description = new DonorDescription();
			$description->donor_id = $this->_donor->id;
			$description->language = $lang;
			$description->text     = $text;
			$description->save();
		}
	}

	private function saveLinks()
	{
		$this->_donor->unlinkAll('links', true);
		$links = explode("\n", $this->links);
		foreach ($links as $url) {
			$url = trim($url, "\r");
			if (empty($url)) {
				continue;
			}
			$link = new DonorLink();
			$link->donor_id = $this->_donor->id;
			$link->link     = $url;
			$link->save();
		}
	}

	private function saveMedias()
	{
		foreach ($this->medias as $id => $media) {
			/** @var DonorMedia $donorMedia */
			$donorMedia = DonorMedia::findOne($id);
			if (is_null($donorMedia)) {
				continue;
			}

			switch ($media['use']) {
				case 'posting':
					$donorMedia->unlinkAll('descriptions', true);
					$this->saveMediaDescriptions($id, $media['description']);
					$donorMedia->is_avatar = false;
					$donorMedia->save();
					break;

				case 'avatar':
					$donorMedia->unlinkAll('descriptions', true);
					$donorMedia->is_avatar = true;
					$donorMedia->save();
					break;

				case 'not_use':
					$donorMedia->delete();
					break;
			}
		}
	}

	private function saveMediaDescriptions($id, $descriptions)
	{
		foreach ($descriptions as $code => $text) {
			$description = new DonorMediaDescription();
			$description->donor_media_id = $id;
			$description->language       = $code;
			$description->text           = $text;
			$description->save();
		}
	}
}