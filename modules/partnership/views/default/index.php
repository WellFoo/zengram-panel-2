<?php
/**
 * @var \yii\web\View $this
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\partnership\Module;
use app\modules\partnership\asset\ModuleAsset;
ModuleAsset::register($this);
//$this->registerCssFile('/css/partner.css');
$this->title = 'Партнерская программа';

$tplOferta = new \app\components\TemplateParser(
	'ofertaPartner',
	\Yii::getAlias('@app/modules/partnership/templates/')
);

$authorized = !is_null(Yii::$app->user->identity);
$modal = Yii::$app->request->get('partner', false);

$link_data = $authorized ? [] : [
	'data' => [
		'toggle' => 'modal',
		'target' => '#regModal'
	]
];

?>

<style>
	#partnership-oferta .modal-body {
		padding-right: 10px;
	}

	.scroller {
		max-height: calc(80vh);
		overflow-y: scroll;
		padding: 0 15px 10px 0;
		position: relative;
	}

	.scroller:after {
		box-shadow: 0 -2px 6px rgba(0, 0, 0, .1);
		top: auto;
		bottom: -1px;
	}

	.scroller::-webkit-scrollbar {
		width: 8px;
	}

	.scroller::-webkit-scrollbar-track {
		-webkit-box-shadow: inset 0 0 4px rgba(0, 0, 0, 0.3);
		border-radius: 8px;
	}

	.scroller::-webkit-scrollbar-thumb {
		border-radius: 10px;
		background-color: rgba(0, 0, 0, 0.3);
	}

	.line {
		margin-bottom: 20px;
	}

	.partner-body {
		font-family: 'geometria';
	}
	.container {
		padding-left:0 !important;
		padding-right:0 !important;
	}
	.header{
		margin-bottom:0px;
	}

</style>
<div class="partner-body">

	<div class="partner-header">
		<div class="container">
			<h1>Зарабатывайте с нами!</h1>

			<p>Наша партнерская программа очень проста<br>
			и принесет Вам реальный доход при минимальных усилиях.</p>
			<br>

			<div class="line"><img src="/img/partner/whiteline.jpg" alt=""></div>


			<p>Множество сервисов предлагают использовать партнерские программы,<br> но только у нас все максимально просто
				и открыто.<br>
				У каждого реферала есть свой личный кабинет, в котором он может отслеживать свои накопления.<br>
				Вы приводите к нам посетителей и получаете свой доход.</p>

			<?=Html::a(
				'Стать партнёром',
				['#'],
				[
					'class' => 'btn btn-danger btn-lg confirm-button-1'.(!$authorized ? '' : ' confirm')
				] + $link_data
			)?>

			<br>

			<a href="#" class="confirm pub">Публичная оферта</a>
		</div>
	</div>






	<h2>Как это работает?</h2>
	<ul class="psis">
		<li><span class="punkt">1</span> Зарегистрируйтесь в нашей партнерской программе.</li>
		<li><span class="punkt">2</span> Получите присвоенную вам персональную реферальную ссылку.</li>
		<li><span class="punkt">3</span> Заведите электронный кошелек WMR для вывода заработанных средств.</li>
		<li><span class="punkt">4</span> Разместите реферальную ссылку на своем сайте или на любом доступном <span class="dlia">для вас интернет ресурсе для привлечения
			заказчиков в наш сервис.</span></li>

	</ul>

	<div class="dohblog">
		<h2>Какие у меня будут доходы?</h2>
<br><br>
		<div class="row">
			<div class="col-md-6 col-xs-12 grline">
				<div style="border-right: 1px solid #1AC663; padding-top:15px; ">
				<div class="percent">&nbsp;5%</div>

				<p class="forpercent">
					* Вы получаете 5% от суммы заказа Вашего реферала. Мы не скрываем от Вас стоимость услуг в отличие от других партнерок, где Вы получаете % от прибыли сервиса. Мы даем % от реальной суммы заказа!
				</p>
				</div>
			</div>
			<div class="col-md-6 col-xs-12" style="padding-top:54px;">
				<div class="percent">&nbsp;20%</div>
				<p class="forpercent">
					* Вы можете получить еще больше выгоды от сотрудничества с нами! Если Ваш реферал тоже стал партнером, Вы получите дополнительные 20% с его дохода. Всю информацию по своим рефералам Вы можете отследить в личном кабинете партнера.
				</p>
			</div>
		</div>
	</div>


	<h2>Как получить деньги?</h2>


	<div class="item">
		<div class="count">1</div>
		<div class="text">Мы предлагаем вывод средств<br>на одну из самых популярных<br>платежных систем - WMR. </div>
	</div>

	<div class="item">
		<div class="count">2</div>
		<div class="text">Вывод средств производится<br>1 раз в месяц, во второй<br>понедельник месяца.</div>
	</div>

	<div class="item">
		<div class="count">3</div>
		<div class="text">Доступна досрочная выплата<br>сумм от 1000 руб. и более в течение<br>3-х рабочих дней.</div>
	</div>

	<div class="item">
		<div class="count">4</div>
		<div class="text">Вы можете использовать заработанные<br>деньги для оплаты своих проектов<br>на сервисе zengram.ru</div>
	</div>

	<h3 style="margin-top:25px; margin-bottom:15px">Начните зарабатывать вместе с нами.<br>
		<span style="font-size:24px; line-height: 35px;">Станьте нашим партнером прямо сейчас!</span>
	</h3>

	<?=Html::a(
		'Стать партнёром',
		['#'],
		[
			'class' => 'btn btn-danger btn-lg confirm-button-1'.(!$authorized ? '' : ' confirm')
		] + $link_data
	)?>


</div>

<div id="partnership-oferta" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
						aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Договор оферта сервиса Zengram</h4>
			</div>
			<div class="modal-body">
				<div class="scroller">
					<?=  $tplOferta->fillTemplate(0); ?>

					<?php ActiveForm::begin(['id' => 'oferta', 'method' => 'post']); ?>
					<input type="hidden" name="confirm_oferta" value="1">

					<div class="pull-right">
						<?= Html::submitButton('С Договором офертой согласен', [
							'class' => 'btn btn-success text-uppercase text-bold',
							'name' => 'comfirmButton',
							'value' => '1',
							'tabindex' => '-1',
						]) ?>
					</div>
					<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div><!-- /.modal -->


<script>

	var oferta = $('#partnership-oferta');


	$(document).ready(function () {
		/*$('nav.nav-top').hide();
		$('.wrap').before($('#partner-head').html());*/

		<?=($modal ? 'oferta.modal(\'show\');' : '')?>

	});

	$(document).on("click", ".confirm", function () {

		oferta.modal('show');

		return false;

	});

</script>
