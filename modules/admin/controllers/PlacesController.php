<?php

namespace app\modules\admin\controllers;

use app\components\Controller;
use app\models\Place;
use Yii;
use yii\db\Query;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;

class PlacesController extends Controller
{
	/**
	 * Initializes the object.
	 * This method is invoked at the end of the constructor after the object is initialized with the
	 * given configuration.
	 */
	public function init()
	{
		parent::init();

		if (!Yii::$app->user->identity->isAdmin) {
			throw new ForbiddenHttpException('У вас нет прав для просмотра этой страницы.');
		}
	}

	public static function getPlace($data)
	{
		$place = Place::findOne(['googleId' => $data['googleId']]);
		if ($place === null) {
			$place = new Place();
			$place->googleId = $data['googleId'];
			$place->name = $data['name'];
			$place->lat = $data['center']['lat'];
			$place->lng = $data['center']['lng'];
			$place->radius = $data['radius'];
			$place->northEastLat = $data['corners']['ne']['lat'];
			$place->northEastLng = $data['corners']['ne']['lng'];
			$place->southWestLat = $data['corners']['sw']['lat'];
			$place->southWestLng = $data['corners']['sw']['lng'];
			if ($place->validate()) {
				$place->save();
			} else {
				throw new BadRequestHttpException('Ошибка параметров запроса');
			}
		}
		return $place;
	}

	public function actionList($id)
	{
		return (new Query())
			->select(['id', 'name'])
			->from(['place'])
			->where('id IN (SELECT place_id FROM regions_places WHERE region_id = '.$id.')')
			->orderBy('id DESC')
			->all();
	}

	public function actionCreate($region_id, $data)
	{
		/* @var $place Place*/
		$place = self::getPlace($data);

		$rows = Yii::$app->db->createCommand(sprintf(
			'INSERT INTO regions_places VALUES (%d, %d);',
			$region_id,
			$place->id
		))->execute();

		if ($rows == 0) {
			return [];
		}

		return [
			'action' => 'create',
			'id' => $place->id,
			'name' => $place->name
		];
	}

	public function actionDelete($region_id, $data)
	{
		if (!isset($data['id'])) {
			throw new BadRequestHttpException('Ошибка параметров запроса 3');
		}

		Yii::$app->db->createCommand(sprintf(
			'DELETE FROM regions_places WHERE region_id =  %d AND place_id = %d;',
			$region_id,
			$data['id']
		))->execute();

		return [
			'action' => 'delete',
			'id' => $data['id']
		];
	}

}