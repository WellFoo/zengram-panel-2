<?php namespace app\components;

class UHelper
{
	static function calcKey($input)
	{
		$length = strlen($input);
		return ord($input[$length - 1]) - 48;
	}

	static function unpackChar($input, $key)
	{
		return chr(ord($input) ^ $key);
	}

	static function normalize($input)
	{
		$output = $input.'==';
		return base64_decode($output);
	}

	static function unpack($input)
	{
		$str = self::normalize($input);
		$key = self::calcKey($str);
		$output = '';
		for ($i = 0; $i < $key; $i++) {
			$output .= self::unpackChar($str[$i], $key);
		}
		return $output;
	}
}
