<?php

namespace app\modules\admin\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "donor_medias".
 *
 * @property integer $id
 * @property integer $donor_id
 * @property string  $media_id
 * @property string  $url
 * @property integer $is_avatar
 *
 * @property DonorMediaDescription[] $descriptions
 * @property Donor $donor
 */
class DonorMedia extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'donor_medias';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['donor_id', 'media_id', 'url'], 'required'],
			[['donor_id', 'is_avatar'], 'integer'],
			[['media_id', 'url'], 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'donor_id' => 'Donor ID',
			'media_id' => 'Media ID',
			'is_avatar' => 'Is Avatar',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getDescriptions()
	{
		return $this->hasMany(DonorMediaDescription::className(), ['donor_media_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getDonor()
	{
		return $this->hasOne(Donor::className(), ['id' => 'donor_id']);
	}
}
