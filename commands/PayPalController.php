<?php namespace app\commands;

use app\models\PaymentHistoryItem;
use PayPal\EBLBaseComponents\PaymentTransactionSearchResultType;
use PayPal\EBLBaseComponents\PaymentTransactionType;
use PayPal\PayPalAPI\GetTransactionDetailsReq;
use PayPal\PayPalAPI\GetTransactionDetailsRequestType;
use PayPal\PayPalAPI\GetTransactionDetailsResponseType;
use PayPal\PayPalAPI\TransactionSearchReq;
use PayPal\PayPalAPI\TransactionSearchRequestType;
use PayPal\PayPalAPI\TransactionSearchResponseType;
use PayPal\Service\PayPalAPIInterfaceServiceService;
use yii\console\Controller;

class PayPalController extends Controller
{
	public function actionFetch()
	{
		$this->import(strtotime('1970-01-01'), time());
	}

	public function actionUpdate()
	{
		$today = strtotime(date('Y-m-d', time()));
		$this->import($today - 86400, time());
	}

	public function import($start, $end)
	{
		$startDate = date('Y-m-d\TH:i:s\Z', $start);
		$endDate = date('Y-m-d\TH:i:s\Z', $end);

		$service  = new PayPalAPIInterfaceServiceService(self::getAcctAndConfig());

		do {
			$requestType = new TransactionSearchRequestType();
			$requestType->StartDate = $startDate;
			$requestType->EndDate = $endDate;

			$searchReq = new TransactionSearchReq();
			$searchReq->TransactionSearchRequest = $requestType;

			/** @var TransactionSearchResponseType $searchResponse */
			/** @noinspection PhpParamsInspection */
			$searchResponse = $service->TransactionSearch($searchReq);
			var_dump($searchResponse);
			if (!is_array($searchResponse->PaymentTransactions) || empty($searchResponse->PaymentTransactions)) {
				break;
			}

			$count = 0;
			foreach ($searchResponse->PaymentTransactions as $txn) {
				$count++;

				/** @var PaymentTransactionSearchResultType $txn */
				if ($txn->Type !== 'Payment' || $txn->Status !== 'Completed') {
					continue;
				}

				PaymentHistoryItem::fromPayPalPayment(
					$this->getTransactionDetails($service, $txn->TransactionID)
				);
			}

			$startDate = $searchResponse->PaymentTransactions[0]->Timestamp;
		} while ($count >= 100);
	}

	/**
	 * @param $service PayPalAPIInterfaceServiceService
	 * @param $id string
	 * @return PaymentTransactionType
	 */
	private function getTransactionDetails($service, $id)
	{
		$transactionDetails = new GetTransactionDetailsRequestType();
		$transactionDetails->TransactionID = $id;

		$request = new GetTransactionDetailsReq();
		$request->GetTransactionDetailsRequest = $transactionDetails;

		/** @var GetTransactionDetailsResponseType $result */
		/** @noinspection PhpParamsInspection */
		$result = $service->GetTransactionDetails($request);
		return $result->PaymentTransactionDetails;
	}

	public static function getConfig()
	{
		return [
			// values: 'sandbox' for testing
			//		   'live' for production
			//         'tls' for testing if your server supports TLSv1.2
			'mode' => 'live',
			// TLSv1.2 Check: Comment the above line, and switch the mode to tls as shown below
			// 'mode' => 'tls'
			'log.LogEnabled' => false,
			'log.FileName' => \Yii::getAlias('@app/runtime/PayPal.log'),
			'log.LogLevel' => 'FINE'

			// These values are defaulted in SDK. If you want to override default values, uncomment it and add your value.
			// 'http.ConnectionTimeOut' => '5000',
			// 'http.Retry' => '2',
		];
	}

	// Creates a configuration array containing credentials and other required configuration parameters.
	public static function getAcctAndConfig()
	{
		$config = [
			// Signature Credential
			'acct1.UserName' => 'support_api1.fast-unfollow.com',
			'acct1.Password' => 'aQX2STUKQA3L5AA78',
			'acct1.Signature' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31Aupvhr2LXNL4GSZRegvDb8DWbTG-',
			// Subject is optional and is required only in case of third party authorization
			// 'acct1.Subject' => '',

			// Sample Certificate Credential
			// 'acct1.UserName' => 'certuser_biz_api1.paypal.com',
			// 'acct1.Password' => 'D6JNKKULHN3G5B8A',
			// Certificate path relative to config folder or absolute path in file system
			// 'acct1.CertPath' => 'cert_key.pem',
			// Subject is optional and is required only in case of third party authorization
			// 'acct1.Subject' => '',

		];

		return array_merge($config, self::getConfig());
	}
}