<?php

use app\models\Account;
use yii\helpers\Html;

/* @var $speeds array
 * @var $account Account
 * @var $status boolean
 */

?>
<div class="acc-pane-inside hidden-xs">
    <div class="col-md-12">
		<div class="row">

			<!-- User info -->
			<figure class="inside-acc-img-set text-center col-sm-2">
				<figcaption>
					<strong>@<?= $account->login; ?></strong>
				</figcaption>
				<img class="acc-ava" src="<?= str_replace('http://', '//', $account->account_avatar) ?>">
				<a class="acc-forgot-pass changeAccountAction" data-id="<?= $account->id; ?>" href="javascript:void"><?= Yii::t('views', 'Change password') ?></a>
			</figure>
			<dl class="inside-acc-subscr-set col-sm-3">
				<dt>
					<i class="fa fa-photo"></i> <?= Yii::t('views', 'Photo') ?>
				</dt>
				<dd id="account_media_<?= $account->id; ?>"><?= $account->account_media; ?></dd>
				<dt>
					<i class="fa fa-users"></i> <?= Yii::t('views', 'Followers') ?>
				</dt>
				<dd id="account_followers_<?= $account->id; ?>"><?= $account->account_followers; ?></dd>
				<dt class="red-inner">
					<i class="fa fa-user"></i> <?= Yii::t('views', 'New followers') ?>
				</dt>
				<dd class="red-inner">+<span style="display: inline" id="lastfollowers_<?= $account->id; ?>"><?= $account->getLastFollowers() ?></span></dd>
				<dt>
					<i class="fa fa-user-plus"></i> <?= Yii::t('views', 'Followings') ?>
				</dt>
				<dd id="account_follows_<?= $account->id; ?>"><?= $account->account_follows; ?></dd>
			</dl>
			<dl class="inside-acc-subscr-set col-sm-3 col-lg-2">
				<dt>
					<i class="fa fa-heart"></i> <?= Yii::t('views', 'Likes') ?>
				</dt>
				<dd id="likes_<?= $account->id; ?>"><?= ($account->options->likes && !$account->options->unfollow) ? $account->likes : '-' ?></dd>
				<dt>
					<i class="fa fa-comment"></i> <?= Yii::t('views', 'Comments') ?>
				</dt>
				<dd id="comments_<?= $account->id; ?>"><?= ($account->options->comment && !$account->options->unfollow) ? $account->comments : '-' ?></dd>
				<dt>
					<i class="fa fa-check"></i> <?= Yii::t('views', 'Follows') ?>
				</dt>
				<dd id="follow_<?= $account->id; ?>"><?= ($account->options->follow && !$account->options->unfollow) ? $account->follow : '-' ?></dd>
				<dt>
					<i class="fa fa-close"></i> <?= Yii::t('views', 'Unfollows') ?>
				</dt>
				<dd id="unfollow_<?= $account->id; ?>"><?= $account->options->unfollow ? $account->unfollow : '-' ?></dd>
			</dl>
			<!-- User info end -->

			<!-- Action settings -->
			<div class="acc-act-set col-sm-4 col-lg-5" data-account-container="<?= $account->id ?>">
				<figure class="a_action">
					<i class="hidden-sm fa fa-toggle-on"></i>
					<a class="hidden-sm helper" href="#" data-toggle="popover" data-title="<?= Yii::t('views', 'Action') ?>"
					   data-content="<?= Yii::t('views', 'Here you can start or stop work with project') ?>"
					   data-original-title="" title=""><i class="fa fa-question-circle"></i></a>
					<figcaption class="hidden-sm"><?= Yii::t('views', 'Action') ?></figcaption>
					<a class="btn btn-success col-md-12 col-sm-12 col-xs-12 projectStart" data-account="<?= $account->id; ?>" data-login="<?= $account->login; ?>"
					   id="start_<?= $account->id; ?>"<?php if ($status == 1) {
						echo ' style="display: none;"';
					} ?>>
					<span<?php if ($account->message) {
						echo ' style="display: block;"';
					} ?> data-toggle="popover" data-title="<?= Yii::t('views', 'Project stopped') ?>"
						 data-content="<?= $account->getErrorMessage(); ?>"
						 class="read-message glyphicon glyphicon-info-sign" aria-hidden="true"></span>
						<?= Yii::t('views', 'Start') ?>
					</a>
					<a class="btn btn-danger col-md-12 col-sm-12 col-xs-12 projectStop" data-account="<?= $account->id; ?>"
					   id="stop_<?= $account->id; ?>"<?php if ($status == 0) {
						echo ' style="display: none;"';
					} ?>>
						<?= Yii::t('views', 'Stop') ?>
					</a>
				</figure>
				<figure class="a_speed">
					<i class="hidden-sm fa fa-tachometer"></i>
					<a  class="hidden-sm helper" href="#" data-toggle="popover" data-title="<?= Yii::t('views', 'Speed of work') ?>"
					   data-content="<?= Yii::t('views', 'Service work speed. We are highly recommending to choose Slow speed for first days and to increase speed with time') ?>"
					   data-original-title="" title=""><i class="fa fa-question-circle"></i></a>
					<figcaption class="hidden-sm"><?= Yii::t('views', 'speed of work') ?></figcaption>
					<div class="btn-group">
						<a class="col-md-12 col-sm-12 col-xs-12 btn btn-default btn-white dropdown-toggle"
						   id="speedButton_<?= $account->id; ?>"
						   data-toggle="dropdown"><?= $speeds[$account->options->speed]; ?> <i
								class="dd fa fa-angle-down"></i></span></a>
						<ul class="dropdown-menu" role="menu">
							<li>
								<a href="javascript:void" data-value="0" data-id="<?= $account->id; ?>" class="setSpeed">
									<?= Yii::t('views', 'Slow') ?>
								</a>
							</li>
							<li>
								<a href="javascript:void" data-value="1" data-id="<?= $account->id; ?>" class="setSpeed">
									<?= Yii::t('views', 'Medium') ?>
								</a>
							</li>
							<li>
								<a href="javascript:void" data-value="2" data-id="<?= $account->id; ?>" class="setSpeed">
									<?= Yii::t('views', 'Fast') ?>
								</a>
							</li>
						</ul>
					</div>

				</figure>
				<figure class="<?= $account->user->use_timer ? '' : 'hide'  ?>">
					<i class="hidden-sm fa fa-clock-o"></i>
					<a  class="hidden-sm helper" href="#" data-toggle="popover" data-title="<?= Yii::t('views', 'Timer of days') ?>"
					   data-content="<?= Yii::t('views', 'Time is counting down only when your activity is started. When time ends, work will be stopped') ?>"
					   data-original-title="" title=""><i class="fa fa-question-circle"></i></a>
					<figcaption class="hidden-sm"><?= Yii::t('views', 'timer of days') ?></figcaption>

					<div class="btn-group" <?= (!$status ? 'style="display: none;"' : '') ?> id="timerButtonDisabled_<?= $account->id; ?>"
					     data-toggle="popover" data-original-title="" data-title="<?= Yii::t('views', 'Unavailable') ?>"
					     data-content="<?= Yii::t('views', 'You must stop works in account to change timer') ?>">
						<a class="col-md-12 col-sm-12 col-xs-12 btn btn-default btn-white dropdown-toggle disabled timerButtonDisabled text-center">
							<?= $account->getTimerText() ?>
							<i class="dd fa fa-angle-down"></i>
						</a>
					</div>
					<div class="btn-group" <?= ($status ? 'style="display: none;"' : '') ?> id="timerButton_<?= $account->id; ?>">
						<a class="col-md-12 col-sm-12 col-xs-12 btn btn-default btn-white dropdown-toggle timerButton" data-toggle="dropdown">
							<?= $account->getTimerText() ?>
							<i class="dd fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu timer-dropdown" role="menu">
							<?php foreach (Account::getTimerValues() as $value => $timer): ?>
								<li><a data-value="<?= $value ?>" data-id="<?= $account->id ?>" href="javascript:void"
								       class="setTimer"><?= $timer ?></a></li>
							<?php endforeach; ?>
						</ul>
					</div>
				</figure>
				<figure>
					<i class="hidden-sm fa fa fa-cogs"></i>
					<a class="hidden-sm helper" href="#" data-toggle="popover" data-title="<?= Yii::t('views', 'Settings') ?>"
					   data-content="<?= Yii::t('views', 'Here you can go to the settings and change action&#39;s configuration, locations, comments and other settings') ?>"
					   data-original-title="" title=""><i class="fa fa-question-circle"></i></a>
					<figcaption class="hidden-sm"><?= Yii::t('views', 'Settings') ?></figcaption>
					<?= Html::a(Yii::t('views', 'Settings'), [
						'options/index',
						'id' => $account->id],
						[
							'class' => 'btn btn-default btn-gray col-md-12 col-sm-12 col-xs-12 optionsButton',
							'id' => 'optionsButton_'.$account->id
						]
					); ?>
				</figure>
			</div>
			<!-- Action settings -->
		</div>
    </div>
</div>