<?php
/* @var $this yii\web\View */
use yii\helpers\Html;

/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\RegisterForm */
/* @var $stataCounters array */

$this->title = Yii::t('landing', 'Get More Instagram Followers App | Instagram Promotion App');
$this->registerMetaTag([
	'name' => 'description',
	'content' => Yii::t('landing', 'Zen-promo, the finest Instagram follower App helps in fast promotion of your account by following, unfollowing, liking & commenting on your behalf. Signup now!')
], 'description');

if (LANGUAGE == 'en') {
	$this->registerMetaTag([
		'name' => 'keywords',
		'content' => 'Get More Instagram Followers App, Instagram Promotion App, Promote Instagram Followers, Get Instagram Followers Fast, How To Get Followers On Instagram, Get More Followers, Get More Instagram Followers, Instagram Promotion, Promote Instagram, Instagram Followers, Increase Instagram Followers'
	]);
}

/*$this->registerMetaTag([
	'name' => 'keywords',
	'content' => Yii::t('landing', 'Buy Real Instagram Followers, Followers On Instagram, Get Instagram Followers, Get Likes on Instagram App, Get More Instagram Followers, How to Get Followers on Instagram, Instagram Followers, Instagram Followers App, More Followers on Instagram, Real Instagram Followers')
], 'keywords');*/
$this->registerJsFile('/js/jquery.bxslider.min.js');
$this->registerCssFile('/css/jquery.bxslider.css');
?>

<!--/sprypay.tag.check:571a7285d9bb4/-->
<script>

	var timer = 0;
	// Переменные, для хранения переменных счётчика с павающей точкой
	var likesFloat = 0;
	var commentsFloat = 0;
	var followsFloat = 0;
	var clientsFloat = 0;

	var percentageOfSeconds = 10;

	var timerActions = {
		'comments': calcSpeed(<?= $stataCounters['comments']['freq'] ?>),
		'likes':    calcSpeed(<?= $stataCounters['likes']['freq'] ?>),
		'follows':  calcSpeed(<?= $stataCounters['follows']['freq'] ?>),
		'clients':  calcSpeed(<?= $stataCounters['clients']['freq'] ?>)
	};

	function calcSpeed(num)
	{
		return num / 7 / 24 / 3600 / percentageOfSeconds;
	}

	$(document).ready(function() {

		$(document).ajaxComplete(function (event, xhr) {
			var url = xhr.getResponseHeader('X-Redirect');
			if (url) {
				window.location = url;
			}
		});

		console.log(timerActions['comments']);
		console.log(timerActions['likes']);
		console.log(timerActions['follows']);
		console.log(timerActions['clients']);

		var likesCount = $('#likesCount');
		var commentsCount = $('#commentsCount');
		var followsCount = $('#followsCount');
		var clientsCount = $('#clientsCount');

		// Инициализация начальных значений, исходя из текущего времени
		var currentTimestamp = <?= time() ?>;
		var differentTime = currentTimestamp - <?= $stataCounters['timestamp_genetated']; ?>

		likesCount.html(Math.round(<?= $stataCounters['likes']['count'] ?> + differentTime*timerActions['likes']*percentageOfSeconds));
		followsCount.html(Math.round(<?= $stataCounters['follows']['count'] ?> + differentTime*timerActions['follows']*percentageOfSeconds));
		commentsCount.html(Math.round(<?= $stataCounters['comments']['count'] ?> + differentTime*timerActions['comments']*percentageOfSeconds));
		clientsCount.html(Math.round(<?= $stataCounters['clients']['count'] ?> + differentTime*timerActions['clients']*percentageOfSeconds));

		likesFloat = parseInt(likesCount.html());
		commentsFloat = parseInt(commentsCount.html());
		followsFloat = parseInt(followsCount.html());
		clientsFloat = parseInt(clientsCount.html());

		function updateTimer() {
			timer++;

			likesFloat += timerActions['likes'];
			likesCount.html(Math.round(likesFloat ));

			commentsFloat += timerActions['comments'];
			commentsCount.html(Math.round(commentsFloat));

			followsFloat += timerActions['follows'];
			followsCount.html(Math.round(followsFloat));

			clientsFloat += timerActions['clients'];
			clientsCount.html(Math.round(clientsFloat));

			setTimeout(updateTimer, 100);
		}


		$('.bxslider').bxSlider({
			pagerCustom: '#bx-pager'
		});
		//$("a[href^='/page/price']").attr('href', '#price-title');

		setTimeout(updateTimer, 1000);
		$('.price-table td').click(function(){
			$('#regModal').modal();
		});

	});


</script>
<?php if (NO_DB_CONNECTION): ?>
	<!--	<div class="alert alert-danger h1 text-center" role="alert" style="margin-bottom: 30px; margin-right: -15px; margin-left: -15px;">-->
	<div id="technicalWorksModal" class="modal fade in bs-example-modal-lg" tabindex="-1" role="dialog"
	     aria-labelledby="myLargeModalLabel"
	     aria-hidden="true">
		<div class="modal-dialog modal-lg accountErrorModal">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
						aria-hidden="true">&times;</span></button>
				<h2 class="text-center"
				    style="margin: 0; line-height: 1.428571429; font-size: 18px !important; color: #ff6666;">
					<?= Yii::t('landing', 'Technical works') ?>
				</h2>

				<p class="text-center"
				   style="color: #ff6666; margin: 25px auto 45px auto; font-size: 18px; line-height: 25px; font-family: 'Open Sans', sans-serif;">
					<?= Yii::t('landing', 'Dear customer, there are technical works to improve the site functionality at the moment. Zengram will be unavailable for about 10 minutes. Please try again later. We apologize for any inconvenience.') ?>
				</p>
			</div>
		</div>
	</div>
	<script>
		$(document).ready(function () {
			$('#technicalWorksModal').modal('show');
		});
	</script>
	<!--	</div>-->
<?php endif; ?>
<div class="landing row">
	<!-- Main block -->
	<section id="landMainBlock" class="container land-main-block">
		<div class="row">

			<div class="land-main-block-text col-xs-12 col-sm-6 col-md-6">
				<div class="row">
					<h1><?= Yii::t('landing', 'Build up yourself in<br>Instagram') ?></h1>

					<p><?= Yii::t('landing', 'Zengram promotes your Instagram<br/>by liking, commenting,<br/>following and unfollowing<br/>on your behalf') ?></p>

					<p><?= Yii::t('landing', 'Free trial for 3 days') ?></p>

					<div class="text-center col-md-7">
						<a class="btn btn-danger btn-lg" data-toggle="modal"
						   data-target="#regModal"><?= Yii::t('app', 'Sign up') ?></a>

						<div class="clearfix"></div>
						<a class="log-in" data-toggle="modal"
						   data-target="#loginModal"><?= Yii::t('app', 'Log in') ?></a>
					</div>
				</div>
			</div>

			<div class="land-main-block-note col-xs-12 col-sm-6 col-md-6">
				<div class="row">
					<div class="land-note">
						<?php $img_alt = str_replace('<br>', ' ', Yii::t('landing', 'First 3 days are free')); ?>
						<?= Html::img(['@img/landing/main-block/note.png'], ['class' => 'hidden-xs', 'alt' => $img_alt, 'title' => $img_alt]) ?>
						<?= Html::img(['@img/landing/main-block/note_mob.png'], ['class' => 'visible-xs', 'alt' => $img_alt, 'title' => $img_alt]) ?>
						<a class="btn btn-danger btn-lg" data-toggle="modal"
						   data-target="#regModal"><?= Yii::t('landing', 'First 3 days are free') ?></a>
					</div>
				</div>
			</div>

			<div class="clearfix"></div>
			<?php if (Yii::$app->language == 'ru'): ?>
			<div class="items">
				<div class="item">
					<p>Зарегистрировано<span id="clientsCount"></span>клиентов</p>
				</div>
				<div class="item">
					<p>Размещено<span id="commentsCount"></span>комментариев</p>
				</div>
				<div class="item">
					<p>Сделано<span id="followsCount"></span>подписок</p>
				</div>
				<div class="item">
					<p>Поставлено<span id="likesCount"></span>лайков</p>
				</div>
				<div class="item">
					<p>Заблокировано <span>0</span>аккаунтов</p>
				</div>
			</div>
			<?php else: ?>
			<div class="text-center">
				<a href="#advantages" class="land-detail btn text-uppercase btn-lg hidden-xs"><?= Yii::t('landing', 'Details') ?></a>
			</div>
			<?php endif;  ?>

		</div>
	</section>
	<!-- Main block end -->

	<!-- Advantages -->
	<section id="advantages" class="container advantages">
		<div class="row">
			<h2><?= Yii::t('landing', 'Our advantages') ?></h2>

			<figure class="advantages-item col-xs-12 col-sm-6 col-md-3">
				<img src="/img/landing/advantages/1.png"
				     alt="<?= Yii::t('landing', 'Buy Real Instagram Followers') ?>"/>
				<figcaption><?= Yii::t('landing', 'Start your 3-day trial right now completely free of charges!') ?></figcaption>
				<p><?= Yii::t('landing', 'Start your 3-day trial right now completely free of charges!') ?></p>
			</figure>

			<figure class="advantages-item col-xs-12 col-sm-6 col-md-4 col-md-offset-1">
				<img src="/img/landing/advantages/2.png" alt="<?= Yii::t('landing', 'Followers On Instagram') ?>"/>
				<figcaption>
					<?= Yii::t('landing', 'Get Instagram followers <br class="hidden-xs">from your city') ?>
				</figcaption>
				<p><?= Yii::t('landing', 'You choose the city from where you want Zengram to attract followers! It’s possible to target multiple locations to expand your reach.') ?></p>
			</figure>

			<figure class="advantages-item col-xs-12 col-sm-6 col-md-3 col-md-offset-1">
				<img src="/img/landing/advantages/3.png" alt="<?= Yii::t('landing', 'Get Instagram Followers') ?>"/>
				<figcaption><?= Yii::t('landing', 'How To Get More Followers On Instagram? It&#39;s so easy') ?></figcaption>
				<p><?= Yii::t('landing', 'To start Zengram, it&#39;s enough to choose the city, actions (to like, follow and comment) and run the project.') ?></p>
			</figure>

			<figure class="advantages-item col-xs-12 col-sm-6 col-md-3">
				<img src="/img/landing/advantages/4.png" alt="<?= Yii::t('landing', 'Get Likes on Instagram App') ?>"/>
				<figcaption><?= Yii::t('landing', 'Start and forget') ?></figcaption>
				<p><?= Yii::t('landing', 'Zengram works on our server, so a browser is the only thing you need. Once Zengram is launched, you can turn off the device: the service will continue the work and you will still get more Instagram followers.') ?></p>
			</figure>

			<figure class="advantages-item col-xs-12 col-sm-6 col-md-4 col-md-offset-1">
				<img src="/img/landing/advantages/5.png"
				     alt="<?= Yii::t('landing', 'Get More Instagram Followers') ?>"/>
				<figcaption><?= Yii::t('landing', 'Perfect safety') ?></figcaption>
				<p><?= Yii::t('landing', 'We work using the secure protocol and we don&#39;t store any user data. It is absolutely safe to work with us!') ?></p>
			</figure>

			<figure class="advantages-item col-xs-12 col-sm-6 col-md-3 col-md-offset-1">
				<img src="/img/landing/advantages/6.png"
				     alt="<?= Yii::t('landing', 'How to Get Followers on Instagram') ?>"/>
				<figcaption><?= Yii::t('landing', 'Fast support') ?></figcaption>
				<p><?= Yii::t('landing', 'You will get support anytime you need it. Feel free to contact us if you have any questions.') ?></p>
			</figure>
		</div>
	</section>
	<!-- Main block end -->

	<!-- About us -->
	<section id="aboutUs" class="container-fluid about-us">

		<section class="container">
			<div class="row">
				<h2><?= Yii::t('landing', 'About us') ?></h2>

				<div class="col-xs-12 col-sm-8 col-md-5 col-xs-offset-0 col-sm-offset-4 col-md-offset-7">
					<div class="row">
						<p><?= Yii::t('landing', 'Zengram is designed to attract attention to your Instagram account. Basically, it&#39;s an app aimed at getting you more followers on Instagram. In even simpler words, it&#39;s made so you can forget the question «How to Get Followers on Instagram?». You will get more Instagram comments, likes and, of course, real Instagram followers.') ?></p>

						<p><?= Yii::t('landing', 'Zengram can help you to get Instagram followers fast, even today. We have done a great job to introduce you this service, that is not just a regular Instagram followers app. It&#39;s a powerful tool able not only to increase your following but also to get real Instagram comments, which indicates the highest level of engagement. There was a project for professional users in a private access for several years. Now we have made it clear, user-friendly and open to public. It stood the test of time and was acknowledged by professional promoters as a complex tool to grow Instagram followers. It does not matter if your account is for personal or business use, Zengram will render your work more efficient. If you have no time to constantly like, comment and follow manually - Zengram will assist you. By the way, if you want you can also buy real Instagram followers, in this case, Zengram will pick the best audience and will do everything for you to go viral. You will become a popular Instagram user with Zengram!') ?></p>
					</div>
				</div>
			</div>
		</section>

	</section>
	<!-- About us end -->

	<?php if (Yii::$app->language == 'ru' || Yii::$app->language == 'rf'): ?>

		<section id="preim" class="container-fluid preim">

			<section class="container">
				<div class="row">
					<h2>Возможности <br class="visible-xs">нашего сервиса</h2>

					<div class="items" id="bx-pager">
						<a class="item" data-slide-index="0" href="#">
							<i class="fa fa-check"></i>

							<p>Подписки и лайки</p>
						</a>
						<a class="item" data-slide-index="1" href="#">
							<i class="fa fa-close"></i>

							<p>Отписки</p>
						</a>
						<a class="item" data-slide-index="2" href="#">
							<i class="fa fa-commenting-o"></i>

							<p>Комментарии</p>
						</a>
						<a class="item" data-slide-index="3" href="#">
							<i class="fa fa-map-marker"></i>

							<p>Таргетинг</p>
						</a>
						<a class="item" data-slide-index="4" href="#">
							<i class="fa fa-map-marker"></i>

							<p>Статистика</p>
						</a>
						<a class="item" data-slide-index="5" href="#">
							<i class="fa fa-heart"></i>

							<p>Взаимные лайки</p>
						</a>
					</div>
					<div class="clearfix"></div>
					<div class="bxslider">
						<div class="slide">
							<div class="col-md-5 desc">
								<p class="title">Подписки и лайки</p>

								<p>
									<?php if (Yii::$app->language == 'ru') { ?>
									Ваши лайки на интересующих аккаунтах будут появляться автоматически, что вызовет
									интерес
									у пользователей, которые в ответ могут подписаться на Вас и оставить Вам много
									взаимных
									лайков.<br/>
									<?php } ?>
									Поиск аудитории осуществляется с использованием таргетинга, который Вы
									можете задать в настройках. Все эти параметры помогут подписаться и ставить лайки
									только
									целевой аудитории.
								</p>
							</div>
							<div class="col-md-7" style="overflow:hidden;">
								<img src="./img/landing/screen.png" height="450">
							</div>
						</div>
						<div class="slide">
							<div class="col-md-5 desc">
								<p class="title">Отписки</p>

								<p>Не дождались взаимной подписки? При достижении лимита Вы сможете воспользоваться
									настройкой автоматических отписок.</p>
							</div>
							<div class="col-md-7" style="overflow:hidden;">
								<img src="./img/landing/screen2.png">
							</div>
						</div>
						<div class="slide">
							<div class="col-md-5 desc">
								<p class="title">Комментарии</p>

								<p>Выбирайте интересующую Вас аудиторию по настройкам таргетинга и отправляйте активным
									пользователям Instagram свои комментарии. Чем больше Ваших комментариев, тем больше
									интереса к Вашему аккаунту, а значит, больше подписчиков и клиентов.</p>
							</div>
							<div class="col-md-7" style="overflow:hidden;">
								<img src="./img/landing/screen3.png">
							</div>
						</div>
						<div class="slide">
							<div class="col-md-5 desc">
								<p class="title">Таргетинг</p>

								<p>
									<?php if (Yii::$app->language == 'ru') { ?>
									Настройки таргетинга помогут Вам найти определенную аудиторию по конкретным параметрам: аккаунтам со схожей тематикой, хештегам, городу или региону.<br/>
									В результате Вы сможете сами подписаться и получить взаимные подписки от потенциальных клиентов, которые регулярно пользуются Instagram.
									<?php } else { ?>
									Настройки таргетинга помогут Вам найти определенную аудиторию по конкретным параметрам: аккаунтам со схожей тематикой, хештегам, городу или региону.
									В результате Вы сможете сами подписаться и получить взаимные подписки от тех людей, которые регулярно пользуются Instagram.
									<?php } ?>
								</p>
							</div>
							<div class="col-md-7" style="overflow:hidden;">
								<img src="./img/landing/screen4.png">
							</div>
						</div>
						<div class="slide">
							<div class="col-md-5 desc">
								<p class="title">Статистика</p>

								<p>Оценить эффективность лайков, подписок и комментариев можно благодаря динамике
									статистики
									Вашего аккаунта. <br/>Детальная и доступная для понимания статистика расскажет о
									ежедневных успехах и поможет проанализировать особенности потенциальных клиентов.
								</p>
							</div>
							<div class="col-md-7" style="overflow:hidden;">
								<img src="./img/landing/screen5.png">
							</div>
						</div>
						<div class="slide">
							<div class="col-md-5 desc">
								<p class="title">Взаимные лайки и лайки своим</p>

								<p>
									<?php if (Yii::$app->language == 'ru') { ?>
									Хотите сделать подписчиков своими клиентами?<br/>
									Проявляйте больше внимания к ним вместе с Zengram.<br/>
									Программа автоматически оставит лайк на последнем фото всех аккаунтов, которые поставили лайки Вам.<br/>
									Если же пользователь подписался на Вас, на всех его новых фото также будут появляться лайки от Вашего аккаунта.
									<?php } else { ?>
									Проявляйте больше внимания вместе с Zengram.<br/>
									Программа автоматически оставит лайк на последнем фото всех аккаунтов, которые поставили лайки Вам.<br/>
									Если же пользователь подписался на Вас, на всех его новых фото также будут появляться лайки от Вашего аккаунта.
									<?php } ?>
								</p>
							</div>
							<div class="col-md-7" style="overflow:hidden;">
								<img src="./img/landing/screen6.png">
							</div>
						</div>
					</div>
				</div>
			</section>

		</section>

		<?php
	else:
		?>

		<!-- Screenshots -->
		<section id="screenshots" class="container-fluid screenshots">

			<section class="container">
				<div class="row">
					<h2><?= Yii::t('landing', 'Screenshots<br class="visible-xs"/> of our service') ?></h2>

					<div class="col-xs-12 col-sm-4 col-md-4">
						<div class="row">
							<?= Html::img(['@img/landing/screenshots/1.jpg'], ['alt' => Yii::t('app', 'Zengram'), 'title' => Yii::t('app', 'Zengram')]) ?>
						</div>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4">
						<div class="row">
							<?= Html::img(['@img/landing/screenshots/2.jpg'], ['alt' => Yii::t('app', 'Zengram'), 'title' => Yii::t('app', 'Zengram')]) ?>
						</div>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4">
						<div class="row">
							<?= Html::img(['@img/landing/screenshots/3.jpg'], ['alt' => Yii::t('app', 'Zengram'), 'title' => Yii::t('app', 'Zengram')]) ?>
						</div>
					</div>
				</div>
			</section>

		</section>
		<!-- Screenshots end -->
		<?php
	endif;
	?>
	<!-- How it's work -->
	<section id="howWork" class="container-fluid how-work">

		<section class="container">
			<div class="row">
				<h2><?= Yii::t('landing', 'How it works') ?></h2>

				<figure class="how-item col-xs-12 col-sm-6 col-md-6">
					<div class="row">
						<img src="/img/landing/how-work/1.jpg" alt="<?= Yii::t('landing', 'Instagram Followers') ?>"/>
						<figcaption><?= Yii::t('landing', '1. Add an Instagram <br class="visible-xs">account.') ?></figcaption>
						<p><?= Yii::t('landing', 'You just need to link your Instagram to your Zengram account, and it will appear on the control panel. It&#39;s as easy as that!') ?></p>
					</div>
				</figure>

				<div class="clearfix"></div>

				<figure class="how-item col-xs-12 col-sm-6 col-md-6 col-sm-push-6">
					<div class="row">
						<img src="/img/landing/how-work/2.jpg"
						     alt="<?= Yii::t('landing', 'Instagram Followers App') ?>"/>
						<figcaption><?= Yii::t('landing', '2. Set up your actions.') ?></figcaption>
						<p><?= Yii::t('landing', 'You need to choose the way to discover new users — geography or hashtag search, or to copy someone else&#39;s followers. Then define the preferred actions: liking, following, commenting, unfollowing. That&#39;s all! The account is ready to get famous.') ?></p>
					</div>
				</figure>

				<figure class="how-item col-xs-12 col-sm-6 col-md-6  col-sm-pull-6">
					<div class="row">
						<img src="/img/landing/how-work/3.jpg"
						     alt="<?= Yii::t('landing', 'More Followers on Instagram') ?>"/>
						<figcaption><?= Yii::t('landing', '3. Relax and enjoy yourself.') ?></figcaption>
						<p><?= Yii::t('landing', 'You just need to click the "start" button. The rest Zengram will do automatically on your behalf. You can even switch off the computer: everything works on our servers, so the flow of followers on Instagram of yours will not cease.') ?></p>
					</div>
				</figure>

				<div class="text-center">
					<a class="btn btn-danger btn-lg" data-toggle="modal"
					   data-target="#regModal"><?= Yii::t('app', 'Sign up') ?></a>
				</div>

			</div>
		</section>

	</section>
	<!-- How it's work end -->
	<?php if (Yii::$app->language == 'ru'): ?>
	<section id="price" class="container-fluid price">
		<div class="container">
			<div class="row">
				<h2><span id="price-title">Цены</span></h2>
				<p class="text-left price-text">Здесь вы можете купить нужное количество времени для работы zengram.<br/>Чем больший пакет вы выбираете, тем меньше стоит для Вас время.	</p>
				<div class="table-responsive">
					<table class="table table-striped price-table">
						<tr>
							<th>количество дней</th>
							<th>стоимость</th>
							<th>экономия</th>
							<th></th>
						</tr>
						<tr>
							<td>3</td>
							<td>99 <span class="fa fa-rub"></span></td>
							<td>0%</td>
							<td><a href="#price1">Выбрать</a></td>
						</tr>
						<tr>
							<td>10</td>
							<td>279 <span class="fa fa-rub"></span></td>
							<td>15%</td>
							<td><a href="#price2">Выбрать</a></td>
						</tr>
						<tr class="green">
							<td>30 <div class="popular"></div></td>
							<td>699 <span class="fa fa-rub"></span></td>
							<td>30%</td>
							<td><a href="#price3">Выбрать</a></td>
						</tr>
						<tr>
							<td>60</td>
							<td>1 099 <span class="fa fa-rub"></span></td>
							<td>45%</td>
							<td><a href="#price4">Выбрать</a></td>
						</tr>
						<tr>
							<td>90</td>
							<td>1 499 <span class="fa fa-rub"></span></td>
							<td>50%</td>
							<td><a href="#price5">Выбрать</a></td>
						</tr>
						<tr>
							<td>180</td>
							<td>2 699 <span class="fa fa-rub"></span></td>
							<td>55%</td>
							<td><a href="#price6">Выбрать</a></td>
						</tr>
						<tr>
							<td>365</td>
							<td>4 999 <span class="fa fa-rub"></span></td>
							<td>60%</td>
							<td><a href="#price7">Выбрать</a></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</section>
	<?php endif; ?>
	<!-- Feedback -->
	<section id="feedback" class="container-fluid feedback">

		<section class="container">
			<div class="row">
				<h2><?= Yii::t('landing', 'Feedback') ?></h2>

				<div class="col-xs-12 col-sm-4 col-md-3">
					<div class="row" style="margin-top: 50px;">
						<?php $img_alt = Yii::t('landing', 'Medo') . ' ' . Yii::t('landing', 'Wedding planner'); ?>
						<?= Html::img(['@img/landing/feedback/cimage000548.jpg'], ['class' => 'user-image', 'alt' => $img_alt, 'title' => $img_alt]) ?>
						<blockquote>
							<?= Yii::t('landing', 'My name is Medo and I am a Wedding Planner, organizing events in exotic corners of our planet. By using the service Zengram I&#39;ve finally managed to promote my account. Although I have tried a lot of services and ways of promotion before, they all failed to actually get real Instagram followers. In Zengram all functions are very comfortable and understandable, and what&#39;s most important for me is that Instagram followers come from a certain city!') ?>
						</blockquote>
						<p class="sign"><?= Yii::t('landing', 'Medo') ?></p>

						<p><?= Yii::t('landing', 'Wedding planner') ?></p>
					</div>
					<div class="separate"></div>
				</div>


				<div class="hidden-xs col-sm-4 col-md-6">
					<img src="/img/landing/feedback/girl.jpg" alt="<?= Yii::t('landing', 'Real Instagram Followers') ?>"
					     style="margin-top: 150px;">
				</div>

				<div class="col-xs-12 col-sm-4 col-md-3">

					<div class="row mtm50" style="margin-bottom: 70px;">
						<?php $img_alt = Yii::t('landing', 'Megan') . ' ' . Yii::t('landing', 'Interior designer'); ?>
						<?= Html::img(['@img/landing/feedback/cimage002041.jpg'], ['class' => 'user-image', 'alt' => $img_alt, 'title' => $img_alt]) ?>
						<blockquote>
							<?= Yii::t('landing', 'A unique service that saves a lot of time. Now I do not have to spend all day on my phone trying to figure out the way how to get more likes on Instagram. The service imitates my social activity, and it does this thoroughly! Not a single Instagram follower has ever suspected anything. It has become much easier to promote my Instagram blog for interior&#39;s design.') ?>
						</blockquote>
						<p class="sign"><?= Yii::t('landing', 'Megan') ?></p>

						<p><?= Yii::t('landing', 'Interior designer') ?></p>
					</div>

					<div class="separate"></div>

					<div class="row">
						<?php $img_alt = Yii::t('landing', 'Gabriel and Madison') . ' ' . Yii::t('landing', 'Cafe owners'); ?>
						<?= Html::img(['@img/landing/feedback/cimage094560.jpg'], ['class' => 'user-image', 'alt' => $img_alt, 'title' => $img_alt]) ?>
						<blockquote>
							<?= Yii::t('landing', 'We are the owners of a small cafe in the centre of Sheffield, struggling to get followers on Instagram. Previously, for the promotion of our Instagram profile we paid about 120 pounds and were glad that it was so cheap) After switching to Zengram we have the same result for $80. A substantial difference, isn&#39;t it? This is definitely the best app to get followers on Instagram. My wife and I are very satisfied!') ?>
						</blockquote>
						<p class="sign"><?= Yii::t('landing', 'Gabriel and Madison') ?></p>

						<p><?= Yii::t('landing', 'Cafe owners') ?></p>
					</div>

				</div>

			</div>
		</section>

	</section>

</div>
