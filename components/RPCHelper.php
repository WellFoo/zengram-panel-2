<?php namespace app\components;

use app\models\Account;
use app\models\BalanceFlow;
use app\models\Demo;
use app\models\Proxy;
use app\models\Users;
use app\models\UsersInfo;
use Yii;
use yii\db\Expression;

class RPCHelper
{
	/**
	 * @param Account $account
	 * @param string $function
	 * @param array $args
	 * @param string|null $server
	 * @param int $timeout
	 * @return array|null
	 */
	public static function runAccountTask($account, $function, $args = [], $server = null, $timeout = 30)
	{
		$queue_name = Yii::$app->params['rabbit_server']['queue_name'];
		if (!is_null($server)) {
			$queue_name .= '_'.$server;
		}
		$task = new RPCClient($queue_name);
		return $task->call($function, [
			'args'     => $args,
			'settings' => self::getServerParams($account),
		], $timeout);
	}

	/**
	 * @param string $server
	 * @param string $function
	 * @param array $args
	 * @param int $timeout
	 * @return array|null
	 */
	public static function runServerTask($server, $function, $args = [], $timeout = 30)
	{
		$queue_name = Yii::$app->params['rabbit_server']['queue_name'].'_'.$server;
		$task = new RPCClient($queue_name);
		return $task->call($function, [
			'args' => $args,
			'settings' => []
		], $timeout);
	}

	/**
	 * @param Account $account
	 * @return array
	 */
	public static function getServerParams($account)
	{
		if ($account->proxy_id) {
			if ($account->proxy->status === Proxy::STATUS_ERROR) {
				$account->proxy_id = $account->getFreeProxyId();
				$account->save();
			}
			$proxy = Account::getProxyString($account->proxy_id);
		} else {
			$proxy = '';
		}

		$user_id = Users::retrieveInfo('id', 0);
		if (!$user_id) {
			$user_id = $account->user_id;
		}

		if (empty($account->ip)) {
			if ($ip = $account->generateIp($user_id)){
				$account->ip = $ip;
				if ($account->id) {
					$account->save();
				}
			} else {
				$account->ip = sprintf('192.168.%d.%d', rand(0, 255), rand(1, 254));
			}
		}

		$password = strlen($account->password) > Account::MAX_PASSWORD_LENGTH
			? call_user_func(MCRYPT_DECODE, $account->password)
			: $account->password;

		$data = [
			'ip'                      => $account->ip,
			'mail'                    => $account->user->mail,
			'proxy'                   => $proxy,
			'login'                   => $account->login,
			'user_id'                 => $user_id,
			'table_id'                => $account->id,
			'password'                => $password,
			'account_id'              => $account->instagram_id,
			'access_token'            => $account->apiKey,
//			'experimental'            => $account->options->experimental,
			'antispam'                => $account->user->antispam,
			'unModeMulti'             => $account->user->unfollow_mode_multiactions,
			'exactGeo'                => $account->user->exactGeo,
			'optionSkipPopularUpdate' => $account->options->skip_popular_update,
			'followForPrivate'      => $account->user->follow_for_private
		];

		return $data;
	}

	/**
	 * @param Account $account
	 * @param string  $initiator
	 * @return boolean
	 */
	public static function startAccount($account, $initiator)
	{
		// выдаем ошибку проверки и одновременно запускаем
		$result_error = null;

		if (!$initiator !== 'admin' && !$account->user->is_payed) {
			$txn = Yii::$app->db->beginTransaction();
			/** @var Demo $demo */
			$demo = Demo::findOne(['login' => strval($account->instagram_id)]);

			if ($demo === null) {
				$demo = new Demo(['user_id' => $account->user->id, 'login' => $account->instagram_id]);
				$demo->save();
			} else {
				if ($demo->user_id !== $account->user_id) {
					$demo->tryed = new Expression('CURRENT_TIMESTAMP');
					$demo->save();
					$txn->commit();
					return \Yii::t('app', 'Free works for account {login} already done', ['login' => $account->login]);
				}
				/** @var UsersInfo $modelInfo */
				$modelInfo = UsersInfo::findOne(['user_id' => $account->user->id]);
				if ($modelInfo === null){
					$modelInfo = new UsersInfo(['user_id' => $account->user->id]);
				}
				$modelInfo->cheater = 1;
				$modelInfo->save();
			}
			$txn->commit();
		}

		$txn = Yii::$app->db->beginTransaction();
		$account->server = self::getIdleServer();
		$account->save();

		$result = self::runAccountTask($account, 'startWorker', [
			'initiator' => $initiator,
			'sameMade'  => $account->user->sameMade,
			'sameBlack' => $account->user->sameBlack,
		], $account->server);

		if ($result['status'] === 'ok') {
			//Ставим отметку о запуске проекта
			$account->monitoring_status = Account::STATUS_WORK;
			$account->save();
			$txn->commit();
			return $result_error ? $result_error : true;
		}

		$txn->rollBack();
		return $result_error ? $result_error : false;
	}

	/**
	 * @param Account $account
	 * @param string  $initiator
	 * @return boolean
	 */
	public static function stopAccount($account, $initiator)
	{
		foreach (Yii::$app->params['workers'] as $worker) {
			self::runAccountTask($account, 'stopWorker', [
				'insta_id'  => $account->instagram_id,
				'initiator' => $initiator,
			], $worker);
		}

		$txn = Yii::$app->db->beginTransaction();
		$account->monitoring_status = Account::STATUS_IDLE;
		$account->save();
		$txn->commit();

		return true;
	}

	/**
	 * @param Account $account
	 */
	public static function countComments($account)
	{
		self::runAccountTask($account, 'countComments');
	}

	/**
	 * @param Account $account
	 * @param integer|null $max_id
	 * @return mixed
	 */
	public static function getMedia($account, $max_id = null)
	{
		$result = self::runAccountTask(
			$account,
			'getMedia',
			['max_id' => $max_id],
			null,
			60
		);

		return $result;
	}

	/**
	 * @param Account $account
	 * @param integer $id
	 * @param integer|null $max_id
	 * @return mixed
	 */
	public static function getMediaComments($account, $id, $max_id = null)
	{
		return self::runAccountTask($account, 'getMediaComments', [
			'id'     => $id,
			'max_id' => $max_id,
		]);
	}

	/**
	 * @param Account $account
	 * @param integer $media_id
	 * @param string $comment_text
	 * @return mixed
	 */
	public static function sendComment($account, $media_id, $comment_text)
	{
		return self::runAccountTask($account, 'sendComment', [
			'media_id'     => $media_id,
			'comment_text' => $comment_text,
		]);
	}

	/**
	 * @param Account $account
	 * @param string  $image
	 * @param string  $caption
	 * @param array   $media_info
	 * @return mixed
	 */
	public static function sendImage($account, $image, $caption, $media_info = [])
	{
		set_time_limit(150);
		return self::runAccountTask($account, 'sendImage', [
			'image'     => $image,
			'caption'   => $caption,
			'media_info' => $media_info
		], null, 120);
	}
	
	/**
	 * @param Account $account
	 * @param $user_id
	 * @return mixed
	 */
	public static function addQueue($account, $user_id)
	{
		return self::runAccountTask($account, 'addQueue', [
			'user_id'     => $user_id
		]);
	}

	/**
	 * @param Account $account
	 * @param integer $media_id
	 * @param integer $comment_id
	 * @return mixed
	 */
	public static function removeComment($account, $media_id, $comment_id)
	{
		return self::runAccountTask($account, 'removeComment', [
			'media_id'   => $media_id,
			'comment_id' => $comment_id,
		]);
	}

	/**
	 * @param Account $account
	 * @param string  $login
	 * @return boolean
	 */
	public static function findUser($account, $login)
	{
		return self::runAccountTask($account, 'findUser', [
			'user_login' => $login
		]);
	}

	public static function sendSecurityCode($account, $code)
	{
		return self::runAccountTask($account, 'enterSecurityCode', [
			'code' => $code
		]);
	}

	/**
	 * @param $server
	 * @return array
	 */
	public static function getServerLoad($server)
	{
		return self::runServerTask($server, 'getServerLoad', [], 3);
	}
	
	public static function getIdleServer()
	{
		$max_val = 0;
		$idle_server = '';
		foreach (Yii::$app->params['workers'] as $server) {
			$result = self::getServerLoad($server);
			if ($result['status'] !== 'ok') {
				continue;
			}
			$max_val = max($max_val, $result['data']['mem']);
			if ($max_val === $result['data']['mem']) {
				$idle_server = $server;
			}
		}

		return $idle_server;
	}
}