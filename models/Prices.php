<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "prices".
 *
 * @property integer $id
 * @property integer $value
 * @property float $price
 * @property integer $bonus
 */
class Prices extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'prices';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['value', 'price', 'bonus'], 'required'],
			[['value', 'bonus'], 'integer'],
			['price', 'number'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'    => Yii::t('app', 'Summ ID'),
			'value' => Yii::t('app', 'Quantity of days'),
			'price' => Yii::t('app', 'Price'),
			'bonus' => Yii::t('app', 'Your bonus'),
		];
	}

	public static function formatPrice($price){
		return number_format($price, Yii::$app->language == 'ru' ? 0 : 2, '.', ' ');
	}
}
