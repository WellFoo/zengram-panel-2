<?php

require_once __DIR__ . DIRECTORY_SEPARATOR . 'Sniffer.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'DB.php';

if (empty($_POST['action'])) {
	echo json_encode([
		'status' => 'error',
		'data' => 'Действие не задано'
	]);
}

if (empty($_POST['key']) || $_POST['key'] !== Sniffer::KEY) {
	echo json_encode([
		'status' => 'error',
		'data' => 'Некорректный ключ'
	]);
}

$sniffer = new Sniffer($_POST['action'], $_POST);
$sniffer->start();